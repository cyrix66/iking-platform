<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://platform.ikingtech.com">
    <img src="https://platformdoc.ikingtech.com/logo.svg" alt="Logo" width="120" height="120">
  </a>

  <h3>金合技术中台</h3>
  <strong>于晏啊，你还是改不了你随手star的毛病</strong><br />
  <strong>现代化的下一代企业级技术中台，简洁、高效、稳定、开放</strong>
  
  <a href="http://www.ikingtech.com/">官网</a>
  |
  <a href="https://platformdoc.ikingtech.com">文档</a>
  |
  <a href="https://platform.ikingtech.com">在线演示</a>
  |
  <a href="https://gitee.com/ikingtech/iking-platform/issues">提交Bug</a>
  |
  <a href="https://gitee.com/ikingtech/iking-platform#%E6%8A%80%E6%9C%AF%E4%BA%A4%E6%B5%81%E7%BE%A4">联系交流</a>
</div>

### 演示环境账号：
* 平台管理员：admin Security123$%^<br />
* 默认租户管理员：sys_admin Security123$%^<br />
<br />

[![Stargazers][stars-shield]][stars-url]
[![Forks][forks-shield]][forks-url]

[![Vuejs][Vuejs]][Vuejs-url]
[![Vite][Vite]][Vite-url]
[![TypeScript][TypeScript]][TypeScript-url]
[![VSCODE][VSCODE]][VSCODE-url]

[![OpenJDK][OpenJDK]][OpenJDK-url]
[![SpringBoot][SpringBoot]][SpringBoot-url]
[![Maven][Maven]][Maven-url]
[![IDEA][IDEA]][IDEA-url]

<div align="center">
  <a href="https://platform.ikingtech.com">
    <img src="https://cdn.o0o0oo.com/ikingtech/platform/docPic/%E5%8F%AF%E8%A7%86%E5%8C%96.png" alt="Logo" width="600" height="100">
  </a>
<a href="https://www.ikingtech.com/solution">
    <img src="https://cdn.o0o0oo.com/ikingtech/platform/docPic/%E6%A1%A3%E6%A1%88.png" alt="Logo" width="600" height="100">
  </a>
</div>




<!-- TABLE OF CONTENTS -->
<details>
  <summary>目录</summary>
  <ol>
    <li>
      <a href="#平台概述">平台概述</a>
      <ul>
        <li><a href="#界面预览">界面预览</a></li>
        <li><a href="#特性">特性</a></li>
        <li><a href="#技术架构">技术架构</a></li>
        <li><a href="#系统架构">系统架构</a></li>
      </ul>
    </li>
    <li>
      <a href="#快速开始">快速开始</a>
      <ul>
        <li><a href="#准备工作">准备工作</a></li>
        <li><a href="#安装">安装</a></li>
      </ul>
    </li>
    <li><a href="#使用说明">使用说明</a></li>
    <li><a href="#开发">开发</a></li>
      <ul>
        <li><a href="#准备工作">准备工作</a></li>
          <ul>
            <li><a href="#前端">前端</a></li>
            <li><a href="#后端">后端</a></li>
          </ul>
        <li><a href="#创建项目">创建项目</a></li>
          <ul>
            <li><a href="#前端">前端</a></li>
            <li><a href="#后端">后端</a></li>
          </ul>
        <li><a href="#环境配置">环境配置</a></li>
          <ul>
            <li><a href="#前端">前端</a></li>
            <li><a href="#后端">后端</a></li>
          </ul>
        <li><a href="#开始开发">开始开发</a></li>
          <ul>
            <li><a href="#前端">前端</a></li>
            <li><a href="#后端">后端</a></li>
          </ul>
      </ul>
    <li><a href="#技术路线">技术路线</a></li>
    <li><a href="#关于我们">关于我们</a></li>
    <li><a href="#贡献">贡献</a></li>
    <li><a href="#License">License</a></li>
    <li><a href="#联系我们">联系我们</a></li>
  </ol>
</details>

## 平台概述
### 界面预览
[![ASSETS-LOGIN][ASSETS-LOGIN]][ASSETS-LOGIN-url]
[![ASSETS-WORKBENCH][ASSETS-WORKBENCH]][ASSETS-WORKBENCH-url]
[![ASSETS-APPROVE-SUBMIT][ASSETS-APPROVE-SUBMIT]][ASSETS-APPROVE-SUBMIT-url]
[![ASSETS-APPROVE-LIST][ASSETS-APPROVE-LIST]][ASSETS-APPROVE-LIST-url]
[![ASSETS-APPROVE-EXECUTE][ASSETS-APPROVE-EXECUTE]][ASSETS-APPROVE-EXECUTE-url]
[![ASSETS-APPLICATION][ASSETS-APPLICATION]][ASSETS-APPLICATION-url]
[![ASSETS-APPLICATION-PAGE][ASSETS-APPLICATION-PAGE]][ASSETS-APPLICATION-PAGE-url]
### 特性
* **企业级产品/UI设计，满足各类业务场景需求。**
* **经历我司内部项目生产环境的反复锤炼，确保高效稳定运行。**
* **双技术架构，单体/微服务无缝切换。**
* **双业务架构，小型管理系统可使用后台管理模式，大型业务系统可使用Saas平台模式。**
* **全面的通用能力支撑，系统管理、文件服务、日志审计、任务调度等应有尽有。**
* **完整实现“中国式审批”的各类需求，开箱即用。**
* **高灵活度的消息中心，随心配置模板、多渠道（站内/微信/短信/钉钉）触达。**
* **深度集成第三方组件/平台，包括积木报表、OnlyOffice、KKFile。**
* **支持多种登录方式，包括短信验证码登录、微信扫码登录、微信小程序登录等。**
* **支持多种数据库，包括MySQL、Postgresql、Oracle。**
* **支持多种对象存储服务，包括Minio、腾讯COS、阿里OSS等。**
* ...
### 技术架构
* 单体/微服务无缝切换
[![TechArch][ASSETS-TECH-ARCH]][ASSETS-TECH-ARCH-url]
* 全模块化设计
[![ModuleArch][ASSETS-MODULE-ARCH]][ASSETS-MODULE-ARCH-url]
### 系统架构
* 功能全景
[![SystemArch][ASSETS-SYSTEM-ARCH]][ASSETS-SYSTEM-ARCH-url]
* 多租户使用流程
[![TenantFlow][ASSETS-TENANT-FLOW]][ASSETS-TENANT-FLOW-url]
## 快速开始
### 准备工作
1. 确保网络通畅，能够正常访问互联网。条件允许时可开启VPN，加快镜像下载速度。
2. 安装Docker服务。
   * 设置Yum源。
     ```shell
     yum install -y yum-utils
     yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
     ```
   * 安装Docker Engine，containerd，以及Docker Compose。<br />
     ```shell
     yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
     ```
   * 启动Docker服务并设置为开机自启。<br />
     ```shell
     systemctl start docker
     systemctl enable docker
     ```
3. 创建映射目录。
    ```shell
    mkdir -p /opt/lite/mysql/conf /opt/lite/mysql/data /opt/lite/redis/data /opt/lite/redis/config /opt/lite/minio/data
    ```
4. 任意目录创建docker-compose.yml文件，并写入以下内容。
    ```yaml
    version: '3.8'
    services:
      mysql:
        image: mysql:8.3.0
        ports:
          - 3306:3306
        volumes:
          - /opt/lite/mysql/conf/mysql.cnf:/etc/mysql/conf.d/mysql.cnf
          - /opt/lite/mysql/data/:/var/lib/mysql/
        privileged: true
        environment:
          - MYSQL_ROOT_PASSWORD=Abc123++
          - MYSQL_DATABASE=iking_framework
          - TZ=Asia/Shanghai
        restart: always
        container_name: mysql
        command: mysqld --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
        healthcheck:
          test: ["CMD", "mysqladmin", "ping", "-h", "localhost"]
          interval: 10s
          timeout: 5s
          start_period: 60s
    
      redis:
        image: redis:7.2.4
        ports:
          - 6379:6379
        volumes:
            - /opt/lite/redis/data:/data
            - /opt/lite/redis/config/redis.conf:/usr/local/etc/redis/redis.conf
        privileged: true
        restart: always
        container_name: redis
        command: redis-server --requirepass Abc123++ --appendonly yes --notify-keyspace-events Ex
        healthcheck:
          test: ["CMD", "redis-cli", "ping"]
          interval: 30s
          timeout: 20s
          start_period: 60s
    
      minio:
        image: minio/minio:RELEASE.2024-01-31T20-20-33Z
        ports:
          - 9000:9000
          - 9090:9090
        volumes:
          - /opt/lite/minio/data:/data
        privileged: true
        environment:
          - MINIO_ROOT_USER=minio
          - MINIO_ROOT_PASSWORD=Abc123++
        restart: always
        container_name: minio
        command: server /data --console-address ":9090" -address ":9000"
        healthcheck:
          test: ["CMD", "curl", "-f", "http://minio:9000/minio/health/live"]
          interval: 30s
          timeout: 20s
          start_period: 60s
    
      lite:
        image: ikingtech/lite:1.1.8
        ports:
          - 8000:8000
        privileged: true
        environment:
            - SRV_PORT=8000
            - MYSQL_HOST=mysql
            - MYSQL_PORT=3306
            - MYSQL_SCHEMA_NAME=iking_framework
            - MYSQL_USERNAME=root
            - MYSQL_PASSWORD=Abc123++
            - REDIS_HOST=redis
            - REDIS_PORT=6379
            - REDIS_DB=1
            - REDIS_PASSWORD=Abc123++
            - OSS_ENABLED=true
            - OSS_STORAGE_TYPE=AWS_S3
            - OSS_HOST=http://172.17.0.1:9000
            - OSS_ACCESS_KEY=minio
            - OSS_SECRET_KEY=Abc123++
            - OSS_DEFAULT_BUCKET=iking
            - OSS_REGION=us-east-1
            - OSS_AUTO_CREATE_BUCKET=true
            - FLYWAY_ENABLE=true
        restart: always
        container_name: lite
    
      ui:
        image: ikingtech/lite-ui:1.1.8
        ports:
          - 80:80
        privileged: true
        restart: always
        container_name: lite-ui
    ```
### 安装
1. 进入docker-compose.yml文件所在目录，执行启动命令，并等待完成。
    ```shell
    docker compose up -d
    ```
2. 启动完成后，执行以下命令确认所有容器启动成功。
    ```shell
    docker ps -a
    ```
3. 浏览器中输入您的服务器IP地址即可访问平台。

💡 特别说明：
1. 如果您的运行环境中已安装了数据库/缓存/对象存储服务等组件，您需要从docker-compose.yml文件中删除相应部分。例如，您的环境中已安装了MySQL数据库，那么从docker-compose.yml中移除以下部分的内容：
    ```yaml
      mysql:
        image: mysql:8.3.0
        ports:
          - 3306:3306
        volumes:
          - /opt/lite/mysql/conf/mysql.cnf:/etc/mysql/conf.d/mysql.cnf 
          - /opt/lite/mysql/data/:/var/lib/mysql/
        privileged: true
        environment:
          - MYSQL_ROOT_PASSWORD=Abc123++
          - MYSQL_DATABASE=iking_framework
          - TZ=Asia/Shanghai
        restart: always
        container_name: mysql
        command: mysqld --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
        healthcheck:
          test: ["CMD", "mysqladmin", "ping", "-h", "localhost"]
          interval: 10s
          timeout: 5s
          start_period: 60s
    ```
    相应的，您还需要将服务启动时的环境变量进行修改。仍以MySQL为例，您需要将环境变量`MYSQL_HOST`、`MYSQL_PORT`、`MYSQL_SCHEMA_NAME`、`MYSQL_USERNAME`、`MYSQL_PASSWORD`修改为您的数据库的信息。<br />
    所有环境变量的说明请参考以下列表：

   | 变量                 | 说明                         |
   |--------------------|----------------------------|
   | SRV_PORT           | 服务运行端口                     |
   | MYSQL_HOST         | 数据库服务地址                    |
   | MYSQL_PORT         | 数据库服务端口                    |
   | MYSQL_SCHEMA_NAME  | 数据库名，默认iking_framework     |
   | MYSQL_USERNAME     | 数据库服务用户名                   |
   | MYSQL_PASSWORD     | 数据库服务密码                    |
   | REDIS_HOST         | Redis服务地址                  |
   | REDIS_PORT         | Redis服务端口                  |
   | REDIS_DB           | Redis DB号                  |
   | REDIS_PASSWORD     | Redis密码                    |
   | OSS_ENABLED        | 是否启用文件服务                   |
   | OSS_STORAGE_TYPE   | 对象存储服务类型，目前仅支持AWS_S3类型     |
   | OSS_HOST           | 对象存储服务地址，格式为http://ip:port |
   | OSS_ACCESS_KEY     | 对象存储服务AccessKey            |
   | OSS_SECRET_KEY     | 对象存储服务SecretKey            |
   | OSS_DEFAULT_BUCKET | 默认桶名称                      |
   | OSS_REGION         | 桶所在区域标识                    |
2. 服务默认使用`iking_framework`作为数据库名称，请确保您的数据库实例中没有同名的数据库。如果您希望自定义数据库名，请修改docker-compose.yml文件中的相应配置。
3. 如果您仅需要启动平台服务，无需额外安装数据库/缓存/对象存储服务，则执行以下命令即可。
    * 前端
      ```shell
      docker run -d -p 80:80 --restart=always --name=lite-ui --privileged=true ikingtech/lite-ui:1.1.6
      ```
    * 后端
      ```shell
      docker run -d --name lite -p 8000:8000\
       -e SRV_PORT=8000\
       -e MYSQL_HOST=${数据库服务地址}\
       -e MYSQL_PORT=${数据库服务端口}\
       -e MYSQL_SCHEMA_NAME=${数据库名称}\
       -e MYSQL_USERNAME=${数据库服务用户名}\
       -e MYSQL_PASSWORD=${数据库服务密码}\
       -e REDIS_HOST=${Redis服务地址}\
       -e REDIS_PORT=${Redis服务端口}\
       -e REDIS_DB=${Redis DB号}\
       -e REDIS_PASSWORD=${Redis服务密码}\
       -e OSS_ENABLED=true\
       -e OSS_STORAGE_TYPE=AWS_S3\
       -e OSS_HOST=${对象存储服务地址，格式为http(s)://ip:port}\
       -e OSS_ACCESS_KEY=${对象存储服务AccessKey}\
       -e OSS_SECRET_KEY=${对象存储服务SecretKey}\
       -e OSS_DEFAULT_BUCKET=${默认桶名称}\
       -e OSS_REGION=${桶所在区域标识}\
       -e FLYWAY_ENABLE=true
       --restart=always\
       ikingtech/lite:1.1.6
      ```
## 开发
以下说明默认在Windows环境开发。
### 准备工作
#### 前端
#### 后端
* 安装Jdk
  1. 访问以下地址，选择`17.0.2 (build 17.0.2+8)`中的`Windows`版本进行下载，大小178M。
      ```text
      https://jdk.java.net/archive/
      ```
      ![Jdk17下载示意](https://cdn.o0o0oo.com/ikingtech/platform/docPic/Jdk17下载示意.png)
  2. 下载完成后在任意目录解压zip包，解压后的目录应为`${解压目录}\jdk-17.0.8`。
* 安装Maven
  1. 访问以下地址，选择`apache-maven-3.9.6-bin.zip`进行下载，大小9M。
      ```text
      https://maven.apache.org/download.cgi
      ```
     ![Maven下载示意](https://cdn.o0o0oo.com/ikingtech/platform/docPic/Maven下载示意.png)
  2. 下载完成后在任意目录解压zip包，解压后的目录应为`${解压目录}\apache-maven-3.9.6`。
### 项目配置
#### 前端
#### 后端
  1. 下载代码。
      ```shell
      git clone https://gitee.com/ikingtech/iking-platform.git
      ```
  2. 使用IDE打开下载后的项目，配置Jdk。
      ![IDEA-JDK配置示意](https://cdn.o0o0oo.com/ikingtech/platform/docPic/IDEA-JDK配置示意.png)
  3. 配置Maven。
      ![IDEA-Maven配置示意](https://cdn.o0o0oo.com/ikingtech/platform/docPic/IDEA-Maven配置示意.png)
### 运行环境配置
#### 前端
#### 后端
1. 默认`application.yml`文件包含以下内容，一般不需要改动。
    ```yaml
    spring:
      profiles:
        active: lite, oss, redis, magic-api, ${ENV:dev}, ${DATA_BASE:mysql}
      application:
        name: lite
    iking:
      framework:
        web:
          version: @project.version@
          arch: single
    ```
2. 修改`application-dev.yml`的内容，配置开发环境。配置示例如下：
    ```yaml
    # 服务端口
    SRV_PORT: 8000
    
    # 数据库服务地址
    MYSQL_HOST: x.x.x.x
    # 数据库服务端口
    MYSQL_PORT: 3306
    # 数据库名称
    MYSQL_SCHEMA_NAME: iking_framework
    # 数据库服务用户名
    MYSQL_USERNAME: root
    # 数据库服务密码
    MYSQL_PASSWORD: Abc123++
    
    # 是否开启Flyway，设置为true时则在服务启动时自动建表，设置为false则不会自动建表
    FLYWAY_ENABLE: true
    # 建表SQL脚本的位置
    FLYWAY_SCRIPT_LOCATIONS: classpath:framework/db/migration/mysql/
    
    # Redis服务地址
    REDIS_HOST: x.x.x.x
    # Redis服务端口
    REDIS_PORT: 6379
    # Redis DB号
    REDIS_DB: 6
    # Redis服务密码
    REDIS_PASSWORD: Abc123++
    
    # 是否开启文件服务
    OSS_ENABLED: true
    # 对象存储协议类型，目前仅支持AWS_S3
    OSS_STORAGE_TYPE: AWS_S3
    # 对象存储服务地址
    OSS_HOST: http://x.x.x.x:9000
    # 对象存储服务AccessKey
    OSS_ACCESS_KEY: minio
    # 对象存储服务SecretKey
    OSS_SECRET_KEY: Abc123++
    # 默认桶名称
    OSS_DEFAULT_BUCKET: iking
    # 桶所在区域标识
    OSS_REGION: us-east-1
    ```
### 开始开发
#### 前端
#### 后端
1. 创建业务功能模块。
    ![创建业务模块示意](https://cdn.o0o0oo.com/ikingtech/platform/docPic/创建业务模块示意.png)
2. 进行业务功能开发。
3. 在`lite`模块中添加业务模块的依赖后，启动项目即可。
## 使用说明
1. 使用平台管理员账户（默认`admin/Security123$%^`）登录系统。
2. 进入租户管理，创建租户。
3. 使用租户管理员账户（默认`${租户标识}_admin/Security123$%^`）登录系统，开始使用平台各项功能。
4. 如果您不想单独创建租户，可使用平台默认租户（租户标识`sys`）。
5. 平台其他各项功能请参考[平台说明文档](https://platformdoc.ikingtech.com)。
## 技术路线
### 前端
- [x] vuejs 3.4.3
- [x] vite 4.5.0
- [x] element plus 2.4.4
- [x] pinia 2.1.7
- [x] vue-router 4.2.5
### 后端
- [x] JDK 17
- [x] Maven 4.0.0
- [x] Spring Boot 3.1.8
- [x] Spring Cloud 2022.0.5
- [x] Spring Cloud Alibaba 2022.0.0.0
- [x] Mybatis-Plus 3.5.5
- [x] Redis 7.2
- [x] MySQL 8.0+/Postgresql 16+/Oracle 11g+12c
- [x] Minio/Tencent COS/Ali OSS
- [ ] RabbitMQ/RocketMQ
- [ ] Prometheus+Grafana
## 关于我们
金合技术开发平台是陕西金合信息科技股份有限公司在内部孵化的技术开发平台，致力于减少项目交付在基础环境上消耗的工时，换而言之金合的技术开发平台是为项目交付而生，让项目不需要在底层的功能或环境上再消耗时间。<br />
目前金合技术中台已经支持交付了内外部40+项目，当前版本为首个开源版本，我们迫切的想听到您的改进意见和功能建议，希望更多的开发者可以参与改进平台，助力更多开发者和企业。
## 贡献
社区力量是开源项目持续发展的唯一动力来源，我们真诚的希望与您一起共建这片学习、交流、成长并且获益的“小宇宙”，您的每一个建议、每一次PR我们都**倍感珍惜**。
如果您对本项目有任何的想法，请您务必留下您的宝贵意见和建议，当然 `Talk is cheap, show me the code`，我们更欢迎您按照如下方式扩展本项目的功能！
1. Fork本仓库。
2. 创建您的功能分支 (`git checkout -b feature/awsome-feature`)。
3. 提交您的修改并推送到分支上 (`git commit -m 'Add some Awsome Feature'` && `git push origin feature/awsome-feature`)。
4. 提交PR。
## License
本项目基于 Apache License Version 2.0 开源协议，请遵守以下规范。
* 不得将本项目应用于危害国家安全、荣誉和利益的行为，不能以任何形式用于非法行为。
* 使用源代码时请保留源文件中的版权声明和作者信息。
* 任何基于本项目二次开发而产生的一切法律纠纷和责任，均与作者无关。
更多信息请参考项目根目录下的`LICENSE`文件。
## 联系我们
<h3>技术交流群</h3>
<img src="https://platformdoc.ikingtech.com/image/doc/lll-qiyeweixin.jpg" width="200">
<br />
<br />
帖严 - tieybrain@gmail.com / ty19880929@hotmail.com

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[forks-shield]: https://gitee.com/ikingtech/iking-platform/badge/fork.svg?theme=dark
[forks-url]: https://gitee.com/ikingtech/iking-platform/members
[stars-shield]: https://gitee.com/ikingtech/iking-platform/badge/star.svg?theme=dark
[stars-url]: https://gitee.com/ikingtech/iking-platform/stargazers
[SpringBoot]: https://img.shields.io/badge/Spring_Boot-6DB33F?style=for-the-badge&logo=spring-boot&logoColor=white
[SpringBoot-url]: https://spring.io/projects/spring-boot
[Maven]: https://img.shields.io/badge/apache_maven-C71A36?style=for-the-badge&logo=apachemaven&logoColor=white
[Maven-url]: https://spring.io/projects/spring-boot
[Vuejs]: https://img.shields.io/badge/Vue%20js-35495E?style=for-the-badge&logo=vuedotjs&logoColor=4FC08D
[Vuejs-url]: https://cn.vuejs.org
[Vite]: https://img.shields.io/badge/Vite-B73BFE?style=for-the-badge&logo=vite&logoColor=FFD62E
[Vite-url]: https://vitejs.dev/
[TypeScript]: https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white
[TypeScript-url]: https://www.typescriptlang.org/
[OpenJDK]: https://img.shields.io/badge/OpenJDK-ED8B00?style=for-the-badge&logo=openjdk&logoColor=white
[OpenJDK-url]: https://openjdk.org/
[IDEA]: https://img.shields.io/badge/IntelliJ_IDEA-000000.svg?style=for-the-badge&logo=intellij-idea&logoColor=white
[IDEA-url]: https://www.jetbrains.com/zh-cn/idea/
[VSCODE]: https://img.shields.io/badge/VSCode-0078D4?style=for-the-badge&logo=visual%20studio%20code&logoColor=white
[VSCODE-url]: https://code.visualstudio.com/
[ASSETS-LOGIN]: https://cdn.o0o0oo.com/ikingtech/platform/docPic/技术中台-登录.png
[ASSETS-LOGIN-url]: https://platform.ikingtech.com
[ASSETS-WORKBENCH]: https://cdn.o0o0oo.com/ikingtech/platform/docPic/技术中台-工作台.png
[ASSETS-WORKBENCH-url]: https://platform.ikingtech.com
[ASSETS-APPROVE-SUBMIT]: https://cdn.o0o0oo.com/ikingtech/platform/docPic/技术中台-审批中心-发起审批.png
[ASSETS-APPROVE-SUBMIT-url]: https://platform.ikingtech.com
[ASSETS-APPROVE-LIST]: https://cdn.o0o0oo.com/ikingtech/platform/docPic/技术中台-审批中心-审批表单列表.png
[ASSETS-APPROVE-LIST-url]: https://platform.ikingtech.com
[ASSETS-APPROVE-EXECUTE]: https://cdn.o0o0oo.com/ikingtech/platform/docPic/技术中台-审批中心-我审批的.png
[ASSETS-APPROVE-EXECUTE-url]: https://platform.ikingtech.com
[ASSETS-APPLICATION]: https://cdn.o0o0oo.com/ikingtech/platform/docPic/技术中台-应用开发.png
[ASSETS-APPLICATION-url]: https://platform.ikingtech.com
[ASSETS-APPLICATION-PAGE]: https://cdn.o0o0oo.com/ikingtech/platform/docPic/技术中台-应用开发-页面设计.png
[ASSETS-APPLICATION-PAGE-url]: https://platform.ikingtech.com
[ASSETS-MODULE-ARCH]: https://cdn.o0o0oo.com/ikingtech/platform/docPic/技术中台-功能模块架构.png
[ASSETS-MODULE-ARCH-url]: https://platform.ikingtech.com
[ASSETS-TECH-ARCH]: https://cdn.o0o0oo.com/ikingtech/platform/docPic/技术中台-技术架构.png
[ASSETS-TECH-ARCH-url]: https://platform.ikingtech.com
[ASSETS-SYSTEM-ARCH]: https://cdn.o0o0oo.com/ikingtech/platform/docPic/技术中台-业务架构.png
[ASSETS-SYSTEM-ARCH-url]: https://platform.ikingtech.com
[ASSETS-TENANT-FLOW]: https://cdn.o0o0oo.com/ikingtech/platform/docPic/技术中台-业务架构-多租户.png
[ASSETS-TENANT-FLOW-url]: https://platform.ikingtech.com
