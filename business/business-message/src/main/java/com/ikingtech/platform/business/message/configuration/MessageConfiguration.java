package com.ikingtech.platform.business.message.configuration;

import com.ikingtech.platform.business.message.service.repository.*;
import com.ikingtech.platform.business.message.service.router.*;
import com.ikingtech.platform.business.message.service.webhook.WebhookDeliver;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;

import java.util.List;

/**
 * @author tie yan
 */
public class MessageConfiguration {

    @Bean
    public MessageTemplateRepository messageTemplateRepository() {
        return new MessageTemplateRepository();
    }

    @Bean
    public MessageChannelDefinitionRepository messageChannelDefinitionRepository() {
        return new MessageChannelDefinitionRepository();
    }

    @Bean
    public MessageParamDefinitionRepository messageParamDefinitionRepository() {
        return new MessageParamDefinitionRepository();
    }

    @Bean
    public MessageReceiverDefinitionRepository messageReceiverDefinitionRepository() {
        return new MessageReceiverDefinitionRepository();
    }

    @Bean
    public MessageRedirectDefinitionRepository messageRedirectDefinitionRepository() {
        return new MessageRedirectDefinitionRepository();
    }

    @Bean
    public MessageSubscriberRepository messageSubscriberRepository() {
        return new MessageSubscriberRepository();
    }

    @Bean
    public MessageSubscriberSendUrlRepository messageSubscriberSendUrlRepository() {
        return new MessageSubscriberSendUrlRepository();
    }

    @Bean
    public MessageSubscriberDataDefinitionRepository messageSubscriberDataDefinitionRepository() {
        return new MessageSubscriberDataDefinitionRepository();
    }

    @Bean
    public MessageRepository messageRepository() {
        return new MessageRepository();
    }

    @Bean
    public MessageRouteProxy messageRouteProxy(List<MessageRouter> routers) {
        return new MessageRouteProxy(routers);
    }

    @Bean
    public WebhookDeliver webhookDeliver(MessageSubscriberRepository subscriberRepo, MessageSubscriberSendUrlRepository sendUrlRepo) {
        return new WebhookDeliver(subscriberRepo, sendUrlRepo);
    }

    @Bean
    public WebhookMessageRouter webhookMessageRouter(MessageChannelDefinitionRepository channelDefinitionRepo) {
        return new WebhookMessageRouter(channelDefinitionRepo);
    }

    @Bean
    @ConditionalOnClass(name = "com.ikingtech.platform.service.push.websocket.WebsocketSingletonDeliver")
    public SystemMessageRouter systemMessageRouter(MessageChannelDefinitionRepository channelDefinitionRepo) {
        return new SystemMessageRouter(channelDefinitionRepo);
    }

    @Bean
    @ConditionalOnClass(name = "com.ikingtech.platform.service.push.sms.SmsDeliver")
    public SmsMessageRouter smsMessageRouter(MessageChannelDefinitionRepository channelDefinitionRepo) {
        return new SmsMessageRouter(channelDefinitionRepo);
    }

    @Bean
    @ConditionalOnClass(name = "com.ikingtech.platform.service.push.wechat.WechatMiniDeliver")
    public WechatMiniMessageRouter wechatMiniMessageRouter(MessageChannelDefinitionRepository channelDefinitionRepo) {
        return new WechatMiniMessageRouter(channelDefinitionRepo);
    }
}
