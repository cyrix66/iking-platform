package com.ikingtech.platform.business.message.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.SortEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("message_channel_definition")
public class MessageChannelDefinitionDO extends SortEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 5261147321487821733L;

    @TableField("tenant_code")
    private String tenantCode;

    @TableField("template_id")
    private String templateId;

    @TableField("business_key")
    private String businessKey;

    @TableField("business_name")
    private String businessName;

    @TableField("message_title")
    private String messageTitle;

    @TableField("channel")
    private String channel;

    @TableField("channel_description")
    private String channelDescription;

    @TableField("channel_id")
    private String channelId;

    @TableField("channel_template_id")
    private String channelTemplateId;

    @TableField("content")
    private String content;

    @TableField("customize_content")
    private String customizeContent;

    @TableField("content_type")
    private String contentType;

    @TableField("voice")
    private Boolean voice;

    @TableField("redirect_layout")
    private String redirectLayout;

    @TableField("status")
    private String status;

    @TableField("support_template")
    private Boolean supportTemplate;

    @TableField("show_notification")
    private Boolean showNotification;
}
