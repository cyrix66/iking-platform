package com.ikingtech.platform.business.message.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.SortEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 消息重定向定义
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("message_redirect_definition")
public class MessageRedirectDefinitionDO extends SortEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -3046999165979130253L;

    @TableField("tenant_code")
    private String tenantCode;

    @TableField("template_id")
    private String templateId;

    @TableField("channel_definition_id")
    private String channelDefinitionId;

    @TableField("redirect_code")
    private String redirectCode;

    @TableField("redirect_to")
    private String redirectTo;

    @TableField("redirect_name")
    private String redirectName;
}
