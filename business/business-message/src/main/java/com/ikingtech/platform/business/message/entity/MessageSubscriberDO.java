package com.ikingtech.platform.business.message.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.TenantEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("message_subscriber")
public class MessageSubscriberDO extends TenantEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 6107636889619290532L;

    @TableField("name")
    private String name;
}
