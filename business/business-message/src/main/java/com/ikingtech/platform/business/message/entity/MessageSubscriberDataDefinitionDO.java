package com.ikingtech.platform.business.message.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.TenantEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("message_subscriber_data_definition")
public class MessageSubscriberDataDefinitionDO extends TenantEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -1210081174634668670L;

    @TableField("subscriber_id")
    private String subscriberId;

    @TableField("send_url_id")
    private String sendUrlId;

    @TableField("field_name")
    private String fieldName;
}
