package com.ikingtech.platform.business.message.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.SortEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("message_template")
public class MessageTemplateDO extends SortEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 8119608278231146235L;

    @TableField("tenant_code")
    private String tenantCode;

    @TableField("business_name")
    private String businessName;

    @TableField("business_key")
    private String businessKey;

    @TableField("message_template_key")
    private String messageTemplateKey;

    @TableField("message_template_title")
    private String messageTemplateTitle;

    @TableField("configured")
    private Boolean configured;

    @TableField("configurable")
    private Boolean configurable;

    @TableLogic
    @TableField("del_flag")
    private Boolean delFlag;
}
