package com.ikingtech.platform.business.message.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum MessageExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 指定消息模板不存在
     */
    MESSAGE_TEMPLATE_NOT_FOUND("指定消息模板不存在"),

    /**
     * 指定消息模板不存在
     */
    MESSAGE_CHANNEL_DEFINITION_NOT_FOUND("messageChannelDefinitionNotFound"),

    /**
     * 已存在相同标识的消息模板
     */
    DUPLICATE_MESSAGE_KEY("已存在相同标识的消息模板"),

    /**
     * 已存在相同标题的消息模板
     */
    DUPLICATE_MESSAGE_TITLE("已存在相同标题的消息模板"),

    /**
     * 指定消息不存在
     */
    MESSAGE_NOT_FOUND("messageNotFound"),

    /**
     * 指定消息模板不存在
     */
    MESSAGE_CHANNEL_ALREADY_DEFINED("messageChannelAlreadyDefined");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "business-message-template";
    }
}
