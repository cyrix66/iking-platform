package com.ikingtech.platform.business.message.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.business.message.entity.MessageChannelDefinitionDO;
import com.ikingtech.platform.business.message.mapper.MessageChannelDefinitionMapper;

/**
 * @author tie yan
 */
public class MessageChannelDefinitionRepository extends ServiceImpl<MessageChannelDefinitionMapper, MessageChannelDefinitionDO> {
}
