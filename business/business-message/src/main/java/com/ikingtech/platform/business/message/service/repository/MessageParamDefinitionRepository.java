package com.ikingtech.platform.business.message.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.business.message.entity.MessageParamDefinitionDO;
import com.ikingtech.platform.business.message.mapper.MessageParamDefinitionMapper;

/**
 * @author tie yan
 */
public class MessageParamDefinitionRepository extends ServiceImpl<MessageParamDefinitionMapper, MessageParamDefinitionDO> {
}
