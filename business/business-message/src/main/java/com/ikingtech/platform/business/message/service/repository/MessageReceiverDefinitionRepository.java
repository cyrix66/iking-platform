package com.ikingtech.platform.business.message.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.business.message.entity.MessageReceiverDefinitionDO;
import com.ikingtech.platform.business.message.mapper.MessageReceiverDefinitionMapper;

/**
 * @author tie yan
 */
public class MessageReceiverDefinitionRepository extends ServiceImpl<MessageReceiverDefinitionMapper, MessageReceiverDefinitionDO> {
}
