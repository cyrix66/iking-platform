package com.ikingtech.platform.business.message.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.business.message.entity.MessageRedirectDefinitionDO;
import com.ikingtech.platform.business.message.mapper.MessageRedirectDefinitionMapper;

/**
 * @author tie yan
 */
public class MessageRedirectDefinitionRepository extends ServiceImpl<MessageRedirectDefinitionMapper, MessageRedirectDefinitionDO> {
}
