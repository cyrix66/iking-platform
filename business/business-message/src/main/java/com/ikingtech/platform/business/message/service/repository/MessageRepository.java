package com.ikingtech.platform.business.message.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.business.message.entity.MessageDO;
import com.ikingtech.platform.business.message.mapper.MessageMapper;

/**
 * @author tie yan
 */
public class MessageRepository extends ServiceImpl<MessageMapper, MessageDO> {
}
