package com.ikingtech.platform.business.message.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.business.message.entity.MessageSubscriberDataDefinitionDO;
import com.ikingtech.platform.business.message.mapper.MessageSubscriberDataDefinitionMapper;

/**
 * @author tie yan
 */
public class MessageSubscriberDataDefinitionRepository extends ServiceImpl<MessageSubscriberDataDefinitionMapper, MessageSubscriberDataDefinitionDO> {
}
