package com.ikingtech.platform.business.message.service.repository;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.data.mybatisplus.helper.base.BaseRepository;
import com.ikingtech.framework.sdk.message.model.MessageSubscriberQueryParamDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.business.message.entity.MessageSubscriberDO;
import com.ikingtech.platform.business.message.mapper.MessageSubscriberMapper;

/**
 * @author tie yan
 */
public class MessageSubscriberRepository extends ServiceImpl<MessageSubscriberMapper, MessageSubscriberDO> implements BaseRepository<MessageSubscriberDO, MessageSubscriberQueryParamDTO> {

    @Override
    public Wrapper<MessageSubscriberDO> conditionalWrapper(MessageSubscriberQueryParamDTO queryParam) {
        return Wrappers.<MessageSubscriberDO>lambdaQuery()
                .in(Tools.Coll.isNotBlank(queryParam.getIds()), MessageSubscriberDO::getId, queryParam.getIds())
                .like(Tools.Str.isNotBlank(queryParam.getName()), MessageSubscriberDO::getName, queryParam.getName())
                .eq(MessageSubscriberDO::getTenantCode, Me.tenantCode());
    }

    @Override
    public Wrapper<MessageSubscriberDO> tenantWrapper() {
        return Wrappers.<MessageSubscriberDO>lambdaQuery()
                .eq(MessageSubscriberDO::getTenantCode, Me.tenantCode());
    }
}
