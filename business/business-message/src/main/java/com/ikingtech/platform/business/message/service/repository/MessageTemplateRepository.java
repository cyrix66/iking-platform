package com.ikingtech.platform.business.message.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.business.message.entity.MessageTemplateDO;
import com.ikingtech.platform.business.message.mapper.MessageTemplateMapper;

/**
 * @author tie yan
 */
public class MessageTemplateRepository extends ServiceImpl<MessageTemplateMapper, MessageTemplateDO> {
}
