package com.ikingtech.platform.business.message.service.router;

import com.ikingtech.framework.sdk.enums.message.MessageSendChannelEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.push.common.MessageRedirect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ikingtech.framework.sdk.utils.Tools.Str.CONTENT_PLACEHOLDER_PREFIX;
import static com.ikingtech.framework.sdk.utils.Tools.Str.CONTENT_PLACEHOLDER_SUFFIX;

/**
 * @author tie yan
 */
public interface MessageRouter {

    RouteResult route(RouteRequest request);

    MessageSendChannelEnum channel();
}
