package com.ikingtech.platform.business.message.service.router;

import com.ikingtech.framework.sdk.enums.message.MessageSendChannelEnum;
import com.ikingtech.framework.sdk.user.model.UserBasicDTO;
import com.ikingtech.platform.business.message.entity.MessageChannelDefinitionDO;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Map;

/**
 * @author tie yan
 */
@Data
public class RouteRequest implements Serializable {

    @Serial
    private static final long serialVersionUID = 8541351724214370152L;

    protected RouteRequest(Builder builder) {
        this.templateId = builder.templateId;
        this.channelDefinition = builder.channelDefinition;
        this.paramDefinitionMap = builder.paramDefinitionMap;
        this.redirectDefinitinoMap = builder.redirectDefinitinoMap;
        this.target = builder.target;
        this.messageWrapper = builder.messageWrapper;
    }

    private String businessKey;

    private String templateId;

    private MessageChannelDefinitionDO channelDefinition;

    private Map<String, String> paramDefinitionMap;

    private Map<String, String> redirectDefinitinoMap;

    private UserBasicDTO target;

    private Object messageWrapper;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String templateId;

        private MessageChannelDefinitionDO channelDefinition;

        private Map<String, String> paramDefinitionMap;

        private Map<String, String> redirectDefinitinoMap;

        private UserBasicDTO target;

        private Object messageWrapper;

        public Builder templateId(String templateId) {
            this.templateId = templateId;
            return this;
        }

        public Builder channelDefinition(MessageChannelDefinitionDO channelDefinition) {
            this.channelDefinition = channelDefinition;
            return this;
        }

        public Builder paramMap(Map<String, String> paramMap) {
            this.paramDefinitionMap = paramMap;
            return this;
        }

        public Builder redirect(Map<String, String> redirect) {
            this.redirectDefinitinoMap = redirect;
            return this;
        }

        public Builder target(UserBasicDTO target) {
            this.target = target;
            return this;
        }

        public Builder messageWrapper(Object messageWrapper) {
            this.messageWrapper = messageWrapper;
            return this;
        }

        public RouteRequest build() {
            return new RouteRequest(this);
        }
    }
}
