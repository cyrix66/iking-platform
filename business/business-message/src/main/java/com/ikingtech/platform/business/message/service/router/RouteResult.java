package com.ikingtech.platform.business.message.service.router;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@AllArgsConstructor
public class RouteResult implements Serializable {

    @Serial
    private static final long serialVersionUID = -966289793721489055L;

    private String content;

    private List<String> redirectTo;

    private Boolean success;

    private String cause;

    private String businessName;

    private String messageTitle;
}
