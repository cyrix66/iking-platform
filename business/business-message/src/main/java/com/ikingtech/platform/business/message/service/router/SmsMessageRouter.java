package com.ikingtech.platform.business.message.service.router;

import com.ikingtech.framework.sdk.enums.message.MessageSendChannelEnum;
import com.ikingtech.platform.business.message.service.repository.MessageChannelDefinitionRepository;
import com.ikingtech.platform.service.push.common.DeliverResult;
import com.ikingtech.platform.service.push.common.PushManager;
import com.ikingtech.platform.service.push.common.PushRequest;

/**
 * @author tie yan
 */
public class SmsMessageRouter extends AbstractMessageRouter {

    public SmsMessageRouter(MessageChannelDefinitionRepository channelDefinitionRepo) {
        super(channelDefinitionRepo);
    }

    @Override
    protected DeliverResult send(PushRequest pushRequest, RouteRequest request) {
        return PushManager.sms().send(pushRequest, request.getTarget().getPhone());
    }

    @Override
    public MessageSendChannelEnum channel() {
        return MessageSendChannelEnum.SMS;
    }
}
