package com.ikingtech.platform.business.message.service.router;

import com.ikingtech.framework.sdk.enums.message.MessageSendChannelEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.business.message.entity.MessageChannelDefinitionDO;
import com.ikingtech.platform.business.message.service.repository.MessageChannelDefinitionRepository;
import com.ikingtech.platform.service.push.common.DeliverResult;
import com.ikingtech.platform.service.push.common.MessageBody;
import com.ikingtech.platform.service.push.common.PushManager;
import com.ikingtech.platform.service.push.common.PushRequest;

import java.util.Map;

import static com.ikingtech.framework.sdk.utils.Tools.Str.CONTENT_PLACEHOLDER_PREFIX;
import static com.ikingtech.framework.sdk.utils.Tools.Str.CONTENT_PLACEHOLDER_SUFFIX;

/**
 * @author tie yan
 */
public class SystemMessageRouter extends AbstractMessageRouter {

    public SystemMessageRouter(MessageChannelDefinitionRepository channelDefinitionRepo) {
        super(channelDefinitionRepo);
    }

    @Override
    protected DeliverResult send(PushRequest pushRequest, RouteRequest request) {
        return PushManager.websocket().send(pushRequest, request.getTarget().getId());
    }

    @Override
    public MessageSendChannelEnum channel() {
        return MessageSendChannelEnum.SYSTEM;
    }
}
