package com.ikingtech.platform.business.message.service.router;

import com.ikingtech.framework.sdk.enums.message.MessageChannelContentTypeEnum;
import com.ikingtech.framework.sdk.enums.message.MessageSendChannelEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.business.message.entity.MessageChannelDefinitionDO;
import com.ikingtech.platform.business.message.service.repository.MessageChannelDefinitionRepository;
import com.ikingtech.platform.service.push.common.DeliverResult;
import com.ikingtech.platform.service.push.common.PushManager;
import com.ikingtech.platform.service.push.common.PushRequest;

import java.util.Map;

/**
 * @author tie yan
 */
public class WebhookMessageRouter extends AbstractMessageRouter{

    public WebhookMessageRouter(MessageChannelDefinitionRepository channelDefinitionRepo) {
        super(channelDefinitionRepo);
    }

    @Override
    public MessageSendChannelEnum channel() {
        return MessageSendChannelEnum.WEBHOOK;
    }

    @Override
    protected String parseBodyText(RouteRequest request, MessageChannelDefinitionDO channelDefinitionEntity, Map<String, Object> messageParamMap) {
        if (MessageChannelContentTypeEnum.SELF.name().equals(channelDefinitionEntity.getContentType())) {
            return Tools.Json.toJsonStr(request.getMessageWrapper());
        }
        if (MessageChannelContentTypeEnum.CUSTOMIZE.name().equals(channelDefinitionEntity.getContentType())) {
            return Tools.Json.toJsonStr(this.convertToTemplateParam(request.getParamDefinitionMap(), messageParamMap));
        }
        return Tools.Str.EMPTY;
    }

    @Override
    protected DeliverResult send(PushRequest pushRequest, RouteRequest request) {
        return PushManager.webhook().send(pushRequest, Tools.Str.EMPTY);
    }
}
