package com.ikingtech.platform.business.message.service.router;

import com.ikingtech.framework.sdk.enums.message.MessageSendChannelEnum;
import com.ikingtech.framework.sdk.enums.system.user.UserSocialTypeEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.business.message.service.repository.MessageChannelDefinitionRepository;
import com.ikingtech.platform.service.push.common.DeliverResult;
import com.ikingtech.platform.service.push.common.PushManager;
import com.ikingtech.platform.service.push.common.PushRequest;

/**
 * @author tie yan
 */
public class WechatMiniMessageRouter extends AbstractMessageRouter {

    public WechatMiniMessageRouter(MessageChannelDefinitionRepository channelDefinitionRepo) {
        super(channelDefinitionRepo);
    }

    @Override
    protected DeliverResult send(PushRequest pushRequest, RouteRequest request) {
        return PushManager.wechatMini().send(pushRequest,
                Tools.Coll.filter(request.getTarget().getSocials(),
                                social -> request.getChannelDefinition().getChannelId().equals(social.getSocialId()) &&
                                        UserSocialTypeEnum.WECHAT_MINI_OPEN_ID.equals(social.getSocialType()))
                        .get(0).getSocialNo());
    }

    /**
     * 获取消息发送渠道
     *
     * @return 消息发送渠道
     */
    @Override
    public MessageSendChannelEnum channel() {
        return MessageSendChannelEnum.WECHAT_MINI_SUBSCRIBE;
    }
}
