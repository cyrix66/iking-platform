package com.ikingtech.platform.business.workbench.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ikingtech.framework.sdk.base.model.BatchParam;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.workbench.model.TodoTaskDeleteBatchParamDTO;
import com.ikingtech.platform.business.workbench.entity.TodoTaskDO;
import com.ikingtech.platform.business.workbench.exception.WorkbenchExceptionInfo;
import com.ikingtech.platform.business.workbench.service.TodoTaskRepository;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.workbench.api.TodoTaskApi;
import com.ikingtech.framework.sdk.workbench.model.TodoTaskDTO;
import com.ikingtech.framework.sdk.workbench.model.TodoTaskQueryParamDTO;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
@ApiController(value = "/workbench/todo-task", name = "工作台-待办任务", description = "工作台-待办任务")
public class TodoTaskController implements TodoTaskApi {

    private final TodoTaskRepository repo;

    @Override
    public R<String> add(TodoTaskDTO todoTask) {
        TodoTaskDO entity = Tools.Bean.copy(todoTask, TodoTaskDO.class);
        entity.setId(Tools.Id.uuid());
        entity.setTenantCode(Me.tenantCode());
        this.repo.save(entity);
        return R.ok(entity.getId());
    }

    @Override
    public R<Object> addBatch(BatchParam<TodoTaskDTO> todoTasks) {
        if (Tools.Coll.isBlank(todoTasks.getList())) {
            return R.ok();
        }
        this.repo.saveBatch(Tools.Coll.convertList(todoTasks.getList(), todoTask -> {
            TodoTaskDO entity = Tools.Bean.copy(todoTask, TodoTaskDO.class);
            entity.setId(Tools.Id.uuid());
            entity.setTenantCode(Me.tenantCode());
            return entity;
        }));
        return R.ok();
    }

    @Override
    public R<Object> delete(String id) {
        this.repo.removeById(id);
        return R.ok();
    }

    @Override
    public R<Object> deleteBatch(TodoTaskDeleteBatchParamDTO deleteParam) {
        this.repo.remove(Wrappers.<TodoTaskDO>lambdaQuery().eq(TodoTaskDO::getBusinessId, deleteParam.getBusinessId()).in(TodoTaskDO::getUserId, deleteParam.getUserIds()));
        return R.ok();
    }

    @Override
    public R<Object> update(TodoTaskDTO todoTask) {
        if (!this.repo.exists(Wrappers.<TodoTaskDO>lambdaQuery().eq(TodoTaskDO::getId, todoTask.getId()))) {
            throw new FrameworkException(WorkbenchExceptionInfo.TODO_TASK_NOT_FOUND);
        }
        this.repo.updateById(Tools.Bean.copy(todoTask, TodoTaskDO.class));
        return R.ok();
    }

    @Override
    public R<List<TodoTaskDTO>> page(TodoTaskQueryParamDTO queryParam) {
        return R.ok(PageResult.build(this.repo.page(new Page<>(queryParam.getPage(), queryParam.getRows()), Wrappers.<TodoTaskDO>lambdaQuery()
                .eq(Tools.Str.isNotBlank(queryParam.getUserId()), TodoTaskDO::getUserId, queryParam.getUserId())
                .like(Tools.Str.isNotBlank(queryParam.getSummary()), TodoTaskDO::getSummary, queryParam.getSummary())
                .orderByDesc(TodoTaskDO::getCreateTime))).convert(entity -> Tools.Bean.copy(entity, TodoTaskDTO.class)));
    }

    @Override
    public R<List<TodoTaskDTO>> all() {
        return R.ok(Tools.Coll.convertList(this.repo.list(), entity -> Tools.Bean.copy(entity, TodoTaskDTO.class)));
    }

    @Override
    public R<TodoTaskDTO> detail(String id) {
        TodoTaskDO entity = this.repo.getById(id);
        if (null == entity) {
            throw new FrameworkException(WorkbenchExceptionInfo.TODO_TASK_NOT_FOUND);
        }

        return R.ok(Tools.Bean.copy(entity, TodoTaskDTO.class));
    }
}
