package com.ikingtech.platform.business.workbench.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@TableName("schedule")
@EqualsAndHashCode(callSuper = true)
public class ScheduleDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -3792819473801733910L;

    @TableField("title")
    private String title;

    @TableField("tenant_code")
    private String tenantCode;

    @TableField("type")
    private String type;

    @TableField("status")
    private String status;

    @TableField("remark")
    private String remark;

    @TableField("business_id")
    private String businessId;

    @TableField("redirect_url")
    private String redirectUrl;

    @TableField("estimate_end_time")
    private LocalDateTime estimateEndTime;

    @TableField("estimate_start_time")
    private LocalDateTime estimateStartTime;

    @TableField("all_day")
    private Boolean allDay;
}