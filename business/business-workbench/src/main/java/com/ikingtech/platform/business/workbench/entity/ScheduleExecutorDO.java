package com.ikingtech.platform.business.workbench.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("schedule_executor")
public class ScheduleExecutorDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -4270248712853243874L;

    @TableField("tenant_code")
    private String tenantCode;

    @TableField("schedule_id")
    private String scheduleId;

    @TableField("executor_id")
    private String executorId;

    @TableField("executor_type")
    private String executorType;
}
