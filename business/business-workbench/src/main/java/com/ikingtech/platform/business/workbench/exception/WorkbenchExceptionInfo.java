package com.ikingtech.platform.business.workbench.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum WorkbenchExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 待办任务不存在
     */
    TODO_TASK_NOT_FOUND("todoTaskNotFound");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "business-workbench";
    }
}
