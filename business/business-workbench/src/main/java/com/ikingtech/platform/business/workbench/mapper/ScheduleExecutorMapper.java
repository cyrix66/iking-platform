package com.ikingtech.platform.business.workbench.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.business.workbench.entity.ScheduleExecutorDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface ScheduleExecutorMapper extends BaseMapper<ScheduleExecutorDO> {
}