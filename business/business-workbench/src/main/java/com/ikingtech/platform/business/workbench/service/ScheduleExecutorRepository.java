package com.ikingtech.platform.business.workbench.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.business.workbench.entity.ScheduleExecutorDO;
import com.ikingtech.platform.business.workbench.mapper.ScheduleExecutorMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author tie yan
 */
@Service
@RequiredArgsConstructor
public class ScheduleExecutorRepository extends ServiceImpl<ScheduleExecutorMapper, ScheduleExecutorDO> {
}
