package com.ikingtech.framework.sdk.application.api;

import java.util.List;

/**
 * @author tie yan
 */
public interface ApplicationMenuApi {

    void saveOrUpdate(String id,
                      String pageName,
                      String parentId,
                      String icon,
                      String link,
                      String component,
                      Boolean sidebar,
                      String activeMenu,
                      Integer sortOrder,
                      String tenantCode,
                      String appCode);

    void removeByPermissionCode(String pageId, String tenantCode, String appCode);

    void removeByPermissionCodes(List<String> pageIds, String tenantCode, String appCode);
}
