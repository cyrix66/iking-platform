package com.ikingtech.framework.sdk.application.api;

import com.ikingtech.framework.sdk.application.model.ApplicationPageDTO;
import com.ikingtech.framework.sdk.application.model.ApplicationPageItemDTO;
import com.ikingtech.framework.sdk.application.model.ApplicationPageQueryParamDTO;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author tie yan
 */
public interface ApplicationPageApi {

    @PostRequest(order = 1, value = "/add", summary = "添加应用页面信息", description = "添加应用页面信息")
    R<String> add(@Parameter(name = "applicationPage", description = "应用页面信息")
                  @RequestBody ApplicationPageDTO applicationPage);

    @PostRequest(order = 2, value = "/delete", summary = "删除应用页面信息", description = "删除应用页面信息")
    R<Object> delete(@Parameter(name = "id", description = "应用页面编号")
                     @RequestBody String id);

    @PostRequest(order = 3, value = "/update", summary = "更新应用页面信息", description = "更新应用页面信息")
    R<Object> update(@Parameter(name = "application", description = "应用信息")
                     @RequestBody ApplicationPageDTO applicationPage);

    @PostRequest(order = 4, value = "/list/page", summary = "分页查询应用列表", description = "分页查询应用列表")
    R<List<ApplicationPageDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                                     @RequestBody ApplicationPageQueryParamDTO queryParam);

    @PostRequest(order = 5, value = "/list/all", summary = "获取所有应用页面信息", description = "获取所有应用页面信息")
    R<List<ApplicationPageDTO>> all();

    @PostRequest(order = 6, value = "/detail", summary = "根据应用页面编号获取应用页面信息", description = "根据应用页面编号获取应用页面信息")
    R<ApplicationPageDTO> detail(@Parameter(name = "id", description = "编号")
                                 @RequestBody String id);

    @PostRequest(order = 7, value = "/list", summary = "查询应用列表", description = "查询应用列表")
    R<List<ApplicationPageDTO>> list(@Parameter(name = "queryParam", description = "查询条件")
                                     @RequestBody ApplicationPageQueryParamDTO queryParam);

    @PostRequest(order = 8, value = "/list/app-code", summary = "根据应用标识获取应用页面信息", description = "根据应用标识获取应用页面信息")
    R<List<ApplicationPageDTO>> listByAppCode(@Parameter(name = "appCode", description = "应用标识")
                                              @RequestBody String appCode);

    @PostRequest(order = 1, value = "/item/home-page/update", summary = "更新主页面信息", description = "更新主页面信息")
    R<Object> updateHomePage(@Parameter(name = "homePage", description = "主页面信息")
                             @RequestBody ApplicationPageItemDTO homePage);

    @PostRequest(order = 1, value = "/item/detail-page/add", summary = "添加详情页面信息", description = "添加详情页面信息")
    R<Object> addDetailPage(@Parameter(name = "detailPage", description = "详情页面信息")
                            @RequestBody ApplicationPageItemDTO detailPage);

    @PostRequest(order = 1, value = "/item/detail-page/delete", summary = "删除详情页面信息", description = "删除详情页面信息")
    R<Object> deleteDetailPage(@Parameter(name = "pageId", description = "应用页面编号")
                               @RequestBody String pageId);

    @PostRequest(order = 1, value = "/item/detail-page/update", summary = "更新详情页面信息", description = "更新详情页面信息")
    R<Object> updateDetailPage(@Parameter(name = "detailPage", description = "详情页面信息")
                               @RequestBody ApplicationPageItemDTO detailPage);
}
