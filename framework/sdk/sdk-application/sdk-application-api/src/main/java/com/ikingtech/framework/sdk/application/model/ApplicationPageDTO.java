package com.ikingtech.framework.sdk.application.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.application.ApplicationPageCategoryEnum;
import com.ikingtech.framework.sdk.enums.application.ApplicationPageTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApplicationPageDTO", description = "应用页面信息")
public class ApplicationPageDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -5809228775126960506L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "code", description = "页面标识")
    private String code;

    @Schema(name = "appId", description = "应用编号")
    private String appId;

    @Schema(name = "appCode", description = "应用标识")
    private String appCode;

    @Schema(name = "parentId", description = "父页面编号")
    private String parentId;

    @Schema(name = "name", description = "页面名称")
    private String name;

    @Schema(name = "icon", description = "页面图标")
    private String icon;

    @Schema(name = "link", description = "页面链接")
    private String link;

    @Schema(name = "type", description = "页面类型")
    private ApplicationPageTypeEnum type;

    @Schema(name = "typeName", description = "页面类型名称")
    private String typeName;

    @Schema(name = "category", description = "页面类别")
    private ApplicationPageCategoryEnum category;

    @Schema(name = "categoryName", description = "页面类别名称")
    private String categoryName;

    @Schema(name = "modelIds", description = "关联模型编号集合")
    private List<String> modelIds;

    @Schema(name = "models", description = "关联模型集合")
    private List<ApplicationModelDTO> models;

    @Schema(name = "homePage", description = "主页面信息")
    private ApplicationPageItemDTO homePage;

    @Schema(name = "detail", description = "详情页面信息")
    private ApplicationPageItemDTO detail;

    @Schema(name = "immediatelyRender", description = "页面定义是否立即生效")
    private Boolean immediatelyRender;

    @Schema(name = "sortOrder", description = "页面排序值")
    private Integer sortOrder;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
