package com.ikingtech.framework.sdk.application.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.application.ApplicationPageItemTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApplicationPageItemDTO", description = "应用页面信息")
public class ApplicationPageItemDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1787268778496641425L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "appId", description = "应用编号")
    private String appId;

    @Schema(name = "appCode", description = "应用标识")
    private String appCode;

    @Schema(name = "pageParentId", description = "父页面编号")
    private String pageParentId;

    @Schema(name = "pageId", description = "页面编号")
    private String pageId;

    @Schema(name = "type", description = "元素类型")
    private ApplicationPageItemTypeEnum type;

    @Schema(name = "typeName", description = "元素类型名称")
    private String typeName;

    @Schema(name = "icon", description = "页面图标")
    private String icon;

    @Schema(name = "link", description = "页面链接")
    private String link;

    @Schema(name = "component", description = "页面组件")
    private String component;

    @Schema(name = "sidebar", description = "是否在侧边栏展示")
    private Boolean sidebar;

    @Schema(name = "registerMenu", description = "是否注册为菜单")
    private Boolean registerMenu;

    @Schema(name = "json", description = "页面JSON")
    private String json;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
