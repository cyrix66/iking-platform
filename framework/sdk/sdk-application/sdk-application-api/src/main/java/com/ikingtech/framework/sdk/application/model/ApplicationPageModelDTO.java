package com.ikingtech.framework.sdk.application.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApplicationPageModelDTO", description = "页面关联模型信息")
public class ApplicationPageModelDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -8906446849462791222L;

    @Schema(name = "pageId", description = "页面编号")
    private String pageId;

    @Schema(name = "tenantCode", description = "租户标识")
    private String tenantCode;

    @Schema(name = "modelId", description = "模型编号")
    private String modelId;
}
