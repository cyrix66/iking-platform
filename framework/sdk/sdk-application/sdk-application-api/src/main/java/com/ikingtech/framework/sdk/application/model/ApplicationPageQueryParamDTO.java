package com.ikingtech.framework.sdk.application.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 岗位信息查询参数
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ApplicationPageQueryParamDTO", description = "应用页面查询参数")
public class ApplicationPageQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -2815045667543932388L;

    @Schema(name = "appCode", description = "应用标识")
    private String appCode;

    @Schema(name = "tenantCode", description = "租户标识")
    private String tenantCode;
    
    @Schema(name = "name", description = "页面名称")
    private String name;

    @Schema(name = "type", description = "页面类型")
    private String type;

    @Schema(name = "code", description = "页面标识")
    private String code;

    @Schema(name = "ids", description = "页面编号集合")
    private List<String> ids;
}
