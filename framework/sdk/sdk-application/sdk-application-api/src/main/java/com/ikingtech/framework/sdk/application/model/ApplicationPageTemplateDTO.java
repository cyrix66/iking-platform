package com.ikingtech.framework.sdk.application.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.application.ApplicationPageTemplateTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author zhang qiang
 */
@Data
@Schema(name = "ApplicationPageTemplateDTO", description = "页面模板信息")
public class ApplicationPageTemplateDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -3147742782300380812L;

    @Schema(name = "id", description = "id")
    private String id;

    @Schema(name = "title", description = "表单名称")
    private String title;

    @Schema(name = "jsonData", description = "模板内容")
    private String jsonData;

    @Schema(name = "type", description = "模板类型")
    private ApplicationPageTemplateTypeEnum type;

    @Schema(name = "description", description = "描述")
    private String description;

    @Schema(name = "internalTemplate", description = "是否为内置模板")
    private Boolean internalTemplate;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

}
