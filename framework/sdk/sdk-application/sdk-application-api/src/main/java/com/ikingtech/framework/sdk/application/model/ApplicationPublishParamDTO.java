package com.ikingtech.framework.sdk.application.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * @author tie yan
 */
@Schema(name = "ApplicationPublishParamDTO", description = "应用发布参数")
public record ApplicationPublishParamDTO(@Schema(name = "appId", description = "应用编号") String appId,
                                         @Schema(name = "tenantCodes", description = "租户标识集合") List<String> tenantCodes,
                                         @Schema(name = "allTenant", description = "是否发布到所有租户")
                                         Boolean allTenant) implements Serializable {

    public ApplicationPublishParamDTO(String id) {
        this(id, Collections.emptyList(), true);
    }
}
