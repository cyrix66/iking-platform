package com.ikingtech.framework.sdk.approve.api;

import com.ikingtech.framework.sdk.approve.model.ApproveFormDTO;
import com.ikingtech.framework.sdk.approve.model.ApproveFormQueryParamDTO;
import com.ikingtech.framework.sdk.approve.model.rpc.ApproveFormBeanDefinitionRegisterParam;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public interface ApproveFormApi {


    @PostRequest(order = 6, value = "/list/page", summary = "分页查询审批表单", description = "分页查询审批表单")
    R<List<ApproveFormDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                                 @RequestBody ApproveFormQueryParamDTO queryParam);


    @PostRequest(order = 6, value = "/count", summary = "查询流程数量", description = "查询流程数量")
    R<Map<String, Long>> count(@Parameter(name = "queryParam", description = "查询条件")
                               @RequestBody ApproveFormQueryParamDTO queryParam);

    /**
     * 上报流程表单定义信息
     *
     * @param registerParam 流程表单定义信息
     * @return 上报结果
     */
    @PostRequest(order = 1, value = "/register", summary = "注册流程表单", description = "注册流程表单")
    R<Object> register(@Parameter(name = "registerParam", description = "流程表单定义信息")
                       @RequestBody ApproveFormBeanDefinitionRegisterParam registerParam);

    /**
     * 取消流程表单注册
     *
     * @param businessType 业务类型
     * @return 上报结果
     */
    @PostRequest(order = 1, value = "/unregister", summary = "取消流程表单注册", description = "取消流程表单注册")
    R<Object> unregister(@Parameter(name = "businessType", description = "业务类型")
                         @RequestBody String businessType);
}
