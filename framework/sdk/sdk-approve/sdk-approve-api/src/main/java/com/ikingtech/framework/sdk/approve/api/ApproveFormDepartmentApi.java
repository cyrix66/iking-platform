package com.ikingtech.framework.sdk.approve.api;

import com.ikingtech.framework.sdk.approve.model.ApproveFormDepartment;
import com.ikingtech.framework.sdk.approve.model.ApproveFormDepartmentManager;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public interface ApproveFormDepartmentApi {

    ApproveFormDepartment loadById(String deptId);

    Map<String, ApproveFormDepartment> mapByIds(List<String> deptIds);

    String findUnitId(String deptId);

    List<String> findUnitIds(List<String> deptIds);

    ApproveFormDepartmentManager loadManager(String deptId);

    String loadIdByParentLevel(String deptId, int level);

    List<String> loadIdByName(String deptName, String tenantCode);
}
