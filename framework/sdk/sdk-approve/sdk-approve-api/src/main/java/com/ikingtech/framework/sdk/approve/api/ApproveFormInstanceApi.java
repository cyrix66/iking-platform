package com.ikingtech.framework.sdk.approve.api;

import com.ikingtech.framework.sdk.approve.model.*;
import com.ikingtech.framework.sdk.base.model.BatchParam;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;


/**
 * @author tie yan
 */
public interface ApproveFormInstanceApi {

    /**
     * 预览表单信息
     *
     * @param previewParam 表单信息
     * @return 预览结果
     */
    @PostRequest(order = 1, value = "/preview", summary = "预览表单信息", description = "预览表单信息")
    R<ApproveFormInstanceDTO> preview(@Parameter(name = "previewParam", description = "表单信息。")
                                      @RequestBody ApproveFormInstancePreviewParamDTO previewParam);

    /**
     * 分页查询表单实例
     *
     * @param queryParam 查询参数
     * @return 表单实例列表
     */
    @PostRequest(order = 2, value = "/list/page", summary = "分页查询表单实例", description = "分页查询表单实例")
    R<List<ApproveFormInstanceBasicDTO>> page(@Parameter(name = "queryParam", description = "查询参数。")
                                              @RequestBody ApproveFormInstanceQueryParamDTO queryParam);

    /**
     * 查询审批流程实例用户执行状态列表
     *
     * @param queryParam 查询参数
     * @return 审批流程实例用户执行状态列表
     */
    @PostRequest(order = 3, value = "/process-instance-user/status/list", summary = "查询审批流程实例用户执行状态列表", description = "查询审批流程实例用户执行状态列表")
    R<List<ApproveProcessInstanceUserExecuteStatusDTO>> listProcessInstanceUserExecuteStatus(@Parameter(name = "queryParam", description = "查询参数。")
                                                                                             @RequestBody ApproveFormInstanceQueryParamDTO queryParam);

    /**
     * 查询表单实例详情
     *
     * @param id 表单实例编号
     * @return 表单实例详情
     */
    @PostRequest(order = 4, value = "/detail/id", summary = "查询表单实例详情", description = "查询表单实例详情")
    R<ApproveFormInstanceDTO> detail(@Parameter(name = "id", description = "表单实例编号。")
                                     @RequestBody String id);

    /**
     * 查询表单实例节点列表
     *
     * @param ids 表单实例编号或业务编号
     * @return 表单实例节点列表
     */
    @PostRequest(order = 5, value = "/process-instance-node/list/ids", summary = "查询表单实例节点列表", description = "查询表单实例节点列表")
    R<List<ApproveProcessInstanceDTO>> listInstanceNode(@Parameter(name = "ids", description = "表单实例编号或业务编号。")
                                                        @RequestBody BatchParam<String> ids);

    /**
     * 查询已执行的表单实例节点列表
     *
     * @param formInstanceId 表单实例编号
     * @return 已执行的表单实例节点列表
     */
    @PostRequest(order = 6, value = "/executed-node/list/id", summary = "查询已执行的表单实例节点列表", description = "查询已执行的表单实例节点列表")
    R<List<ApproveProcessInstanceNodeDTO>> listExecutedInstanceNode(@Parameter(name = "id", description = "表单实例编号。")
                                                                    @RequestBody String formInstanceId);

    /**
     * 添加草稿
     *
     * @param formInstance 审批流程信息
     * @return 草稿编号
     */
    @PostRequest(order = 7, value = "/draft/add", summary = "添加草稿", description = "添加草稿")
    R<String> addDraft(@Parameter(name = "formInstance", description = "审批流程信息。")
                       @RequestBody ApproveFormInstanceDTO formInstance);

    /**
     * 删除草稿
     *
     * @param id 表单实例编号
     * @return 删除结果
     */
    @PostRequest(order = 8, value = "/draft/delete", summary = "删除草稿", description = "删除草稿")
    R<Object> deleteDraft(@Parameter(name = "id", description = "审批流程实例编号。")
                          @RequestBody String id);

    /**
     * 更新草稿
     *
     * @param formInstance 审批流程信息
     * @return 更新结果
     */
    @PostRequest(order = 9, value = "/draft/update", summary = "更新草稿", description = "更新草稿")
    R<Object> updateDraft(@Parameter(name = "formInstance", description = "审批流程信息。")
                          @RequestBody ApproveFormInstanceDTO formInstance);

    /**
     * 分页查询草稿
     *
     * @param queryParam 分页查询参数
     * @return 草稿列表
     */
    @PostRequest(order = 10, value = "/draft/page", summary = "分页查询草稿", description = "分页查询草稿")
    R<List<ApproveFormInstanceBasicDTO>> pageDraft(@Parameter(name = "queryParam", description = "分页查询参数。")
                                                   @RequestBody ApproveFormInstanceDraftQueryParamDTO queryParam);

    /**
     * 分享表单实例
     *
     * @param shareParam 分享参数
     * @return 分享结果
     */
    @PostRequest(order = 11, value = "/share", summary = "分享表单实例", description = "分享表单实例")
    R<Object> share(@Parameter(name = "shareParam", description = "分享参数。")
                    @RequestBody ApproveFormInstanceShareParamDTO shareParam);

    /**
     * 取消分享
     *
     * @param formInstanceId 表单实例编号
     * @return 取消分享结果
     */
    @PostRequest(order = 12, value = "/share/cancel", summary = "取消分享", description = "取消分享")
    R<Object> cancelShare(@Parameter(name = "formInstanceId", description = "表单实例编号。")
                          @RequestBody String formInstanceId);

    /**
     * 提交表单实例
     *
     * @param formInstance 审批流程信息
     * @return 提交结果
     */
    @PostRequest(order = 13, value = "/submit", summary = "提交表单实例", description = "提交表单实例")
    R<ApproveFormInstanceBasicDTO> submit(@Parameter(name = "formInstance", description = "审批流程信息。")
                                          @RequestBody ApproveFormInstanceDTO formInstance);

    /**
     * 重新提交表单实例
     *
     * @param reSubmitParam 审批流程提交信息
     * @return 重新提交结果
     */
    @PostRequest(order = 14, value = "/re-submit", summary = "重新提交表单实例", description = "重新提交表单实例")
    R<ApproveFormInstanceBasicDTO> reSubmit(@Parameter(name = "reSubmitParam", description = "审批流程提交信息。")
                                            @RequestBody ApproveFormOperationDTO reSubmitParam);

    /**
     * 更新表单实例
     *
     * @param submitParam 审批流程提交信息
     * @return 更新结果
     */
    @PostRequest(order = 15, value = "/update", summary = "更新表单实例", description = "更新表单实例")
    R<Object> update(@Parameter(name = "submitParam", description = "审批流程提交信息。")
                     @RequestBody ApproveFormSubmitParamDTO submitParam);

    /**
     * 取消更新
     *
     * @param cancelUpdateParam 撤销修改参数
     * @return 取消更新结果
     */
    @PostRequest(order = 16, value = "/update/cancel", summary = "取消更新", description = "取消更新")
    R<Object> cancelUpdate(@Parameter(name = "cancelUpdateParam", description = "撤销修改参数。")
                           @RequestBody ApproveFormOperationDTO cancelUpdateParam);

    /**
     * 撤销
     *
     * @param revokeParam 撤销参数
     * @return 撤销结果
     */
    @PostRequest(order = 17, value = "/revoke", summary = "撤销", description = "撤销")
    R<Object> revoke(@Parameter(name = "revokeParam", description = "撤销参数。")
                     @RequestBody ApproveFormOperationDTO revokeParam);

    /**
     * 取消撤销
     *
     * @param cancelRevokeParam 中止撤销参数
     * @return 取消撤销结果
     */
    @PostRequest(order = 18, value = "/revoke/cancel", summary = "取消撤销", description = "取消撤销")
    R<Object> cancelRevoke(@Parameter(name = "cancelRevokeParam", description = "中止撤销参数。")
                           @RequestBody ApproveFormOperationDTO cancelRevokeParam);

    /**
     * 催办
     *
     * @param remindParam 催办参数
     * @return 催办结果
     */
    @PostRequest(order = 19, value = "/remind", summary = "催办", description = "催办")
    R<Object> remind(@Parameter(name = "remindParam", description = "催办参数。")
                     @RequestBody ApproveFormOperationDTO remindParam);

    /**
     * 查看抄送
     *
     * @param carbonCopyReadParam 查看抄送参数
     * @return 查看抄送结果
     */
    @PostRequest(order = 20, value = "/carbon-copy/read", summary = "查看抄送", description = "查看抄送")
    R<Object> carbonCopyRead(@Parameter(name = "remindParam", description = "查看抄送参数。")
                             @RequestBody ApproveFormOperationDTO carbonCopyReadParam);

    /**
     * 评论
     *
     * @param commentParam 评论参数
     * @return 评论结果
     */
    @PostRequest(order = 21, value = "/comment", summary = "评论", description = "评论")
    R<Object> comment(@Parameter(name = "commentParam", description = "评论参数。")
                      @RequestBody ApproveFormOperationDTO commentParam);

    /**
     * 审批同意
     *
     * @param passParam 审批同意参数
     * @return 审批同意结果
     */
    @PostRequest(order = 22, value = "/pass", summary = "审批同意", description = "审批同意")
    R<ApproveFormInstanceBasicDTO> pass(@Parameter(name = "passParam", description = "审批同意参数。")
                                        @RequestBody ApproveFormOperationDTO passParam);

    /**
     * 批量审批同意
     *
     * @param passParam 审批同意参数
     * @return 审批同意结果
     */
    @PostRequest(order = 23, value = "/pass/batch", summary = "批量审批同意", description = "批量审批同意")
    R<Object> passBatch(@Parameter(name = "passParam", description = "审批同意参数。")
                        @RequestBody BatchParam<ApproveFormOperationDTO> passParam);

    /**
     * 审批拒绝
     *
     * @param rejectParam 审批拒绝参数
     * @return 审批拒绝结果
     */
    @PostRequest(order = 24, value = "/reject", summary = "审批拒绝", description = "审批拒绝")
    R<ApproveFormInstanceBasicDTO> reject(@Parameter(name = "rejectParam", description = "审批拒绝参数。")
                                          @RequestBody ApproveFormOperationDTO rejectParam);

    /**
     * 审批退回
     *
     * @param backParam 审批退回参数
     * @return 审批退回结果
     */
    @PostRequest(order = 25, value = "/back", summary = "审批退回", description = "审批退回")
    R<ApproveFormInstanceBasicDTO> back(@Parameter(name = "backParam", description = "审批退回参数。")
                                        @RequestBody ApproveFormOperationDTO backParam);

    /**
     * 加签
     *
     * @param appendParam 加签参数
     * @return 加签结果
     */
    @PostRequest(order = 26, value = "/append", summary = "加签", description = "加签")
    R<Object> append(@Parameter(name = "appendParam", description = "加签参数。")
                     @RequestBody ApproveFormOperationDTO appendParam);
}
