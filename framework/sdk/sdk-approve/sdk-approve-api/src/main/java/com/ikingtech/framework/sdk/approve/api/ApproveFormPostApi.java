package com.ikingtech.framework.sdk.approve.api;

import com.ikingtech.framework.sdk.approve.model.ApproveFormPost;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public interface ApproveFormPostApi {

    Map<String, ApproveFormPost> mapByIds(List<String> deptIds);

    List<String> loadIdByName(String userName, String tenantCode);
}
