package com.ikingtech.framework.sdk.approve.api;

import com.ikingtech.framework.sdk.approve.model.ApproveFormRole;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public interface ApproveFormRoleApi {

    Map<String, ApproveFormRole> mapByIds(List<String> roleIds);

    List<String> loadIdByName(String roleName, String tenantCode);
}
