package com.ikingtech.framework.sdk.approve.api;

import com.ikingtech.framework.sdk.approve.model.ApproveFormUser;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public interface ApproveFormUserApi {

    List<String> loadRoleIds(List<String> userIds);

    List<String> loadDepartmentIds(List<String> userIds);

    List<String> loadPostIds(List<String> userIds);

    List<String> loadIdByRoleId(String roleId);

    List<String> loadIdByRoleIds(List<String> roleIds);

    List<String> loadIdByDepartmentId(String deptId);

    List<String> loadIdByDepartmentIds(List<String> deptIds);

    List<String> loadIdByPostId(String postId);

    List<ApproveFormUser> loadByIds(List<String> userIds);

    List<ApproveFormUser> loadByRoleIds(List<String> roleIds);

    Map<String, ApproveFormUser> mapByIds(List<String> deptIds);

    ApproveFormUser loadById(String userId);

    List<String> loadIdByName(String userName);

    List<ApproveFormUser> loadByMenuCode(String menuCode);
}
