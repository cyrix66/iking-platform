package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.enums.approve.ApproveConditionApplicableTypeEnum;
import com.ikingtech.framework.sdk.enums.common.ConditionComparatorEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveCondCompDTO", description = "工作流条件比较式信息")
public class ApproveCondCompDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 4048997292568183307L;

    @Schema(name = "id", description = "工作流分支节点条件比较式编号")
    private String id;

    @Schema(name = "id", description = "所属表单编号")
    private String formId;

    @Schema(name = "id", description = "所属工作流流程配置编号")
    private String processId;

    @Schema(name = "nodeId", description = "所属工作流节点编号")
    private String nodeId;

    @Schema(name = "widgetCode", description = "表单控件标识")
    private String widgetCode;

    @Schema(name = "widgetName", description = "工作流分支节点条件比较式字段名称")
    private String widgetName;

    @Schema(name = "widgetDescription", description = "工作流分支节点条件比较式字段名称（显示）")
    private String widgetDescription;

    @Schema(name = "comparator", description = "工作流分支节点条件比较式运算符")
    private ConditionComparatorEnum comparator;

    @Schema(name = "comparatorName", description = "工作流分支节点条件比较式运算符名称")
    private String comparatorName;

    @Schema(name = "compareToValues", description = "工作流分支节点条件比较式字段值")
    private List<String> compareToValues;

    @Schema(name = "datePattern", description = "日期格式")
    private String datePattern;

    @Schema(name = "applicableType", description = "适用类型")
    private ApproveConditionApplicableTypeEnum applicableType;
}
