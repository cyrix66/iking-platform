package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.enums.approve.ApproveConditionApplicableTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveCondGroupDTO", description = "工作流条件组信息")
public class ApproveCondGroupDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1127555663052968665L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "formId", description = "所属审批表单编号")
    private String formId;

    @Schema(name = "processId", description = "所属审批流程编号")
    private String processId;

    @Schema(name = "nodeId", description = "所属审批流程节点编号")
    private String nodeId;

    @Schema(name = "name", description = "审批流程分支条件组名称")
    private String name;

    @Schema(name = "comparisons", description = "工作流条件节点比较式集合")
    private List<ApproveCondCompDTO> comparisons;

    @Schema(name = "applicableType", description = "适用类型")
    private ApproveConditionApplicableTypeEnum applicableType;
}
