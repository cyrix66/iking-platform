package com.ikingtech.framework.sdk.approve.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.approve.ApproveFormStatusEnum;
import com.ikingtech.framework.sdk.enums.approve.ApproveFormTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormBasicDTO", description = "工作流表单基础配置信息")
public class ApproveFormBasicDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -970222530799937360L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "code", description = "表单标识")
    private String code;

    @Schema(name = "processCode", description = "流程标识")
    private String processCode;

    @Schema(name = "name", description = "表单名称")
    private String name;

    @Schema(name = "icon", description = "表单图标")
    private String icon;

    @Schema(name = "iconBackground", description = "表单图标背景色")
    private String iconBackground;

    @Schema(name = "groupId", description = "表单所属分组编号")
    private String groupId;

    @Schema(name = "groupName", description = "表单所属分组名称")
    private String groupName;

    @Schema(name = "remark", description = "表单说明")
    private String remark;

    @Schema(name = "status", description = "表单状态")
    private ApproveFormStatusEnum status;

    @Schema(name = "statusName", description = "表单状态名称")
    private String statusName;

    @Schema(name = "customizeInitiator", description = "表单是否支持配置发起人")
    private Boolean customizeInitiator;

    @Schema(name = "initiators", description = "表单发起人列表")
    private List<ApproveFormInitiatorDTO> initiators;

    @Schema(name = "managers", description = "表单管理者列表")
    private List<ApproveFormManagerDTO> managers;

    @Schema(name = "deptId", description = "表单所属部门编号")
    private String deptId;

    @Schema(name = "dept", description = "表单所属部门")
    private ApproveFormDepartment dept;

    @Schema(name = "manageByCurrentUser", description = "是否允许当前登录人进行管理")
    private Boolean manageByCurrentUser;

    @Schema(name = "businessType", description = "表单业务类型")
    private String businessType;

    @Schema(name = "type", description = "表单分类")
    private ApproveFormTypeEnum type;

    @Schema(name = "typeName", description = "表单分类名称")
    private String typeName;

    @Schema(name = "latestVersion", description = "表单配置是否为最新版本")
    private Boolean latestVersion;

    @Schema(name = "versionNo", description = "表单配置版本号")
    private Integer versionNo;

    @Schema(name = "sortOrder", description = "分组排序值")
    private Integer sortOrder;

    @Schema(name = "visible", description = "是否可见")
    private Boolean visible;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
