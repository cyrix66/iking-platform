package com.ikingtech.framework.sdk.approve.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ApproveFormDTO", description = "工作流表单基础配置信息")
public class ApproveFormDTO extends ApproveFormBasicDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -970222530799937360L;

    @Schema(name = "formViewConfig", description = "表单视图配置")
    private ApproveFormViewDTO formViewConfig;

    @Schema(name = "process", description = "表单流程配置")
    private ApproveProcessDTO process;

    @Schema(name = "formAdvanceConfig", description = "表单高级配置")
    private ApproveFormAdvanceConfigDTO formAdvanceConfig;
}
