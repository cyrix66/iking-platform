package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.enums.system.department.DeptTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormDepartment", description = "审批部门信息")
public class ApproveFormDepartment implements Serializable {

    @Serial
    private static final long serialVersionUID = 2089832979199891253L;

    private String deptId;

    private String deptFullPath;

    private String deptName;

    private DeptTypeEnum deptType;

    private String deptManagerId;

    private String deptManagerName;

    private String deptManagerAvatar;
}
