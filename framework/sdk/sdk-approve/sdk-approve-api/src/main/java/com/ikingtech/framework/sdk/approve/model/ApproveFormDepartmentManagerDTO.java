package com.ikingtech.framework.sdk.approve.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormDepartmentManagerDTO", description = "审批部门信息")
public class ApproveFormDepartmentManagerDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 2089832979199891253L;

    private String userId;

    private String userName;

    private String userAvatar;
}
