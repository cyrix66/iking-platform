package com.ikingtech.framework.sdk.approve.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormGroupDTO", description = "工作流表单分组信息")
public class ApproveFormGroupDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 293770060371957766L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "name", description = "分组名称")
    private String name;

    @Schema(name = "formCount", description = "分组下表单数量，页面展示时使用，新增或更新时忽略。")
    private Integer formCount;

    @Schema(name = "remark", description = "分组说明")
    private String remark;

    @Schema(name = "renamable", description = "分组是否允许重命名")
    private Boolean renamable;

    @Schema(name = "deletable", description = "分组是否允许删除")
    private Boolean deletable;

    @Schema(name = "sortable", description = "分组是否允许排序")
    private Boolean sortable;

    @Schema(name = "forms", description = "分组下的表单列表，页面展示时使用，新增或更新时忽略。")
    private List<ApproveFormBasicDTO> forms;

    @Schema(name = "sortOrder", description = "分组排序值")
    private Integer sortOrder;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
