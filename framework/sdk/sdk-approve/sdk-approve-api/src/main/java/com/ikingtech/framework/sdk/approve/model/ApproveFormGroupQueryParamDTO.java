package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ApproveFormGroupQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 5928125250375226816L;

    private String name;
}
