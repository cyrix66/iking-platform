package com.ikingtech.framework.sdk.approve.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.base.model.BaseModel;
import com.ikingtech.framework.sdk.enums.approve.ApproveFormInstanceCabonCopyReadStatusEnum;
import com.ikingtech.framework.sdk.enums.approve.ApproveFormTypeEnum;
import com.ikingtech.framework.sdk.enums.approve.ApproveProcessInstanceStatusEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ApproveFormInstanceBasicDTO", description = "表单实例基础信息")
public class ApproveFormInstanceBasicDTO extends BaseModel implements Serializable {

    @Serial
    private static final long serialVersionUID = -246614748156196445L;

    @Schema(name = "serialNo", description = "审批流水号")
    private String serialNo;

    @Schema(name = "formId", description = "表单编号")
    private String formId;

    @Schema(name = "formVisible", description = "表单是否可见")
    private Boolean formVisible;

    @Schema(name = "formName", description = "表单名称")
    private String formName;

    @Schema(name = "formType", description = "表单类型")
    private ApproveFormTypeEnum formType;

    @Schema(name = "formTypeName", description = "表单类型名称")
    private String formTypeName;

    @Schema(name = "businessType", description = "业务类型")
    private String businessType;

    @Schema(name = "businessDataId", description = "业务数据编号")
    private String businessDataId;

    @Schema(name = "title", description = "表单标题")
    private String title;

    @Schema(name = "initiatorId", description = "发起人编号")
    private String initiatorId;

    @Schema(name = "initiatorName", description = "发起人姓名")
    private String initiatorName;

    @Schema(name = "initiatorDeptId", description = "发起人部门编号")
    private String initiatorDeptId;

    @Schema(name = "initiatorDeptName", description = "发起人部门名称")
    private String initiatorDeptName;

    @Schema(name = "initiateTime", description = "发起时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime initiateTime;

    @Schema(name = "accomplishTime", description = "完成时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime accomplishTime;

    @Schema(name = "approveTime", description = "审批时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime approveTime;

    @Schema(name = "carbonCopyTime", description = "抄送时间")
    private LocalDateTime carbonCopyTime;

    @Schema(name = "waitTime", description = "等待时长")
    private String waitTime;

    @Schema(name = "consumeTime", description = "审批耗时")
    private String consumeTime;

    @Schema(name = "viewProperty", description = "表单视图属性")
    private String viewProperty;

    @Schema(name = "reportTemplateId", description = "表单报表编号")
    private String reportTemplateId;

    @Schema(name = "formData", description = "表单实例业务数据")
    private String formData;

    @Schema(name = "formDataSummary", description = "表单实例业务数据摘要")
    private String formDataSummary;

    @Schema(name = "processStatus", description = "流程状态")
    private ApproveProcessInstanceStatusEnum processStatus;

    @Schema(name = "processStatusName", description = "流程状态名称")
    private String processStatusName;

    @Schema(name = "currentExecutorUserId", description = "当前执行者用户集合")
    private List<ApproveProcessInstanceUserDTO> currentInstanceUsers;

    @Schema(name = "carbonCopyReadStatus", description = "抄送读取状态")
    private ApproveFormInstanceCabonCopyReadStatusEnum carbonCopyReadStatus;

    @Schema(name = "carbonCopyReadStatusName", description = "抄送读取状态名称")
    private String carbonCopyReadStatusName;

    @Schema(name = "formReportTemplateId", description = "表单报表配置模板编号")
    private String formReportTemplateId;

    @Schema(name = "visible", description = "表单在审批中心是否可见")
    private Boolean visible;

    @Schema(name = "shared", description = "是否已分享")
    private Boolean shared;

    @Schema(name = "draft", description = "是否是草稿")
    private Boolean draft;

    @Schema(name = "triggerConditionEnabled", description = "是否已开启触发条件")
    private Boolean triggerConditionEnabled;

    @Schema(name = "nextNodeUserIds", description = "下一节点的用户编号")
    private List<String> nextUserIds;
}
