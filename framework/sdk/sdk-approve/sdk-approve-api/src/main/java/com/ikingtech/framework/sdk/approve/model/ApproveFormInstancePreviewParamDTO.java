package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.enums.approve.ApproveFormInstancePreviewTypeEnum;
import com.ikingtech.framework.sdk.enums.approve.ApproveFormTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormInstancePreviewParamDTO", description = "审批流程预览参数")
public class ApproveFormInstancePreviewParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 4541505965478312971L;

    @Schema(name = "formInstanceId", description = "表单实例编号")
    private String formInstanceId;

    @Schema(name = "formId", description = "表单编号")
    private String formId;

    @Schema(name = "formTitle", description = "审批标题")
    private String formTitle;

    @Schema(name = "previewType", description = "预览类型")
    private ApproveFormInstancePreviewTypeEnum previewType;

    @Schema(name = "formType", description = "表单类型")
    private ApproveFormTypeEnum formType;

    @Schema(name = "formCode", description = "表单业务类型")
    private String formCode;

    @Schema(name = "formData", description = "表单数据")
    private String formData;

    @Schema(name = "businessDataId", description = "业务数据编号")
    private String businessDataId;

    @Schema(name = "initiatorDeptId", description = "发起人部门编号")
    private String initiatorDeptId;
}
