package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.enums.approve.ApproveAppendTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormOperationDTO", description = "审批流程操作参数")
public class ApproveFormOperationDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -7040947792326512802L;

    @Schema(name = "formInstanceId", description = "表单实例编号")
    private String formInstanceId;

    @Schema(name = "formData", description = "审批表单数据")
    private String formData;

    @Schema(name = "appendType", description = "加签方式")
    private ApproveAppendTypeEnum appendType;

    @Schema(name = "appendExecutorUserIds", description = "加签执行者用户编号列表")
    private List<String> appendExecutorUserIds;

    @Schema(name = "backToInstanceNodeId", description = "退回至的实例节点编号")
    private String backToInstanceNodeId;

    @Schema(name = "backToInstanceNodeUserIds", description = "退回至的实例节点用户编号集合")
    private List<String> backToInstanceNodeUserIds;

    @Schema(name = "approveComment", description = "审批意见填写提示")
    private String approveComment;

    @Schema(name = "attachments", description = "附件")
    private List<ApproveRecordNodeAttachmentDTO> attachments;

    @Schema(name = "images", description = "图片")
    private List<String> images;

    @Schema(name = "reviewedInstanceNodes", description = "重新预览的审批实例节点集合")
    private List<ApproveProcessInstanceNodeDTO> reviewedInstanceNodes;
}
