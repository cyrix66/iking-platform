package com.ikingtech.framework.sdk.approve.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormPost", description = "审批部门信息")
public class ApproveFormPost implements Serializable {

    @Serial
    private static final long serialVersionUID = 1148304451765547488L;

    private String postId;

    private String postName;
}
