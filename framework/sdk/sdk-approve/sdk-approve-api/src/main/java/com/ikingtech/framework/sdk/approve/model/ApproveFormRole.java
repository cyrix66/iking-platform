package com.ikingtech.framework.sdk.approve.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormRole", description = "审批角色信息")
public class ApproveFormRole implements Serializable {

    @Serial
    private static final long serialVersionUID = 428830733371701369L;

    private String roleId;

    private String roleName;
}
