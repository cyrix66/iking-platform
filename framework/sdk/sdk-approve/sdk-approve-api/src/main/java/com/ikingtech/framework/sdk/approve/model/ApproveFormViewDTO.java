package com.ikingtech.framework.sdk.approve.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormViewDTO", description = "工作流表单设计配置信息")
public class ApproveFormViewDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -970222530799937360L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "formId", description = "表单编号")
    private String formId;

    @Schema(name = "processNodeId", description = "表单编号")
    private String processNodeId;

    @Schema(name = "property", description = "表单视图定义信息")
    private String property;

    @Schema(name = "widgets", description = "表单控件信息集合")
    private List<ApproveFormWidgetDTO> widgets;

    @Schema(name = "submitLabel", description = "表单提交按钮名称")
    private String submitLabel;

    @Schema(name = "refuseLabel", description = "表单拒绝按钮名称")
    private String refuseLabel;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
