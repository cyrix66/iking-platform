package com.ikingtech.framework.sdk.approve.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveProcessDTO", description = "审批流程配置信息")
public class ApproveProcessDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -156907688322379165L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "formId", description = "表单编号")
    private String formId;

    @Schema(name = "name", description = "审批流程名称")
    private String name;

    @Schema(name = "property", description = "审批流程图配置信息")
    private String property;

    @Schema(name = "rootNode", description = "审批流程根节点配置信息")
    private ApproveProcessNodeDTO rootNode;
}
