package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.enums.approve.ApproveExecutorCategoryEnum;
import com.ikingtech.framework.sdk.enums.approve.ApproveExecutorTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveProcessExecutorDTO", description = "审批对象信息")
public class ApproveProcessExecutorDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -156907688322379165L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "formId", description = "所属表单编号")
    private String formId;

    @Schema(name = "nodeId", description = "所属节点编号")
    private String nodeId;

    @Schema(name = "executorType", description = "执行者类型")
    private ApproveExecutorTypeEnum executorType;

    @Schema(name = "executorId", description = "执行者编号")
    private String executorId;

    @Schema(name = "defaultExecutorId", description = "默认执行者编号")
    private String defaultExecutorId;

    @Schema(name = "executorName", description = "执行者名称")
    private String executorName;

    @Schema(name = "executorAvatar", description = "执行者头像")
    private String executorAvatar;

    @Schema(name = "executorDeptIds", description = "执行者所属部门编号集合")
    private List<String> executorDeptIds;

    @Schema(name = "executorParentDeptLevel", description = "执行者所在部门父部门级别")
    private Integer executorParentDeptLevel;

    @Schema(name = "executorCategory", description = "执行者分类")
    private ApproveExecutorCategoryEnum executorCategory;
}
