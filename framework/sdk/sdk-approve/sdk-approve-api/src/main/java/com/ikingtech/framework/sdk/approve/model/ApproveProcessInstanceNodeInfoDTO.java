package com.ikingtech.framework.sdk.approve.model;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class ApproveProcessInstanceNodeInfoDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 2840421718466485371L;

    private String businessDataId;

    private List<ApproveProcessInstanceNodeDTO> processInstanceNode;
}
