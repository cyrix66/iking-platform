package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.enums.approve.ApproveProcessInstanceUserStatusEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveProcessInstanceNodeExecutorUserDTO", description = "审批执行者用户信息")
public class ApproveProcessInstanceUserDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -156907688322379165L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "formId", description = "所属表单编号")
    private String formId;

    @Schema(name = "processId", description = "所属审批流编号")
    private String processId;

    @Schema(name = "formInstanceId", description = "表单实例编号")
    private String formInstanceId;

    @Schema(name = "processInstanceId", description = "审批流实例编号")
    private String processInstanceId;

    @Schema(name = "userId", description = "执行者用户编号")
    private String userId;

    @Schema(name = "userName", description = "执行者用户姓名")
    private String userName;

    @Schema(name = "userAvatar", description = "执行者用户头像")
    private String userAvatar;

    @Schema(name = "status", description = "执行者用户执行状态")
    private ApproveProcessInstanceUserStatusEnum status;

    @Schema(name = "statusName", description = "执行者用户执行状态名称")
    private String statusName;

    @Schema(name = "sortOrder", description = "排序值")
    private Integer sortOrder;
}
