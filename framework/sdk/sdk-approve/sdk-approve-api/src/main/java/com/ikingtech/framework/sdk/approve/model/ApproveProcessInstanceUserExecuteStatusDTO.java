package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.enums.approve.*;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class ApproveProcessInstanceUserExecuteStatusDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1079789690433005660L;

    private String businessDataId;

    private String userId;

    private ApproveProcessInstanceUserStatusEnum processInstanceUserStatus;

    private ApproveProcessNodeTypeEnum processNodeType;

    private ApproveRecordNodeUserStatusEnum recordNodeUserStatus;

    private ApproveRecordNodeStatusEnum recordNodeStatus;
}
