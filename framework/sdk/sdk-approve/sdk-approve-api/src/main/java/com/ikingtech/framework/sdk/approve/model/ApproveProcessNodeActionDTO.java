package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.base.model.BaseModel;
import com.ikingtech.framework.sdk.enums.approve.ApproveHandleTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ApproveProcessNodeActionDTO", description = "审批流程节点操作信息")
public class ApproveProcessNodeActionDTO extends BaseModel implements Serializable {

    @Serial
    private static final long serialVersionUID = -4622693079950813679L;

    @Schema(name = "formId", description = "所属表单编号")
    private String formId;

    @Schema(name = "processId", description = "所属流程编号")
    private String processId;

    @Schema(name = "processNodeId", description = "所属流程配置节点编号")
    private String processNodeId;

    @Schema(name = "action", description = "节点操作")
    private ApproveHandleTypeEnum action;

    @Schema(name = "actionName", description = "节点操作名称")
    private String actionName;

    @Schema(name = "actionAliasName", description = "节点操作别名")
    private String actionAliasName;
}
