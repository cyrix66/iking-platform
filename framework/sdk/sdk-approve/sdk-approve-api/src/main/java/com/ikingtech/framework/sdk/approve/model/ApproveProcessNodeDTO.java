package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.base.model.BaseModel;
import com.ikingtech.framework.sdk.enums.approve.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ApproveProcessNodeDTO", description = "审批流程配置节点信息")
public class ApproveProcessNodeDTO extends BaseModel implements Serializable {

    @Serial
    private static final long serialVersionUID = -156907688322379165L;

    @Schema(name = "formId", description = "所属发起表单编号")
    private String formId;

    @Schema(name = "processId", description = "所属审批流程编号")
    private String processId;

    @Schema(name = "parentId", description = "前一节点编号")
    private String parentId;

    @Schema(name = "name", description = "节点名称")
    private String name;

    @Schema(name = "type", description = "节点类型")
    private ApproveProcessNodeTypeEnum type;

    @Schema(name = "approveType", description = "审批方式")
    private ApproveTypeEnum approveType;

    @Schema(name = "approvalCategory", description = "审批对象类别")
    private ApprovalCategoryEnum approvalCategory;

    @Schema(name = "deptWidgetName", description = "部门选择组件名称")
    private String deptWidgetName;

    @Schema(name = "userWidgetName", description = "用户选择组件名称")
    private String userWidgetName;

    @Schema(name = "approvals", description = "审批对象列表")
    private List<ApproveProcessExecutorDTO> approvals;

    @Schema(name = "initiatorSpecifiedScopeType", description = "发起人自选-选择范围类型")
    private ApproveInitiatorSpecifiedScopeTypeEnum initiatorSpecifiedScopeType;

    @Schema(name = "roleSpecifiedScopeType", description = "指定角色-选择范围类型")
    private ApproveRoleSpecifiedScopeTypeEnum roleSpecifiedScopeType;

    @Schema(name = "singleApproval", description = "发起人自选-选人方式是否单选")
    private Boolean singleApproval;

    @Schema(name = "multiExecutorType", description = "多人审批方式")
    private ApproveMultiExecutorTypeEnum multiExecutorType;

    @Schema(name = "executorEmptyStrategy", description = "审批人为空时的策略")
    private ApproveExecutorEmptyStrategyEnum executorEmptyStrategy;

    @Schema(name = "initiatorSpecifyCarbonCopy", description = "是否由发起人指定抄送人")
    private Boolean initiatorSpecifyCarbonCopy;

    @Schema(name = "reSubmitToBack", description = "再次发起时是否由退回人直接审批")
    private Boolean reSubmitToBack;

    @Schema(name = "backToInitiator", description = "退回时是否直接退回到发起节点")
    private Boolean backToInitiator;

    @Schema(name = "reserveApprovals", description = "审批人为空时指定的审批人列表")
    private List<ApproveProcessExecutorDTO> reserveApprovals;

    @Schema(name = "conditionOrder", description = "条件优先级")
    private Integer conditionOrder;

    @Schema(name = "unconditionalExecute", description = "是否无条件执行")
    private Boolean unconditionalExecute;

    @Schema(name = "conditions", description = "条件节点列表")
    private List<ApproveProcessNodeDTO> conditions;

    @Schema(name = "conditionGroups", description = "工作流条件节点条件组集合")
    private List<ApproveCondGroupDTO> conditionGroups;

    @Schema(name = "parallels", description = "并行分支节点集合")
    private List<ApproveProcessNodeDTO> parallels;

    @Schema(name = "expireRemindEnabled", description = "是否开启节点执行超时提醒")
    private Boolean expireRemindEnabled;

    @Schema(name = "expireRemindEnabled", description = "节点执行超时配置")
    private List<ApproveProcessNodeExpireConfigDTO> expireConfigs;

    @Schema(name = "relatedFormId", description = "关联流程配置编号")
    private String relatedFormId;

    @Schema(name = "actions", description = "节点可执行操作集合")
    private List<ApproveProcessNodeActionDTO> actions;

    @Schema(name = "permissions", description = "审批节点表单权限")
    private List<ApproveProcessViewPermissionDTO> permissions;

    @Schema(name = "autoExecuteComment", description = "节点自动执行(通过/拒绝)时的审批意见")
    private String autoExecuteComment;

    @Schema(name = "children", description = "子节点")
    private ApproveProcessNodeDTO children;
}
