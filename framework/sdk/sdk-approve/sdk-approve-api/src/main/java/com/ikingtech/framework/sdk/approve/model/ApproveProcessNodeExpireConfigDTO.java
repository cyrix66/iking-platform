package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.base.model.BaseModel;
import com.ikingtech.framework.sdk.enums.approve.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalTime;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ApproveProcessNodeExpireConfigDTO", description = "审批流程节点超时配置信息")
public class ApproveProcessNodeExpireConfigDTO extends BaseModel implements Serializable {

    @Serial
    private static final long serialVersionUID = 2330932341079326815L;

    @Schema(name = "formId", description = "所属发起表单编号")
    private String formId;

    @Schema(name = "processId", description = "所属审批流程编号")
    private String processId;

    @Schema(name = "nodeId", description = "所属审批流程配置节点编号")
    private String nodeId;

    @Schema(name = "expireStrategy", description = "节点执行超时策略")
    private ApproveProecessNodeExecuteExpireStrategyEnum expireStrategy;

    @Schema(name = "expireStrategyName", description = "节点执行超时策略名称")
    private String expireStrategyName;

    @Schema(name = "expireType", description = "节点执行限时类型")
    private ApproveProecessNodeExecuteExpireTypeEnum expireType;

    @Schema(name = "expireTypeName", description = "节点执行限时类型名称")
    private String expireTypeName;

    @Schema(name = "delayTime", description = "执行超时时间（秒）")
    private Long delayTime;

    @Schema(name = "expireWidgetName", description = "节点执行截止日期组件名称")
    private String expireWidgetName;

    @Schema(name = "fixedTime", description = "执行超时时间（时分）")
    private LocalTime fixedTime;

    @Schema(name = "expireExecutors", description = "节点执行超时后的执行者")
    private List<ApproveProcessExecutorDTO> expireExecutors;
}
