package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.base.model.BaseModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ApproveProcessParallelInstanceDTO", description = "审批流程并行实例信息")
public class ApproveProcessParallelInstanceDTO extends BaseModel implements Serializable {

    @Serial
    private static final long serialVersionUID = 3065346074721701587L;

    @Schema(name = "formId", description = "所属表单编号")
    private String formId;

    @Schema(name = "processId", description = "所属审批流编号")
    private String processId;

    @Schema(name = "formInstanceId", description = "表单实例编号")
    private String formInstanceId;

    @Schema(name = "processInstanceId", description = "审批流实例编号")
    private String processInstanceId;

    @Schema(name = "nodeId", description = "节点配置编号")
    private String nodeId;

    @Schema(name = "processInstanceNodeId", description = "审批流实例节点编号")
    private String processInstanceNodeId;

    @Schema(name = "processInstanceNodes", description = "节点实例")
    private List<ApproveProcessInstanceNodeDTO> processInstanceNodes;
}
