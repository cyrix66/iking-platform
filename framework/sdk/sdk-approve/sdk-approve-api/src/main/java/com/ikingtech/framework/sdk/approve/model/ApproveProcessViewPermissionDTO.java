package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.enums.approve.ApproveViewPermissionTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveProcessPermissionDTO", description = "审批节点表单权限信息")
public class ApproveProcessViewPermissionDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -6597725738873392941L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "formId", description = "所属表单编号")
    private String formId;

    @Schema(name = "nodeId", description = "所属节点编号")
    private String nodeId;

    @Schema(name = "nodeCode", description = "所属节点标识")
    private String nodeCode;

    @Schema(name = "widgetCode", description = "表单控件标识")
    private String widgetCode;

    @Schema(name = "widgetName", description = "表单控件名称")
    private String widgetName;

    @Schema(name = "permissionType", description = "表单权限类型")
    private ApproveViewPermissionTypeEnum permissionType;

    @Schema(name = "permissionTypeName", description = "表单权限类型名称")
    private String permissionTypeName;
}
