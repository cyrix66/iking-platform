package com.ikingtech.framework.sdk.approve.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveRecordNodeAttachmentDTO", description = "审批附件信息")
public class ApproveRecordNodeAttachmentDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -5807064073176572611L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "formId", description = "所属表单编号")
    private String formId;

    @Schema(name = "processId", description = "所属审批流编号")
    private String processId;

    @Schema(name = "formInstanceId", description = "表单实例编号")
    private String formInstanceId;

    @Schema(name = "processInstanceId", description = "审批流实例编号")
    private String processInstanceId;

    @Schema(name = "recordId", description = "审批记录编号")
    private String recordId;

    @Schema(name = "recordNodeId", description = "审批记录节点编号")
    private String recordNodeId;

    @Schema(name = "attachmentId", description = "附件编号")
    private String attachmentId;

    @Schema(name = "attachmentName", description = "附件名称")
    private String attachmentName;

    @Schema(name = "attachmentSuffix", description = "附件扩展名")
    private String attachmentSuffix;

    @Schema(name = "attachmentSize", description = "附件大小")
    private String attachmentSize;
}
