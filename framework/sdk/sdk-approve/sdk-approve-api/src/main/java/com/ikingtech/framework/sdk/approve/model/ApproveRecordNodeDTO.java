package com.ikingtech.framework.sdk.approve.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.approve.ApproveMultiExecutorTypeEnum;
import com.ikingtech.framework.sdk.enums.approve.ApproveProcessNodeTypeEnum;
import com.ikingtech.framework.sdk.enums.approve.ApproveRecordNodeStatusEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveRecordNodeDTO", description = "审批流程记录节点信息")
public class ApproveRecordNodeDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -1057954524813793827L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "recordId", description = "流程记录编号")
    private String recordId;

    @Schema(name = "nodeId", description = "流程节点编号")
    private String nodeId;

    @Schema(name = "formInstanceId", description = "表单实例编号")
    private String formInstanceId;

    @Schema(name = "processInstanceId", description = "流程实例编号")
    private String processInstanceId;

    @Schema(name = "name", description = "节点名称")
    private String name;

    @Schema(name = "type", description = "审批记录节点类型")
    private ApproveProcessNodeTypeEnum type;

    @Schema(name = "status", description = "审批记录节点状态")
    private ApproveRecordNodeStatusEnum status;

    @Schema(name = "statusName", description = "审批记录节点状态名称")
    private String statusName;

    @Schema(name = "formData", description = "节点表单数据")
    private String formData;

    @Schema(name = "multiExecutorType", description = "多人审批类型")
    private ApproveMultiExecutorTypeEnum multiExecutorType;

    @Schema(name = "multiExecutorTypeName", description = "多人审批类型名称")
    private String multiExecutorTypeName;

    @Schema(name = "executorUser", description = "执行者")
    private List<ApproveRecordNodeUserDTO> executorUsers;

    @Schema(name = "appendExecutorUsers", description = "加签执行者")
    private List<ApproveRecordNodeUserDTO> appendExecutorUsers;

    @Schema(name = "backToRecordNodeId", description = "退回至记录节点编号")
    private String backToRecordNodeId;

    @Schema(name = "backToRecordNodeName", description = "退回至记录节点名称")
    private String backToRecordNodeName;

    @Schema(name = "approveComment", description = "审批意见/评论")
    private String approveComment;

    @Schema(name = "attachment", description = "附件地址")
    private List<ApproveRecordNodeAttachmentDTO> attachments;

    @Schema(name = "attachment", description = "附件地址")
    private List<String> images;

    @Schema(name = "sortOrder", description = "排序值")
    private Integer sortOrder;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;
}
