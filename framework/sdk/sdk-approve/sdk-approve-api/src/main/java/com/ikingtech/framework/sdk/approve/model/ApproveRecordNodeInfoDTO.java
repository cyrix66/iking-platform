package com.ikingtech.framework.sdk.approve.model;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class ApproveRecordNodeInfoDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -6872067223292723157L;

    private String businessDataId;

    private ApproveRecordNodeDTO recordNode;

    private ApproveProcessNodeDTO processNode;
}
