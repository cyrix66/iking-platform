package com.ikingtech.framework.sdk.approve.model.rpc;

import com.ikingtech.framework.sdk.enums.approve.ApproveFormTypeEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class ApproveFormBeanDefinition implements Serializable {

    @Serial
    private static final long serialVersionUID = 7918458207189405504L;

    /**
     * 表单类型
     */
    private ApproveFormTypeEnum type;

    /**
     * 业务类型
     */
    private String code;

    /**
     * 业务类型
     */
    private String businessType;

    /**
     * 表单名称
     */
    private String name;

    /**
     * 是否允许配置发起人
     */
    private Boolean customizeInitiator;

    /**
     * 表单字段定义
     */
    private List<ApproveFormWidgetBeanDefinition> widgets;

    /**
     * 在审批中心该表单是否不可见
     */
    private Boolean visible;

    /**
     * 表单json数据
     */
    private String formJson;
}
