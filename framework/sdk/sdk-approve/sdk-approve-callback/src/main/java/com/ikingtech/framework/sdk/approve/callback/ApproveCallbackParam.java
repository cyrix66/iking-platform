package com.ikingtech.framework.sdk.approve.callback;

import com.ikingtech.framework.sdk.enums.approve.ApproveHandleTypeEnum;
import com.ikingtech.framework.sdk.enums.approve.ApproveProcessInstanceStatusEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Administrator
 */
@Data
public class ApproveCallbackParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 523944435120176328L;

    private ApproveHandleTypeEnum handleType;

    private String formInstanceId;

    private String businessType;

    private String businessDataId;

    private String currentUserId;

    private String currentUserName;

    private List<String> nextUserIds;

    private LocalDateTime approveTime;

    private String initiatorId;

    private ApproveProcessInstanceStatusEnum processStatus;

    private String formData;
}
