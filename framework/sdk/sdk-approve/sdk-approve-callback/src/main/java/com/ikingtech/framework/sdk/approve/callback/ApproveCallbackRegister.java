package com.ikingtech.framework.sdk.approve.callback;

import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

import static com.ikingtech.framework.sdk.cache.constants.CacheConstants.APPROVE_PROCESS_CALLBACK;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class ApproveCallbackRegister implements ApplicationRunner {

    private final StringRedisTemplate redisTemplate;

    private final List<ApproveProcessCallback> callbacks;

    @Value("${spring.application.name}")
    private String serverName;

    @Override
    public void run(ApplicationArguments args) {
        this.redisTemplate.opsForSet().remove(APPROVE_PROCESS_CALLBACK, this.serverName);
        if (Tools.Coll.isNotBlank(this.callbacks)) {
            this.redisTemplate.opsForSet().add(APPROVE_PROCESS_CALLBACK, this.serverName);
        }
    }
}
