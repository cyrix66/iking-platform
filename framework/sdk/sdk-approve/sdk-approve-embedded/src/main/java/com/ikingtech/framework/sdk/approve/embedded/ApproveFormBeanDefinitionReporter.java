package com.ikingtech.framework.sdk.approve.embedded;

import com.ikingtech.framework.sdk.approve.model.rpc.ApproveFormBeanDefinitionRegisterParam;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentProxy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * @author tie yan
 */
@Slf4j
public class ApproveFormBeanDefinitionReporter {

    private boolean stop = false;

    @Async
    public void run() {
        while (!stop) {
            if (Tools.Coll.isNotBlankMap(ApproveFormBeanDefinitionFactory.APPROVE_FORM_BEAN_DEFINITION_MAP)) {
                ApproveFormBeanDefinitionRegisterParam reportParam = new ApproveFormBeanDefinitionRegisterParam();
                reportParam.setBeanDefinitions(new ArrayList<>(ApproveFormBeanDefinitionFactory.APPROVE_FORM_BEAN_DEFINITION_MAP.values()));
                R<Object> result = FrameworkAgentProxy.agent().execute(FrameworkAgentTypeEnum.APPROVE_FORM_REGISTER, reportParam);
                if (result.isSuccess()) {
                    this.stop = true;
                }
            }
            try {
                TimeUnit.MINUTES.sleep(5);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
