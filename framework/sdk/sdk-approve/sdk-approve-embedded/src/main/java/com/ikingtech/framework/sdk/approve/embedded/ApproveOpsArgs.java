package com.ikingtech.framework.sdk.approve.embedded;

import lombok.Getter;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Getter
public class ApproveOpsArgs implements Serializable {

    @Serial
    private static final long serialVersionUID = -478128399278451214L;

    public ApproveOpsArgs(Builder builder) {
        this.formInstanceId = builder.formInstanceId;
        this.formData = builder.formData;
        this.approveComment = builder.approveComment;
        this.attachments = builder.attachments;
        this.images = builder.images;
    }

    private final String formInstanceId;

    private final String formData;

    private final String approveComment;

    private final List<ApproveOpsAttachmentArgs> attachments;

    private final List<String> images;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder implements Serializable {

        @Serial
    private static final long serialVersionUID = -5581146342113324494L;

        private String formInstanceId;

        private String formData;

        private String approveComment;

        private List<ApproveOpsAttachmentArgs> attachments;

        private List<String> images;

        public Builder formInstanceId(String formInstanceId) {
            this.formInstanceId = formInstanceId;
            return this;
        }

        public Builder formData(String formData) {
            this.formData = formData;
            return this;
        }

        public Builder approveComment(String approveComment) {
            this.approveComment = approveComment;
            return this;
        }

        public Builder attachments(List<ApproveOpsAttachmentArgs> attachments) {
            this.attachments = attachments;
            return this;
        }

        public Builder images(List<String> images) {
            this.images = images;
            return this;
        }

        public ApproveOpsArgs build() {
            return new ApproveOpsArgs(this);
        }
    }
}
