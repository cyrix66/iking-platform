package com.ikingtech.framework.sdk.approve.embedded;

import lombok.Getter;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Getter
public class ApproveOpsAttachmentArgs extends ApproveOpsArgs implements Serializable {

    @Serial
    private static final long serialVersionUID = 5183418006771486856L;

    public ApproveOpsAttachmentArgs(Builder builder) {
        super(builder);
        this.attachmentId = builder.attachmentId;
        this.attachmentName = builder.attachmentName;
        this.attachmentSuffix = builder.attachmentSuffix;
        this.attachmentSize = builder.attachmentSize;
    }

    private final String attachmentId;

    private final String attachmentName;

    private final String attachmentSuffix;

    private final String attachmentSize;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends ApproveOpsArgs.Builder {

        @Serial
        private static final long serialVersionUID = -6221127928128707091L;

        private String attachmentId;

        private String attachmentName;

        private String attachmentSuffix;

        private String attachmentSize;

        public Builder attachmentId(String attachmentId) {
            this.attachmentId = attachmentId;
            return this;
        }

        public Builder attachmentName(String attachmentName) {
            this.attachmentName = attachmentName;
            return this;
        }

        public Builder attachmentSuffix(String attachmentSuffix) {
            this.attachmentSuffix = attachmentSuffix;
            return this;
        }

        public Builder attachmentSize(String attachmentSize) {
            this.attachmentSize = attachmentSize;
            return this;
        }

        @Override
        public ApproveOpsAttachmentArgs build() {
            return new ApproveOpsAttachmentArgs(this);
        }
    }
}
