package com.ikingtech.framework.sdk.approve.embedded.annotation;

import com.ikingtech.framework.sdk.enums.approve.ApproveFormWidgetTypeEnum;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * @author tie yan
 */
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface Widget {

    String label();

    String type();

    boolean required() default false;
}
