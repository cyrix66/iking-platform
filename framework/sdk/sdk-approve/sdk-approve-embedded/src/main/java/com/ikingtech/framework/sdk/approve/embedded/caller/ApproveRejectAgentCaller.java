package com.ikingtech.framework.sdk.approve.embedded.caller;

import com.ikingtech.framework.sdk.approve.model.ApproveFormInstanceBasicDTO;
import com.ikingtech.framework.sdk.approve.model.ApproveFormOperationDTO;
import com.ikingtech.framework.sdk.approve.rpc.api.ApproveFormInstanceRpcApi;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentCaller;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class ApproveRejectAgentCaller implements FrameworkAgentCaller {

    private final ApproveFormInstanceRpcApi rpcApi;

    @Override
    public R<Object> call(Object data) {
        try {
            R<ApproveFormInstanceBasicDTO> result = this.rpcApi.reject((ApproveFormOperationDTO) data);
            return result.isSuccess() ? R.ok(result.getData()) : R.failed(result.getMsg());
        } catch (Exception e) {
            throw new FrameworkException(e.getMessage());
        }
    }

    @Override
    public FrameworkAgentTypeEnum type() {
        return FrameworkAgentTypeEnum.APPROVE_REJECT;
    }
}
