package com.ikingtech.framework.sdk.approve.embedded.configuration;

import com.ikingtech.framework.sdk.approve.api.ApproveFormApi;
import com.ikingtech.framework.sdk.approve.api.ApproveFormInstanceApi;
import com.ikingtech.framework.sdk.approve.embedded.ApproveFormBeanDefinitionFactory;
import com.ikingtech.framework.sdk.approve.embedded.ApproveFormBeanDefinitionReporter;
import com.ikingtech.framework.sdk.approve.embedded.caller.*;
import com.ikingtech.framework.sdk.approve.embedded.runner.*;
import com.ikingtech.framework.sdk.approve.rpc.api.ApproveFormInstanceRpcApi;
import com.ikingtech.framework.sdk.approve.rpc.api.ApproveFormRpcApi;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentCaller;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
public class ApproveEmbeddedConfiguration {

    @Bean
    public ApproveFormBeanDefinitionReporter approveFormBeanDefinitionReporter() {
        return new ApproveFormBeanDefinitionReporter();
    }

    @Bean
    public ApproveFormBeanDefinitionFactory approveFormBeanDefinitionFactory(ApproveFormBeanDefinitionReporter reporter) {
        return new ApproveFormBeanDefinitionFactory(reporter);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    @ConditionalOnExpression("!'${spring.application.name}'.equals('server')")
    public FrameworkAgentCaller approveFormRegisterAgentCaller(ApproveFormRpcApi rpcApi) {
        return new ApproveFormRegisterCaller(rpcApi);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    @ConditionalOnExpression("!'${spring.application.name}'.equals('server')")
    public FrameworkAgentCaller approveFormUnregisterAgentCaller(ApproveFormRpcApi rpcApi) {
        return new ApproveFormUnregisterCaller(rpcApi);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    @ConditionalOnExpression("!'${spring.application.name}'.equals('server')")
    public FrameworkAgentCaller approveSubmitAgentCaller(ApproveFormInstanceRpcApi rpcApi) {
        return new ApproveSubmitAgentCaller(rpcApi);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    @ConditionalOnExpression("!'${spring.application.name}'.equals('server')")
    public FrameworkAgentCaller approvePassAgentCaller(ApproveFormInstanceRpcApi rpcApi) {
        return new ApprovePassAgentCaller(rpcApi);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    @ConditionalOnExpression("!'${spring.application.name}'.equals('server')")
    public FrameworkAgentCaller approveRejectAgentCaller(ApproveFormInstanceRpcApi rpcApi) {
        return new ApproveRejectAgentCaller(rpcApi);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    @ConditionalOnExpression("!'${spring.application.name}'.equals('server')")
    public FrameworkAgentCaller approveBackAgentCaller(ApproveFormInstanceRpcApi rpcApi) {
        return new ApproveBackAgentCaller(rpcApi);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    @ConditionalOnExpression("!'${spring.application.name}'.equals('server')")
    public FrameworkAgentCaller approveReSubmitAgentCaller(ApproveFormInstanceRpcApi rpcApi) {
        return new ApproveReSubmitAgentCaller(rpcApi);
    }

    @Bean
    @ConditionalOnExpression("null == '${iking.framework.web.arch}' || '${iking.framework.web.arch}'.equals('') || '${iking.framework.web.arch}'.equals('single') || '${spring.application.name}'.equals('server')")
    public FrameworkAgentRunner approveFormRegisterAgentRunner(ApproveFormApi api) {
        return new ApproveFormRegisterRunner(api);
    }

    @Bean
    @ConditionalOnExpression("null == '${iking.framework.web.arch}' || '${iking.framework.web.arch}'.equals('') || '${iking.framework.web.arch}'.equals('single') || '${spring.application.name}'.equals('server')")
    public FrameworkAgentRunner approveFormUnregisterAgentRunner(ApproveFormApi api) {
        return new ApproveFormUnregisterRunner(api);
    }

    @Bean
    @ConditionalOnExpression("null == '${iking.framework.web.arch}' || '${iking.framework.web.arch}'.equals('') || '${iking.framework.web.arch}'.equals('single') || '${spring.application.name}'.equals('server')")
    public FrameworkAgentRunner approveSubmitAgentRunner(ApproveFormInstanceApi api) {
        return new ApproveSubmitAgentRunner(api);
    }

    @Bean
    @ConditionalOnExpression("null == '${iking.framework.web.arch}' || '${iking.framework.web.arch}'.equals('') || '${iking.framework.web.arch}'.equals('single') || '${spring.application.name}'.equals('server')")
    public FrameworkAgentRunner approvePassAgentRunner(ApproveFormInstanceApi api) {
        return new ApprovePassAgentRunner(api);
    }

    @Bean
    @ConditionalOnExpression("null == '${iking.framework.web.arch}' || '${iking.framework.web.arch}'.equals('') || '${iking.framework.web.arch}'.equals('single') || '${spring.application.name}'.equals('server')")
    public FrameworkAgentRunner approveRejectAgentRunner(ApproveFormInstanceApi api) {
        return new ApproveRejectAgentRunner(api);
    }

    @Bean
    @ConditionalOnExpression("null == '${iking.framework.web.arch}' || '${iking.framework.web.arch}'.equals('') || '${iking.framework.web.arch}'.equals('single') || '${spring.application.name}'.equals('server')")
    public FrameworkAgentRunner approveBackAgentRunner(ApproveFormInstanceApi api) {
        return new ApproveBackAgentRunner(api);
    }

    @Bean
    @ConditionalOnExpression("null == '${iking.framework.web.arch}' || '${iking.framework.web.arch}'.equals('') || '${iking.framework.web.arch}'.equals('single') || '${spring.application.name}'.equals('server')")
    public FrameworkAgentRunner approveReSubmitAgentRunner(ApproveFormInstanceApi api) {
        return new ApproveReSubmitAgentRunner(api);
    }

    @Bean
    @ConditionalOnExpression("null == '${iking.framework.web.arch}' || '${iking.framework.web.arch}'.equals('') || '${iking.framework.web.arch}'.equals('single') || '${spring.application.name}'.equals('server')")
    public FrameworkAgentRunner approvePreviewAgentRunner(ApproveFormInstanceApi api) {
        return new ApprovePreviewAgentRunner(api);
    }
}
