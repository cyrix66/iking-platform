package com.ikingtech.framework.sdk.approve.embedded.runner;

import com.ikingtech.framework.sdk.approve.api.ApproveFormInstanceApi;
import com.ikingtech.framework.sdk.approve.model.ApproveFormInstanceBasicDTO;
import com.ikingtech.framework.sdk.approve.model.ApproveFormOperationDTO;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentRunner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class ApproveBackAgentRunner implements FrameworkAgentRunner {

    private final ApproveFormInstanceApi api;

    @Override
    public R<Object> run(Object data) {
        try {
            R<ApproveFormInstanceBasicDTO> result = this.api.back((ApproveFormOperationDTO) data);
            return result.isSuccess() ? R.ok(result.getData()) : R.failed(result.getMsg());
        } catch (Exception e) {
            throw new FrameworkException(e.getMessage());
        }
    }

    @Override
    public FrameworkAgentTypeEnum type() {
        return FrameworkAgentTypeEnum.APPROVE_BACK;
    }
}
