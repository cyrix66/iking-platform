package com.ikingtech.framework.sdk.approve.embedded.runner;

import com.ikingtech.framework.sdk.approve.api.ApproveFormInstanceApi;
import com.ikingtech.framework.sdk.approve.model.ApproveFormInstanceBasicDTO;
import com.ikingtech.framework.sdk.approve.model.ApproveFormOperationDTO;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentRunner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class ApprovePassAgentRunner implements FrameworkAgentRunner {

    private final ApproveFormInstanceApi api;

    /**
     * 执行客户端请求
     *
     * @param data 请求数据
     * @return 执行结果
     */
    @Override
    public R<Object> run(Object data) {
        try {
            R<ApproveFormInstanceBasicDTO> result = this.api.pass((ApproveFormOperationDTO) data);
            return result.isSuccess() ? R.ok(result.getData()) : R.failed(result.getMsg());
        } catch (Exception e) {
            throw new FrameworkException(e.getMessage());
        }
    }

    /**
     * 获取客户端类型
     *
     * @return 客户端类型
     */
    @Override
    public FrameworkAgentTypeEnum type() {
        return FrameworkAgentTypeEnum.APPROVE_PASS;
    }
}
