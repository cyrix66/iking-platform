package com.ikingtech.framework.sdk.approve.rpc.api;

import com.ikingtech.framework.sdk.approve.api.ApproveFormInstanceApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "ApproveFormInstanceRpcApi", path = "/approve/form/instance")
public interface ApproveFormInstanceRpcApi extends ApproveFormInstanceApi {
}
