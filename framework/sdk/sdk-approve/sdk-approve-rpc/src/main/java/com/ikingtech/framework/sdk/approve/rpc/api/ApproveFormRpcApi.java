package com.ikingtech.framework.sdk.approve.rpc.api;

import com.ikingtech.framework.sdk.approve.api.ApproveFormApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "ApproveFormRpcApi", path = "/approve/form")
public interface ApproveFormRpcApi extends ApproveFormApi {
}
