package com.ikingtech.framework.sdk.attachment.api;

import com.ikingtech.framework.sdk.attachment.model.AttachmentDTO;
import com.ikingtech.framework.sdk.attachment.model.AttachmentQueryParamDTO;
import com.ikingtech.framework.sdk.base.model.BatchParam;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author tie yan
 */
public interface AttachmentApi {

    /**
     * 保存附件
     *
     * @param attachments 附件信息
     * @return 附件信息
     */
    @PostRequest(order = 1, value = "/save", summary = "保存附件", description = "保存附件")
    R<Object> save(@Parameter(name = "attachment", description = "新增参数")
                   @RequestBody AttachmentDTO attachments);

    /**
     * 批量删除附件
     *
     * @param businessIds 附件主键编号
     * @return 删除结果
     */
    @PostRequest(order = 2, value = "/delete/batch", summary = "批量删除附件", description = "批量删除附件")
    R<Object> deleteBatch(@Parameter(name = "ids", description = "删除参数")
                          @RequestBody BatchParam<String> businessIds);

    /**
     * 获取附件详情
     *
     * @param id 附件主键编号
     * @return 附件信息
     */
    @PostRequest(order = 3, value = "/detail", summary = "获取附件详情", description = "获取附件详情")
    R<AttachmentDTO> detail(@Parameter(name = "id", description = "主键编号")
                            @RequestBody String id);

    /**
     * 获取附件列表
     *
     * @param queryParam 查询参数
     * @return 附件列表
     */
    @PostRequest(order = 4, value = "/list/page", summary = "获取附件列表", description = "获取附件列表")
    R<List<AttachmentDTO>> page(@Parameter(name = "queryParam", description = "查询参数")
                                @RequestBody AttachmentQueryParamDTO queryParam);

    /**
     * 获取所有附件列表
     *
     * @return 附件列表
     */
    @PostRequest(order = 5, value = "/list/all", summary = "获取所有附件列表", description = "获取所有附件列表")
    R<List<AttachmentDTO>> all();

    /**
     * 根据业务ID列表获取附件列表
     *
     * @param businessIds 业务ID列表
     * @return 附件列表
     */
    @PostRequest(order = 7, value = "/list/business-ids", summary = "根据业务ID列表获取附件列表", description = "根据业务ID列表获取附件列表")
    R<List<AttachmentDTO>> listByBusinessIds(@Parameter(name = "businessIds", description = "业务ID列表")
                                             @RequestBody BatchParam<String> businessIds);
}
