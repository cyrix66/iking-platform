package com.ikingtech.framework.sdk.attachment.embedded;

import com.ikingtech.framework.sdk.attachment.model.AttachmentDTO;
import com.ikingtech.framework.sdk.attachment.model.AttachmentFileDTO;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentProxy;

import java.util.List;

/**
 * @author tie yan
 */
public class AttachmentOps {

    public static void save(String businessId, String ossFileId) {
        save(businessId, Tools.Coll.newList(ossFileId));
    }

    public static void save(String businessId, List<String> ossFileIds) {
        AttachmentDTO attachment = new AttachmentDTO();
        attachment.setBusinessId(businessId);
        attachment.setFiles(Tools.Coll.convertList(ossFileIds, ossFileId -> {
            AttachmentFileDTO attachmentFile = new AttachmentFileDTO();
            attachmentFile.setOssFileId(ossFileId);
            return attachmentFile;
        }));
        FrameworkAgentProxy.agent().execute(FrameworkAgentTypeEnum.ATTACHMENT_SAVE, attachment);
    }

    public static void remove(String businessId) {
        remove(Tools.Coll.newList(businessId));
    }

    public static void remove(List<String> businessIds) {
        FrameworkAgentProxy.agent().execute(FrameworkAgentTypeEnum.ATTACHMENT_REMOVE, businessIds);
    }
}
