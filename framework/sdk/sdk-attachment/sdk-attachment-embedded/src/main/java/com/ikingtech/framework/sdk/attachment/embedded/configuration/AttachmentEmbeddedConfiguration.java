package com.ikingtech.framework.sdk.attachment.embedded.configuration;

import com.ikingtech.framework.sdk.attachment.api.AttachmentApi;
import com.ikingtech.framework.sdk.attachment.embedded.caller.AttachmentRemoveCaller;
import com.ikingtech.framework.sdk.attachment.embedded.caller.AttachmentSaveCaller;
import com.ikingtech.framework.sdk.attachment.embedded.runner.AttachmentRemoveRunner;
import com.ikingtech.framework.sdk.attachment.embedded.runner.AttachmentSaveRunner;
import com.ikingtech.framework.sdk.attachment.rpc.api.AttachmentRpcApi;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentCaller;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
public class AttachmentEmbeddedConfiguration {

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    @ConditionalOnExpression("!'${spring.application.name}'.equals('server')")
    public FrameworkAgentCaller attachmentSaveAgentCaller(AttachmentRpcApi rpcApi) {
        return new AttachmentSaveCaller(rpcApi);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    @ConditionalOnExpression("!'${spring.application.name}'.equals('server')")
    public FrameworkAgentCaller attachmentRemoveAgentCaller(AttachmentRpcApi rpcApi) {
        return new AttachmentRemoveCaller(rpcApi);
    }

    @Bean
    @ConditionalOnExpression("null == '${iking.framework.web.arch}' || '${iking.framework.web.arch}'.equals('') || '${iking.framework.web.arch}'.equals('single') || '${spring.application.name}'.equals('server')")
    public FrameworkAgentRunner attachmentSaveAgentRunner(AttachmentApi api) {
        return new AttachmentSaveRunner(api);
    }

    @Bean
    @ConditionalOnExpression("null == '${iking.framework.web.arch}' || '${iking.framework.web.arch}'.equals('') || '${iking.framework.web.arch}'.equals('single') || '${spring.application.name}'.equals('server')")
    public FrameworkAgentRunner attachmentRemoveAgentRunner(AttachmentApi api) {
        return new AttachmentRemoveRunner(api);
    }
}
