package com.ikingtech.framework.sdk.attachment.embedded.runner;

import com.ikingtech.framework.sdk.attachment.api.AttachmentApi;
import com.ikingtech.framework.sdk.attachment.rpc.api.AttachmentRpcApi;
import com.ikingtech.framework.sdk.base.model.BatchParam;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentCaller;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentRunner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class AttachmentRemoveRunner implements FrameworkAgentRunner {

    private final AttachmentApi api;

    @Override
    public R<Object> run(Object data) {
        try {
            return this.api.deleteBatch((BatchParam.build(Tools.Obj.list(data))));
        } catch (Exception e) {
            throw new FrameworkException(e.getMessage());
        }
    }

    @Override
    public FrameworkAgentTypeEnum type() {
        return FrameworkAgentTypeEnum.ATTACHMENT_REMOVE;
    }
}
