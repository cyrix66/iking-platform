package com.ikingtech.framework.sdk.attachment.embedded.runner;

import com.ikingtech.framework.sdk.attachment.api.AttachmentApi;
import com.ikingtech.framework.sdk.attachment.model.AttachmentDTO;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentRunner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class AttachmentSaveRunner implements FrameworkAgentRunner {

    private final AttachmentApi api;

    @Override
    public R<Object> run(Object data) {
        try {
            return this.api.save((AttachmentDTO) data);
        } catch (Exception e) {
            throw new FrameworkException(e.getMessage());
        }
    }

    @Override
    public FrameworkAgentTypeEnum type() {
        return FrameworkAgentTypeEnum.ATTACHMENT_SAVE;
    }
}
