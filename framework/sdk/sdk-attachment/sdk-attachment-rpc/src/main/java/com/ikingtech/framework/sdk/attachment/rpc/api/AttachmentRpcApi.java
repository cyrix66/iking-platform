package com.ikingtech.framework.sdk.attachment.rpc.api;

import com.ikingtech.framework.sdk.attachment.api.AttachmentApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "AttachmentRpcApi", path = "/attachment")
public interface AttachmentRpcApi extends AttachmentApi {
}
