package com.ikingtech.framework.sdk.authenticate.embedded.configuration;

import com.ikingtech.framework.sdk.authenticate.embedded.authenticate.*;
import com.ikingtech.framework.sdk.authenticate.embedded.core.service.DefaultLocalUserIdentityLoaderImpl;
import com.ikingtech.framework.sdk.authenticate.embedded.core.service.IdentityExtensionLoaderImpl;
import com.ikingtech.framework.sdk.authenticate.embedded.core.service.LoadBalancerUserIdentityLoaderImpl;
import com.ikingtech.framework.sdk.authenticate.embedded.core.service.LocalUserIdentityLoaderImpl;
import com.ikingtech.framework.sdk.authenticate.embedded.core.UserIdentityLoader;
import com.ikingtech.framework.sdk.authenticate.embedded.propertires.AuthenticateProperties;
import com.ikingtech.framework.sdk.authenticate.extension.IdentityExtensionLoader;
import com.ikingtech.framework.sdk.user.api.UserApi;
import com.ikingtech.framework.sdk.user.rpc.api.UserRpcApi;
import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@Configuration
@EnableConfigurationProperties({AuthenticateProperties.class})
public class AuthenticateEmbeddedConfiguration {

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    @ConditionalOnClass(name = "com.ikingtech.framework.sdk.user.rpc.api.UserRpcApi")
    public UserIdentityLoader loadbalancerUserIdentityLoader (UserRpcApi userRpcApi) {
        return new LoadBalancerUserIdentityLoaderImpl(userRpcApi);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "single")
    @ConditionalOnClass(name = "com.ikingtech.platform.service.system.user.controller.UserController")
    public UserIdentityLoader localUserIdentityLoader (UserApi userApi) {
        return new LocalUserIdentityLoaderImpl(userApi);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "single")
    @ConditionalOnMissingBean(UserIdentityLoader.class)
    public UserIdentityLoader defaultLocalUserIdentityLoader () {
        return new DefaultLocalUserIdentityLoaderImpl();
    }

    @Bean
    public DelegateAuthenticate delegateAuthenticate(List<Authenticate> authenticates, StringRedisTemplate redisTemplate) {
        return new DelegateAuthenticate(authenticates, redisTemplate);
    }

    @Bean
    public Authenticate passwordAuthenticate(StringRedisTemplate redisTemplate, List<IdentityExtensionLoader> loaders, UserIdentityLoader userIdentityLoader) {
        return new PasswordAuthenticate(redisTemplate, loaders, userIdentityLoader);
    }

    @Bean
    public Authenticate tokenAuthenticate(StringRedisTemplate redisTemplate,
                                          List<IdentityExtensionLoader> loaders,
                                          UserIdentityLoader userIdentityLoader,
                                          AuthenticateProperties properties) {
        return new TokenAuthenticate(redisTemplate, loaders, userIdentityLoader, properties);
    }

    @Bean
    public Authenticate socialAuthenticate(StringRedisTemplate redisTemplate, List<IdentityExtensionLoader> loaders, UserIdentityLoader userIdentityLoader) {
        return new SocialAuthenticate(redisTemplate, loaders, userIdentityLoader);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    public IdentityExtensionLoader identityExtensionLoader(StringRedisTemplate redisTemplate, LoadBalancerClient loadBalancerClient) {
        return new IdentityExtensionLoaderImpl(redisTemplate, loadBalancerClient);
    }
}
