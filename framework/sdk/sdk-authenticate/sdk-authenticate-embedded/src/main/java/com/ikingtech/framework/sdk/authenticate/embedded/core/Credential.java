package com.ikingtech.framework.sdk.authenticate.embedded.core;

import com.ikingtech.framework.sdk.enums.authenticate.SignEndpointTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 身份凭证信息
 *
 * @author tie yan
 */
@Data
@Schema(name = "Credential", description = "身份凭证信息")
public class Credential implements Serializable {

	@Serial
    private static final long serialVersionUID = 7039538291388391511L;

	@Schema(name = "credentialName", description = "凭证名称，支持用户名、手机号、邮箱、身份证号")
	private String credentialName;

	@Schema(name = "password", description = "密码")
	private String password;

	@Schema(name = "token", description = "身份令牌")
	private String token;

	@Schema(name = "authorizeCode", description = "OAuth2.0 授权码")
	private String authorizeCode;

	@Schema(name = "endpointType", description = "登录端点类型")
	private SignEndpointTypeEnum endpointType;

	@Schema(name = "socialId", description = "社交平台标识")
	private String socialId;

	@Schema(name = "socialNo", description = "社交号")
	private String socialNo;

	@Schema(name = "phone", description = "手机号")
	private String phone;

	@Schema(name = "smsVerifyCode", description = "短信验证码")
	private String smsVerifyCode;
}
