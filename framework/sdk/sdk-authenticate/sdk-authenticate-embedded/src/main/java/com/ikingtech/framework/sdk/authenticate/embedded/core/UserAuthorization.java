package com.ikingtech.framework.sdk.authenticate.embedded.core;

/**
 * @author tie yan
 */
public interface UserAuthorization {

    void authorize();
}
