package com.ikingtech.framework.sdk.authenticate.embedded.core.service;

import com.ikingtech.framework.sdk.authenticate.embedded.core.UserIdentityLoader;
import com.ikingtech.framework.sdk.context.security.Identity;

/**
 * @author tie yan
 */
public class DefaultLocalUserIdentityLoaderImpl implements UserIdentityLoader {

    @Override
    public Identity loadByCredential(String credentialName) {
        return null;
    }

    @Override
    public Identity loadBySocial(String socialId, String socialNo) {
        return null;
    }
}
