package com.ikingtech.framework.sdk.authenticate.embedded.core.service;

import com.ikingtech.framework.sdk.authenticate.embedded.core.UserIdentityLoader;
import com.ikingtech.framework.sdk.context.security.Identity;
import com.ikingtech.framework.sdk.user.api.UserApi;
import com.ikingtech.framework.sdk.user.model.UserSocialQueryParamDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class LocalUserIdentityLoaderImpl implements UserIdentityLoader {

    private final UserApi userApi;

    @Override
    public Identity loadByCredential(String credentialName) {
        return Tools.Bean.copy(this.userApi.getInfoByCredential(credentialName).getData(), Identity.class);
    }

    @Override
    public Identity loadBySocial(String socialId, String socialNo) {
        UserSocialQueryParamDTO param = new UserSocialQueryParamDTO();
        param.setSocialId(socialId);
        param.setSocialNo(socialNo);
        return  Tools.Bean.copy(this.userApi.getInfoBySocial(param).getData(), Identity.class);
    }
}
