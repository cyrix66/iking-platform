package com.ikingtech.framework.sdk.authenticate.embedded.propertires;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.Serial;
import java.io.Serializable;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@Data
@ConfigurationProperties(GLOBAL_CONFIG_PREFIX + ".auth")
public class AuthenticateProperties implements Serializable {

    @Serial
    private static final long serialVersionUID = 6277126123444283500L;

    private OAuth2Sso oauth2Sso;

    @Data
    public static class OAuth2Sso implements Serializable{

        @Serial
        private static final long serialVersionUID = -6556550454069106977L;

        private Boolean enabled;

        private String redirectUri;

        private String tokenEndpoint;

        private String userInfoEndpoint;

        private String usernameField;
    }
}
