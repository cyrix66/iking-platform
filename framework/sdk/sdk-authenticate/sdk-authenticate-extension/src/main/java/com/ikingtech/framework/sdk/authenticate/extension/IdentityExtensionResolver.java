package com.ikingtech.framework.sdk.authenticate.extension;

import com.ikingtech.framework.sdk.context.security.Identity;
import com.ikingtech.framework.sdk.core.response.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
@Slf4j
@RestController
@RequiredArgsConstructor
public class IdentityExtensionResolver {

    private final List<IdentityExtensionLoader> loaders;

    @PostMapping("/identity/extension/resolve")
    public R<Map<String, Object>> resolve(@RequestBody Identity identity) {
        if (null == identity) {
            return R.failed("identity extension resolver param is blank");
        }
        Map<String, Object> result = new HashMap<>();
        this.loaders.forEach(loader -> result.putAll(loader.load(identity)));
        return R.ok(result);
    }
}
