package com.ikingtech.framework.sdk.authenticate.extension.configuration;

import com.ikingtech.framework.sdk.authenticate.extension.IdentityExtensionLoader;
import com.ikingtech.framework.sdk.authenticate.extension.IdentityExtensionReporterRegister;
import com.ikingtech.framework.sdk.authenticate.extension.IdentityExtensionResolver;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
public class AuthenticateExtensionConfiguration {

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".auth", name = "enable-extension", havingValue = "true")
    public IdentityExtensionReporterRegister identityExtensionCollector(StringRedisTemplate redisTemplate, List<IdentityExtensionLoader> loaders) {
        return new IdentityExtensionReporterRegister(redisTemplate, loaders);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    public IdentityExtensionResolver identityExtensionResolver(List<IdentityExtensionLoader> loaders) {
        return new IdentityExtensionResolver(loaders);
    }
}
