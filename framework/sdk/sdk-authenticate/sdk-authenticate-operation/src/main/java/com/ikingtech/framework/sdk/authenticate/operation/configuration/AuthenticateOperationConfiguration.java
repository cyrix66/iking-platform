package com.ikingtech.framework.sdk.authenticate.operation.configuration;

import com.ikingtech.framework.sdk.authenticate.operation.IdentityOps;
import com.ikingtech.framework.sdk.authenticate.operation.IdentityValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * @author tie yan
 */
public class AuthenticateOperationConfiguration {

    @Bean
    public IdentityOps identityOps(StringRedisTemplate redisTemplate) {
        return new IdentityOps(redisTemplate);
    }

    @Bean
    public IdentityValidator identityValidator(StringRedisTemplate redisTemplate) {
        return new IdentityValidator(redisTemplate);
    }
}
