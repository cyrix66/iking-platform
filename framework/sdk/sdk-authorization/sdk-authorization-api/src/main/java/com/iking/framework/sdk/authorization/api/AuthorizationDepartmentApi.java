package com.iking.framework.sdk.authorization.api;

import com.iking.framework.sdk.authorization.model.AuthorizationUserDepartment;
import com.ikingtech.framework.sdk.enums.system.role.DataScopeTypeEnum;

import java.util.List;

/**
 * @author tie yan
 */
public interface AuthorizationDepartmentApi {

    List<String> loadFullPathAll();

    List<String> loadFullPath(List<String> ids);

    List<String> loadFullPath(List<AuthorizationUserDepartment> departments,
                              DataScopeTypeEnum dataScopeType);

    List<String> loadSubFullPath(List<AuthorizationUserDepartment> departments,
                                 DataScopeTypeEnum dataScopeType);
}
