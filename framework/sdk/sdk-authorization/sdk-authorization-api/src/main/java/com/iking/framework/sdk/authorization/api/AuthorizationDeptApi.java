package com.iking.framework.sdk.authorization.api;

import java.util.List;

/**
 * @author tie yan
 */
public interface AuthorizationDeptApi {

    List<String> loadFullPathAll();

    List<String> loadFullPathByDeptIds(List<String> deptIds);

    List<String> loadSubFullPathByDeptIds(List<String> deptIds);
}
