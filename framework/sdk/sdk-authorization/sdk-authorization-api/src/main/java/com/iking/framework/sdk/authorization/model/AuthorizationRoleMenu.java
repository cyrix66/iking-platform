package com.iking.framework.sdk.authorization.model;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class AuthorizationRoleMenu implements Serializable {

    @Serial
    private static final long serialVersionUID = -1929561720985794233L;

    private String roleId;

    private String menuId;
}
