package com.iking.framework.sdk.authorization.model;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class AuthorizationUser implements Serializable {

    @Serial
    private static final long serialVersionUID = -9164740048732260470L;

    private String userId;

    private String username;

    private List<AuthorizationUserRole> roles;

    private List<AuthorizationUserDepartment> departments;
}
