package com.iking.framework.sdk.authorization.model;

import com.ikingtech.framework.sdk.enums.system.department.DeptTypeEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class AuthorizationUserDepartment implements Serializable {

    @Serial
    private static final long serialVersionUID = -1904761159605843529L;

    private String userId;

    private String deptId;

    private DeptTypeEnum deptType;

    private String deptFullPath;
}
