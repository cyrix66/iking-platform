package com.ikingtech.framework.sdk.base.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseQueryParam extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 5956095657008371027L;

    @Schema(name = "ids", description = "主键集合")
    private List<String> ids;
}
