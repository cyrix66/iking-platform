package com.ikingtech.framework.sdk.base.model;

import com.ikingtech.framework.sdk.utils.Tools;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * 批量查询条件基类
 * @author tie yan
 */
@Data
@Schema(name = "BatchParam", description = "批量查询条件基类")
public class BatchParam<T extends Serializable> implements Serializable {

    @Serial
    private static final long serialVersionUID = 8081218414069596606L;

    @Schema(name = "list", description = "批量查询条件，例如编号集合等")
    private List<T> list;

    public static <T extends Serializable> BatchParam<T> build(Collection<T> list) {
        BatchParam<T> batchParamDTO = new BatchParam<>();
        if (Tools.Coll.isBlank(list)) {
            batchParamDTO.setList(new ArrayList<>());
        } else {
            batchParamDTO.setList(new ArrayList<>(list));
        }
        return batchParamDTO;
    }

    public static <T extends Serializable> BatchParam<T> empty() {
        BatchParam<T> batchParamDTO = new BatchParam<>();
        batchParamDTO.setList(Collections.emptyList());
        return batchParamDTO;
    }
}
