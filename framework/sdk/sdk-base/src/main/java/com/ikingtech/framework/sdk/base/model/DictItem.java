package com.ikingtech.framework.sdk.base.model;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
public record DictItem(String value, String label) implements Serializable {

    @Serial
    private static final long serialVersionUID = -3739344454867855361L;
}
