package com.ikingtech.framework.sdk.base.model;

import com.ikingtech.framework.sdk.enums.common.DragTargetPositionEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 拖动排序参数
 *
 * @author tie yan
 */
@Data
@Schema(name = "DragOrderParam", description = "拖动排序参数")
public class DragOrderParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 1484237365148277903L;

    @Schema(name = "parentId", description = "被拖动元素所属父元素编号")
    private String parentId;

    @Schema(name = "targetParentId", description = "拖动目标元素所属父元素编号， 跨父元素拖动时指定")
    private String targetParentId;

    @Schema(name = "currentId", description = "被拖动元素编号")
    private String currentId;

    @Schema(name = "currentOrder", description = "被拖动元素排序值")
    private Integer currentOrder;

    @Schema(name = "targetId", description = "拖动目标元素编号")
    private String targetId;

    @Schema(name = "targetOrder", description = "拖动目标元素排序值")
    private Integer targetOrder;

    @Schema(name = "position", description = "相对拖动目标位置")
    private DragTargetPositionEnum position;
}
