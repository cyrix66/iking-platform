package com.ikingtech.framework.sdk.base.model;

import com.ikingtech.framework.sdk.enums.common.IdPolicyEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "IdPolicy", description = "主键生成策略信息")
public class IdPolicy implements Serializable {

    @Serial
    private static final long serialVersionUID = -253565424303630203L;

    @Schema(name = "policy", description = "主键生成策略")
    private IdPolicyEnum policy;
}
