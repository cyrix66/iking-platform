package com.ikingtech.framework.sdk.cache.constants;

import com.ikingtech.framework.sdk.utils.Tools;

import static com.ikingtech.framework.sdk.context.constant.SecurityConstants.TOKEN_PREFIX;

/**
 * 缓存常量
 *
 * @author tie yan
 */
public interface CacheConstants {

    String USER_AUTH_DETAILS = "user_auth_details";

    String USER_CONFIG = "user_config";

    String ACCESS_TOKEN = "access_token";

    String LOGIN_USER = "login_user";

    String IDENTITY_EXTENSION_REPORTER = "identity_extension_reporter";

    String APPROVE_PROCESS_CALLBACK = "approve_process_callback";

    String SYSTEM_VARIABLE = "system_variable";

    String ABNORMAL_TENANT = "abnormal_tenants";

    String MASTER_DATA_EVENT_REMINDER = "master_data_event_reminder";

    String EXTENSION_INFO_COLLECTOR = "extension_info_collector";

    static String accessTokenKeyFormat(String token) {
        return Tools.Str.join(CacheConstants.ACCESS_TOKEN, token, ":");
    }

    static String accessTokenKeyFormatWithoutPrefix(String tokenWithPrefix) {
        if (Tools.Str.isBlank(tokenWithPrefix) ||
                !tokenWithPrefix.startsWith(TOKEN_PREFIX)) {
            return tokenWithPrefix;
        }
        return Tools.Str.join(CacheConstants.ACCESS_TOKEN, tokenWithPrefix.replace(TOKEN_PREFIX, ""), ":");
    }

    static String loginUserFormat(String userId, String endpoint) {
        return Tools.Str.join(Tools.Coll.newList(CacheConstants.LOGIN_USER, userId, endpoint), ":");
    }

    static String userAuthDetailsFormat(String username, String tenantCode) {
        return Tools.Str.isBlank(tenantCode) ? Tools.Str.EMPTY : Tools.Str.join(Tools.Coll.newList(CacheConstants.USER_AUTH_DETAILS, username, tenantCode), ":");
    }

    static String userConfigFormat(String userId, String tenantCode) {
        return Tools.Str.isBlank(tenantCode) ? Tools.Str.EMPTY : Tools.Str.join(Tools.Coll.newList(CacheConstants.USER_CONFIG, userId, tenantCode), ":");
    }
}
