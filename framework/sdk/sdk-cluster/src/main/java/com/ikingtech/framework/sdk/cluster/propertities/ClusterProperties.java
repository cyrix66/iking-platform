package com.ikingtech.framework.sdk.cluster.propertities;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@Data
@ConfigurationProperties(GLOBAL_CONFIG_PREFIX + ".cluster")
public class ClusterProperties implements Serializable {

    @Serial
    private static final long serialVersionUID = -2800385490397921259L;

    private boolean enable = false;

    private Integer port = 6010;

    private List<String> node;
}
