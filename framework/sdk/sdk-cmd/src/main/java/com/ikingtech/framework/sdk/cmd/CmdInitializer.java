package com.ikingtech.framework.sdk.cmd;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

import java.io.IOException;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class CmdInitializer implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) throws IOException {
        CmdTracerManager.initLogDir();
    }
}
