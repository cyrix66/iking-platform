package com.ikingtech.framework.sdk.cmd;

import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.utils.Tools;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

/**
 * @author tie yan
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CmdTracer {

    public static void trace(List<String> cmd) {
        trace(Tools.Coll.join(cmd, " "));
    }

    public static void trace(String cmd) {
        File outputFile = CmdTracerManager.get();
        try (FileWriter outputFileWriter = new FileWriter(outputFile, true)) {
            outputFileWriter
                    .append(Tools.Str.format("{} -- command execute trace[{}]", Tools.DateTime.Formatter.simple(), cmd))
                    .append(System.lineSeparator());
            outputFileWriter.flush();
        } catch (Exception e) {
            throw new FrameworkException(Tools.Str.format("记录安装日志异常！[{}][{}]", e.getMessage()));
        }
    }
}
