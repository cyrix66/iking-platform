package com.ikingtech.framework.sdk.cmd.cmder;

import com.ikingtech.framework.sdk.utils.Tools;

import java.util.List;

/**
 * @author tie yan
 */
public class Mvn extends AbstractCmder {

    private final String prompt = this.isWin() ? "mvn.cmd" : "mvn";

    protected final List<String> commands = Tools.Coll.newList(this.prompt);

    public void execute() {
        this.execute(this.commands);
    }

    public static Package pck() {
        Package packageCmd = new Package();
        packageCmd.commands.addAll(Tools.Coll.newList("clean", "package"));
        return packageCmd;
    }

    public static class Package extends Mvn {

        public Package srcDir(String srcDir) {
            this.commands.addAll(Tools.Coll.newList("-f", srcDir));
            return this;
        }
    }
}
