package com.ikingtech.framework.sdk.cmd.cmder;

import com.ikingtech.framework.sdk.utils.Tools;

import java.util.List;

/**
 * @author tie yan
 */
public class Yum extends AbstractCmder {

    private final String prompt = "yum";

    protected final List<String> commands = Tools.Coll.newList(this.prompt);

    public void execute() {
        this.execute(this.commands);
    }

    public static Install install() {
        Install install = new Install();
        install.commands.addAll(Tools.Coll.newList("install", "-y"));
        return install;
    }

    public static Remove remove() {
        Remove remove = new Remove();
        remove.commands.add("remove");
        return remove;
    }

    public static ConfigManager configManager() {
        return new ConfigManager();
    }

    public static class ConfigManager extends AbstractCmder {

        private String repoUrl;


        public ConfigManager addRepo(String repoUrl) {
            this.repoUrl = repoUrl;
            return this;
        }

        public void execute() {
            List<String> args = Tools.Coll.newList("yum-config-manager");

            if (Tools.Str.isNotBlank(this.repoUrl)) {
                args.add("--add-repo");
                args.add(this.repoUrl);
            }

            this.execute(args);
        }
    }

    public static class Install extends Yum {

        public Yum packages(String... packages) {
            this.commands.addAll(Tools.Coll.newList(packages));
            return this;
        }
    }

    public static class Remove extends Yum {

        public Remove packages(String... packages) {
            this.commands.addAll(Tools.Coll.newList(packages));
            return this;
        }
    }
}
