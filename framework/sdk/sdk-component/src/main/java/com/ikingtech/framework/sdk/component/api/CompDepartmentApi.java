package com.ikingtech.framework.sdk.component.api;

import com.ikingtech.framework.sdk.component.model.ComponentDepartment;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
@Service
public interface CompDepartmentApi {

    List<ComponentDepartment> listByName(String name, Boolean dataScopeOnly);

    List<ComponentDepartment> listByParentId(String parentId, Boolean dataScopeOnly);

    ComponentDepartment load(String parentId);

    ComponentDepartment getRootDepartment();

    Map<String, String> extractDepartmentFullName(Map<String, String> deptFullPathMap);
}
