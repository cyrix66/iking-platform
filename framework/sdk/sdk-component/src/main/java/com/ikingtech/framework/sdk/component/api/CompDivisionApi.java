package com.ikingtech.framework.sdk.component.api;

import com.ikingtech.framework.sdk.component.model.DivisionElement;
import com.ikingtech.framework.sdk.enums.system.division.DivisionLevelEnum;

import java.util.List;

/**
 * @author tie yan
 */
public interface CompDivisionApi {

    List<DivisionElement> load(DivisionLevelEnum level, Long parentCode);
}
