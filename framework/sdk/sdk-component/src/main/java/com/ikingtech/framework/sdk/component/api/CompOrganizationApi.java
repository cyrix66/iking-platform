package com.ikingtech.framework.sdk.component.api;

import com.ikingtech.framework.sdk.component.model.ComponentOrganization;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tie yan
 */
@Service
public interface CompOrganizationApi {

    ComponentOrganization getRootOrganization();

    List<ComponentOrganization> listByName(String name);

    List<ComponentOrganization> listByParentId(String parentId);
}
