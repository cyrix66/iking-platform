package com.ikingtech.framework.sdk.component.api;

import com.ikingtech.framework.sdk.component.model.ComponentUser;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tie yan
 */
@Service
public interface CompUserApi {

    List<ComponentUser> listByIdsAndName(List<String> ids, String name, Boolean dataScopeOnly);

    List<ComponentUser> listByDeptId(String deptId);
}
