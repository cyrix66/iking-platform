package com.ikingtech.framework.sdk.component.model;

import com.ikingtech.framework.sdk.enums.system.department.DeptTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ComponentDepartment", description = "业务组件-部门信息")
public class ComponentDepartment extends PickerElementDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 889988166549158768L;

    @Schema(name = "fullPath", description = "单位/部门全路径")
    private String fullPath;

    @Schema(name = "fullName", description = "单位/部门名称(全路径)")
    private String fullName;

    @Schema(name = "managerId", description = "单位/部门主管编号")
    private String managerId;

    @Schema(name = "managerName", description = "单位/部门主管名称")
    private String managerName;

    @Schema(name = "userCount", description = "用户数")
    private Integer userCount;

    @Schema(name = "deptType", description = "组织架构类型")
    private DeptTypeEnum deptType;

    @Schema(name = "deptTypeName", description = "组织架构类型名称")
    private String deptTypeName;

    @Schema(name = "available", description = "是否可以选择")
    private Boolean available;

    @Schema(name = "unity", description = "所属组织")
    private ComponentDepartment unity;
}
