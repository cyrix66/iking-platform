package com.ikingtech.framework.sdk.component.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ComponentPost", description = "业务组件-岗位信息")
public class ComponentPost extends PickerElementDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -4669004257644705508L;

    @Schema(name = "tenantCode", description = "租户标识")
    private String tenantCode;
}
