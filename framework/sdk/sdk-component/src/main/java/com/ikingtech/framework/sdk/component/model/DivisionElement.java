package com.ikingtech.framework.sdk.component.model;

import com.ikingtech.framework.sdk.enums.system.division.DivisionCategoryEnum;
import com.ikingtech.framework.sdk.enums.system.division.DivisionLevelEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class DivisionElement implements Serializable {

    @Serial
    private static final long serialVersionUID = 265596789252709433L;

    private String name;

    private Long code;

    private Long parentCode;

    private DivisionLevelEnum level;

    private String levelName;

    private DivisionCategoryEnum category;

    private String categoryName;
}
