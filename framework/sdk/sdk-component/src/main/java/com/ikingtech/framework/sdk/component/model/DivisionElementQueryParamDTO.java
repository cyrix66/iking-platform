package com.ikingtech.framework.sdk.component.model;

import com.ikingtech.framework.sdk.enums.system.division.DivisionLevelEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class DivisionElementQueryParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 5668954788424213588L;

    private DivisionLevelEnum upToLevel;

    private DivisionLevelEnum downLevel;

    private DivisionLevelEnum parentLevel;

    private Long parentCode;
}
