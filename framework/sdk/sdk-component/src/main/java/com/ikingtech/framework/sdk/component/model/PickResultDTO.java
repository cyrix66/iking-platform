package com.ikingtech.framework.sdk.component.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "MixPickDTO", description = "混合选择器数据查询结果")
public class PickResultDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -1691830578913214115L;

    @Schema(name = "rootDepartment", description = "根部门")
    private ComponentDepartment rootDepartment;

    @Schema(name = "departments", description = "部门列表")
    private List<ComponentDepartment> departments;

    @Schema(name = "roles", description = "角色列表")
    private List<ComponentRole> roles;

    @Schema(name = "posts", description = "岗位列表")
    private List<ComponentPost> posts;

    @Schema(name = "posts", description = "用户列表")
    private List<ComponentUser> users;
}
