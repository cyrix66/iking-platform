package com.ikingtech.framework.sdk.component.model;

import com.ikingtech.framework.sdk.enums.common.ElementTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "PickerElementDTO", description = "选择器元素")
public class PickerElementDTO implements Serializable {
    @Serial
    private static final long serialVersionUID = -9200835746857490322L;

    @Schema(name = "elementType", description = "元素类型")
    private ElementTypeEnum elementType;

    @Schema(name = "elementId", description = "元素编号")
    private String elementId;

    @Schema(name = "elementName", description = "元素名称")
    private String elementName;
}
