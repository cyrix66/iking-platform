package com.ikingtech.framework.sdk.component.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "PickerElementQueryParamDTO", description = "选择器元素查询条件")
public class PickerElementQueryParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 4754133746640210207L;

    @Schema(name = "rootDepartmentOnly", description = "是否仅查询根部门")
    private Boolean rootDepartmentOnly;

    @Schema(name = "parentDepartmentId", description = "父部门编号")
    private String parentDepartmentId;

    @Schema(name = "name", description = "用户姓名")
    private String name;

    @Schema(name = "userIds", description = "用户编号集合")
    private List<String> userIds;

    @Schema(name = "dataScopeOnly", description = "是否受数据权限控制")
    private Boolean dataScopeOnly;
}
