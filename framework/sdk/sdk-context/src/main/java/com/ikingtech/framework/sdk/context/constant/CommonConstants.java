package com.ikingtech.framework.sdk.context.constant;

import java.util.Locale;

/**
 * 通用常量
 *
 * @author tie yan
 */
public class CommonConstants {

    private CommonConstants() {
        // do noting
    }

    /**
     * 全局配置前缀
     */
    public static final String GLOBAL_CONFIG_PREFIX = "iking.framework";

    /**
     * 默认语言环境
     */
    public static final String ENV_DEFAULT_LANG = Locale.SIMPLIFIED_CHINESE.toLanguageTag();

    public static final Integer JOB_HANDLER_REPORT_RUNNER_ORDER = 1880930;

    public static final Integer MESSAGE_TEMPLATE_BEAN_DEFINITION_REPORT_RUNNER_ORDER = 1880932;

    public static final Integer APPROVE_FORM_BEAN_DEFINITION_REPORT_RUNNER_ORDER = 1880932;

    public static final Integer LOG_CONSUMER_RUNNER_ORDER = 1880935;

    public static final Integer AUTHENTICATION_FILTER_ORDER = 1880936;

    public static final Integer SECURITY_FILTER_ORDER = 1880937;

    public static final Integer CHARTS_FILTER_ORDER = 1880938;
}
