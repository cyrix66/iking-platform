package com.ikingtech.framework.sdk.context.constant;

import java.time.LocalDateTime;

/**
 * @author tie yan
 */
public class SecurityConstants {

    private SecurityConstants() {
        // do noting
    }

    public static final String HEADER_ENV_LANGUAGE = "X-ENV-LANGUAGE";

    public static final String HEADER_VERSION = "X-VERSION";

    public static final String HEADER_TENANT_CODE = "X-TENANT";

    public static final String HEADER_DOMAIN_CODE = "X-DOMAIN";

    public static final String HEADER_APP_CODE = "X-APP";

    public static final String HEADER_MENU_ID = "Menu-Id";

    public static final String HEADER_PASSWORD_ENCRYPT = "X-PASSWORD-ENCRYPT";

    public static final String HEADER_CALLER = "X-CALLER";

    public static final String HEADER_INNER_IDENTITY = "X-INNER-IDENTITY";

    public static final String TOKEN_PREFIX = "Bearer ";

    public static final String GLOBAL_TOKEN = TOKEN_PREFIX + "iking";

    public static final String GLOBAL_MENU_ID = "global";

    public static final String DEFAULT_TENANT_CODE = "sys";

    public static final String DEFAULT_TENANT_NAME = "默认租户";

    public static final LocalDateTime DEFAULT_PASSWORD_MODIFY_TIME = LocalDateTime.of(1970, 1, 1, 12, 0, 0);
}
