package com.ikingtech.framework.sdk.context.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Getter
public class SystemInitEvent extends ApplicationEvent implements Serializable {

    @Serial
    private static final long serialVersionUID = -437955586042546846L;

    private final List<String> tenantCodes;

    public SystemInitEvent(List<String> tenantCodes, Object source) {
        super(source);
        this.tenantCodes = tenantCodes;
    }
}
