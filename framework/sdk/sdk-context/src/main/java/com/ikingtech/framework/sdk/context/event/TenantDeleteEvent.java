package com.ikingtech.framework.sdk.context.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Getter
public class TenantDeleteEvent extends ApplicationEvent implements Serializable {

    @Serial
    private static final long serialVersionUID = 8724657788033643237L;

    private final String code;

    public TenantDeleteEvent(Object source, String code) {
        super(source);
        this.code = code;
    }
}
