package com.ikingtech.framework.sdk.context.event.application;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Getter
public class ApplicationMenuRemoveEvent extends ApplicationEvent implements Serializable {

    @Serial
    private static final long serialVersionUID = -1985185647474577349L;

    private final String appCode;

    public ApplicationMenuRemoveEvent(Object source, String appCode) {
        super(source);
        this.appCode = appCode;
    }
}
