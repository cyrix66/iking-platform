package com.ikingtech.framework.sdk.context.event.application;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * @author tie yan
 */
@Getter
public class ApplicationPublishEvent extends ApplicationEvent implements Serializable {

    @Serial
    private static final long serialVersionUID = 3358056441195516542L;

    private final String appCode;

    private final List<String> tenantCodes;

    public ApplicationPublishEvent(Object source, String appCode) {
        super(source);
        this.appCode = appCode;
        this.tenantCodes = Collections.emptyList();
    }

    public ApplicationPublishEvent(Object source, String appCode, List<String> tenantCodes) {
        super(source);
        this.appCode = appCode;
        this.tenantCodes = tenantCodes;
    }
}
