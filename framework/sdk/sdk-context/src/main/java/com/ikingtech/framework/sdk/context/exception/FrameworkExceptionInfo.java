package com.ikingtech.framework.sdk.context.exception;

/**
 * 业务异常信息
 *
 * @author tie yan
 */
public interface FrameworkExceptionInfo {

    /**
     * 异常信息
     *
     * @return 异常信息
     */
    default String message() {
        return "";
    }

    /**
     * 业务模块名
     *
     * @return 业务模块名
     */
    default String moduleName() {
        return "";
    }
}
