package com.ikingtech.framework.sdk.data.mybatisplus.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 带排序字段实体类基类
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SortEntity extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 8570638937681545364L;

    @TableField("sort_order")
    private Integer sortOrder;

    @TableField(value = "parent_id", exist = false)
    private String parentId;

    @TableField(value = "full_path", exist = false)
    private String fullPath;
}
