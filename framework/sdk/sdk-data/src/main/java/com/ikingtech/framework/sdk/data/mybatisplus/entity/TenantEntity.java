package com.ikingtech.framework.sdk.data.mybatisplus.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 实体类基类
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TenantEntity extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -7598384243290245280L;

    @TableField(value = "tenant_code")
    private String tenantCode;
}
