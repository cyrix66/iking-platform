package com.ikingtech.framework.sdk.data.mybatisplus.helper;

import com.ikingtech.framework.sdk.data.mybatisplus.entity.SortEntity;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.IntSupplier;
import java.util.function.Supplier;

/**
 * @author tie yan
 */
public abstract class AbstractDragHelper<T extends SortEntity> implements DragHelper<T> {

    protected Supplier<T> currentNode;

    protected Supplier<T> targetNode;

    protected Supplier<String> targetParentNodeFullPath;

    protected Supplier<List<T>> nodeAfterCurrent;

    protected Supplier<List<T>> nodeAfterTarget;

    protected BiFunction<Integer, Integer, List<T>> nodeBetweenCurrentAndTarget;

    protected Boolean beforeTarget;

    protected IntSupplier maxSortOrder;
}
