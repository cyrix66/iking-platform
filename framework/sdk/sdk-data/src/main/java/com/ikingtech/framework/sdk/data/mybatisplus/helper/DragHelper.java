package com.ikingtech.framework.sdk.data.mybatisplus.helper;

import com.ikingtech.framework.sdk.data.mybatisplus.entity.SortEntity;

import java.util.List;

/**
 * @author tie yan
 */
public interface DragHelper<T extends SortEntity> {

    List<T> drag();
}
