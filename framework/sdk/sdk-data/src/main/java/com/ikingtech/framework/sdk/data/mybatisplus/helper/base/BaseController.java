package com.ikingtech.framework.sdk.data.mybatisplus.helper.base;

import com.ikingtech.framework.sdk.base.model.BaseModel;
import com.ikingtech.framework.sdk.base.model.BaseQueryParam;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import com.ikingtech.framework.sdk.utils.Tools;

/**
 * @author tie yan
 */
public abstract class BaseController<T extends BaseModel, E extends BaseEntity, Q extends BaseQueryParam> extends AbstractController<T, E, Q> {


    protected BaseController(BaseRepository<E, Q> repo,
                             Class<T> modelClass,
                             Class<E> entityClass) {
        super(repo, modelClass, entityClass);
    }
}
