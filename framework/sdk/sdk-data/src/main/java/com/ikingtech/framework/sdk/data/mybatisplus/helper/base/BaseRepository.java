package com.ikingtech.framework.sdk.data.mybatisplus.helper.base;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author tie yan
 */
public interface BaseRepository<E, Q> extends IService<E> {

    default Wrapper<E> conditionalWrapper(Q queryParam) {
        return Wrappers.emptyWrapper();
    }

    default Wrapper<E> tenantWrapper() {
        return Wrappers.emptyWrapper();
    }
}
