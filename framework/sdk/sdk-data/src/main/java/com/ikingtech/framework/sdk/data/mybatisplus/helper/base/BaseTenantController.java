package com.ikingtech.framework.sdk.data.mybatisplus.helper.base;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ikingtech.framework.sdk.base.model.BaseModel;
import com.ikingtech.framework.sdk.base.model.BaseQueryParam;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.TenantEntity;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Collections;
import java.util.List;

/**
 * @author tie yan
 */
public abstract class BaseTenantController<T extends BaseModel, E extends TenantEntity, Q extends BaseQueryParam> extends AbstractController<T, E, Q> {

    protected BaseTenantController(BaseRepository<E, Q> repo,
                                   Class<T> modelClass,
                                   Class<E> entityClass) {
        super(repo, modelClass, entityClass);
    }

    @Override
    public E entityConvert(T model) {
        E entity = Tools.Bean.copy(model, this.entityClass);
        entity.setTenantCode(Me.tenantCode());
        if (Tools.Str.isBlank(model.getId())) {
            entity.setId(Tools.Id.uuid());
        }
        return entity;
    }
}
