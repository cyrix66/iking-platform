package com.ikingtech.framework.sdk.datasource.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.datasource.model.*;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public interface DatasourceApi {

    /**
     * 添加数据源信息
     *
     * @param datasource 数据源信息
     * @return 返回添加结果
     */
    @PostRequest(order = 1, value = "/add", summary = "添加数据源信息", description = "添加数据源信息")
    R<String> add(@Parameter(name = "datasource", description = "数据源信息")
                  @RequestBody DatasourceDTO datasource);

    /**
     * 删除数据源
     *
     * @param id 数据源编号
     * @return 返回删除结果
     */
    @PostRequest(order = 2, value = "/delete", summary = "删除数据源", description = "删除数据源")
    R<Object> delete(@Parameter(name = "id", description = "数据源编号")
                     @RequestBody String id);

    /**
     * 更新数据源信息
     *
     * @param datasource 数据源信息
     * @return 返回更新结果
     */
    @PostRequest(order = 3, value = "/update", summary = "更新数据源信息", description = "更新数据源信息")
    R<Object> update(@Parameter(name = "datasource", description = "数据源信息")
                     @RequestBody DatasourceDTO datasource);

    /**
     * 分页查询数据源信息
     *
     * @param queryParam 查询条件
     * @return 返回分页查询结果
     */
    @PostRequest(order = 4, value = "/list/page", summary = "分页查询数据源信息", description = "分页查询数据源信息")
    R<List<DatasourceDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                                @RequestBody DatasourceQueryParamDTO queryParam);

    /**
     * 查询所有数据源信息
     *
     * @return 返回所有数据源信息
     */
    @PostRequest(order = 5, value = "/list/all", summary = "查询所有数据源信息", description = "查询所有数据源信息")
    R<List<DatasourceDTO>> all();

    /**
     * 查询指定数据源信息
     *
     * @param id 数据源编号
     * @return 返回指定数据源信息
     */
    @PostRequest(order = 6, value = "/detail/id", summary = "查询指定数据源信息", description = "查询指定数据源信息")
    R<DatasourceDTO> detail(@Parameter(name = "id", description = "数据源编号")
                            @RequestBody String id);

    /**
     * 查询指定数据源的数据库表信息
     *
     * @param datasource 数据源信息
     * @return 返回指定数据源的数据库表信息
     */
    @PostRequest(order = 7, value = "/schema/list", summary = "查询指定数据源的数据库表信息", description = "查询指定数据源的数据库表信息")
    R<List<String>> listSchema(@Parameter(name = "datasource", description = "数据源信息")
                               @RequestBody DatasourceDTO datasource);

    /**
     * 查询指定数据源的数据库表信息
     *
     * @param id 数据源编号
     * @return 返回指定数据源的数据库表信息
     */
    @PostRequest(order = 8, value = "/table/list/id", summary = "查询指定数据源的数据库表信息", description = "查询指定数据源的数据库表信息")
    R<List<DatasourceTableDTO>> listTableById(@Parameter(name = "id", description = "数据源编号")
                                              @RequestBody String id);

    @PostRequest(order = 9, value = "/table/list", summary = "查询指定数据源的数据表信息", description = "查询指定数据源的数据表信息")
    R<List<DatasourceTableDTO>> listTable(@Parameter(name = "datasource", description = "数据源信息")
                                          @RequestBody DatasourceFieldDTO datasource);

    @PostRequest(order = 10, value = "/execute", summary = "指定数据源执行sql", description = "指定数据源执行sql")
    R<List<Map<String, Object>>> execute(@Parameter(name = "execute", description = "执行sql的目标和信息")
                                         @RequestBody DatasourceExecuteDTO execute);

    @PostRequest(order = 11, value = "/field/list", summary = "查询指定库表字段信息", description = "查询指定库表字段信息")
    R<List<DatasourceTableFieldDTO>> listField(@Parameter(name = "field", description = "数据源信息")
                                               @RequestBody DatasourceFieldDTO field);
}
