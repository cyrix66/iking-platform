package com.ikingtech.framework.sdk.datasource.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 数据源执行信息
 * @author tie yan
 */
@Data
@Schema(name = "DatasourceExecuteDTO", description = "数据源执行信息")
public class DatasourceExecuteDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 958426688718043322L;
    
    @Schema(name = "id", description = "数据源ID信息")
    private String id;

    @Schema(name = "sql", description = "执行的sql语句")
    private String sql;

}
