package com.ikingtech.framework.sdk.datasource.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author bruce lee
 */
@Data
@Schema(name = "DatasourceFieldDTO", description = "数据源字段信息")
public class DatasourceFieldDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 4342623152770390825L;

    @Schema(name = "id", description = "数据源ID信息")
    private String id;

    @Schema(name = "schemaName", description = "库名")
    private String schemaName;

    @Schema(name = "tableName", description = "表名")
    private String tableName;

}
