package com.ikingtech.framework.sdk.department.api;

import com.ikingtech.framework.sdk.base.model.DragOrderParam;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.department.model.DeptBasicDTO;
import com.ikingtech.framework.sdk.department.model.DeptDTO;
import com.ikingtech.framework.sdk.department.model.DeptQueryParamDTO;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author tie yan
 */
public interface DeptApi {

    /**
     * 添加部门信息
     *
     * @param dept 部门信息
     * @return 返回部门编号
     */
    @PostRequest(order = 1, value = "/add", summary = "添加部门信息", description = "添加部门信息")
    R<String> add(@Parameter(name = "dept", description = "部门信息")
                  @RequestBody DeptDTO dept);

    /**
     * 删除部门
     *
     * @param id 编号
     * @return 返回结果
     */
    @PostRequest(order = 2, value = "/delete", summary = "删除部门", description = "删除部门")
    R<Object> delete(@Parameter(name = "id", description = "编号")
                     @RequestBody String id);

    /**
     * 更新部门信息
     *
     * @param dept 部门信息
     * @return 返回结果
     */
    @PostRequest(order = 3, value = "/update", summary = "更新部门信息", description = "更新部门信息")
    R<Object> update(@Parameter(name = "dept", description = "部门信息")
                     @RequestBody DeptDTO dept);

    /**
     * 分页查询部门信息
     *
     * @param queryParam 查询条件
     * @return 返回部门信息列表
     */
    @PostRequest(order = 4, value = "/list/page", summary = "分页查询部门信息", description = "分页查询部门信息")
    R<List<DeptDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                          @RequestBody DeptQueryParamDTO queryParam);

    /**
     * 查询所有部门信息
     *
     * @param excludeId 排除部门编号
     * @return 返回部门信息列表
     */
    @PostRequest(order = 5, value = "/info/list/all", summary = "查询所有部门信息", description = "查询所有部门信息")
    R<List<DeptBasicDTO>> all(@Parameter(name = "excludeId", description = "排除部门编号")
                              @RequestParam(required = false, value = "excludeId") String excludeId);

    /**
     * 条件查询部门信息
     *
     * @param queryParam 排除部门编号
     * @return 返回部门信息列表
     */
    @PostRequest(order = 5, value = "/info/list", summary = "条件查询部门信息", description = "条件查询部门信息")
    R<List<DeptBasicDTO>> list(@Parameter(name = "queryParam", description = "查询条件")
                               @RequestBody DeptQueryParamDTO queryParam);

    /**
     * 查询部门详细信息
     *
     * @param id 编号
     * @return 返回部门信息
     */
    @PostRequest(order = 6, value = "/detail/id", summary = "查询部门详细信息", description = "查询部门详细信息")
    R<DeptDTO> detail(@Parameter(name = "id", description = "编号")
                      @RequestBody String id);

    /**
     * 查询所有子部门信息列表
     *
     * @param queryParam 查询条件
     * @return 返回部门信息列表
     */
    @PostRequest(order = 7, value = "/sub/all/list", summary = "查询所有子部门信息列表", description = "查询所有子部门信息列表")
    R<List<DeptDTO>> listSubAll(@Parameter(name = "queryParam", description = "查询条件")
                                @RequestBody DeptQueryParamDTO queryParam);

    /**
     * 查询所有子部门信息列表
     *
     * @param queryParam 查询条件
     * @return 返回部门信息列表
     */
    @PostRequest(order = 8, value = "/info/sub/all/list", summary = "查询所有子部门信息列表", description = "查询所有子部门信息列表")
    R<List<DeptBasicDTO>> listSubInfoAll(@Parameter(name = "queryParam", description = "查询条件")
                                         @RequestBody DeptQueryParamDTO queryParam);

    /**
     * 拖拽部门
     *
     * @param dragParam 拖拽参数
     * @return 返回结果
     */
    @PostRequest(order = 9, value = "/drag", summary = "拖拽部门", description = "拖拽部门")
    R<Object> drag(@Parameter(name = "dragParam", required = true, description = "拖拽参数。")
                   @RequestBody DragOrderParam dragParam);
}
