package com.ikingtech.framework.sdk.department.api;

import com.ikingtech.framework.sdk.base.model.DictItem;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public interface DeptDictApi {

    DictItem load(String value, String tenantCode);

    Map<String, DictItem> load(List<String> values, String tenantCode);
}
