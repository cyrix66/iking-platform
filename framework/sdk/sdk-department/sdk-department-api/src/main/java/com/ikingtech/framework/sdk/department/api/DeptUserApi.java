package com.ikingtech.framework.sdk.department.api;

import com.ikingtech.framework.sdk.department.model.DeptManagerDTO;

import java.util.List;
import java.util.Map;

/**
 * 为了避免循环依赖，将部门模块中的用户相关操作抽象为接口，由用户模块实现
 *
 * @author tie yan
 */
public interface DeptUserApi {

    /**
     * 移动用户到父部门
     *
     * @param parentId 父部门编号
     * @param deptIds 当前部门编号集合
     */
    void moveUser(String parentId, List<String> deptIds, String tenantCode);

    /**
     * 获取管理员用户信息
     *
     * @param userId 管理员用户编号
     * @return 管理员用户信息
     */
    DeptManagerDTO loadManager(String userId);

    /**
     * 批量获取管理员用户信息
     *
     * @param userIds 管理员用户编号集合
     * @return 管理员用户信息列表
     */
    List<DeptManagerDTO> loadManagers(List<String> userIds);

    /**
     * 获取指定部门下的用户数量
     *
     * @param deptIds 部门编号集合
     * @return 管理员用户信息列表
     */
    Map<String, Integer> getUserCount(List<String> deptIds);

}
