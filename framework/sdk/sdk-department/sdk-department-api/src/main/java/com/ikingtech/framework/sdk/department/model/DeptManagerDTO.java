package com.ikingtech.framework.sdk.department.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class DeptManagerDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 3880805768358920655L;

    @Schema(name = "userId", description = "用户编号")
    private String userId;

    @Schema(name = "userName", description = "用户姓名")
    private String userName;

    @Schema(name = "userAvatar", description = "用户头像")
    private String userAvatar;
}
