package com.ikingtech.framework.sdk.department.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "DeptQueryParamDTO", description = "查询参数")
public class DeptQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 8767303210649373544L;

    @Schema(name = "ids", description = "部门编号集合")
    private List<String> ids;

    @Schema(name = "name", description = "部门名称")
    private String name;

    @Schema(name = "managerId", description = "主管用户编号")
    private String managerId;

    @Schema(name = "phone", description = "联系电话")
    private String phone;

    @Schema(name = "parentDeptId", description = "父部门编号")
    private String parentDeptId;

    @Schema(name = "type", description = "部门类型")
    private String type;
}
