package com.ikingtech.framework.sdk.department.rpc.api;

import com.ikingtech.framework.sdk.department.api.DeptApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "DeptRpcApi", path = "/system/dept")
public interface DeptRpcApi extends DeptApi {
}
