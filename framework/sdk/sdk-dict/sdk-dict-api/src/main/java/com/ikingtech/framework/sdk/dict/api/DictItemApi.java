package com.ikingtech.framework.sdk.dict.api;

import com.ikingtech.framework.sdk.base.model.BatchParam;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.dict.model.DictItemDTO;
import com.ikingtech.framework.sdk.dict.model.DictItemQueryParamDTO;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public interface DictItemApi {

    /**
     * 添加字典项
     *
     * @param item 字典项信息
     * @return 返回添加结果
     */
    @PostRequest(order = 1, value = "/add", summary = "添加字典项", description = "添加字典项")
    R<String> add(@Parameter(name = "item", description = "字典项信息")
                  @RequestBody DictItemDTO item);

    /**
     * 删除字典项
     *
     * @param id 编号
     * @return 返回删除结果
     */
    @PostRequest(order = 2, value = "/delete", summary = "删除字典项", description = "删除字典项")
    R<Object> delete(@Parameter(name = "id", description = "编号")
                     @RequestBody String id);

    /**
     * 更新字典项
     *
     * @param item 字典项信息
     * @return 返回更新结果
     */
    @PostRequest(order = 3, value = "/update", summary = "更新字典项", description = "更新字典项")
    R<Object> update(@Parameter(name = "item", description = "字典项信息")
                     @RequestBody DictItemDTO item);

    /**
     * 分页查询字典项
     *
     * @param queryParam 查询条件
     * @return 返回分页结果
     */
    @PostRequest(order = 4, value = "/list/page", summary = "分页查询字典项", description = "分页查询字典项")
    R<List<DictItemDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                              @RequestBody DictItemQueryParamDTO queryParam);

    /**
     * 查询所有字典项
     *
     * @return 返回所有字典项
     */
    @PostRequest(order = 5, value = "/list/all", summary = "查询所有字典项", description = "查询所有字典项")
    R<List<DictItemDTO>> all();

    /**
     * 根据编号查询字典项
     *
     * @param id 编号
     * @return 返回字典项
     */
    @PostRequest(order = 6, value = "/detail/id", summary = "根据编号查询字典项", description = "根据编号查询字典项")
    R<DictItemDTO> detail(@Parameter(name = "id", description = "编号")
                          @RequestBody String id);

    /**
     * 根据字典名称查询字典项Map
     *
     * @param dictName 字典名称
     * @return 返回字典项列表
     */
    @PostRequest(order = 7, value = "/map/list/dict-name", summary = "根据字典名称查询字典项Map", description = "根据字典名称查询字典项Map")
    R<Map<String, String>> mapByDictName(@Parameter(name = "dictName", description = "字典名称")
                                                @RequestBody String dictName);

    /**
     * 根据字典编号查询字典项
     *
     * @param dictId 字典编号
     * @return 返回字典项列表
     */
    @PostRequest(order = 7, value = "/list/dict-id", summary = "根据字典编号查询字典项", description = "根据字典编号查询字典项")
    R<List<DictItemDTO>> listByDictId(@Parameter(name = "dictId", description = "字典编号")
                                      @RequestBody String dictId);

    /**
     * 根据字典code集合查询字典项
     *
     * @param dictCodes 字典code集合
     * @return 返回字典项列表
     */
    @PostRequest(order = 8, value = "/list/dict-codes", summary = "根据字典code集合查询字典项", description = "根据字典code集合查询字典项")
    R<List<DictItemDTO>> listByDictCodes(@Parameter(name = "dictCodes", description = "字典code集合")
                                         @RequestBody BatchParam<String> dictCodes);
}
