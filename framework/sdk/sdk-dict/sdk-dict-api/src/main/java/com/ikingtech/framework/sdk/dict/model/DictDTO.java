package com.ikingtech.framework.sdk.dict.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.system.dictionary.DictTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import jakarta.validation.constraints.NotBlank;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@Schema(name = "DictDTO", description = "字典信息，继承了IdPolicy基类，业务方如需自行指定编号，则将IdPolicy中的policy字段设置为MANUAL")
public class DictDTO implements Serializable {

	@Serial
    private static final long serialVersionUID = -5712226916782810750L;

	@Schema(name = "id", description = "字典编号")
	private String id;

	@Schema(name = "tenantCode", description = "租户标识")
	private String tenantCode;

	@NotBlank(message = "dictName")
	@Schema(name = "name", description = "字典名称")
	private String name;

	@Schema(name = "code", description = "字典枚举名称")
	private String code;

	@Length(max = 200, message = "dictRemark")
	@Schema(name = "remark", description = "备注信息")
	private String remark;

	@Schema(name = "type", description = "字典类型")
	private DictTypeEnum type;

	@Schema(name = "typeName", description = "字典类型名称")
	private String typeName;

	@Schema(name = "createBy", description = "创建人编号")
	private String createBy;

	@Schema(name = "createName", description = "创建人姓名")
	private String createName;

	@Schema(name = "updateBy", description = "更新人编号")
	private String updateBy;

	@Schema(name = "updateName", description = "更新人姓名")
	private String updateName;

	@Schema(name = "createTime", description = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private LocalDateTime createTime;

	@Schema(name = "updateTime", description = "更新时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private LocalDateTime updateTime;
}
