package com.ikingtech.framework.sdk.dict.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@Schema(name = "DictItemDTO", description = "字典项信息")
public class DictItemDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -9158581552128356014L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "tenantCode", description = "租户标识")
    private String tenantCode;

    @Schema(name = "parentId", description = "父字典项编号")
    private String parentId;

    @Schema(name = "dictId", description = "所属字典编号")
    private String dictId;

    @Schema(name = "dictCode", description = "所属字典标识")
    private String dictCode;

    @Schema(name = "value", description = "字典项值")
    private String value;

    @Schema(name = "label", description = "字典项名称")
    private String label;

    @Schema(name = "remark", description = "备注信息")
    private String remark;

    @Schema(name = "fullPath", description = "字典项全路径")
    private String fullPath;

    @Schema(name = "preset", description = "是否为预置项")
    private Boolean preset;

    @Schema(name = "sortOrder", description = "排序值")
    private Integer sortOrder;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
