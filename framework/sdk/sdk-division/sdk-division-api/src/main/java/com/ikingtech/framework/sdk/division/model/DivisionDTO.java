package com.ikingtech.framework.sdk.division.model;

import com.ikingtech.framework.sdk.enums.system.division.DivisionCategoryEnum;
import com.ikingtech.framework.sdk.enums.system.division.DivisionLevelEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import jakarta.validation.constraints.NotBlank;
import java.io.Serial;
import java.io.Serializable;

/**
 * 行政区划信息
 * @author tie yan
 */
@Data
@Schema(name = "DivisionDTO", description = "行政区划信息")
public class DivisionDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 6029782571539770702L;

    @Schema(name = "code", description = "行政区划编号")
    private Long code;

    @Schema(name = "parentCode", description = "父级行政区划编号")
    private Long parentCode;

    @NotBlank(message = "divisionName")
    @Length(max = 32, message = "divisionName")
    @Schema(name = "name", description = "行政区划名称")
    private String name;

    @Schema(name = "divisionLevel", description = "行政区划级别")
    private DivisionLevelEnum divisionLevel;

    @Schema(name = "divisionLevelName", description = "行政区划级别名称")
    private String divisionLevelName;

    @Schema(name = "divisionCategory", description = "行政区划类别")
    private DivisionCategoryEnum divisionCategory;

    @Schema(name = "divisionCategoryName", description = "行政区划类别名称")
    private String divisionCategoryName;
}
