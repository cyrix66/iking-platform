package com.ikingtech.framework.sdk.enums.application;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApplicationPageItemTypeEnum {

    HOME("主页面"),

    DETAIL("详情");

    public final String description;
}
