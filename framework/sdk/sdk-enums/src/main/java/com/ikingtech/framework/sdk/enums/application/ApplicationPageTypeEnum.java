package com.ikingtech.framework.sdk.enums.application;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApplicationPageTypeEnum {

    GROUP("页面分组"),

    NORMAL("普通表单"),

    APPROVE("流程表单"),

    CUSTOMIZE("自定义页面"),

    BUSINESS("业务页面");

    public final String description;
}
