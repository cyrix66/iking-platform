package com.ikingtech.framework.sdk.enums.application;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApplicationStatusEnum {

    UNAUDITED("未审核"),

    PUBLISHED("已发布"),

    UNPUBLISHED("未发布");

    public final String description;
}
