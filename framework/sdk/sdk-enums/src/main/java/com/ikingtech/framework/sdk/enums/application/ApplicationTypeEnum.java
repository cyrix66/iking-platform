package com.ikingtech.framework.sdk.enums.application;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApplicationTypeEnum {

    EMBEDDED("内置应用"),

    PLATFORM("平台应用"),

    THIRD_PARTY("第三方应用");

    public final String description;
}
