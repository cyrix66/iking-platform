package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * 审批条件适用类型枚举
 * 该枚举定义了审批条件的两种适用类型：流程条件和节点条件。
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApproveConditionApplicableTypeEnum {

    /**
     * 流程条件，表示审批条件适用于整个流程
     */
    PROCESS("流程条件"),

    /**
     * 节点条件，表示审批条件仅适用于特定的流程节点
     */
    NODE("节点条件");

    public final String description;
}

