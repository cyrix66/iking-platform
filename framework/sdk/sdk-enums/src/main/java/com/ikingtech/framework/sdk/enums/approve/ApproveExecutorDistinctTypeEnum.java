package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * 审批人去重规则
 *
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApproveExecutorDistinctTypeEnum {

    /**
     * 在流程中出现多次时，仅保留第一个
     */
    KEEP_FIRST("在流程中出现多次时，仅保留第一个"),

    /**
     * 仅在连续出现时，自动去重
     */
    CONTINUOUS_APPEARS_ONLY("仅在连续出现时，自动去重");

    public final String description;
}
