package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApproveExecutorTypeEnum {

    ALL("所有人"),

    CURRENT_EXECUTOR("当前执行者"),

    INITIATOR("发起人"),

    USER("用户"),

    ROLE("角色"),

    DEPT("组织架构"),

    POST("岗位");

    public final String description;
}
