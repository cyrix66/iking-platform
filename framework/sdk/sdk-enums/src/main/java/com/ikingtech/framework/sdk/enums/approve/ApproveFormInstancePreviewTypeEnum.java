package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApproveFormInstancePreviewTypeEnum {

    BY_FORM_CONFIG("表单配置"),

    BY_FORM_MODIFIED("表单数据变更"),

    BY_DRAFT("草稿");

    public final String description;
}
