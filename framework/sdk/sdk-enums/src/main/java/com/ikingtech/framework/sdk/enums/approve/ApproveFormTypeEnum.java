package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApproveFormTypeEnum {

    /**
     * 仅流程
     */
    PROCESS_ONLY("仅流程"),

    /**
     * 仅表单
     */
    FORM_ONLY("仅表单"),

    /**
     * 流程表单
     */
    PROCESS_FORM("流程表单"),

    /**
     * 数据表单
     */
    DATA_FORM("数据表单");

    public final String description;
}
