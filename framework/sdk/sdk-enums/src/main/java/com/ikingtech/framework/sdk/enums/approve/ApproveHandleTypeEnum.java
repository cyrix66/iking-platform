package com.ikingtech.framework.sdk.enums.approve;

import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApproveHandleTypeEnum {

    SUBMIT("发起", Tools.Coll.newList(ApproveProcessNodeTypeEnum.INITIATOR)),

    RESUBMIT("重新发起", Tools.Coll.newList(ApproveProcessNodeTypeEnum.INITIATOR)),

    REVOKE("撤销", Tools.Coll.newList(ApproveProcessNodeTypeEnum.INITIATOR, ApproveProcessNodeTypeEnum.APPROVE, ApproveProcessNodeTypeEnum.DISPOSE)),

    PASS("通过", Tools.Coll.newList(ApproveProcessNodeTypeEnum.APPROVE)),

    FINISH("办结", Tools.Coll.newList(ApproveProcessNodeTypeEnum.DISPOSE)),

    REJECT("拒绝", Tools.Coll.newList(ApproveProcessNodeTypeEnum.APPROVE, ApproveProcessNodeTypeEnum.DISPOSE)),

    BACK("退回", Tools.Coll.newList(ApproveProcessNodeTypeEnum.APPROVE, ApproveProcessNodeTypeEnum.DISPOSE)),

    APPEND("加签", Tools.Coll.newList(ApproveProcessNodeTypeEnum.APPROVE, ApproveProcessNodeTypeEnum.DISPOSE)),

    REMIND("催办", Tools.Coll.newList(ApproveProcessNodeTypeEnum.APPROVE, ApproveProcessNodeTypeEnum.DISPOSE));

    public final String description;

    public final List<ApproveProcessNodeTypeEnum> nodeTypes;
}
