package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApproveInitiatorSpecifiedScopeTypeEnum {

    ALL("全公司"),

    BY_ROLE("按角色指定"),

    BY_USER("按成员指定");

    public final String description;
}
