package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApproveMultiExecutorTypeEnum {

    /**
     * 无
     */
    NONE("无"),

    /**
     * 依次处理(按顺序依次审批)
     */
    SEQUENCE("依次审批"),

    /**
     * 会签(须所有审批人同意)
     */
    ALL("会签"),

    /**
     * 或签(一名审批人统一或拒绝即可)
     */
    ANY("或签");

    public final String description;
}
