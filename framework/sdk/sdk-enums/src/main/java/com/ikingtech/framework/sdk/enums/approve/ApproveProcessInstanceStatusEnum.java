package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApproveProcessInstanceStatusEnum {

    WAIT_FOR_INITIATE("待发起"),

    APPROVING("审批中"),

    PASS("已通过"),

    REJECT("已拒绝"),

    REVOKE("已撤销");

    public final String description;
}
