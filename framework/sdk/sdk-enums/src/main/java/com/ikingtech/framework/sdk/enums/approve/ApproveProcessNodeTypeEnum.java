package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApproveProcessNodeTypeEnum {

    INITIATOR("发起人节点"),

    APPROVE("审批节点"),

    CARBON_COPY("抄送节点"),

    DISPOSE("办理节点"),

    BRANCH("分支节点"),

    PARALLEL("并行节点"),

    SUB_PROCESS("子流程节点"),

    CONDITION("条件节点"),

    END("结束节点");

    public final String description;
}
