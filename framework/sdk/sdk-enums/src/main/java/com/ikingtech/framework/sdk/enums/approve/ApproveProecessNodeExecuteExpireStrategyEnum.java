package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApproveProecessNodeExecuteExpireStrategyEnum {

    REMIND("自动提醒"),

    PASS("自动通过"),

    REJECT("自动拒绝");

    public final String description;
}
