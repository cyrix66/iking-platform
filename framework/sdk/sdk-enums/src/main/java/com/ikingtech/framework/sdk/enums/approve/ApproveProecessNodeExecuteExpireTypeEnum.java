package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApproveProecessNodeExecuteExpireTypeEnum {

    FIXED("定时"),

    DELAY("计时"),

    FIXED_DELAY("定时后计时");

    public final String description;
}
