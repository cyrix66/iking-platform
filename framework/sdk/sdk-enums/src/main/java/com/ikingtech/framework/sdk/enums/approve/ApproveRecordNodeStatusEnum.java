package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApproveRecordNodeStatusEnum {

    /**
     * 发起
     */
    INITIATE("发起"),

    /**
     * 同意
     */
    PASS("已同意"),

    /**
     * 撤销
     */
    REVOKE("撤销"),

    /**
     * 拒绝
     */
    REJECT("已拒绝"),

    /**
     * 转交
     */
    TRANSFER("已转交"),

    /**
     * 加签
     */
    APPEND("加签"),

    /**
     * 退回
     */
    BACK("退回"),

    /**
     * 评论
     */
    COMMENT("评论"),

    /**
     * 自动通过
     */
    AUTO_PASS("自动通过"),

    /**
     * 自动拒绝
     */
    AUTO_REJECT("自动拒绝");

    public final String description;
}
