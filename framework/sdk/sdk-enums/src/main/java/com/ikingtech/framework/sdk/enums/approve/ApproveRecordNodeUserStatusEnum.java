package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApproveRecordNodeUserStatusEnum {

    /**
     * 审批中
     */
    RUNNING("执行中"),

    /**
     * 已通过
     */
    END("完成");

    public final String description;
}
