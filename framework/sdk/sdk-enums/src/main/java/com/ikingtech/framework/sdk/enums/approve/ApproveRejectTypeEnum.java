package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApproveRejectTypeEnum {

    /**
     * 驳回至前一处理人
     */
    END("直接结束"),

    /**
     * 驳回至指定处理人
     */
    TO_SPECIFIED("退回至指定节点");

    public final String description;
}
