package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApproveRoleSpecifiedScopeTypeEnum {

    INITIATOR_DEPT("发起人所在部门"),

    SPECIFIED_DEPT("指定部门"),

    INITIATOR_PARENT_DEPT("发起人上级部门");

    public final String description;
}
