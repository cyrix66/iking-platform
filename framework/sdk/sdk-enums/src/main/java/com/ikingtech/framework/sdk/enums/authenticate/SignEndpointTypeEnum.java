package com.ikingtech.framework.sdk.enums.authenticate;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum SignEndpointTypeEnum {
    PC("PC端"),

    WECHAT_MINI("小程序端");

    public final String description;
}
