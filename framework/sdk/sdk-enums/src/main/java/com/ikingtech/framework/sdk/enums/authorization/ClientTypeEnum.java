package com.ikingtech.framework.sdk.enums.authorization;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ClientTypeEnum {

    USER("用户"),

    THIRD_PARTY("第三方");

    public final String description;
}
