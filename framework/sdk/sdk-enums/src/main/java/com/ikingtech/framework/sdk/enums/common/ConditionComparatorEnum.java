package com.ikingtech.framework.sdk.enums.common;

import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ConditionComparatorEnum {

    EQ("相等", "eq"),

    LIKE("模糊匹配", "like"),

    GT("大于", "gt"),

    GE("大于等于", "ge"),

    LT("小于", "lt"),

    LE("小于等于", "le"),

    LESS("小于", "lt"),

    LESS_EQUAL("小于等于", "le"),

    GREATER("大于", "gt"),

    GREATER_EQUAL("大于等于", "ge"),

    EQUAL("等于", "eq"),

    NOT_EQUAL("不等于", "not equal"),

    BETWEEN("介于", "between"),

    IN("在范围内", "in"),

    ALL("完全包含", Tools.Str.EMPTY),

    ANY("包含任意", Tools.Str.EMPTY);

    public final String description;

    public final String expression;
}
