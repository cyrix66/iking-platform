package com.ikingtech.framework.sdk.enums.common;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ElementTypeEnum {

    USER("用户"),

    ROLE("角色"),

    DEPT("组织架构"),

    POST("岗位");

    public final String description;
}
