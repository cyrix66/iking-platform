package com.ikingtech.framework.sdk.enums.common;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum FrameworkAgentTypeEnum {

    /**
     * 操作日志推送
     */
    OPERATION_LOG("op-log-report", "操作日志推送"),

    /**
     * 登录日志推送
     */
    AUTH_LOG("auth-log-report", "登录日志推送"),

    /**
     * 系统日志推送
     */
    SYSTEM_LOG("sys-log-report", "系统日志推送"),

    /**
     * 消息体注册
     */
    MESSAGE_TEMPLATE_REPORT("msg-report", "消息体注册"),

    /**
     * 发送消息
     */
    MESSAGE_SEND("msg-send", "发送消息"),

    /**
     * 创建定时任务
     */
    JOB_START("job-add", "创建定时任务"),

    /**
     * 删除定时任务
     */
    JOB_STOP("job-delete", "删除定时任务"),

    /**
     * 审批表单注册
     */
    APPROVE_FORM_REGISTER("approve-form-register", "审批表单注册"),

    /**
     * 审批表单注册
     */
    APPROVE_FORM_UNREGISTER("approve-form-unregister", "取消审批表单注册"),

    /**
     * 提交审批
     */
    APPROVE_PREVIEW("approve-preview", "预览审批"),

    /**
     * 提交审批
     */
    APPROVE_SUBMIT("approve-submit", "提交审批"),

    /**
     * 审批通过
     */
    APPROVE_PASS("approve-pass", "审批通过"),

    /**
     * 批量审批通过
     */
    APPROVE_PASS_BATCH("approve-pass-batch", "批量审批通过"),

    /**
     * 审批拒绝
     */
    APPROVE_REJECT("approve-reject", "审批拒绝"),

    /**
     * 审批退回
     */
    APPROVE_BACK("approve-back", "审批退回"),

    /**
     * 重新提交审批
     */
    APPROVE_RE_SUBMIT("approve-reSubmit", "重新提交审批"),

    /**
     * 保存附件
     */
    ATTACHMENT_SAVE("attachment-save", "保存附件"),

    /**
     * 移除附件
     */
    ATTACHMENT_REMOVE("attachment-delete", "移除附件");

    public final String agentName;

    public final String description;
}
