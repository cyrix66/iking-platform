package com.ikingtech.framework.sdk.enums.common;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum FrameworkServerFeedbackTypeEnum {

    APPROVE_PROCESS_CALLBACK_FEEDBACK("approve-process-callback-feedback", "审批回调"),

    JOB_FEEDBACK("job-feedback", "定时任务回调"),

    SIGN_EVENT_FEEDBACK("sign-event", "登录事件"),

    USER_INFO_EVENT_FEEDBACK("user-info-event", "用户信息回调"),

    USER_EXTENSION_INFO_COLLECT_FEEDBACK("user-extension-info-collect", "用户扩展信息收集");

    public final String serverAction;

    public final String description;
}
