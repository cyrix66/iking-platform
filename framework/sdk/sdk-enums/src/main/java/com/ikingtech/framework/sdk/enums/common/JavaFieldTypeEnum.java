package com.ikingtech.framework.sdk.enums.common;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum JavaFieldTypeEnum {

    STRING("字符串", "String", "Tools.Str.isNotBlank({})"),

    LIST("集合", "List<{}>", "Tools.Coll.isNotBlank({})"),

    DATE("日期(Date)", "Date", "null != {}"),

    LOCAL_DATE_TIME("日期(LocalDateTime)", "LocalDateTime", "null != {}"),

    LOCAL_DATE("日期(LocalDate)", "LocalDate", "null != {}"),

    LOCAL_TIME("日期(LocalTime)", "LocalTime", "null != {}"),

    BOOLEAN("布尔", "Boolean", "null != {}"),

    INT("数字(int)", "Integer", "null != {}"),

    LONG("数字(long)", "Long", "null != {}"),

    DOUBLE("数字(double)", "Double", "null != {}"),

    FLOAT("数字(float)", "Float", "null != {}"),

    MODEL("模型", "", "null != {}");

    public final String description;

    public final String modifier;

    public final String checkNull;
}
