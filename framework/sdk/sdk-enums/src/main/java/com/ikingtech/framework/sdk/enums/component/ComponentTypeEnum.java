package com.ikingtech.framework.sdk.enums.component;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ComponentTypeEnum {

    /**
     * 人员单选(部门)
     */
    SINGLE_USER_BY_DEPT_SELECT("人员单选(部门)"),

    /**
     * 人员单选(角色)
     */
    SINGLE_USER_BY_ROLE_SELECT("人员单选(角色)"),

    /**
     * 人员单选(岗位)
     */
    SINGLE_USER_BY_POST_SELECT("人员单选(岗位)"),

    /**
     * 人员单选(指定范围)
     */
    SINGLE_USER_BY_SPECIFIED_SELECT("人员单选(指定范围)"),

    /**
     * 人员多选(部门)
     */
    MULTI_USER_BY_DEPT_SELECT("人员多选(部门)"),

    /**
     * 人员多选(角色)
     */
    MULTI_USER_BY_ROLE_SELECT("人员多选(角色)"),

    /**
     * 人员多选(岗位)
     */
    MULTI_USER_BY_POST_SELECT("人员多选(岗位)"),

    /**
     * 人员多选(指定范围)
     */
    MULTI_USER_BY_SPECIFIED_SELECT("人员多选(指定范围)"),

    /**
     * 人员多选(多维度)
     */
    MULTI_USER_BY_DIMENSIONS_SELECT("人员多选(多维度)"),

    /**
     * 部门单选
     */
    SINGLE_DEPT_SELECT("部门单选"),

    /**
     * 部门多选
     */
    MULTI_DEPT_SELECT("部门多选"),

    /**
     * 角色单选
     */
    SINGLE_ROLE_SELECT("角色单选"),

    /**
     * 角色多选
     */
    MULTI_ROLE_SELECT("角色多选"),

    /**
     * 岗位单选
     */
    SINGLE_POST_SELECT("岗位单选"),

    /**
     * 岗位多选
     */
    MULTI_POST_SELECT("岗位多选");

    public final String description;
}
