package com.ikingtech.framework.sdk.enums.domain;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DomainEnum {

    PLATFORM("平台管理后台"),

    TENANT("租户管理后台"),

    APPLICATION("应用");

    public final String description;
}
