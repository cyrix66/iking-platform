package com.ikingtech.framework.sdk.enums.integration;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum PlatformConfigTypeEnum {

    WECHAT_MINI_CONFIG("微信小程序"),

    CDN_CONFIG("CDN");

    public final String description;
}
