package com.ikingtech.framework.sdk.enums.message;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum MessageChannelContentTypeEnum {

    SYSTEM("系统消息模板"),

    WECHAT_MINI_MESSAGE_TEMPLATE("微信小程序消息模板"),

    SELF("原始模板"),

    CUSTOMIZE("自定义");

    public final String description;
}
