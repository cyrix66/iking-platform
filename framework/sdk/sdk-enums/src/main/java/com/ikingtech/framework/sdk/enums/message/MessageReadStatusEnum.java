package com.ikingtech.framework.sdk.enums.message;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum MessageReadStatusEnum {

    /**
     * 已读
     */
    READ("已读"),

    /**
     * 未读
     */
    NO_READ("未读");

    public final String description;
}
