package com.ikingtech.framework.sdk.enums.message;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum MessageReceiverTypeEnum {

    /**
     * 按用户指定
     */
    USER("按用户指定"),

    /**
     * 按菜单指定
     */
    MENU("按菜单指定"),

    /**
     * 按角色指定
     */
    ROLE("按角色指定"),

    /**
     * 按部门指定
     */
    DEPARTMENT("按部门指定"),

    /**
     * 按岗位指定
     */
    POST("按岗位指定"),

    /**
     * 全部用户
     */
    ALL("全部用户");

    public final String description;
}
