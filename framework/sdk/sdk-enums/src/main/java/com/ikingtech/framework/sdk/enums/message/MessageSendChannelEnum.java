package com.ikingtech.framework.sdk.enums.message;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum MessageSendChannelEnum {

    SYSTEM("系统消息"),

    WECHAT_MINI_SUBSCRIBE("微信小程序-订阅消息"),

    SMS("短信"),

    DING_TALK_ROBOT("钉钉群机器人消息"),

    WEBHOOK("Webhook消息");

    public final String description;
}
