package com.ikingtech.framework.sdk.enums.pay;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum CashierSupplierEnum {

    ALI_PAY("支付宝支付"),

    WECHAT_PAY("微信支付"),

    ALL_IN_PAY("通联支付");

    public final String description;
}
