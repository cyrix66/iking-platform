package com.ikingtech.framework.sdk.enums.scffold;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ScaffoldModelCreationTypeEnum {

    MANUALLY("手动添加"),

    SYNC_WITH_DATASOURCE("数据源同步");

    public final String description;
}
