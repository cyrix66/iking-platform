package com.ikingtech.framework.sdk.enums.scffold;

import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ScaffoldSchemaTableFieldTypeEnum {

    VARCHAR("字符串(varchar)", "varchar", "  {} varchar({}) COMMENT '{}',\n", ScaffoldModelFieldTypeEnum.STRING),

    CHAR("字符串(char)", "char", "  {} char({}) COMMENT '{}',\n", ScaffoldModelFieldTypeEnum.STRING),


    TEXT("字符串(text)", "text", "  {} text COMMENT '{}',\n", ScaffoldModelFieldTypeEnum.STRING),


    LONG_TEXT("字符串(longText)", "longText", "  {} longText COMMENT '{}',\n", ScaffoldModelFieldTypeEnum.STRING),


    BLOB("字符串(blob)", "blob", "  {} blob COMMENT '{}',\n", ScaffoldModelFieldTypeEnum.STRING),


    DATETIME("日期(datetime)", "datetime", "  {} datetime COMMENT '{}',\n", ScaffoldModelFieldTypeEnum.LOCAL_DATE_TIME),


    TIMESTAMP("时间戳(timestamp)", "timestamp", "  {} timestamp COMMENT '{}',\n", ScaffoldModelFieldTypeEnum.LOCAL_DATE_TIME),


    INT("数字(int)", "int", "  {} int COMMENT '{}',\n", ScaffoldModelFieldTypeEnum.INT),


    TINYINT("数字(tinyint)", "tinyint", "  {} tinyint COMMENT '{}',\n", ScaffoldModelFieldTypeEnum.BOOLEAN),


    DOUBLE("数字(double)", "double", "  {} double COMMENT '{}',\n", ScaffoldModelFieldTypeEnum.DOUBLE),

    
    FLOAT("数字(float)", "float", "  {} float COMMENT '{}',\n", ScaffoldModelFieldTypeEnum.FLOAT);

    public final String description;

    public final String dbType;

    public final String sqlTemplate;

    public final ScaffoldModelFieldTypeEnum javaType;

    public String sqlFormat(String fieldName, String fieldLength, String fieldRemark) {
        return VARCHAR.equals(this) ? Tools.Str.format(this.sqlTemplate, fieldName, fieldLength, fieldRemark) : Tools.Str.format(this.sqlTemplate, fieldName, fieldRemark);
    }

    public static ScaffoldSchemaTableFieldTypeEnum valueOfDbType(String dbTypeStr) {
        for (ScaffoldSchemaTableFieldTypeEnum value : ScaffoldSchemaTableFieldTypeEnum.values()) {
            if (value.dbType.equals(dbTypeStr)) {
                return value;
            }
        }
        return null;
    }
}
