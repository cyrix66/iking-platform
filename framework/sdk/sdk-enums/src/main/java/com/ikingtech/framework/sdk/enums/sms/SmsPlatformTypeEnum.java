package com.ikingtech.framework.sdk.enums.sms;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum SmsPlatformTypeEnum {

    ALI("阿里云短信"),

    TENCENT("腾讯云短信"),

    SAN_YI_HENG_XIN("三一恒信短信平台");

    public final String description;
}
