package com.ikingtech.framework.sdk.enums.sms;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum SmsTemplateStatusEnum {

    APPROVING("审核中"),

    APPROVED("审核通过"),

    APPROVE_FAIL("审核失败");

    public final String description;
}
