package com.ikingtech.framework.sdk.enums.system.department;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DeptIntroductionTypeEnum {

    MANUALLY("手动录入"),

    EXTERNAL_LINK("外部链接"),

    ROUTER("内部路由");

    public final String description;
}
