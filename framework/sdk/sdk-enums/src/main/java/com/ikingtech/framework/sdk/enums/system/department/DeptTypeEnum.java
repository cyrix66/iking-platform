package com.ikingtech.framework.sdk.enums.system.department;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DeptTypeEnum {

    UNITY("单位"),

    SECTION("部门");

    public final String description;
}
