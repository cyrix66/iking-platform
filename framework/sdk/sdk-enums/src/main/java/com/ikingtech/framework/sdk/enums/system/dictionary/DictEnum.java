package com.ikingtech.framework.sdk.enums.system.dictionary;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DictEnum {

    SEX("性别"),

    DATA_SCOPE("数据权限"),

    MENU_TYPE("菜单类型"),

    MENU_JUMP_TYPE("菜单跳转类型"),

    MENU_VIEW_TYPE("菜单显示类型"),

    USER_LOCK_TYPE("用户锁定类型"),

    DICT_TYPE("字典类型"),

    TENANT_TYPE("租户类型"),

    TENANT_STATUS("租户状态"),

    DEPT_CATEGORY("部门类别");

    public final String description;
}
