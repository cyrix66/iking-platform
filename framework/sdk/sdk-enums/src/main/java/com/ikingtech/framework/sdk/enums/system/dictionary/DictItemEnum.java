package com.ikingtech.framework.sdk.enums.system.dictionary;

import com.ikingtech.framework.sdk.enums.system.menu.MenuJumpTypeEnum;
import com.ikingtech.framework.sdk.enums.system.menu.MenuTypeEnum;
import com.ikingtech.framework.sdk.enums.system.menu.MenuViewTypeEnum;
import com.ikingtech.framework.sdk.enums.system.role.DataScopeTypeEnum;
import com.ikingtech.framework.sdk.enums.system.tenant.TenantStatusEnum;
import com.ikingtech.framework.sdk.enums.system.tenant.TenantTypeEnum;
import com.ikingtech.framework.sdk.enums.system.user.UserLockTypeEnum;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DictItemEnum {

    // ====================性别===============
    MALE("男", DictEnum.SEX, 1),

    FEMALE("女", DictEnum.SEX, 2),

    // ====================数据权限===============

    ALL(DataScopeTypeEnum.ALL.description, DictEnum.DATA_SCOPE, 1),

    SECTION_WITH_CHILD(DataScopeTypeEnum.SECTION_WITH_CHILD.description, DictEnum.DATA_SCOPE, 2),

    SECTION(DataScopeTypeEnum.SECTION.description, DictEnum.DATA_SCOPE, 3),

    UNITY_WITH_CHILD(DataScopeTypeEnum.UNITY_WITH_CHILD.description, DictEnum.DATA_SCOPE, 2),

    UNITY(DataScopeTypeEnum.UNITY.description, DictEnum.DATA_SCOPE, 3),

    DEFINE(DataScopeTypeEnum.DEFINE.description, DictEnum.DATA_SCOPE, 4),


    // ====================菜单===============

    MENU(MenuTypeEnum.MENU.description, DictEnum.MENU_TYPE, 1),

    PAGE_TAB(MenuTypeEnum.PAGE_TAB.description, DictEnum.MENU_TYPE, 2),

    PAGE_BUTTON(MenuTypeEnum.PAGE_BUTTON.description, DictEnum.MENU_TYPE, 2),

    GLOBAL_BUTTON(MenuTypeEnum.GLOBAL_BUTTON.description, DictEnum.MENU_TYPE, 3),

    // ====================菜单跳转类型===============

    NONE(MenuJumpTypeEnum.NONE.description, DictEnum.MENU_JUMP_TYPE, 1),

    FRONT_ROUTE(MenuJumpTypeEnum.FRONT_ROUTE.description, DictEnum.MENU_JUMP_TYPE, 2),

    IFRAME(MenuJumpTypeEnum.IFRAME.description, DictEnum.MENU_JUMP_TYPE, 3),

    EXTERNAL_LINK(MenuJumpTypeEnum.EXTERNAL_LINK.description, DictEnum.MENU_JUMP_TYPE, 4),

    // ====================菜单显示类型===============

    BUSINESS(MenuViewTypeEnum.BUSINESS.description, DictEnum.MENU_VIEW_TYPE, 1),

    MANAGE(MenuViewTypeEnum.MANAGE.description, DictEnum.MENU_VIEW_TYPE, 2),

    // ====================用户锁定类型===============

    NO_LOCK(UserLockTypeEnum.NO_LOCK.description, DictEnum.USER_LOCK_TYPE, 1),

    PERMANENTLY_LOCK(UserLockTypeEnum.PERMANENTLY_LOCK.description, DictEnum.USER_LOCK_TYPE, 2),

    // ====================字典类型类型===============

    SYSTEM_DICT("预置字典", DictEnum.DICT_TYPE, 1),

    BUSINESS_DICT("系统字典", DictEnum.DICT_TYPE, 2),

    // ====================租户类型类型===============

    MASTER(TenantTypeEnum.MASTER.description, DictEnum.TENANT_TYPE, 1),

    TENANT_TYPE_NORMAL(TenantTypeEnum.NORMAL.description, DictEnum.TENANT_TYPE, 2),

    // ====================租户状态类型===============

    TENANT_STATUS_NORMAL(TenantStatusEnum.NORMAL.description, DictEnum.TENANT_STATUS, 1),

    DATA_MIGRATING(TenantStatusEnum.DATA_MIGRATING.description, DictEnum.TENANT_STATUS, 2),

    DATA_MIGRATE_FAIL(TenantStatusEnum.DATA_MIGRATE_FAIL.description, DictEnum.TENANT_STATUS, 3),

    FREEZE(TenantStatusEnum.FREEZE.description, DictEnum.TENANT_STATUS, 4);

    public final String description;

    public final DictEnum dict;

    public final Integer sortOrder;
}
