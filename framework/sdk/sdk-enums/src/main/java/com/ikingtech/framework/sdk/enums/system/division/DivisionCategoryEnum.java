package com.ikingtech.framework.sdk.enums.system.division;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DivisionCategoryEnum {

    NONE("无", 0),

    CITY_CENTER("主城区", 111),

    CITY_OUT("城乡接合区", 112),

    TOWN_CENTER_1("镇中心区", 121),

    TOWN_OUT("镇乡接合区", 122),

    SPECIAL_REGION("特殊区域", 123),

    TOWN_CENTER_2("乡中心区", 210),

    VILLAGE("村庄", 220);

    public final String description;

    public final Integer code;

    public static DivisionCategoryEnum valueOf(int code) {
        for (DivisionCategoryEnum value : DivisionCategoryEnum.values()) {
            if (value.code.equals(code)) {
                return value;
            }
        }
        return null;
    }
}
