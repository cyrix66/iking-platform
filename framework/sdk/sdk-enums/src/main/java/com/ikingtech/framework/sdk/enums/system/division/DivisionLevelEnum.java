package com.ikingtech.framework.sdk.enums.system.division;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DivisionLevelEnum {

    /**
     * 省
     */
    PROVINCE("省", "省、直辖市、自治区", 1),

    /**
     * 市
     */
    CITY("市", "地级市", 2),

    /**
     * 区
     */
    DISTRICT("区", "市辖区、县（旗）、县级市、自治县（自治旗）、特区、林区", 3),

    /**
     * 镇/街道
     */
    TOWN("镇", "镇、乡、民族乡、县辖区、街道", 4),

    /**
     * 村
     */
    VILLAGE("村", "村、居委会", 5),

    /**
     * 村
     */
    INVALID("未知", "无效的行政级别", -1);

    public final String description;

    public final String summary;

    public final Integer code;

    public static DivisionLevelEnum valueOf(int code) {
        for (DivisionLevelEnum value : DivisionLevelEnum.values()) {
            if (value.code.equals(code)) {
                return value;
            }
        }
        return null;
    }

    public String cacheKey() {
        return "division:" + this.name();
    }

    public String cacheKey(Long parentCode) {
        return "division:" + this.name() + ":" + parentCode;
    }
}
