package com.ikingtech.framework.sdk.enums.system.menu;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum RoleMenuAssignedStatusEnum {

    /**
     * 已分配
     */
    CHECKED("已分配"),

    /**
     * 部分分配
     */
    PARTIAL_CHECKED("部分分配"),

    /**
     * 未分配
     */
    UNCHECKED("未分配");

    public final String description;
}
