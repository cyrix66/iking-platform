package com.ikingtech.framework.sdk.enums.system.role;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DataScopeTypeEnum {

    SECTION("本部门数据"),

    SECTION_WITH_CHILD("本部门及以下数据"),

    UNITY("本单位数据"),

    UNITY_WITH_CHILD("本单位及以下数据"),

    DEFINE("指定部门数据"),

    ALL("全部数据");

    public final String description;
}
