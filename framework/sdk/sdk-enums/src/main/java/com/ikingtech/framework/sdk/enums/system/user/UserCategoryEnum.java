package com.ikingtech.framework.sdk.enums.system.user;

import com.ikingtech.framework.sdk.enums.domain.DomainEnum;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum UserCategoryEnum {

    PLATFORM_ADMINISTRATOR("平台管理员", DomainEnum.PLATFORM),

    TENANT_ADMINISTRATOR("租户管理员", DomainEnum.TENANT),

    NORMAL_USER("普通用户", null);

    public final String description;

    public final DomainEnum domain;
}
