package com.ikingtech.framework.sdk.enums.system.user;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum UserLockTypeEnum {

    /**
     * 未锁定
     */
    NO_LOCK("未锁定"),

    /**
     * 永久锁定
     */
    PERMANENTLY_LOCK("已锁定");

    public final String description;
}
