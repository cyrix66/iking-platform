package com.ikingtech.framework.sdk.enums.system.variable;

import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum VariableEnum {

    /**
     * 账户锁定时间
     */
    ACCOUNT_LOCK_EXPIRE("10", "账户锁定时间", VariableTypeEnum.SYSTEM),

    /**
     * Token有效期
     */
    TOKEN_EXPIRE("30", "Token有效期", VariableTypeEnum.SYSTEM),

    /**
     * Token续期类型
     */
    TOKEN_DELAY_TYPE(TokenDelayTypeEnum.AUTO.name(), "Token续期类型", VariableTypeEnum.SYSTEM),

    /**
     * 密码有效期
     */
    PASSWORD_EXPIRE("180", "密码有效期", VariableTypeEnum.SYSTEM),

    /**
     * 允许同时登录
     */
    ALLOW_MULTI_SIGN_IN(AllowMultiSignInEnum.ALLOW.name(), "允许同时登录", VariableTypeEnum.SYSTEM),

    /**
     * 用户默认密码
     */
    USER_DEFAULT_PASSWORD("Security123$%^", "用户默认密码", VariableTypeEnum.SYSTEM);

    public final String value;

    public final String description;

    public final VariableTypeEnum type;

    public static boolean validate(String name, String value) {
        VariableEnum variableEnum;
        try {
            variableEnum = valueOf(name);
        } catch (IllegalArgumentException e) {
            return false;
        }
        switch (variableEnum) {
            case ACCOUNT_LOCK_EXPIRE:
            case TOKEN_EXPIRE:
            case PASSWORD_EXPIRE:
                long parseLong;
                try {
                    parseLong = Long.parseLong(value);
                } catch (NumberFormatException e) {
                    return false;
                }
                return parseLong > 0;
            case TOKEN_DELAY_TYPE:
                try {
                    TokenDelayTypeEnum.valueOf(value);
                } catch (IllegalArgumentException e) {
                    return false;
                }
                return true;
            case ALLOW_MULTI_SIGN_IN:
                try {
                    AllowMultiSignInEnum.valueOf(value);
                } catch (IllegalArgumentException e) {
                    return false;
                }
                return true;
            case USER_DEFAULT_PASSWORD:
                return Tools.Str.isNotBlank(value);
            default:
                return true;
        }
    }

    public static Long resolveTokenExpire(Object cachedVariable) {
        try {
            return Long.valueOf((String) cachedVariable);
        } catch (NumberFormatException e) {
            return 30L;
        }
    }

    public static String resolveTokenDelayType(Object cachedVariable) {
        try {
            return (String) cachedVariable;
        } catch (Exception e) {
            return TokenDelayTypeEnum.AUTO.name();
        }
    }

    public static Long resolvePasswordExpire(Object cachedVariable) {
        try {
            return Long.valueOf((String) cachedVariable);
        } catch (NumberFormatException e) {
            return 180L;
        }
    }

    public static String resolveAllowMultiSignIn(Object cachedVariable) {
        try {
            return (String) cachedVariable;
        } catch (NumberFormatException e) {
            return AllowMultiSignInEnum.ALLOW.name();
        }
    }

    public static String resolveUserDefaultPassword(Object cachedVariable) {
        try {
            String password = (String) cachedVariable;
            return Tools.Str.isBlank(password) ? USER_DEFAULT_PASSWORD.value : password;
        } catch (NumberFormatException e) {
            return USER_DEFAULT_PASSWORD.value;
        }
    }
}
