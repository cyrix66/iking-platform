package com.ikingtech.framework.sdk.enums.wechat;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum WechatMiniSubscribeMessageTemplateTypeEnum {

    ONE_TIME("一次性订阅"),

    LONG_TERM("长期订阅");

    public final String description;
}
