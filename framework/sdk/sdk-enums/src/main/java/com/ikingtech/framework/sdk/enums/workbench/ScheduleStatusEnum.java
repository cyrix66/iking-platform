package com.ikingtech.framework.sdk.enums.workbench;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ScheduleStatusEnum {

    NOT_STARED("未开始"),

    STARTED("已开始"),

    OVERDUE("已过期");

    public final String description;
}
