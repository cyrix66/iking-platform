package com.ikingtech.framework.sdk.enums.workbench;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ScheduleTypeEnum {

    NORMAL("正常"),

    OVERTIME("加班"),

    VACATION("请假"),

    TRAVEL("出差"),

    MEETING("会议"),

    TRAINING("培训"),

    OTHER("其他");

    public final String description;
}
