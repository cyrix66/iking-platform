package com.ikingtech.framework.sdk.extension.collector;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public interface ExtensionInfoCollector {

    List<String> queryForIds(Object queryParam);

    Map<String, Map<String, Object>> queryByIds(List<String> ids);

    Map<String, Object> queryById(String id);
}
