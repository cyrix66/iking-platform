package com.ikingtech.framework.sdk.extension.collector;

import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

import static com.ikingtech.framework.sdk.cache.constants.CacheConstants.EXTENSION_INFO_COLLECTOR;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class ExtensionInfoCollectorRegister implements ApplicationRunner {

    private final StringRedisTemplate redisTemplate;

    private final List<ExtensionInfoCollector> collectors;

    @Value("${spring.application.name}")
    private String serverName;

    @Override
    public void run(ApplicationArguments args) {
        this.redisTemplate.opsForSet().remove(EXTENSION_INFO_COLLECTOR, this.serverName);
        if (Tools.Coll.isNotBlank(this.collectors)) {
            this.redisTemplate.opsForSet().add(EXTENSION_INFO_COLLECTOR, this.serverName);
        }
    }
}
