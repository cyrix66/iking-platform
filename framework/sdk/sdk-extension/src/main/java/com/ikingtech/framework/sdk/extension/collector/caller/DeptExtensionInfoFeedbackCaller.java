package com.ikingtech.framework.sdk.extension.collector.caller;

import com.ikingtech.framework.sdk.context.security.Identity;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.core.support.LogHelper;
import com.ikingtech.framework.sdk.extension.collector.model.ExtensionInfoCollectParam;
import com.ikingtech.framework.sdk.enums.common.FrameworkServerFeedbackTypeEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.support.server.FrameworkServerFeedbackCaller;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Set;

import static com.ikingtech.framework.sdk.cache.constants.CacheConstants.EXTENSION_INFO_COLLECTOR;
import static com.ikingtech.framework.sdk.context.constant.SecurityConstants.*;
import static com.ikingtech.framework.sdk.enums.common.FrameworkServerFeedbackTypeEnum.USER_EXTENSION_INFO_COLLECT_FEEDBACK;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class DeptExtensionInfoFeedbackCaller implements FrameworkServerFeedbackCaller {

    private final StringRedisTemplate redisTemplate;

    private final LoadBalancerClient loadBalancerClient;

    @Override
    public R<Object> call(Object data) {
        ExtensionInfoCollectParam param = (ExtensionInfoCollectParam) data;
        // 获取用户提醒集合
        Set<String> reporters = this.redisTemplate.opsForSet().members(EXTENSION_INFO_COLLECTOR);
        if (Tools.Coll.isNotBlank(reporters)) {
            // 遍历用户提醒集合
            Tools.Coll.distinct(reporters).forEach(reporter -> {
                // 选择服务实例
                ServiceInstance client = this.loadBalancerClient.choose(reporter);
                if (null != client) {
                    try {
                        // 发送HTTP请求
                        String resultStr = Tools.Http.post(Tools.Http.SCHEMA_HTTP + client.getHost() + ":" + client.getPort() + "/user/extension/resolve",
                                Tools.Json.toJsonStr(param),
                                Tools.Coll.newMap(
                                        Tools.Coll.Kv.of(HEADER_INNER_IDENTITY, Identity.inner()),
                                        Tools.Coll.Kv.of(HEADER_CALLER, "INNER"),
                                        Tools.Coll.Kv.of(HEADER_TENANT_CODE, Me.tenantCode())
                                ));
                        LogHelper.info(USER_EXTENSION_INFO_COLLECT_FEEDBACK.serverAction, "用户扩展信息回调结果[{}]", resultStr);
                    } catch (Exception e) {
                        LogHelper.info(USER_EXTENSION_INFO_COLLECT_FEEDBACK.serverAction,"用户扩展信息回调异常[{}]", e.getMessage());
                    }
                }
            });
        }
        return R.ok();
    }

    @Override
    public FrameworkServerFeedbackTypeEnum type() {
        return USER_EXTENSION_INFO_COLLECT_FEEDBACK;
    }
}
