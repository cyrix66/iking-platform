package com.ikingtech.framework.sdk.extension.collector.model;

import com.ikingtech.framework.sdk.base.model.BatchParam;
import com.ikingtech.framework.sdk.utils.Tools;

import java.io.Serializable;

/**
 * @author tie yan
 */
public record ExtensionInfoCollectParam(UserExtensionInfoCollectTypeEnum type,
                                        Object queryParam,
                                        BatchParam<String> ids,
                                        String id) implements Serializable {

    public ExtensionInfoCollectParam(Object queryParam) {
        this(UserExtensionInfoCollectTypeEnum.QUERY_FOR_IDS, queryParam, BatchParam.empty(), Tools.Str.EMPTY);
    }

    public ExtensionInfoCollectParam(BatchParam<String> ids) {
        this(UserExtensionInfoCollectTypeEnum.QUERY_BY_IDS, null, ids, Tools.Str.EMPTY);
    }

    public ExtensionInfoCollectParam(String id) {
        this(UserExtensionInfoCollectTypeEnum.QUERY_BY_ID, null, BatchParam.empty(), id);
    }

    public enum UserExtensionInfoCollectTypeEnum {

        QUERY_FOR_IDS,

        QUERY_BY_IDS,

        QUERY_BY_ID;
    }
}
