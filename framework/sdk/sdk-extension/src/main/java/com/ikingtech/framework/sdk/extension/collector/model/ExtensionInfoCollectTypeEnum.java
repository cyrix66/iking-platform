package com.ikingtech.framework.sdk.extension.collector.model;

/**
 * @author tie yan
 */
public enum ExtensionInfoCollectTypeEnum {

    QUERY_FOR_IDS,

    QUERY_BY_IDS,

    QUERY_BY_ID;
}
