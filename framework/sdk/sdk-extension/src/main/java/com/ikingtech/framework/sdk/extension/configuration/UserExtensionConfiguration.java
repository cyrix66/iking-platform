package com.ikingtech.framework.sdk.extension.configuration;

import com.ikingtech.framework.sdk.extension.collector.ExtensionInfoCollector;
import com.ikingtech.framework.sdk.extension.collector.ExtensionInfoCollectorRegister;
import com.ikingtech.framework.sdk.extension.collector.ExtensionInfoCollectResolver;
import com.ikingtech.framework.sdk.extension.collector.caller.DeptExtensionInfoFeedbackCaller;
import com.ikingtech.framework.sdk.extension.properties.UserExtensionProperties;
import com.ikingtech.framework.sdk.extension.reminder.UserInfoEventReminder;
import com.ikingtech.framework.sdk.extension.reminder.UserInfoEventReminderRegister;
import com.ikingtech.framework.sdk.extension.reminder.UserInfoEventReminderResolver;
import com.ikingtech.framework.sdk.extension.reminder.caller.UserInfoEventFeedbackCaller;
import com.ikingtech.framework.sdk.web.support.server.FrameworkServerFeedbackCaller;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@EnableConfigurationProperties({UserExtensionProperties.class})
public class UserExtensionConfiguration {

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".user", name = "event-enable", havingValue = "true")
    public UserInfoEventReminderRegister userInfoEventReminderRegister(StringRedisTemplate redisTemplate, List<UserInfoEventReminder> reminders) {
        return new UserInfoEventReminderRegister(redisTemplate, reminders);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".user", name = "event-enable", havingValue = "true")
    public UserInfoEventReminderResolver userInfoEventReminderResolver(List<UserInfoEventReminder> reminders) {
        return new UserInfoEventReminderResolver(reminders);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    public FrameworkServerFeedbackCaller userInfoEventFeedbackCaller(StringRedisTemplate redisTemplate, LoadBalancerClient loadBalancerClient) {
        return new UserInfoEventFeedbackCaller(redisTemplate, loadBalancerClient);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".user", name = "extension-enable", havingValue = "true")
    public ExtensionInfoCollectorRegister userExtensionInfoCollectorRegister(StringRedisTemplate redisTemplate, List<ExtensionInfoCollector> collectors) {
        return new ExtensionInfoCollectorRegister(redisTemplate, collectors);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".user", name = "extension-enable", havingValue = "true")
    public ExtensionInfoCollectResolver userExtensionInfoCollectorResolver(List<ExtensionInfoCollector> collectors) {
        return new ExtensionInfoCollectResolver(collectors);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    public FrameworkServerFeedbackCaller userExtensionInfoFeedbackCaller(StringRedisTemplate redisTemplate, LoadBalancerClient loadBalancerClient) {
        return new DeptExtensionInfoFeedbackCaller(redisTemplate, loadBalancerClient);
    }
}
