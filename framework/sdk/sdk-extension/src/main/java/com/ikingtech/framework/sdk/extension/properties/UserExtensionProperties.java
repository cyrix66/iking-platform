package com.ikingtech.framework.sdk.extension.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.Serial;
import java.io.Serializable;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@Data
@ConfigurationProperties(GLOBAL_CONFIG_PREFIX + ".user")
public class UserExtensionProperties implements Serializable {

    @Serial
    private static final long serialVersionUID = -5487014679242111494L;

    private Boolean eventEnable;
    
    private Boolean extensionEnable;
}
