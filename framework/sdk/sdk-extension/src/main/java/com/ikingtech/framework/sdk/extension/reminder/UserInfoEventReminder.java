package com.ikingtech.framework.sdk.extension.reminder;

import com.ikingtech.framework.sdk.extension.reminder.model.UserInfoEvent;

/**
 * @author tie yan
 */
public interface UserInfoEventReminder {

    void add(UserInfoEvent user);

    void delete(String id);

    void update(UserInfoEvent user);
}
