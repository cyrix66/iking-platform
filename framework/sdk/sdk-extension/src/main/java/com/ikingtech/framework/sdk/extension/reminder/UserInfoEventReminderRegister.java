package com.ikingtech.framework.sdk.extension.reminder;

import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

import static com.ikingtech.framework.sdk.cache.constants.CacheConstants.MASTER_DATA_EVENT_REMINDER;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class UserInfoEventReminderRegister implements ApplicationRunner {

    private final StringRedisTemplate redisTemplate;

    private final List<UserInfoEventReminder> reminders;

    @Value("${spring.application.name}")
    private String serverName;

    @Override
    public void run(ApplicationArguments args) {
        this.redisTemplate.opsForSet().remove(MASTER_DATA_EVENT_REMINDER, this.serverName);
        if (Tools.Coll.isNotBlank(this.reminders)) {
            this.redisTemplate.opsForSet().add(MASTER_DATA_EVENT_REMINDER, this.serverName);
        }
    }
}
