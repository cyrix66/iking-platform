package com.ikingtech.framework.sdk.extension.reminder;

import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.core.support.LogHelper;
import com.ikingtech.framework.sdk.extension.reminder.model.UserInfoEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

import static com.ikingtech.framework.sdk.cache.constants.CacheConstants.MASTER_DATA_EVENT_REMINDER;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class UserInfoEventReminderResolver {

    private final List<UserInfoEventReminder> reminders;

    @PostMapping("/user/event/resolve")
    public R<Object> resolve(@RequestBody UserInfoEvent userInfoEvent) {
        if (null == userInfoEvent) {
            LogHelper.info(MASTER_DATA_EVENT_REMINDER, "事件信息为空");
            throw new FrameworkException("userEventIsNull");
        }
        switch (userInfoEvent.getType()) {
            // 执行添加事件操作
            case ADD -> this.reminders.forEach(reminder -> reminder.add(userInfoEvent));
            // 执行删除事件操作
            case DELETE -> this.reminders.forEach(reminder -> reminder.delete(userInfoEvent.getId()));
            // 执行更新事件操作
            case UPDATE -> this.reminders.forEach(reminder -> reminder.update(userInfoEvent));
        };
        return R.ok();
    }
}
