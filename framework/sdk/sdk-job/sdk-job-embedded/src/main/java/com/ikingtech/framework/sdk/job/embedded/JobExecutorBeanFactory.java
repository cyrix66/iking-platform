package com.ikingtech.framework.sdk.job.embedded;

import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.job.embedded.annotation.Job;
import com.ikingtech.framework.sdk.job.model.rpc.JobExecutorBean;
import com.ikingtech.framework.sdk.utils.Tools;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.MethodIntrospector;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.AnnotatedElementUtils;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.JOB_HANDLER_REPORT_RUNNER_ORDER;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class JobExecutorBeanFactory implements ApplicationRunner, ApplicationContextAware, Ordered {

    protected static final Map<String, JobExecutorBean> JOB_EXECUTOR_BEAN_MAP = new HashMap<>();

    private ApplicationContext applicationContext;

    protected static String clientId = "";

    @Override
    public void run(ApplicationArguments args) {
        Map<String, Object> allBeans = this.applicationContext.getBeansOfType(Object.class);
        setClientId(this.applicationContext.getEnvironment().getProperty("spring.application.name"));

        allBeans.forEach((beanName, bean) -> {
            Map<Method, Job> annotatedMethods;
            try {
                annotatedMethods = MethodIntrospector.selectMethods(bean.getClass(), (MethodIntrospector.MetadataLookup<Job>) method -> AnnotatedElementUtils.findMergedAnnotation(method, Job.class));
            } catch (Exception e) {
                throw new FrameworkException(Tools.Str.format("job handler retrieve error for bean[{}]{}.", beanName, e.getMessage()));
            }
            if (!annotatedMethods.isEmpty()) {
                for (Map.Entry<Method, Job> methodEntry : annotatedMethods.entrySet()) {
                    Method executeMethod = methodEntry.getKey();
                    Job job = methodEntry.getValue();
                    JobExecutorBean jobExecutorBean = new JobExecutorBean();
                    jobExecutorBean.setHandler(job.handler());
                    jobExecutorBean.setBean(bean);
                    jobExecutorBean.setMethod(executeMethod);
                    JOB_EXECUTOR_BEAN_MAP.put(job.handler(), jobExecutorBean);
                }
            }
        });
    }

    public static String getClientId() {
        return clientId;
    }

    private static void setClientId(String clientId) {
        JobExecutorBeanFactory.clientId = clientId;
    }

    public static JobExecutorBean getJobExecutor(String handler) {
        return JOB_EXECUTOR_BEAN_MAP.get(handler);
    }

    @Override
    public int getOrder() {
        return JOB_HANDLER_REPORT_RUNNER_ORDER;
    }

    @Override
    public void setApplicationContext(@NonNull ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
