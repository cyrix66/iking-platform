package com.ikingtech.framework.sdk.job.embedded;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.job.model.rpc.JobExecuteInfo;
import com.ikingtech.framework.sdk.job.model.rpc.JobExecutorBean;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import org.springframework.web.bind.annotation.RequestBody;

import java.lang.reflect.InvocationTargetException;

/**
 * @author tie yan
 */
@ApiController(value = "/job", name = "调度中心-任务回调", description = "调度中心-任务回调")
public class JobResolver {

    @PostRequest(order = 1,value = "/resolve", summary = "解析任务", description = "解析任务")
    public R<Object> resolve(@RequestBody JobExecuteInfo jobParam) {
        if (null == jobParam) {
            return R.failed("job resolver param is blank");
        }
        JobExecutorBean jobExecutorBean = JobExecutorBeanFactory.getJobExecutor(jobParam.getExecutorHandler());
        if (null != jobExecutorBean) {
            try {
                jobExecutorBean.getMethod().invoke(jobExecutorBean.getBean(), jobParam.getExecuteParam());
                return R.ok();
            } catch (IllegalAccessException | InvocationTargetException e) {
                return R.failed(Tools.Str.format("job executor invoke exception[{}][{}]", jobParam, e.getMessage()));
            }
        }
        return R.failed(Tools.Str.format("job executor not found[{}][{}]", jobParam, jobParam.getExecutorHandler()));
    }

}
