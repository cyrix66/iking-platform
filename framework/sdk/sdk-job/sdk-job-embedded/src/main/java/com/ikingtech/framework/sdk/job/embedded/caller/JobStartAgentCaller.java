package com.ikingtech.framework.sdk.job.embedded.caller;

import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.job.model.JobDTO;
import com.ikingtech.framework.sdk.job.rpc.api.JobRpcApi;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentCaller;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class JobStartAgentCaller implements FrameworkAgentCaller {

    private final JobRpcApi rpcApi;

    @Override
    public R<Object> call(Object data) {
        try {
            return this.rpcApi.start((JobDTO) data);
        } catch (Exception e) {
            throw new FrameworkException(e.getMessage());
        }
    }

    @Override
    public FrameworkAgentTypeEnum type() {
        return FrameworkAgentTypeEnum.JOB_START;
    }
}
