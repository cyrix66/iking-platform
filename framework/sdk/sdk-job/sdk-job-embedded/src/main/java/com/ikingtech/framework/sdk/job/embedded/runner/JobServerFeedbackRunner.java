package com.ikingtech.framework.sdk.job.embedded.runner;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkServerFeedbackTypeEnum;
import com.ikingtech.framework.sdk.job.embedded.JobResolver;
import com.ikingtech.framework.sdk.job.model.rpc.JobExecuteInfo;
import com.ikingtech.framework.sdk.web.support.server.FrameworkServerFeedbackRunner;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class JobServerFeedbackRunner implements FrameworkServerFeedbackRunner {

    private final JobResolver resolver;

    @Override
    public R<Object> run(Object jobParam) {
        return this.resolver.resolve((JobExecuteInfo)jobParam);
    }

    @Override
    public FrameworkServerFeedbackTypeEnum type() {
        return FrameworkServerFeedbackTypeEnum.JOB_FEEDBACK;
    }
}
