package com.ikingtech.framework.sdk.job.rpc.api;

import com.ikingtech.framework.sdk.job.api.JobApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "JobRpcApi", path = "/job")
public interface JobRpcApi extends JobApi {
}
