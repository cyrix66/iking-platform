package com.ikingtech.framework.sdk.job.scheduler;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class ScheduledJob<T extends Serializable> implements Serializable {

    @Serial
    private static final long serialVersionUID = 976477847972267965L;

    /**
     * 任务流水号
     */
    private String serialNo;

    /**
     * 任务执行时间
     */
    private Long executeTime;

    /**
     * 发起方透传参数
     */
    private T extension;
}
