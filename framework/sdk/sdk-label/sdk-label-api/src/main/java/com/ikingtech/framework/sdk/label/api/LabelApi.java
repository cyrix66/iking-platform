package com.ikingtech.framework.sdk.label.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.label.model.LabelAssignDTO;
import com.ikingtech.framework.sdk.label.model.LabelQueryParamDTO;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import com.ikingtech.framework.sdk.label.model.LabelDTO;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author tie yan
 */
public interface LabelApi {

    /**
     * 添加标签信息
     *
     * @param label 标签信息
     * @return 标签信息
     */
    @PostRequest(order = 1, value = "/add", summary = "添加标签信息", description = "添加标签信息")
    R<String> add(@Parameter(name = "label", description = "标签信息")
                  @RequestBody LabelDTO label);

    /**
     * 删除标签
     *
     * @param id 标签编号
     * @return 标签信息
     */
    @PostRequest(order = 2, value = "/delete", summary = "删除标签", description = "删除标签")
    R<Object> delete(@Parameter(name = "id", description = "编号")
                     @RequestBody String id);

    /**
     * 更新标签信息
     *
     * @param label 标签信息
     * @return 标签信息
     */
    @PostRequest(order = 3, value = "/update", summary = "更新标签信息", description = "更新标签信息")
    R<Object> update(@Parameter(name = "label", description = "标签信息")
                     @RequestBody LabelDTO label);

    /**
     * 分配标签
     *
     * @param labelAssign 标签信息
     * @return 标签信息
     */
    @PostRequest(order = 4, value = "/assign", summary = "分配标签", description = "分配标签")
    R<Object> assign(@Parameter(name = "post", description = "标签信息")
                     @RequestBody LabelAssignDTO labelAssign);

    /**
     * 根据查询条件获取标签列表
     *
     * @param queryParam 查询条件
     * @return 标签列表
     */
    @PostRequest(order = 5, value = "/list/page", summary = "根据查询条件获取标签列表", description = "根据查询条件获取标签列表")
    R<List<LabelDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                           @RequestBody LabelQueryParamDTO queryParam);
}
