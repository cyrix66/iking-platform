package com.ikingtech.framework.sdk.label.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author yang
 */
@Data
@Schema(name = "LabelDTO", description = "标签信息")
public class LabelDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 3548427426771971768L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "name", description = "标签名称")
    private String name;

    @Schema(name = "businessKey", description = "业务标识")
    private String businessKey;

    @Schema(name = "businessName", description = "业务名称")
    private String businessName;
}
