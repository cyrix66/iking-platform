package com.ikingtech.framework.sdk.label.embedded;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class LabelRelationResourceQueryParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 9017577275628591594L;

    private String name;
}
