package com.ikingtech.framework.sdk.limit.model;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 限流结果
 *
 * @author zhangqiang
 */
@Data
public class RateLimitResult implements Serializable {
    @Serial
    private static final long serialVersionUID = -3709361525311728991L;

    private Boolean success;

    private String errMsg;

}
