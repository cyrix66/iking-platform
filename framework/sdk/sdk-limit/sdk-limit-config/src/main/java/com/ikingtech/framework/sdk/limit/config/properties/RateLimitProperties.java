package com.ikingtech.framework.sdk.limit.config.properties;

import com.ikingtech.framework.sdk.context.constant.CommonConstants;
import com.ikingtech.framework.sdk.enums.limit.RateLimitPriorityEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @author zhangqiang
 */
@Data
@ConfigurationProperties(prefix = CommonConstants.GLOBAL_CONFIG_PREFIX + ".rate-limit")
public class RateLimitProperties {
    /**
     * 是否开启限流
     */
    private boolean enable;
    /**
     * 优先级
     */
    private RateLimitPriorityEnum priority = RateLimitPriorityEnum.COMPLEMENT;
    /**
     * 限流前缀
     */
    private String cacheKeyPrefix = "rate_limit";
    /**
     * 限流时间(s)
     */
    private Integer time = 60;
    /**
     * 限流次数
     */
    private Integer count = 100;
    /**
     * 需要限流的地址
     */
    private List<Request> includeList;
    /**
     * 不需要限流的地址
     */
    private List<Request> excludeList;

    @Data
    public static class Request {
        /**
         * 请求类型
         */
        private String type;
        /**
         * uri
         */
        private String uri;
        /**
         * 限流时间(s)
         */
        private Integer time;
        /**
         * 限流次数
         */
        private Integer count;
    }

}
