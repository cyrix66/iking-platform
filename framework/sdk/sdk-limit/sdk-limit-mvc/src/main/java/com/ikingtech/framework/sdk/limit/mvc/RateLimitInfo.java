package com.ikingtech.framework.sdk.limit.mvc;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 限流信息
 *
 * @author zhangqiang
 */
@Data
public class RateLimitInfo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1552540120837517016L;
    /**
     * 限流时间
     */
    private Integer time;
    /**
     * 限流次数
     */
    private Integer count;
    /**
     * 请求方式
     */
    private String requestType;
    /**
     * 请求地址
     */
    private String mapping;
    /**
     * 带服务名的请求地址
     */
    private String fullMapping;
    /**
     * 应用名
     */
    private String applicationName;
    /**
     * 应用服务器ip地址
     */
    private String selfServerAddr;
    /**
     * 应用所在服务器端口
     */
    private String selfServerPort;
}
