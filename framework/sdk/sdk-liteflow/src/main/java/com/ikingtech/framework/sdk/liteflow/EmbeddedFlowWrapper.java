package com.ikingtech.framework.sdk.liteflow;

import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.utils.Tools;
import com.yomahub.liteflow.builder.el.LiteFlowChainELBuilder;
import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.flow.LiteflowResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

import java.util.Map;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class EmbeddedFlowWrapper implements ApplicationRunner {

    private final FlowExecutor flowExecutor;

    @Override
    public void run(ApplicationArguments args) {
//        RULE_CHAIN_MAP.forEach((name, chain) -> LiteFlowChainELBuilder.createChain().setChainId(name).setEL(chain).build());
    }

    public <T> void execute(String ruleChainName, Object param, Class<T> context) {
        LiteflowResponse response = flowExecutor.execute2Resp(ruleChainName, param, context);
        if (!response.isSuccess()) {
            throw new FrameworkException("流程执行失败[{}]", response.getMessage());
        }
    }
}
