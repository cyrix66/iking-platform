package com.ikingtech.framework.sdk.liteflow;

import lombok.Data;

import java.io.File;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class ScaffoldModelPublishContext implements Serializable {

    private static final long serialVersionUID = -4688418790216166200L;

    private List<String> fullClassNames;

    private String workDirName;

    private File workDir;

    private String jarFilePath;
}
