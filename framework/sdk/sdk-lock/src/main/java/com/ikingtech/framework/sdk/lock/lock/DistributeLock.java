package com.ikingtech.framework.sdk.lock.lock;

/**
 * @author tie yan
 */
public interface DistributeLock {

    /**
     * Tries to acquire a lock for the given key.
     *
     * @param lockKey key to acquire lock for
     * @param expireSecond time in seconds after which lock will expire
     *
     * @return true if lock is acquired successfully, false otherwise
     */
    boolean tryLock(String lockKey, Long expireSecond);

    /**
     * Releases the lock for the given key.
     *
     * @param lockKey key to release lock for
     */
    void unlock(String lockKey);
}
