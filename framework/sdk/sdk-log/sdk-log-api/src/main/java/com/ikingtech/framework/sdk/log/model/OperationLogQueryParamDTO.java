package com.ikingtech.framework.sdk.log.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "OperationLogQueryParamDTO", description = "操作日志查询参数")
public class OperationLogQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -6547062484109627663L;

    @Schema(name = "method", description = "方法名称")
    private String method;

    @Schema(name = "requestMethod", description = "请求方式")
    private String requestMethod;

    @Schema(name = "requestUrl", description = "请求地址")
    private String requestUrl;

    @Schema(name = "operateUserId", description = "操作用户名id")
    private String operateUserId;

    @Schema(name = "operateUsername", description = "操作用户名")
    private String operateUsername;

    @Schema(name = "ip", description = "客户端IP地址")
    private String ip;

    @Schema(name = "location", description = "客户端IP归属地")
    private String location;

    @Schema(name = "success", description = "是否操作成功")
    private Boolean success;

    @Schema(name = "message", description = "返回信息")
    private String message;

    @Schema(name = "operationStartTime", description = "开始时间（yyyy-MM-dd HH:mm:ss）")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime operationStartTime;

    @Schema(name = "operationEndTime", description = "结束时间（yyyy-MM-dd HH:mm:ss）")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime operationEndTime;
}
