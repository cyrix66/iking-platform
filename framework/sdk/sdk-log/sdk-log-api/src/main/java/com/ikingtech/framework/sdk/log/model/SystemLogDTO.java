package com.ikingtech.framework.sdk.log.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@Schema(name = "SystemLogDTO", description = "系统日志信息")
public class SystemLogDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1356731577566688159L;

    @Schema(name = "id", description = "日志主键")
    private String id;

    @Schema(name = "traceId", description = "链路编号")
    private String traceId;

    @Schema(name = "packageName", description = "包名")
    private String packageName;

    @Schema(name = "method", description = "方法")
    private String method;

    @Schema(name = "message", description = "日志内容")
    private String message;

    @Schema(name = "exceptionStack", description = "异常堆栈")
    private String exceptionStack;

    @Schema(name = "executeTime", description = "执行时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime executeTime;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;
}
