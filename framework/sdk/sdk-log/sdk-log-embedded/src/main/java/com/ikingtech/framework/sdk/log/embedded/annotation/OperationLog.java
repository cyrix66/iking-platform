package com.ikingtech.framework.sdk.log.embedded.annotation;

import java.lang.annotation.*;

/**
 * @author tie yan
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OperationLog {

    String value() default "";

    String dataId() default "";

    boolean saveRequestData() default true;

    boolean saveResponseData() default true;
}
