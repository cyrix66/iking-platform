package com.ikingtech.framework.sdk.log.embedded.caller;

import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.log.model.rpc.SystemLogBatchReportParam;
import com.ikingtech.framework.sdk.log.rpc.api.LogRpcApi;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentCaller;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class SystemLogReportAgentCaller implements FrameworkAgentCaller {

    private final LogRpcApi rpcApi;

    @Override
    public R<Object> call(Object data) {
        try {
            return this.rpcApi.reportSystemLog((SystemLogBatchReportParam) data);
        } catch (Exception e) {
            throw new FrameworkException(e.getMessage());
        }
    }

    @Override
    public FrameworkAgentTypeEnum type() {
        return FrameworkAgentTypeEnum.SYSTEM_LOG;
    }
}
