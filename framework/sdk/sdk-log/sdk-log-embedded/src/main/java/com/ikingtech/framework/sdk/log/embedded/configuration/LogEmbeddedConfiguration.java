package com.ikingtech.framework.sdk.log.embedded.configuration;

import com.ikingtech.framework.sdk.log.api.LogApi;
import com.ikingtech.framework.sdk.log.embedded.appender.LogAppender;
import com.ikingtech.framework.sdk.log.embedded.aspect.AuthLogAspect;
import com.ikingtech.framework.sdk.log.embedded.aspect.OperationLogAspect;
import com.ikingtech.framework.sdk.log.embedded.caller.AuthLogReportAgentCaller;
import com.ikingtech.framework.sdk.log.embedded.caller.OperationLogReportAgentCaller;
import com.ikingtech.framework.sdk.log.embedded.caller.SystemLogReportAgentCaller;
import com.ikingtech.framework.sdk.log.embedded.properties.LogProperties;
import com.ikingtech.framework.sdk.log.embedded.runner.AuthLogReportAgentRunner;
import com.ikingtech.framework.sdk.log.embedded.runner.OperationLogReportAgentRunner;
import com.ikingtech.framework.sdk.log.embedded.runner.SystemLogReportAgentRunner;
import com.ikingtech.framework.sdk.log.rpc.api.LogRpcApi;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentCaller;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentRunner;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@EnableConfigurationProperties({LogProperties.class})
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".log", name = {"enabled"}, havingValue = "true", matchIfMissing = true)
public class LogEmbeddedConfiguration {

    @Bean(initMethod = "init")
    public OperationLogAspect operationLogAspect(LogProperties properties, MessageSource messageSource) {
        return new OperationLogAspect(properties, messageSource);
    }

    @Bean(initMethod = "init")
    public AuthLogAspect authLogAspect(MessageSource messageSource) {
        return new AuthLogAspect(messageSource);
    }

    @Bean
    public LogAppender appender(LogProperties properties) {
        return new LogAppender(properties);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    @ConditionalOnExpression("!'${spring.application.name}'.equals('server')")
    public FrameworkAgentCaller authLogReportAgentCaller(LogRpcApi rpcApi) {
        return new AuthLogReportAgentCaller(rpcApi);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    @ConditionalOnExpression("!'${spring.application.name}'.equals('server')")
    public FrameworkAgentCaller systemLogReportAgentCaller(LogRpcApi rpcApi) {
        return new SystemLogReportAgentCaller(rpcApi);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    @ConditionalOnExpression("!'${spring.application.name}'.equals('server')")
    public FrameworkAgentCaller operationLogReportAgentCaller(LogRpcApi rpcApi) {
        return new OperationLogReportAgentCaller(rpcApi);
    }

    @Bean
    @ConditionalOnExpression("null == '${iking.framework.web.arch}' || '${iking.framework.web.arch}'.equals('') || '${iking.framework.web.arch}'.equals('single') || '${spring.application.name}'.equals('server')")
    public FrameworkAgentRunner authLogReportAgentRunner(LogApi api) {
        return new AuthLogReportAgentRunner(api);
    }

    @Bean
    @ConditionalOnExpression("null == '${iking.framework.web.arch}' || '${iking.framework.web.arch}'.equals('') || '${iking.framework.web.arch}'.equals('single') || '${spring.application.name}'.equals('server')")
    public FrameworkAgentRunner systemLogReportAgentRunner(LogApi api) {
        return new SystemLogReportAgentRunner(api);
    }

    @Bean
    @ConditionalOnExpression("null == '${iking.framework.web.arch}' || '${iking.framework.web.arch}'.equals('') || '${iking.framework.web.arch}'.equals('single') || '${spring.application.name}'.equals('server')")
    public FrameworkAgentRunner operationLogReportAgentRunner(LogApi api) {
        return new OperationLogReportAgentRunner(api);
    }
}
