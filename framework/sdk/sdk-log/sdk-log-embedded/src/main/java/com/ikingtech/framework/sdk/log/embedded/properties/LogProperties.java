package com.ikingtech.framework.sdk.log.embedded.properties;

import ch.qos.logback.classic.Level;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@Data
@ConfigurationProperties(GLOBAL_CONFIG_PREFIX + ".log")
public class LogProperties {

    private Boolean enabled;

    private String level = Level.ALL.levelStr;

    private List<String> className;

    private Flush flush = new Flush();

    @Data
    public static class Flush {

        public Flush() {
            this.mode = "rpc";
        }

        private String mode;
    }

    private Boolean locationEnable;
}
