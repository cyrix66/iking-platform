package com.ikingtech.framework.sdk.log.embedded.runner;

import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.log.api.LogApi;
import com.ikingtech.framework.sdk.log.model.rpc.OperationLogReportParam;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentRunner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class OperationLogReportAgentRunner implements FrameworkAgentRunner {

    private final LogApi api;

    @Override
    public R<Object> run(Object data) {
        try {
            return this.api.reportOperationLog((OperationLogReportParam) data);
        } catch (Exception e) {
            throw new FrameworkException(e.getMessage());
        }
    }

    @Override
    public FrameworkAgentTypeEnum type() {
        return FrameworkAgentTypeEnum.OPERATION_LOG;
    }
}
