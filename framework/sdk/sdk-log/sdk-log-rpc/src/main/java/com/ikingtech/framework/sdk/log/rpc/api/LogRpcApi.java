package com.ikingtech.framework.sdk.log.rpc.api;

import com.ikingtech.framework.sdk.log.api.LogApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "LogRpcApi", path = "/log")
public interface LogRpcApi extends LogApi {
}
