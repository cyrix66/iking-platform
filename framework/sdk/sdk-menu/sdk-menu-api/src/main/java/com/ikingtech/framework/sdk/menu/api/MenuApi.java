package com.ikingtech.framework.sdk.menu.api;

import com.ikingtech.framework.sdk.base.model.DragOrderParam;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.system.menu.MenuViewTypeEnum;
import com.ikingtech.framework.sdk.menu.model.MenuDTO;
import com.ikingtech.framework.sdk.menu.model.RoleMenuAssignDTO;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author tie yan
 */
public interface MenuApi {

    /**
     * 添加菜单信息
     *
     * @param menu 菜单信息
     * @return 返回添加结果
     */
    @PostRequest(order = 1, value = "/add", summary = "添加菜单信息", description = "添加菜单信息")
    R<String> add(@Parameter(name = "menu", description = "菜单信息")
                  @RequestBody MenuDTO menu);

    /**
     * 删除菜单
     *
     * @param id 编号
     * @return 返回删除结果
     */
    @PostRequest(order = 2, value = "/delete", summary = "删除菜单", description = "删除菜单")
    R<Object> delete(@Parameter(name = "id", description = "编号")
                     @RequestBody String id);

    /**
     * 更新菜单信息
     *
     * @param menu 菜单信息
     * @return 返回更新结果
     */
    @PostRequest(order = 3, value = "/update", summary = "更新菜单信息", description = "更新菜单信息")
    R<Object> update(@Parameter(name = "menu", description = "菜单信息")
                     @RequestBody MenuDTO menu);

    /**
     * 获取所有菜单信息
     *
     * @return 返回所有菜单信息
     */
    @PostRequest(order = 4, value = "/list/all", summary = "获取所有菜单信息", description = "获取所有菜单信息")
    R<List<MenuDTO>> all();

    /**
     * 根据编号获取菜单详情
     *
     * @param id 编号
     * @return 返回菜单详情
     */
    @PostRequest(order = 5, value = "/detail/id", summary = "根据编号获取菜单详情", description = "根据编号获取菜单详情")
    R<MenuDTO> detail(@Parameter(name = "id", description = "编号")
                      @RequestBody String id);

    /**
     * 根据编号获取菜单详情
     *
     * @param permissionCode 编号
     * @return 返回菜单详情
     */
    @PostRequest(order = 5, value = "/detail/permission-code", summary = "根据权限码获取菜单详情", description = "根据权限码获取菜单详情")
    R<MenuDTO> getByCode(@Parameter(name = "permissionCode", description = "权限码")
                         @RequestBody String permissionCode);

    /**
     * 根据角色编号获取角色菜单分配信息
     *
     * @param roleId 角色编号
     * @return 返回角色菜单分配信息
     */
    @PostRequest(order = 6, value = "/list/role-id", summary = "根据角色编号获取角色菜单分配信息", description = "根据角色编号获取角色菜单分配信息")
    R<RoleMenuAssignDTO> listByRoleId(@Parameter(name = "roleId", description = "角色编号，仅支持传入一个角色编号")
                                      @RequestBody String roleId);

    /**
     * 根据登录用户获取菜单列表
     *
     * @return 返回菜单列表
     */
    @PostRequest(order = 7, value = "/list/login-user", summary = "根据登录用户获取菜单列表", description = "根据登录用户获取菜单列表")
    R<List<MenuDTO>> listByLoginUser(@Parameter(name = "viewType", description = "可视类型")
                                     @RequestParam(required = false, value = "viewType") MenuViewTypeEnum viewType,
                                     @Parameter(name = "showAllViewType", description = "是否展示所有可视类型的菜单")
                                     @RequestBody(required = false) Boolean showAllViewType);

    @PostRequest(order = 10, value = "/drag", summary = "拖拽排序", description = "拖拽排序，调用拖拽排序接口后需再次调用列表查询接口展示最新排序结果。")
    R<Object> drag(@Parameter(name = "dragParam", description = "拖拽参数。")
                   @RequestBody DragOrderParam dragParam);
}
