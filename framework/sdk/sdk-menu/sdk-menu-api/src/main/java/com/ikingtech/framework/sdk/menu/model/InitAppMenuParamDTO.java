package com.ikingtech.framework.sdk.menu.model;

import com.ikingtech.framework.sdk.enums.system.menu.MenuDefinition;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "InitAppMenuParamDTO", description = "菜单初始化参数")
public class InitAppMenuParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -2790661939493107074L;

    @Schema(name = "domainCode", description = "域标识")
    private String domainCode;

    @Schema(name = "appCode", description = "应用标识")
    private String appCode;

    @Schema(name = "tenantCode", description = "租户标识")
    private String tenantCode;

    @Schema(name = "menuDefinitions", description = "菜单定义信息")
    private List<MenuDefinition> menuDefinitions;
}
