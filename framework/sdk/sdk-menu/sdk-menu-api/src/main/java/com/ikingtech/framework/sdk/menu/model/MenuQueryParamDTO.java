package com.ikingtech.framework.sdk.menu.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 菜单查询参数
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "MenuQueryParamDTO", description = "菜单查询参数")
public class MenuQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -6389275900959906872L;

    @Schema(name = "domainCode", description = "所属域标识")
    private String domainCode;

    @Schema(name = "tenantCode", description = "所属租户标识")
    private String tenantCode;

    @Schema(name = "appCode", description = "所属应用标识")
    private String appCode;

    @Schema(name = "menuViewType", description = "菜单可视类型", allowableValues = "MANAGE, BUSINESS, MOBILE")
    private String menuViewType;

    @Schema(name = "menuType", description = "菜单类型", allowableValues = "MENU, PAGE_TAB, PAGE_BUTTON, GLOBAL_BUTTON")
    private String menuType;

    @Schema(name = "menuJumpType", description = "菜单跳转类型", allowableValues = "NONE, FRONT_ROUTE, IFRAME, EXTERNAL_LINK")
    private String menuJumpType;

    @Schema(name = "parentId", description = "父菜单编号")
    private String parentId;

    @Schema(name = "name", description = "菜单名称")
    private String name;

    @Schema(name = "keepAlive", description = "路由缓冲")
    private Boolean keepAlive;

    @Schema(name = "icon", description = "菜单图标")
    private String icon;

    @Schema(name = "link", description = "跳转链接")
    private String link;

    @Schema(name = "remark", description = "描述")
    private String remark;

    @Schema(name = "euName", description = "菜单英文名称")
    private String euName;

    @Schema(name = "visible", description = "菜单是否显示")
    private Boolean visible;
}
