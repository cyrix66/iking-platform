package com.ikingtech.framework.sdk.message.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.message.model.MessageDTO;
import com.ikingtech.framework.sdk.message.model.MessageQueryParamDTO;
import com.ikingtech.framework.sdk.message.model.rpc.MessageSendParam;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author tie yan
 */
public interface MessageManagementApi {

    /**
     * 分页查询消息信息
     *
     * @param queryParam 消息信息查询参数，包含分页参数、排序参数、查询条件
     * @return 消息信息列表
     */
    @PostRequest(order = 1, value = "/list/page", summary = "分页查询消息信息", description = "分页查询消息信息")
    R<List<MessageDTO>> page(@Parameter(name = "queryParam", description = "消息信息查询参数，包含分页参数、排序参数、查询条件。")
                             @RequestBody MessageQueryParamDTO queryParam);

    /**
     * 根据消息编号读取消息
     *
     * @param id 消息编号
     * @return 消息信息
     */
    @PostRequest(order = 2, value = "/read/id", summary = "根据消息编号读取消息", description = "根据消息编号读取消息")
    R<Object> readById(@Parameter(name = "id", description = "消息编号。")
                       @RequestBody String id);

    /**
     * 读取所有消息
     *
     * @return 所有消息信息
     */
    @PostRequest(order = 3, value = "/read/all", summary = "读取所有消息", description = "读取所有消息")
    R<Object> readAll();

    /**
     * 发送消息
     *
     * @param sendParam 待发送消息信息
     * @return 消息发送结果
     */
    @PostRequest(order = 4, value = "/send", summary = "发送消息", description = "发送消息")
    R<Object> send(@Parameter(name = "sendParam", description = "待发送消息信息。")
                   @RequestBody MessageSendParam sendParam);
}
