package com.ikingtech.framework.sdk.message.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.message.model.rpc.MessageTemplateBeanDefinitionReportParam;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author tie yan
 */
public interface MessageTemplateApi {

    /**
     * 消息模板定义信息上报
     *
     * @param reportParam 消息模板定义信息
     * @return 执行结果
     */
    @PostRequest(order = 14,value = "/report", summary = "消息模板定义信息上报", description = "消息模板定义信息上报")
    R<Object> register(@RequestBody MessageTemplateBeanDefinitionReportParam reportParam);
}
