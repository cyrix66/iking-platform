package com.ikingtech.framework.sdk.message.model;

import com.ikingtech.framework.sdk.enums.message.MessageChannelContentTypeEnum;
import com.ikingtech.framework.sdk.enums.message.MessageChannelStatusEnum;
import com.ikingtech.framework.sdk.enums.message.MessageSendChannelEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "MessageChannelDefinitionDTO", description = "消息体推送渠道定义信息")
public class MessageChannelDefinitionDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -2387412884464691547L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "businessName", description = "业务类型名称")
    private String businessName;

    @Schema(name = "messageTitle", description = "消息标题")
    private String messageTitle;

    @Schema(name = "templateId", description = "消息模板编号")
    private String templateId;

    @Schema(name = "channel", description = "消息渠道类型")
    private MessageSendChannelEnum channel;

    @Schema(name = "channelName", description = "消息渠道名称")
    private String channelName;

    @Schema(name = "channelDescription", description = "消息渠道描述")
    private String channelDescription;

    @Schema(name = "channelId", description = "消息发送渠道编号")
    private String channelId;

    @Schema(name = "channelTemplateId", description = "消息发送渠道模板编号")
    private String channelTemplateId;

    @Schema(name = "voice", description = "是否播放语音")
    private Boolean voice;

    @Schema(name = "contentType", description = "消息体文本类型")
    private MessageChannelContentTypeEnum contentType;

    @Schema(name = "contentTypeName", description = "消息体文本类型名称")
    private String contentTypeName;

    @Schema(name = "content", description = "消息体文本模板")
    private String content;

    @Schema(name = "customizeContent", description = "消息体文本")
    private String customizeContent;

    @Schema(name = "redirectLayout", description = "消息跳转样式")
    private String redirectLayout;

    @Schema(name = "status", description = "消息发送渠道状态")
    private MessageChannelStatusEnum status;

    @Schema(name = "statusName", description = "消息发送渠道状态名称")
    private String statusName;

    @Schema(name = "supportTemplate", description = "消息渠道是否支持模板")
    private Boolean supportTemplate;

    @Schema(name = "showNotification", description = "消息渠道是否展示消息提醒")
    private Boolean showNotification;

    @Schema(name = "configured", description = "是否已配置")
    private Boolean configured;

    @Schema(name = "redirectDefinitions", description = "消息跳转定义信息集合")
    private List<MessageRedirectDefinitionDTO> redirectDefinitions;

    @Schema(name = "paramDefinitions", description = "消息体参数")
    private List<MessageParamDefinitionDTO> paramDefinitions;
}
