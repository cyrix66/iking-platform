package com.ikingtech.framework.sdk.message.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "MessageRedirectDefinitionDTO", description = "消息接跳转定义信息")
public class MessageRedirectDefinitionDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -3046999165979130253L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "templateId", description = "消息模板编号")
    private String templateId;

    @Schema(name = "channelDefinitionId", description = "消息体定义编号")
    private String channelDefinitionId;

    @Schema(name = "redirectTo", description = "消息跳转链接")
    private String redirectTo;

    @Schema(name = "redirectName", description = "消息跳转链接名称")
    private String redirectName;
}
