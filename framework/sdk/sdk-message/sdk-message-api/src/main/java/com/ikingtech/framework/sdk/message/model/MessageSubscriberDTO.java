package com.ikingtech.framework.sdk.message.model;

import com.ikingtech.framework.sdk.base.model.BaseModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "MessageSubscriberDTO", description = "消息订阅方")
public class MessageSubscriberDTO extends BaseModel implements Serializable {

    @Serial
    private static final long serialVersionUID = 9062527116320258425L;

    @Schema(name = "tenantCode", description = "租户标识")
    private String tenantCode;

    @Schema(name = "name", description = "订阅方名称")
    private String name;

    @Schema(name = "sendUrls", description = "推送地址")
    private List<MessageSubscriberSendUrlDTO> sendUrls;
}
