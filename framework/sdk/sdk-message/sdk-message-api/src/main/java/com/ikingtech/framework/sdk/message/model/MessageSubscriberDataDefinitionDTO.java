package com.ikingtech.framework.sdk.message.model;

import com.ikingtech.framework.sdk.base.model.BaseModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(name = "MessageSubscriberDataDefinitionDTO", description = "消息订阅方数据定义")
public class MessageSubscriberDataDefinitionDTO extends BaseModel implements Serializable {

    @Serial
    private static final long serialVersionUID = -1210081174634668670L;

    @Schema(name = "subscriberId", description = "订阅方编号")
    private String subscriberId;

    @Schema(name = "sendUrlId", description = "推送地址编号")
    private String sendUrlId;

    @Schema(name = "fieldName", description = "字段名称")
    private String fieldName;
}
