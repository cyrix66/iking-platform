package com.ikingtech.framework.sdk.message.model;

import com.ikingtech.framework.sdk.base.model.BaseQueryParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "MessageSubscriberQueryParamDTO", description = "消息订阅方")
public class MessageSubscriberQueryParamDTO extends BaseQueryParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -595268433533724217L;

    @Schema(name = "name", description = "订阅方名称")
    private String name;

    @Schema(name = "sendUrl", description = "推送地址")
    private String sendUrl;
}
