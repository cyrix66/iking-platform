package com.ikingtech.framework.sdk.message.model;

import com.ikingtech.framework.sdk.base.model.BaseModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "MessageSubscriberSendUrlDTO", description = "消息订阅方推送地址")
public class MessageSubscriberSendUrlDTO extends BaseModel implements Serializable {

    @Serial
    private static final long serialVersionUID = -124324296230259536L;

    @Schema(name = "sendUrl", description = "推送地址")
    private String sendUrl;

    @Schema(name = "apiKey", description = "API Key")
    private String apiKey;

    @Schema(name = "dataDefinitions", description = "数据结构定义")
    private List<MessageSubscriberDataDefinitionDTO> dataDefinitions;
}
