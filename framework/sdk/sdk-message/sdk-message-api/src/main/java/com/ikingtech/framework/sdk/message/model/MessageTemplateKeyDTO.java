package com.ikingtech.framework.sdk.message.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 消息模板标识信息
 *
 * @author tie yan
 */
@Data
@Schema(name = "MessageTemplateKeyDTO", description = "消息模板标识信息")
public class MessageTemplateKeyDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 8506381701223479030L;

    @Schema(name = "messageTemplateId", description = "消息模板编号")
    private String messageTemplateId;

    @Schema(name = "messageTemplateKey", description = "消息模板标识")
    private String messageTemplateKey;
}
