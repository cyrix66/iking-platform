package com.ikingtech.framework.sdk.message.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "MessageTemplateQueryParamDTO", description = "消息模板查询参数")
public class MessageTemplateQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 8767303210649373544L;

    @Schema(name = "businessKey", description = "消息模板所属业务标识")
    private String businessKey;

    @Schema(name = "businessName", description = "消息模板所属业务标识名称")
    private String businessName;

    @Schema(name = "messageTemplateKey", description = "消息模板标识")
    private String messageTemplateKey;

    @Schema(name = "messageTemplateTitle", description = "消息模板标题")
    private String messageTemplateTitle;

    @Schema(name = "channel", description = "消息推送渠道")
    private String channel;
}
