package com.ikingtech.framework.sdk.message.model;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author Administrator
 */
@Data
public class MessageTenantDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1005973870000778460L;

    private String tenantId;

    private String tenantCode;

    private String tenantName;
}
