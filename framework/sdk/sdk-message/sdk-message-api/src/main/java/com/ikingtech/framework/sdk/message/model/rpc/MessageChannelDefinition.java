package com.ikingtech.framework.sdk.message.model.rpc;

import com.ikingtech.framework.sdk.enums.message.MessageSendChannelEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class MessageChannelDefinition implements Serializable {

    @Serial
    private static final long serialVersionUID = 477174126307745018L;

    private MessageSendChannelEnum channel;

    private String channelName;

    private String channelId;

    private String channelTemplateId;

    private String templateContent;

    private Boolean showNotification;

    private List<MessageRedirectDefinition> redirectDefinitions;
}
