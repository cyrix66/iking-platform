package com.ikingtech.framework.sdk.message.model.rpc;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class MessageRedirectDefinition implements Serializable {

    @Serial
    private static final long serialVersionUID = -8781125695855054477L;

    private String redirectCode;

    private String redirectTo;

    private String redirectName;

    private Integer sortOrder;
}
