package com.ikingtech.framework.sdk.message.model.rpc;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class MessageTemplateBeanDefinitionReportParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -5923764646412610112L;

    private List<MessageTemplateBeanDefinition> beanDefinitions;
}
