package com.ikingtech.framework.sdk.message.embedded.annotation;

import com.ikingtech.framework.sdk.enums.message.MessageSendChannelEnum;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author tie yan
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MessageChannel {

    /**
     * 系统内置消息发送渠道
     */
    MessageSendChannelEnum type();

    /**
     * 消息渠道编号
     */
    String id() default "";

    /**
     * 消息渠道名称
     */
    String name() default "";

    /**
     * 消息渠道模板编号
     */
    String templateId() default "";

    /**
     * 是否展示提醒
     */
    boolean showNotification() default true;

    /**
     * 系统内置消息发送渠道跳转链接
     */
    MessageRedirect[] redirect() default {};

    /**
     * 消息渠道模板编号
     */
    String content() default "";
}
