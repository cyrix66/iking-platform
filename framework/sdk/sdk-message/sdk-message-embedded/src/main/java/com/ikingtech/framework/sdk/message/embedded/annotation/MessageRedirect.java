package com.ikingtech.framework.sdk.message.embedded.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author tie yan
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MessageRedirect {

    String link();

    String linkName() default "";

    String code() default "";

    int order() default 1;
}
