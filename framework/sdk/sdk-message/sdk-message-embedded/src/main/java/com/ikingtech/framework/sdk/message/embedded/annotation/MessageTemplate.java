package com.ikingtech.framework.sdk.message.embedded.annotation;

import com.ikingtech.framework.sdk.context.constant.SecurityConstants;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * @author tie yan
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface MessageTemplate {

    /**
     * 消息唯一标识
     */
    String key();

    /**
     * 消息模板标题
     */
    String name();

    /**
     * 业务标识
     */
    String businessKey();

    /**
     * 业务名称
     */
    String businessName();

    /**
     * 系统内置消息模板
     */
    boolean internal() default false;

    /**
     * 系统内置消息发送渠道
     */
    MessageChannel[] channel() default {};

    /**
     * 是否发送给全体用户
     */
    boolean all() default false;

    /**
     * 消息模板所属租户
     */
    String tenant() default SecurityConstants.DEFAULT_TENANT_CODE;
}
