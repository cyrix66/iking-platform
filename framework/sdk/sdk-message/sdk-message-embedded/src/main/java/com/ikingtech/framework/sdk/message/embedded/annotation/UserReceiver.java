package com.ikingtech.framework.sdk.message.embedded.annotation;

import java.lang.annotation.*;

/**
 * @author tie yan
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface UserReceiver {

    String description();
}
