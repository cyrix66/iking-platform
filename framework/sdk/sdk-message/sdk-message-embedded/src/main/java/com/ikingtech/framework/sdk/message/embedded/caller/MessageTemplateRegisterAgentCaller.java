package com.ikingtech.framework.sdk.message.embedded.caller;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.message.model.rpc.MessageTemplateBeanDefinitionReportParam;
import com.ikingtech.framework.sdk.message.rpc.api.MessageTemplateRpcApi;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentCaller;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author tie yan
 */
@Service
@RequiredArgsConstructor
public class MessageTemplateRegisterAgentCaller implements FrameworkAgentCaller {

    private final MessageTemplateRpcApi api;

    @Override
    public R<Object> call(Object data) {
        MessageTemplateBeanDefinitionReportParam reportParam = (MessageTemplateBeanDefinitionReportParam) data;
        this.api.register(reportParam);
        return R.ok();
    }

    @Override
    public FrameworkAgentTypeEnum type() {
        return FrameworkAgentTypeEnum.MESSAGE_TEMPLATE_REPORT;
    }
}
