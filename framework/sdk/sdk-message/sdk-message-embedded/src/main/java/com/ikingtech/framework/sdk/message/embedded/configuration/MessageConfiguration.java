package com.ikingtech.framework.sdk.message.embedded.configuration;

import com.ikingtech.framework.sdk.message.api.MessageManagementApi;
import com.ikingtech.framework.sdk.message.api.MessageTemplateApi;
import com.ikingtech.framework.sdk.message.embedded.MessageTemplateBeanDefinitionFactory;
import com.ikingtech.framework.sdk.message.embedded.MessageTemplateBeanDefinitionReporter;
import com.ikingtech.framework.sdk.message.embedded.caller.MessageSendAgentCaller;
import com.ikingtech.framework.sdk.message.embedded.caller.MessageTemplateRegisterAgentCaller;
import com.ikingtech.framework.sdk.message.embedded.runner.MessageSendAgentRunner;
import com.ikingtech.framework.sdk.message.embedded.runner.MessageTemplateReportAgentRunner;
import com.ikingtech.framework.sdk.message.rpc.api.MessageManagementRpcApi;
import com.ikingtech.framework.sdk.message.rpc.api.MessageTemplateRpcApi;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentCaller;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
public class MessageConfiguration {

    @Bean
    public MessageTemplateBeanDefinitionReporter messageTemplateBeanDefinitionReporter() {
        return new MessageTemplateBeanDefinitionReporter();
    }

    @Bean
    public MessageTemplateBeanDefinitionFactory messageTemplateBeanDefinitionFactory(MessageTemplateBeanDefinitionReporter reporter) {
        return new MessageTemplateBeanDefinitionFactory(reporter);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service", matchIfMissing = true)
    public FrameworkAgentCaller messageTemplateRegisterAgentCaller(MessageTemplateRpcApi rpcApi) {
        return new MessageTemplateRegisterAgentCaller(rpcApi);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service", matchIfMissing = true)
    public FrameworkAgentCaller messageSendAgentCaller(MessageManagementRpcApi rpcApi) {
        return new MessageSendAgentCaller(rpcApi);
    }

    @Bean
    public FrameworkAgentRunner messageTemplateRegisterAgentRunner(MessageTemplateApi api) {
        return new MessageTemplateReportAgentRunner(api);
    }

    @Bean
    public FrameworkAgentRunner messageSendAgentRunner(MessageManagementApi api) {
        return new MessageSendAgentRunner(api);
    }
}
