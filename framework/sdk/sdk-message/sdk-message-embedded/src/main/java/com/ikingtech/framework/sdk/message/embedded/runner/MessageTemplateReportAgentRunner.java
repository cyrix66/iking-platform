package com.ikingtech.framework.sdk.message.embedded.runner;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.message.api.MessageTemplateApi;
import com.ikingtech.framework.sdk.message.model.rpc.MessageTemplateBeanDefinitionReportParam;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentRunner;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author tie yan
 */
@Service
@RequiredArgsConstructor
public class MessageTemplateReportAgentRunner implements FrameworkAgentRunner {

    private final MessageTemplateApi api;

    @Override
    public R<Object> run(Object data) {
        MessageTemplateBeanDefinitionReportParam reportParam = (MessageTemplateBeanDefinitionReportParam) data;
        this.api.register(reportParam);
        return R.ok();
    }

    @Override
    public FrameworkAgentTypeEnum type() {
        return FrameworkAgentTypeEnum.MESSAGE_TEMPLATE_REPORT;
    }
}
