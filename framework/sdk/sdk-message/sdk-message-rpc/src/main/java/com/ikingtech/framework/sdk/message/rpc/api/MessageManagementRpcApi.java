package com.ikingtech.framework.sdk.message.rpc.api;

import com.ikingtech.framework.sdk.message.api.MessageManagementApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "MessageManagementRpcApi", path = "/message/management")
public interface MessageManagementRpcApi extends MessageManagementApi {
}
