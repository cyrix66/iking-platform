package com.ikingtech.framework.sdk.office.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.office.model.dto.AffixFileDto;
import com.ikingtech.framework.sdk.office.model.vo.AffixFileVo;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author zhuxg
 */
public interface AffixFileApi {


    /**
     * 新增
     *
     * @param dto 新增参数
     * @return String
     */
    @PostRequest(value = "/add", summary = "新增", description = "新增，新增成功后返回编号。")
    R<String> add(@Parameter(name = "affix", description = "附件信息")
                  @RequestBody @Validated AffixFileDto dto
    );

    /**
     * 删除
     *
     * @param id 删除参数
     * @return string
     */
    @GetRequest(value = "/delete/{id}", summary = "删除", description = "删除，删除成功后返回编号。")
    R<String> delete(@Parameter(name = "id", description = "附件id")
                     @PathVariable(value = "id") String id);

    /**
     * 根据业务id查询业务附件信息
     *
     * @param businessId 业务id
     * @return AffixFileVo
     */
    @GetRequest(value = "/query/by/business/{businessId}", summary = "根据业务id查询业务附件信息", description = "根据业务id查询业务附件信息,返回业务附件列表。")
    R<List<AffixFileVo>> queryByBusinessId(@Parameter(name = "businessId", description = "业务id")
                                           @PathVariable(value = "businessId") String businessId);

    /**
     * 批量新增
     *
     * @param affixFiles 文件集合
     * @return String
     */
    @PostRequest(value = "/add/batch", summary = "批量新增", description = "批量新增")
    R<String> addBatch(@Parameter(name = "affixFiles", description = "批量新增提交参数")
                       @RequestBody @Validated List<AffixFileDto> affixFiles);

    /**
     * 批量删除
     *
     * @param ids 删除id集合
     * @return String
     */
    @GetRequest(value = "/delete/batch/{ids}", summary = "批量删除", description = "批量删除")
    R<String> deleteBatch(@Parameter(name = "ids", description = "主键id")
                          @PathVariable(value = "ids") List<String> ids);

    /**
     * 根据业务id删除附件信息
     *
     * @param businessId 删除id
     * @return String
     */
    @GetRequest(value = "/delete/by/business/{businessId}", summary = "根据业务id删除附件信息", description = "根据业务id删除附件信息")
    R<String> deleteByBusinessId(@Parameter(name = "businessId", description = "业务id")
                                 @PathVariable(value = "businessId") String businessId);


    @GetRequest(value = "/detail/{id}", summary = "详情", description = "根据主键id查询详情")
    R<AffixFileVo> detail(@Parameter(name = "id", description = "主键id")
                          @PathVariable(value = "id") String id);

    @GetRequest(value = "/mapByBusinessIds", summary = "根据业务id查询map", description = "根据主键id查询详情")
    R<Map<String, List<AffixFileVo>>> mapByBusinessIds(@RequestParam(value = "businessIds") List<String> businessIds);

    @GetRequest(value = "/delete/batch/businessIds", summary = "根据业务id批量删除", description = "根据业务id批量删除")
    R<String> deleteBatchBusinessIds(@Parameter(name = "businessIds", description = "业务ids")
                                     @RequestParam(value = "businessIds") List<String> businessIds);
}
