package com.ikingtech.framework.sdk.office.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.office.model.dto.OfficeVersionDTO;
import com.ikingtech.framework.sdk.office.model.vo.DocumentHistoryVO;
import com.ikingtech.framework.sdk.office.model.vo.HistoryDataVO;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

public interface DocumentHistoryApi {

    @GetRequest(value = "/getList/{id}", summary = "获取文档版本历史", description = "获取文档版本历史")
    R<DocumentHistoryVO> getDocumentHistoryByTmpId(@Parameter(name = "id", description = "模板id")
                     @PathVariable(value = "id") String id);

    @PostRequest(value = "/getVersionInfo", summary = "获取文档版本信息", description = "获取文档版本信息")
    R<HistoryDataVO> getVersionInfo(@Parameter(name = "officeVersionDTO", description = "获取文档版本信息参数")
                                    @RequestBody OfficeVersionDTO officeVersionDTO);
}
