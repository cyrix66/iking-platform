package com.ikingtech.framework.sdk.office.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.office.model.dto.OfficeDataManagementDto;
import com.ikingtech.framework.sdk.office.model.query.OfficeDataManagementQuery;
import com.ikingtech.framework.sdk.office.model.vo.OfficeDataAllVo;
import com.ikingtech.framework.sdk.office.model.vo.OfficeDataManagementVo;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * office基础数据api
 * <p>
 * 1. 基础数据可用于office模板替换参数
 * 2. Object类型数据可用于自定义数据参数变量
 *
 * @author lqb
 */
public interface OfficeDataApi {

    @PostRequest(value = "/base/add", summary = "新增", description = "新增基础数据")
    R<String> baseAdd(@Parameter(name = "dto", description = "新增基础数据提交参数")
                      @RequestBody OfficeDataManagementDto dto);

    @PostRequest(value = "/base/update", summary = "修改", description = "修改基础数据")
    R<String> baseUpdate(@Parameter(name = "dto", description = "修改基础数据提交参数")
                         @RequestBody OfficeDataManagementDto dto);

    @PostRequest(value = "/base/query/list", summary = "查询", description = "根据归属模块查询数据（包含公共模块数据）")
    R<List<OfficeDataManagementVo>> baseQueryList(@Parameter(name = "query", description = "查询参数")
                                                  @RequestBody OfficeDataManagementQuery query);

    @GetRequest(value = "/base/details/{id}", summary = "详情", description = "详情")
    R<OfficeDataManagementVo> baseDetails(@Parameter(name = "id", description = "基础数据id")
                                          @PathVariable(value = "id") String id);

    @PostRequest(value = "/base/preview/{id}", summary = "预览", description = "预览数据")
    R<Object> basePreview(@Parameter(name = "id", description = "基础数据id")
                          @PathVariable(value = "id") String id,
                          @Parameter(name = "params", description = "预览参数")
                          @RequestBody Map<String, Object> params);

    @PostRequest(value = "/all", summary = "查询全部", description = "查询全部")
    R<List<OfficeDataAllVo>> all(@Parameter(name = "query", description = "查询参数")
                                 @RequestBody OfficeDataManagementQuery query);

}
