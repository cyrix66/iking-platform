package com.ikingtech.framework.sdk.office.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.office.model.dto.OfficeDataCustomFieldsDto;
import com.ikingtech.framework.sdk.office.model.dto.OfficeDataCustomManagementDto;
import com.ikingtech.framework.sdk.office.model.dto.OfficeDataParamsTypeDto;
import com.ikingtech.framework.sdk.office.model.enums.LogicOperator;
import com.ikingtech.framework.sdk.office.model.query.OfficeDataManagementQuery;
import com.ikingtech.framework.sdk.office.model.vo.OfficeDataCustomFieldsVo;
import com.ikingtech.framework.sdk.office.model.vo.OfficeDataCustomManagementVo;
import com.ikingtech.framework.sdk.office.model.vo.OfficeDataParamsTypeVo;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * office自定义数据api
 * 自定义数据可用于office模板替换参数
 *
 * @author lqb
 */
public interface OfficeDataCustomApi {

    @PostRequest(value = "/management/add", summary = "新增分组", description = "新增分组")
    R<String> managementAdd(@Parameter(name = "dto", description = "分组提交数据")
                            @RequestBody OfficeDataCustomManagementDto dto);

    @PostRequest(value = "/management/update", summary = "修改分组", description = "修改分组")
    R<String> managementUpdate(@Parameter(name = "dto", description = "分组提交数据")
                               @RequestBody OfficeDataCustomManagementDto dto);

    @PostRequest(value = "/management/list", summary = "分组列表", description = "分组列表")
    R<List<OfficeDataCustomManagementVo>> managementList(@Parameter(name = "query", description = "查询参数")
                                                         @RequestBody OfficeDataManagementQuery query);

    @GetRequest(value = "/management/delete/{id}", summary = "删除分组", description = "删除分组")
    R<String> managementDelete(@Parameter(name = "id", description = "自定义分组id")
                               @PathVariable(value = "id") String id);

    @PostRequest(value = "/fields/add", summary = "新增自定义字段", description = "新增自定义字段")
    R<String> fieldsAdd(@Parameter(name = "dto", description = "自定义字段提交参数")
                        @RequestBody OfficeDataCustomFieldsDto dto);

    @PostRequest(value = "/fields/update", summary = "修改自定义字段", description = "修改自定义字段")
    R<String> fieldsUpdate(@Parameter(name = "dto", description = "自定义字段提交参数")
                           @RequestBody OfficeDataCustomFieldsDto dto);

    @GetRequest(value = "/fields/detail/{id}", summary = "自定义字段详情", description = "自定义字段详情")
    R<OfficeDataCustomFieldsVo> fieldsDetail(@Parameter(name = "id", description = "自定义字段id")
                                             @PathVariable(value = "id") String id);

    @GetRequest(value = "/fields/delete/{id}", summary = "删除字段", description = "删除字段")
    R<String> fieldsDelete(@Parameter(name = "id", description = "自定义字段id")
                           @PathVariable(value = "id") String id);

    @PostRequest(value = "/custom/preview/{id}", summary = "预览", description = "预览自定义数据")
    R<Object> customPreview(@Parameter(name = "id", description = "自定义数据id")
                            @PathVariable(value = "id") String id,
                            @Parameter(name = "params", description = "预览参数")
                            @RequestBody Map<String, Object> params);

    @GetRequest(value = "/custom/logic/operator", summary = "自定义逻辑运算符", description = "自定义逻辑运算符")
    R<List<LogicOperator>> customLogicOperator();

    @GetRequest(value = "/custom/params/type", summary = "查询全部参数类型", description = "查询全部参数类型")
    R<List<OfficeDataParamsTypeVo>> customParamsType();

    @GetRequest(value = "/management/list/business/type/{businessType}", summary = "根据业务类型查询自定义数据", description = "根据业务类型查询自定义数据")
    R<List<OfficeDataCustomManagementVo>> managementListByBusinessType(@Parameter(name = "businessType", description = "业务类型")
                                                                       @PathVariable(value = "businessType") String businessType);

    @GetRequest(value = "/copy/{id}", summary = "复制字段", description = "复制字段")
    R<String> copy(@Parameter(name = "id", description = "字段id")
                   @PathVariable(value = "id") String id);

    @PostRequest(value = "/params/type/add", summary = "新增参数类型", description = "新增参数类型")
    R<String> paramsTypeAdd(@Parameter(name = "dto", description = "新增参数类型提交参数")
                            @RequestBody OfficeDataParamsTypeDto dto);

    @PostRequest(value = "/params/type/update", summary = "修改参数类型", description = "修改参数类型")
    R<String> paramsTypeUpdate(@Parameter(name = "dto", description = "修改参数类型提交参数")
                               @RequestBody OfficeDataParamsTypeDto dto);

    @PostRequest(value = "/params/type/delete/{id}", summary = "删除参数类型", description = "删除参数类型")
    R<String> paramsTypeDelete(@Parameter(name = "id", description = "参数类型id")
                               @PathVariable(value = "id") String id);

}
