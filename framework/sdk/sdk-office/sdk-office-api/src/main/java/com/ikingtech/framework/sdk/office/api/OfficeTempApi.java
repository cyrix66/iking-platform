package com.ikingtech.framework.sdk.office.api;


import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.office.model.dto.*;
import com.ikingtech.framework.sdk.office.model.vo.OfficeTempRecordVo;
import com.ikingtech.framework.sdk.office.model.vo.OfficeTempVo;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * office模板api
 *
 * @author lqb
 */
public interface OfficeTempApi {

    @PostRequest(value = "/add", summary = "新增", description = "新增模板")
    R<String> add(@Parameter(name = "dto", description = "提交数据")
                  @RequestBody OfficeTempDto dto);

    @PostRequest(value = "/update", summary = "修改", description = "修改模板")
    R<String> update(@Parameter(name = "dto", description = "提交数据")
                     @RequestBody OfficeTempDto dto);

    @GetRequest(value = "/detail/{id}", summary = "详情", description = "查询详情")
    R<OfficeTempVo> detail(@Parameter(name = "id", description = "模板id")
                           @PathVariable(value = "id") String id);

    @PostRequest(value = "/records/{id}", summary = "历史记录", description = "查询历史记录")
    R<List<OfficeTempRecordVo>> records(@PathVariable(value = "id") String id);

    @PostRequest(value = "/getEnableOnlyOffice/{id}", summary = "查询启用模板", description = "查询启用模板")
    R<OfficeTempRecordVo> getEnableOnlyOffice(@PathVariable(value = "id") String id);


    @PostRequest(value = "/only/office/call/back/{id}", summary = "onlyOffice回调", description = "onlyOffice回调")
    R<String> onlyOfficeCallBack(@Parameter(name = "id", description = "模板id")
                                 @PathVariable(value = "id") String id,
                                 @Parameter(name = "dto", description = "office模板文件回调提交参数")
                                 @RequestBody OfficeTempFileDto dto);

    @PostRequest(value = "/make", summary = "生成文件", description = "根据模板生成office文件")
    R<String> make(@Parameter(name = "dto", description = "数据参数")
                   @RequestBody OfficeMakeDto dto);

    @PostRequest(value = "/get/only/office/token", summary = "获取token", description = "获取onlyofficetoken")
    R<String> getOnlyOfficeToken(@Parameter(name = "dto", description = "获取参数")
                                 @RequestBody OfficeTokenDto dto);

    @GetRequest(value = "/copy/{id}", summary = "复制模板", description = "复制模板")
    R<String> copy(@Parameter(name = "id", description = "模板id")
                   @PathVariable(value = "id") String id);

    @GetRequest(value = "/delete/{id}", summary = "删除", description = "删除模板")
    R<String> delete(@Parameter(name = "id", description = "模板id")
                     @PathVariable(value = "id") String id);



    @GetRequest(value = "/setDocumentVersion/{id}", summary = "设置当前版本为最新版本", description = "设置当前版本为最新版本")
    R<String> setDocumentVersion(@Parameter(name = "id", description = "模板id")
                                 @PathVariable(value = "id") String id);

    @PostRequest(value = "/restoreVersion", summary = "还原版本", description = "还原版本")
    R<String> restoreVersion(@Parameter(name = "officeVersionDTO", description = "还原版本参数")
                             @RequestBody OfficeVersionDTO officeVersionDTO);

    @GetRequest(value = "/copyTemp/{id}", summary = "复制模板", description = "复制模板")
    R<String> copyTemp(@Parameter(name = "id", description = "模板id")
                   @PathVariable(value = "id") String id);
    @GetRequest(value = "/getLastDocVersion/{id}", summary = "获取模板最新版本", description = "获取模板最新版本")
    R<OfficeTempRecordVo> getLastDocVersion(@Parameter(name = "id", description = "模板id")
                                          @PathVariable(value = "id") String id);

}
