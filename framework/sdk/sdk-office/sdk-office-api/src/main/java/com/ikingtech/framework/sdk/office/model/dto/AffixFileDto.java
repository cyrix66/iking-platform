package com.ikingtech.framework.sdk.office.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;


/**
 * 附件信息
 *
 * @author lqb
 * @since 2023/08/14
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "AffixFileDto", description = "附件")
public class AffixFileDto {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "ossFileId", description = "oss文件id")
    private String ossFileId;

    @Schema(name = "businessId", description = "归属业务id")
    private String businessId;

    @Schema(name = "originName", description = "原始文件名称")
    private String originName;

    @Schema(name = "tenantCode", description = "租户标识")
    private String tenantCode;

    @Schema(name = "fileSize", description = "文件大小")
    private String fileSize;

    @Schema(name = "suffix", description = "文件后缀")
    private String suffix;

    @Schema(name = "url", description = "文件访问相对路径")
    private String url;

    @Schema(name = "dirName", description = "文件服务器目录")
    private String dirName;

    @Schema(name = "path", description = "文件服务器路径")
    private String path;

    @Schema(name = "md5", description = "文件md5")
    private String md5;

    @Schema(name = "delFlag", description = "是否已删除")
    private Boolean delFlag = Boolean.FALSE;

    @Schema(name = "sortOrder", description = "排序值")
    private Integer sortOrder;

    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }
}
