package com.ikingtech.framework.sdk.office.model.dto;


import com.ikingtech.framework.sdk.office.model.enums.ConditionsEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * office-自定义条件分支提交参数
 * </p>
 *
 * @author lqb
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataCustomBranchDto", description = "office-自定义条件分支提交参数")
public class OfficeDataCustomBranchDto {

    @Schema(name = "branchName", description = "分支名称")
    private String branchName;

    @Schema(name = "conditions", description = "条件关系 AND-满足所有 OR-满足任意")
    private ConditionsEnum conditions;

    @Schema(name = "conditionStatement", description = "条件关系表达式")
    private String conditionStatement;

    @Schema(name = "logicExpression", description = "函数文本表达式")
    private String logicExpression;

    @Schema(name = "logicList", description = "分支逻辑运算")

    private List<OfficeDataCustomBranchLogicDto> logicList = new ArrayList<>();

    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }

}
