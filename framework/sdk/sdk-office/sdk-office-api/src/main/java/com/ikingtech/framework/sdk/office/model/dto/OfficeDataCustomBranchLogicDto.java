package com.ikingtech.framework.sdk.office.model.dto;


import com.ikingtech.framework.sdk.office.model.enums.LogicOperator;
import com.ikingtech.framework.sdk.office.model.enums.OfficeDataEnums;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

/**
 * <p>
 * office-自定义分支逻辑运算提交参数
 * </p>
 *
 * @author lqb
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataCustomBranchLogicDto", description = "office-自定义分支逻辑运算提交参数")
public class OfficeDataCustomBranchLogicDto {

    @Schema(name = "leftType", description = "运算符左边参数类型 SYSTEM-系统参数 CUSTOM-自定义")
    private OfficeDataEnums.ParamsType leftType;

    @Schema(name = "leftParam", description = "运算符左边参数")
    private String leftParam;

    @Schema(name = "logicOperator", description = "运算符 LESS-小于 LESS_EQUAL-小于等于 GREATER-大于 GREATER_EQUAL-大于等于 EQUAL-等于 BETWEEN-介于 IN-在范围内")
    private LogicOperator.LogicOperatorEnum logicOperator;

    @Schema(name = "rightType", description = "运算符右边参数类型 SYSTEM-系统参数 CUSTOM-自定义")
    private OfficeDataEnums.ParamsType rightType;

    @Schema(name = "rightParam", description = "运算符右边参数")
    private String rightParam;

    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }

}
