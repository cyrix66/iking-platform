package com.ikingtech.framework.sdk.office.model.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * office-自定义字段提交参数
 * </p>
 *
 * @author lqb
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataCustomFieldsDto", description = "office-自定义字段提交参数")
public class OfficeDataCustomFieldsDto {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "officeDataCustomManagementId", description = "自定义数据管理id")
    private String officeDataCustomManagementId;

    @Schema(name = "fieldName", description = "字段名称")
    private String fieldName;

    @Schema(name = "branchList", description = "分支条件")
    private List<OfficeDataCustomBranchDto> branchList = new ArrayList<>();

    @Schema(name = "params", description = "字段构成参数")
    private List<OfficeDataCustomFieldsParamsDto> params = new ArrayList<>();


    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }

}
