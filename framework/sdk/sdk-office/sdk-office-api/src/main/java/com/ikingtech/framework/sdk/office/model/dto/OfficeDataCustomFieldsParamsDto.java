package com.ikingtech.framework.sdk.office.model.dto;


import com.ikingtech.framework.sdk.office.model.enums.OfficeDataEnums;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

/**
 * <p>
 * office-自定义字段参数提交参数
 * </p>
 *
 * @author lqb
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataCustomFieldsParamsDto", description = "office-自定义字段参数提交参数")
public class OfficeDataCustomFieldsParamsDto {

    private String id;

    @Schema(name = "fieldType", description = "字段类型 BASE-基础参数 CUSTOM-自定义")
    private OfficeDataEnums.FieldType fieldType;

    @Schema(name = "officeDataManagementId", description = "系统基础数据管理id BASE时 必填")
    private String officeDataManagementId;

    @Schema(name = "fieldId", description = "字段id")
    private String fieldId;

    @Schema(name = "fieldKey", description = "字段key")
    private String fieldKey;

    @Schema(name = "fieldName", description = "字段名称")
    private String fieldName;

    @Schema(name = "params", description = "筛选参数 deptId=222&name=陕煤")
    private String params;


    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }

}
