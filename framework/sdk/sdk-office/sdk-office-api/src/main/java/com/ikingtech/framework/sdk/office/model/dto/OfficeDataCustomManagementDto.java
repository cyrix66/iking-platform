package com.ikingtech.framework.sdk.office.model.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

/**
 * <p>
 * office-自定义数据管理提交参数
 * </p>
 *
 * @author lqb
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataCustomManagementDto", description = "office-自定义数据管理提交参数")
public class OfficeDataCustomManagementDto {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "businessType", description = "数据所属业务模块")
    private String businessType;

    @Schema(name = "name", description = "分组名称")
    private String name;

    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }

}
