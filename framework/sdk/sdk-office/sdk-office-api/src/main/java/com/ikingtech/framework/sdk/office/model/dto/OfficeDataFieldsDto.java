package com.ikingtech.framework.sdk.office.model.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

/**
 * <p>
 * 办公文件管理-基础数据api相应字段提交参数
 * </p>
 *
 * @author lqb
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataFieldsDto", description = "办公文件管理-基础数据api相应字段提交参数")
public class OfficeDataFieldsDto {

    @Schema(name = "fieldName", description = "字段名称")
    private String fieldName;

    @Schema(name = "fieldKey", description = "key")
    private String fieldKey;

    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }

}
