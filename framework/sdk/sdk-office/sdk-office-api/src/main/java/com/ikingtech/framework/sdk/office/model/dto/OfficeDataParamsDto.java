package com.ikingtech.framework.sdk.office.model.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

/**
 * <p>
 * 办公文件管理-基础数据api请求参数提交参数
 * </p>
 *
 * @author lqb
 * @since 2023-09-19 03:01:06
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataParamsDto", description = "办公文件管理-基础数据api请求参数提交参数")
public class OfficeDataParamsDto {

    @Schema(name = "paramName", description = "参数名称")
    private String paramName;

    @Schema(name = "filtration", description = "过滤参数")
    private Boolean filtration;

    @Schema(name = "paramType", description = "参数类型")
    private String paramType;

    @Schema(name = "paramKey", description = "参数key")
    private String paramKey;

    @Schema(name = "paramValue", description = "参数值")
    private String paramValue;

    @Schema(name = "defaultValue", description = "默认值")
    private String defaultValue;

    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }

}
