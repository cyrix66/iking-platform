package com.ikingtech.framework.sdk.office.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 生成office文件提交参数
 *
 * @author lqb
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeMakeDto", description = "生成office文件提交参数")
public class OfficeMakeDto {

    @Schema(name = "officeTempId", description = "office模板id")
    private String officeTempId;

    @Schema(name = "businessTypeList", description = "模板参数业务类型")
    private Set<String> businessTypeList = new HashSet<>();

    @Schema(name = "customParams", description = "自定义参数")
    private Map<String, Object> customParams = new HashMap<>();
}
