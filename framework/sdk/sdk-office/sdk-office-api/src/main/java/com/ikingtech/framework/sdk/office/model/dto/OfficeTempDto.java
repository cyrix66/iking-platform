package com.ikingtech.framework.sdk.office.model.dto;


import com.ikingtech.framework.sdk.office.model.enums.FileTypeEnum;
import com.ikingtech.framework.sdk.oss.model.OssFileDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

/**
 * <p>
 * office-模板提交参数
 * </p>
 *
 * @author lqb
 * @since 2023-09-22 11:34:02
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeTempDto", description = "office-模板提交参数")
public class OfficeTempDto {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "tempName", description = "模板名称")
    private String tempName;

    @Schema(name = "fileType", description = "文件类型 EXCEL-表格 WORD-文档")
    private FileTypeEnum fileType;

    @Schema(name = "affixFileId", description = "附件文件id")
    private String affixFileId;


    /**
     * 附件上传oss信息
     */
    private OssFileDTO wordTempOss;

    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }

}
