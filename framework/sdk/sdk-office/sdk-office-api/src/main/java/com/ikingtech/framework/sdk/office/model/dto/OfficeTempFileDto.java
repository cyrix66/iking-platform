package com.ikingtech.framework.sdk.office.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * office模板文件回调提交参数
 *
 * @author lqb
 * @since 2023/09/25
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeTempFileDto", description = "office-office模板文件回调提交参数")
public class OfficeTempFileDto {

    /**
     * 文件临时路径
     */
    @Schema(name = "url", description = "文件临时路径 仅当 status 值等于 2, 3, 6 或 7 时，链接才存在")
    private String url;

    @Schema(name = "status", description = "状态 1 - 正在编辑文档 2 - 文档已准备好保存 3 - 发生文档保存错误 4 - 文档已关闭，没有任何更改 6 - 正在编辑文档，但保存了当前文档状态 7 - 强制保存文档时发生错误")
    private Integer status;
}
