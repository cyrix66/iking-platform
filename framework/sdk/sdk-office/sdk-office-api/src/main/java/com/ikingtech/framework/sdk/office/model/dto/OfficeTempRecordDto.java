package com.ikingtech.framework.sdk.office.model.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;


/**
 * <p>
 * office模板文件历史记录提交参数
 * </p>
 *
 * @author lqb
 * @since 2023-09-22 11:34:54
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeTempRecordDto", description = "office模板文件历史记录提交参数")
public class OfficeTempRecordDto {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "officeTempId", description = "模板id")
    private String officeTempId;

    @Schema(name = "enable", description = "是否启用")
    private Boolean enable;

    @Schema(name = "affixFileId", description = "附件文件id")
    private String affixFileId;

    @Schema(name = "fileName", description = "文件名称")
    private String fileName;

    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }

}
