package com.ikingtech.framework.sdk.office.model.dto;

import lombok.Data;

import java.util.List;

/**
 * only office token 令牌提交参数
 *
 * @author lqb
 */
@Data
public class OfficeTokenDto {

    /**
     * 文档参数
     */
    private Document document;

    /**
     * 定义与编辑器界面有关的参数：打开模式（查看器或编辑器）、界面语言、附加按钮等）
     */
    private EditorConfig editorConfig;
}

@Data
class Document {
    /**
     * 文档的唯一标识，用于标识文档的版本。
     */
    private String key;
    /**
     * 文档的权限
     */
    private Permissions permissions;
    /**
     * 文档的访问地址
     */
    private String url;
}

@Data
class Permissions {
    /**
     * 定义是否可以评论文档。
     * 如果评论权限设置为 "true"，文档 侧栏 将包含 评论 菜单选项；
     * 如果 mode 参数设置为 edit，文档注释将仅对文档编辑器可用。
     * 默认值与 edit 参数的值一致。
     */
    private Boolean comment;

    /**
     * 定义用户可以编辑、删除和/或查看其评论的 组。 该对象具有以下参数：
     */
    private CommentGroups commentGroups;

    /**
     * 定义是否可以将内容复制到剪贴板。
     * 如果参数设置为 false，则粘贴内容将仅在当前文档编辑器中可用。
     * 默认值为 true。
     */
    private Boolean copy;

    /**
     * 定义用户是否只能删除他/她的评论。
     * 默认值为 false。
     */
    private Boolean deleteCommentAuthorOnly;

    /**
     * 定义文档是可以下载还是只能在线查看或编辑。
     * 如果下载权限设置为 "false"，则 文件 菜单中将不存在 下载为... 菜单选项。
     * 默认值为 true。
     */
    private Boolean download;

    /**
     * 定义文档是可以编辑还是只能查看。
     * 如果编辑权限设置为 "true", 文件 菜单将包含 编辑文档 菜单选项；
     * 请注意，如果编辑权限设置为 "false"，文档将在查看器中打开，即使 mode 参数设置为 edit，您也 无法将其切换到编辑器。
     * 默认值为 true。
     */
    private Boolean edit;

    /**
     * 定义用户是否只能编辑他/她的评论。
     * 默认值为 false。
     */
    private Boolean editCommentAuthorOnly;

    /**
     * 定义是否可以填写表单。
     * 只有将 mode 参数设置为 edit时，文档编辑器才可以填写表单。
     * 默认值与 edit 或 review 参数的值一致。
     */
    private Boolean fillForms;

    /**
     * 定义是否可以更改内容控制设置。
     * 如果 mode 参数设置为 edit，内容控制修改将仅可用于文档编辑器。
     * 默认值为 true。
     */
    private Boolean modifyContentControl;

    /**
     * 定义过滤器是否可以全局应用（true）影响所有其他用户，或本地应用（false），即仅适用于当前用户。
     * 如果 mode 参数设置为 edit，则过滤器修改仅可用于电子表格编辑器。
     * 默认值为 true。
     */
    private Boolean modifyFilter;

    /**
     * 定义是否可以打印文档。
     * 如果打印权限设置为 "false"，则 文件 菜单中将不存在 打印 菜单选项。
     * 默认值为 true。
     */
    private Boolean print;

    /**
     * 定义是否可以查看文档。
     * 如果审阅权限设置为 true，文档 状态栏 将包含 审阅 菜单选项；
     * 如果 mode 参数设置为 edit，文档审阅将仅对文档编辑器可用。
     * 默认值与 edit 参数的值一致。
     */
    private Boolean review;

    /**
     * 定义用户可以接受/拒绝其更改的 组。
     * [""] 值意味着用户可以查看不属于任何这些组的人所做的更改（例如，如果文档是在第三方编辑器中查看的）。
     * 如果值为 []，则用户无法查看任何组所做的更改。
     * 如果值为 "" 或未指定，则用户可以查看任何用户所做的更改。
     */
    private List<String> reviewGroups;
    /**
     * 定义其信息显示在编辑器中的用户组：
     * 用户名显示在编辑器标题的编辑用户列表中，
     * 输入文本时，会显示用户光标和工具提示及其名称，
     * 在严格的共同编辑模式下锁定对象时，会显示用户名。
     * ["Group1", ""] 表示显示Group1中的用户和不属于任何组的用户的信息。 [] 表示根本不显示任何用户信息。 undefined 或 "" 值表示显示所有用户的信息。
     */
    private List<String> userInfoGroups;

}

@Data
class CommentGroups {
    // 用户可以编辑其他用户发表的评论，
    private List<String> edit;
    // 用户可以删除其他用户的评论，
    private List<String> remove;
    // 用户可以查看其他用户发表的评论，
    private String view;
}

@Data
class EditorConfig {

    /**
     * 指定 文档存储服务 的绝对 URL （必须由在自己的服务器上使用 ONLYOFFICE 文档服务器的软件集成商 实现）。
     */
    private String callbackUrl;

    /**
     * 定义编辑器打开模式。
     * 可以是 view 以打开文档进行查看，
     * 也可以是 edit 以在编辑模式下打开文档，从而允许对文档数据进行更改。
     * 默认值为 "edit"。
     */
    private String mode;

    /**
     * 定义当前查看或编辑文档的用户：
     */
    private User user;
}

@Data
class User {
    // 用户所属的组
    private String group;
    // 用户的标识。 长度限制为 128 个符号。
    //此信息被存储并用于区分共同作者, 在保存和突出显示历史记录（在 更改列表中）时指出最后更改的 作者，并根据用户数量计算有权访问许可证的用户。
    //我们建议使用一些唯一的匿名哈希。 不要在此字段中使用敏感数据，例如姓名或电子邮件。
    private String id;
    // 用户的全名。 长度限制为 128 个符号
    private String name;
}
