package com.ikingtech.framework.sdk.office.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;


@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeRestoreVersionDTO", description = "还原版本DTO")
public class OfficeVersionDTO {
   private String id;
   private  int version;
}
