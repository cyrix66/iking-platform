package com.ikingtech.framework.sdk.office.model.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 条件枚举
 *
 * @author lqb
 */
@Getter
@RequiredArgsConstructor
public enum ConditionsEnum {

    AND("满足所有"),
    OR("满足任意"),
    ;
    private final String description;

}
