package com.ikingtech.framework.sdk.office.model.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * office模板枚举
 *
 * @author lqb
 * @since 2023/09/21
 */
@Getter
@RequiredArgsConstructor
public enum FileTypeEnum {

    EXCEL(".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "表格"),

    WORD(".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "文档");

    /**
     * 后缀
     */
    private final String suffix;
    /**
     * 导出文件类型
     */
    private final String contentType;
    /**
     * 描述
     */
    private final String description;
}
