package com.ikingtech.framework.sdk.office.model.enums;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


/**
 * 逻辑运算符枚举
 *
 * @author lqb
 */
@Getter
@RequiredArgsConstructor
public class LogicOperator {

    @Schema(name = "key", description = "key")
    private String key;

    @Schema(name = "name", description = "名称")
    private String name;

    @Schema(name = "symbol", description = "符号")
    private String symbol;

    LogicOperator(String key, String name, String symbol) {
        this.key = key;
        this.name = name;
        this.symbol = symbol;
    }

    @Getter
    @RequiredArgsConstructor
    public enum LogicOperatorEnum {
        EQUAL("等于", "=="),

        NOT_EQUAL("不等于", "!="),

        LESS("小于", "<"),

        GREATER("大于", ">"),

        LESS_EQUAL("小于等于", "<="),

        GREATER_EQUAL("大于等于", ">="),

        IN("包含", "include"),

        NOT_IN("不包含", "!include"),
        ;

        private final String symbol;
        /**
         * 描述
         */
        private final String description;

        public LogicOperator toLogicOperator() {
            return new LogicOperator(this.name(), this.getDescription(), this.getSymbol());
        }
    }

}
