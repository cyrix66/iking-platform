package com.ikingtech.framework.sdk.office.model.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 办公文件枚举
 *
 * @author lqb
 * @since 2023/09/19
 */
public class OfficeDataEnums {

    /**
     * 数据来源类型
     *
     * @author lqb
     * @since 2023/09/18
     */
    @Getter
    @RequiredArgsConstructor
    public enum ResourceType {
        API("接口"),
        JSON("json字符串"),
        ;
        private final String description;
    }

    /**
     * 接口请求方式
     *
     * @author lqb
     * @since 2023/09/18
     */
    @Getter
    @RequiredArgsConstructor
    public enum ApiMethod {
        POST("post请求"),
        GET("get请求"),
        ;
        private final String description;
    }


    /**
     * 返回类型
     *
     * @author lqb
     * @since 2023/09/18
     */
    @Getter
    @RequiredArgsConstructor
    public enum ResultType {
        OBJECT("对象"),
        LIST("列表"),
        ;
        private final String description;
    }

    /**
     * 数据源类型
     *
     * @author lqb
     * @since 2023/09/25
     */
    @Getter
    @RequiredArgsConstructor
    public enum FieldType {
        BASE("基础数据"),
        CUSTOM("自定义数据"),
        ;
        private final String description;
    }

    @Getter
    @RequiredArgsConstructor
    public enum ParamsType {
        SYSTEM("系统数据"),
        CUSTOM("自定义数据"),
        ;
        private final String description;
    }

    /**
     * 表单数据类型
     *
     * @author lqb
     */
    @Getter
    @RequiredArgsConstructor
    public enum FormDataType {
        FORM("表单"),
        TABLE("表格"),
        LIST("列表");
        private final String description;
    }

}
