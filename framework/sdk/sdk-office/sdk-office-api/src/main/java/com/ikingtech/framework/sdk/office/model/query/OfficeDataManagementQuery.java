package com.ikingtech.framework.sdk.office.model.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.HashSet;
import java.util.Set;

/**
 * 基础数据管理查询参数
 *
 * @author lqb
 * @since 2023/09/19
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "DataBaseManagementQuery", description = "基础数据管理查询参数")
public class OfficeDataManagementQuery {

    @Schema(name = "businessTypeList", description = "数据所属业务模块")
    private Set<String> businessTypeList = new HashSet<>();

    @Schema(name = "name", description = "搜索")
    private String name;

    @Schema(name = "createBy", description = "创建人id")
    private String createBy;
}
