package com.ikingtech.framework.sdk.office.model.query;


import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * office-模板查询参数
 * </p>
 *
 * @author lqb
 * @since 2023-09-22 11:34:02
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeTempQuery", description = "office-模板查询参数")
public class OfficeTempQuery extends PageParam {


}
