package com.ikingtech.framework.sdk.office.model.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 附件信息返回参数
 *
 * @author lqb
 * @since 2023/08/15
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "AffixFileVo", description = "附件信息返回参数")
public class AffixFileVo implements Serializable {

    private static final long serialVersionUID =1L;
    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "businessId", description = "业务id")
    private String businessId;

    @Schema(name = "ossFileId", description = "oss文件id")
    private String ossFileId;

    @Schema(name = "originName", description = "原始文件名称")
    private String originName;

    @Schema(name = "tenantCode", description = "租户标识")
    private String tenantCode;

    @Schema(name = "fileSize", description = "文件大小")
    private String fileSize;

    @Schema(name = "suffix", description = "文件后缀")
    private String suffix;

    @Schema(name = "url", description = "文件访问相对路径")
    private String url;

    @Schema(name = "dirName", description = "文件服务器目录")
    private String dirName;

    @Schema(name = "path", description = "文件服务器路径")
    private String path;

    @Schema(name = "md5", description = "文件md5")
    private String md5;
}
