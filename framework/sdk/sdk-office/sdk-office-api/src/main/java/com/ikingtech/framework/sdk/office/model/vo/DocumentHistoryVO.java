package com.ikingtech.framework.sdk.office.model.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
@Schema(name = "DocumentHistoryListVO", description = "文档历史版本")
public class DocumentHistoryVO {

    private int currentVersion;
    private List<HistoryVO> history;
    private List<HistoryDataVO> historyData;

}
