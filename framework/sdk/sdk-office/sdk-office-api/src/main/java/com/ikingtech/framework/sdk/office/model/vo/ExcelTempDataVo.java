package com.ikingtech.framework.sdk.office.model.vo;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * excel温度数据vo
 *
 * @author lqb
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "ExcelTempDataVo", description = "封装word模板数据")
public class ExcelTempDataVo implements Serializable {
    private static final long serialVersionUID = 1614572025641806104L;

    @Schema(name = "totalData", description = "汇总数据")
    JSONObject totalData = JSONUtil.createObj();

    @Schema(name = "listData", description = "列表数据")
    JSONArray listData = JSONUtil.createArray();
}
