package com.ikingtech.framework.sdk.office.model.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@Schema(name = "HistoryDataVO", description = "文档历史数据")
public class HistoryDataVO {
    private String fileType;
    private int version;
    private String key;
    private String url;
}
