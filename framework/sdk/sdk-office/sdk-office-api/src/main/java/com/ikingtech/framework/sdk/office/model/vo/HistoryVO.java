package com.ikingtech.framework.sdk.office.model.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@Schema(name = "HistoryVO", description = "文档历史")
public class HistoryVO {
    private int version;
    private LocalDateTime created;
    private UserVO user;
    private String key;

}
