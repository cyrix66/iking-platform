package com.ikingtech.framework.sdk.office.model.vo;

import lombok.Data;

/**
 * only office 回调返回参数
 *
 * @author lqb
 */
@Data
public class OfficeCallbackR {

    private int error;

    public OfficeCallbackR(int error) {
        this.error = error;
    }
}
