package com.ikingtech.framework.sdk.office.model.vo;

import com.ikingtech.framework.sdk.office.model.enums.OfficeDataEnums;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 全部数据返回参数
 *
 * @author lqb
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataAllVo", description = "office-全部数据返回参数")
public class OfficeDataAllVo {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "fieldType", description = "数据类型 BASE-基础数据 CUSTOM-自定义数据")
    private OfficeDataEnums.FieldType fieldType;

    @Schema(name = "businessType", description = "数据所属业务模块")
    private String businessType;

    @Schema(name = "name", description = "数据来源名称")
    private String name;

    @Schema(name = "managementId", description = "数据管理id")
    private String managementId;

    @Schema(name = "resultType", description = "返回类型 OBJECT-对象 LIST-列表集合")
    private OfficeDataEnums.ResultType resultType;

    @Schema(name = "params", description = "请求参数")
    private List<OfficeDataParamsVo> params;

    @Schema(name = "fields", description = "返回字段")
    private List<OfficeDataFieldsAllVo> fields;

    @Schema(name = "filterParams", description = "过滤参数")
    private String filterParams;

    @Schema(name = "contentData", description = "数据")
    private Object contentData;
}
