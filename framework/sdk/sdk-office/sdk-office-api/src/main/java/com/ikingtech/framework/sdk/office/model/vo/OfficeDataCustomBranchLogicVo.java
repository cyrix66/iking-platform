package com.ikingtech.framework.sdk.office.model.vo;


import com.ikingtech.framework.sdk.office.model.enums.LogicOperator;
import com.ikingtech.framework.sdk.office.model.enums.OfficeDataEnums;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * office-自定义分支逻辑运算返回参数
 * </p>
 *
 * @author lqb
 * @since 2023-10-16 11:33:23
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataCustomBranchLogicVo", description = "office-自定义分支逻辑运算返回参数")
public class OfficeDataCustomBranchLogicVo {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "officeDataCustomFieldsId", description = "自定义字段id")
    private String officeDataCustomFieldsId;

    @Schema(name = "officeDataCustomBranchId", description = "自定义分支id")
    private String officeDataCustomBranchId;

    @Schema(name = "leftType", description = "运算符左边参数类型 SYSTEM-系统参数 CUSTOM-自定义")
    private OfficeDataEnums.ParamsType leftType;

    @Schema(name = "leftParam", description = "运算符左边参数")
    private String leftParam;

    @Schema(name = "logicOperator", description = "运算符 LESS-小于 LESS_EQUAL-小于等于 GREATER-大于 GREATER_EQUAL-大于等于 EQUAL-等于 BETWEEN-介于 IN-在范围内")
    private LogicOperator.LogicOperatorEnum logicOperator;

    @Schema(name = "rightType", description = "运算符右边参数类型 SYSTEM-系统参数 CUSTOM-自定义")
    private OfficeDataEnums.ParamsType rightType;

    @Schema(name = "rightParam", description = "运算符右边参数")
    private String rightParam;

}
