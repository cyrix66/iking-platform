package com.ikingtech.framework.sdk.office.model.vo;


import com.ikingtech.framework.sdk.office.model.enums.ConditionsEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * office-自定义条件分支返回参数
 * </p>
 *
 * @author lqb
 * @since 2023-10-16 11:33:23
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataCustomBranchVo", description = "office-自定义条件分支返回参数")
public class OfficeDataCustomBranchVo {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "officeDataCustomFieldsId", description = "自定义字段id")
    private String officeDataCustomFieldsId;

    @Schema(name = "branchName", description = "分支名称")
    private String branchName;

    @Schema(name = "conditions", description = "条件关系 AND-满足所有 OR-满足任意")
    private ConditionsEnum conditions;

    @Schema(name = "conditionStatement", description = "条件关系表达式")
    private String conditionStatement;

    @Schema(name = "logicExpression", description = "函数文本表达式")
    private String logicExpression;

    @Schema(name = "logicList", description = "分支逻辑运算")
    private List<OfficeDataCustomBranchLogicVo> logicList = new ArrayList<>();
}
