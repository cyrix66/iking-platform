package com.ikingtech.framework.sdk.office.model.vo;


import com.ikingtech.framework.sdk.office.model.enums.OfficeDataEnums;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * office-自定义字段参数返回参数
 * </p>
 *
 * @author lqb
 * @since 2023-10-16 11:33:23
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataCustomFieldsParamsVo", description = "office-自定义字段参数返回参数")
public class OfficeDataCustomFieldsParamsVo {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "officeDataCustomFieldsId", description = "自定义字段id")
    private String officeDataCustomFieldsId;

    @Schema(name = "fieldType", description = "字段类型 BASE-基础参数 CUSTOM-自定义")
    private OfficeDataEnums.FieldType fieldType;

    @Schema(name = "officeDataManagementId", description = "系统基础数据管理id BASE时 必填")
    private String officeDataManagementId;

    @Schema(name = "fieldKey", description = "字段key")
    private String fieldKey;

    @Schema(name = "fieldId", description = "字段id")
    private String fieldId;

    @Schema(name = "fieldName", description = "字段名称")
    private String fieldName;

    @Schema(name = "params", description = "筛选参数 例如：#deptId=222&name=陕煤")
    private String params;


}
