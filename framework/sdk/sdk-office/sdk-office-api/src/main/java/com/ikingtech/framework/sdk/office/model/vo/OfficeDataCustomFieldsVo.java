package com.ikingtech.framework.sdk.office.model.vo;


import com.ikingtech.framework.sdk.office.model.enums.OfficeDataEnums;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * office-自定义字段返回参数
 * </p>
 *
 * @author lqb
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataCustomFieldsVo", description = "office-自定义字段返回参数")
public class OfficeDataCustomFieldsVo {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "officeDataCustomManagementId", description = "自定义数据管理id")
    private String officeDataCustomManagementId;

    @Schema(name = "fieldName", description = "字段名称")
    private String fieldName;

    @Schema(name = "fieldKey", description = "字段key")
    private String fieldKey;

    @Schema(name = "fieldType", description = "字段类型 BASE-基础数据 CUSTOM-自定义数据")
    private OfficeDataEnums.FieldType fieldType;

    @Schema(name = "remark", description = "备注")
    private String remark;

    @Schema(name = "branchList", description = "分支条件")
    private List<OfficeDataCustomBranchVo> branchList = new ArrayList<>();

    @Schema(name = "params", description = "字段构成参数")
    private List<OfficeDataCustomFieldsParamsVo> params = new ArrayList<>();
}
