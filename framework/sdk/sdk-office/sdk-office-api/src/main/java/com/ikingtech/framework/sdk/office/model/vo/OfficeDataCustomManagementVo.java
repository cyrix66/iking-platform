package com.ikingtech.framework.sdk.office.model.vo;


import com.ikingtech.framework.sdk.office.model.enums.OfficeDataEnums;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * office-自定义数据管理返回参数
 * </p>
 *
 * @author lqb
 * @since 2023-10-13 09:35:03
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataCustomManagementVo", description = "office-自定义数据管理返回参数")
public class OfficeDataCustomManagementVo {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "businessType", description = "数据所属业务模块")
    private String businessType;

    @Schema(name = "name", description = "分组名称")
    private String name;

    @Schema(name = "resultType", description = "返回类型 OBJECT-对象 LIST-列表集合")
    private OfficeDataEnums.ResultType resultType;

    @Schema(name = "fields", description = "自定义字段集合")
    private List<OfficeDataCustomFieldsVo> fields;
}
