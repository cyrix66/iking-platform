package com.ikingtech.framework.sdk.office.model.vo;

import com.ikingtech.framework.sdk.office.model.enums.OfficeDataEnums;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 全部字段返回参数
 *
 * @author lqb
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataFieldsVo", description = "office-全部字段返回参数")
public class OfficeDataFieldsAllVo {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "fieldType", description = "数据类型 BASE-基础数据 CUSTOM-自定义数据")
    private OfficeDataEnums.FieldType fieldType;

    @Schema(name = "managementId", description = "数据管理id")
    private String managementId;

    @Schema(name = "fieldName", description = "字段名称")
    private String fieldName;

    @Schema(name = "fieldKey", description = "key")
    private String fieldKey;

    @Schema(name = "name", description = "字段名称")
    private String name;

}
