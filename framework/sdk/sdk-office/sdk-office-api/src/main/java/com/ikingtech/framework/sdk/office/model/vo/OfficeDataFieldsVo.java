package com.ikingtech.framework.sdk.office.model.vo;


import com.ikingtech.framework.sdk.office.model.enums.OfficeDataEnums;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 办公文件管理-基础数据api相应字段返回参数
 * </p>
 *
 * @author lqb
 * @since 2023-09-19 03:01:06
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataFieldsVo", description = "office-基础数据api相应字段返回参数")
public class OfficeDataFieldsVo {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "dataBaseManagementId", description = "数据管理id")
    private String dataBaseManagementId;

    @Schema(name = "fieldType", description = "字段类型 BASE-基础数据 CUSTOM-自定义数据")
    private OfficeDataEnums.FieldType fieldType;

    @Schema(name = "fieldName", description = "字段名称")
    private String fieldName;

    @Schema(name = "name", description = "字段名称")
    private String name;

    @Schema(name = "fieldKey", description = "key")
    private String fieldKey;

}
