package com.ikingtech.framework.sdk.office.model.vo;


import com.ikingtech.framework.sdk.office.model.enums.OfficeDataEnums;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * 办公文件管理-基础数据管理返回参数
 * </p>
 *
 * @author lqb
 * @since 2023-09-19 03:01:06
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataManagementVo", description = "办公文件管理-基础数据管理返回参数")
public class OfficeDataManagementVo {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "service", description = "调用的服务")
    private String service;

    @Schema(name = "businessType", description = "数据所属业务模块")
    private String businessType;

    @Schema(name = "name", description = "数据来源名称")
    private String name;

    @Schema(name = "resourceType", description = "数据来源类型 API-接口 JSON-json字符串 ")
    private OfficeDataEnums.ResourceType resourceType;

    @Schema(name = "content", description = "内容 API-接口请求地址 JSON-json内容")
    private String content;

    @Schema(name = "apiMethod", description = "请求方法 POST-post请求 GET-get请求  type=API 必穿")
    private OfficeDataEnums.ApiMethod apiMethod;

    @Schema(name = "resultType", description = "返回类型 OBJECT-对象 LIST-列表集合")
    private OfficeDataEnums.ResultType resultType;

    @Schema(name = "params", description = "请求参数")
    private List<OfficeDataParamsVo> params;

    @Schema(name = "fields", description = "返回字段")
    private List<OfficeDataFieldsVo> fields;

    @Schema(name = "contentData", description = "数据")
    private Object contentData;
}
