package com.ikingtech.framework.sdk.office.model.vo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * office-参数类型返回参数
 * </p>
 *
 * @author lqb
 * @since 2024-01-04 04:25:03
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataParamsTypeVo", description = "office-参数类型返回参数")
public class OfficeDataParamsTypeVo {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "key", description = "key")
    private String key;

    @Schema(name = "name", description = "参数类型名称")
    private String name;


}
