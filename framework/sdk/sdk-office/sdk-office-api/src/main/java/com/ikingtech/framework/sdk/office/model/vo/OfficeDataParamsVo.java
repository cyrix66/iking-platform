package com.ikingtech.framework.sdk.office.model.vo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 办公文件管理-基础数据api请求参数返回参数
 * </p>
 *
 * @author lqb
 * @since 2023-09-19 03:01:06
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeDataParamsVo", description = "办公文件管理-基础数据api请求参数返回参数")
public class OfficeDataParamsVo {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "dataBaseManagementId", description = "数据管理id")
    private String dataBaseManagementId;

    @Schema(name = "filtration", description = "过滤参数")
    private Boolean filtration;

    @Schema(name = "paramName", description = "参数名称")
    private String paramName;

    @Schema(name = "paramType", description = "参数类型")
    private String paramType;

    @Schema(name = "paramKey", description = "参数key")
    private String paramKey;

    @Schema(name = "paramValue", description = "参数值")
    private String paramValue;

    @Schema(name = "defaultValue", description = "默认值")
    private String defaultValue;


}
