package com.ikingtech.framework.sdk.office.model.vo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * office模板文件历史记录返回参数
 * </p>
 *
 * @author lqb
 * @since 2023-09-22 11:34:54
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeTempRecordVo", description = "office模板文件历史记录返回参数")
public class OfficeTempRecordVo {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "officeTempId", description = "模板id")
    private String officeTempId;

    @Schema(name = "enable", description = "是否启用")
    private Boolean enable;

    @Schema(name = "affixFileId", description = "附件文件id")
    private String affixFileId;

    @Schema(name = "ossId", description = "文件id")
    private String ossId;

    @Schema(name = "fileName", description = "文件名称")
    private String fileName;

    @Schema(name = "delFlag", description = "是否已删除")
    private Boolean delFlag;


}
