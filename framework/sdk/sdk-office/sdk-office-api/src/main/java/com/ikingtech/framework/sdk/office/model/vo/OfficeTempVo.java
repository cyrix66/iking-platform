package com.ikingtech.framework.sdk.office.model.vo;


import com.ikingtech.framework.sdk.office.model.enums.FileTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * office-模板返回参数
 * </p>
 *
 * @author lqb
 * @since 2023-09-22 11:34:02
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "OfficeTempVo", description = "office-模板返回参数")
public class OfficeTempVo {

    @Schema(name = "id", description = "主键id")
    private String id;

    @Schema(name = "tempName", description = "模板名称")
    private String tempName;

    @Schema(name = "fileType", description = "文件类型 EXCEL-表格 WORD-文档")
    private FileTypeEnum fileType;

    @Schema(name = "officeTempRecordId", description = "启用的记录")
    private String officeTempRecordId;

    @Schema(name = "fileName", description = "文件名称")
    private String fileName;

    @Schema(name = "affixFileId", description = "附件文件id")
    private String affixFileId;

    @Schema(name = "ossFileId", description = "oss文件id")
    private String ossFileId;

}
