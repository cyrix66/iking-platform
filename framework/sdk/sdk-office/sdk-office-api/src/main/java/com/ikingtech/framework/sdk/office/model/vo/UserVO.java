package com.ikingtech.framework.sdk.office.model.vo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserVO{
    private String id;
    private String name;
}
