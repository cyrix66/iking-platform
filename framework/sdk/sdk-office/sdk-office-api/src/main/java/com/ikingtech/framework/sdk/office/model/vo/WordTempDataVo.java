package com.ikingtech.framework.sdk.office.model.vo;

import com.ikingtech.framework.sdk.office.model.enums.OfficeDataEnums;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 封装word模板数据
 *
 * @author lqb
 */
@Getter
@Setter
@Accessors(chain = true)
@Schema(name = "WordTempDataVo", description = "封装word模板数据")
public class WordTempDataVo implements Serializable {

    private static final long serialVersionUID = 4210360222974819770L;

    private String dataFlag;

    private OfficeDataEnums.FormDataType formDataType;

    private String formData;
}
