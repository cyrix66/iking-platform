package com.ikingtech.framework.sdk.office.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@Data
@ConfigurationProperties(prefix = GLOBAL_CONFIG_PREFIX + ".office")
public class OfficeProperties {

    /**
     * 是否开启office服务<br />
     * 默认不开启
     */
    private Boolean enabled;

    /**
     * onlyOffice密码
     */
    private String jwtSecret;

    /**
     * 回调接口地址
     */
    private String callBackUrl;

}
