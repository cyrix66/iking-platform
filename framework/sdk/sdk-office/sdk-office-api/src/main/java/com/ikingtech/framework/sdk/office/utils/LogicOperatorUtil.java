package com.ikingtech.framework.sdk.office.utils;

import java.util.List;

/**
 * 逻辑运算工具类
 *
 * @author lqb
 */
public class LogicOperatorUtil {

    /**
     * 等于
     *
     * @param obj   对象
     * @param value 价值
     * @return boolean
     * @author lqb
     */
    public static <T extends Comparable<T>> boolean eq(T obj, T value) {
        if (obj == null || value == null) {
            return false;
        }
        return obj.compareTo(value) == 0;
    }

    /**
     * 不等于
     *
     * @param obj   对象
     * @param value 价值
     * @return boolean
     * @author lqb
     */
    public static <T extends Comparable<T>> boolean neq(T obj, T value) {
        return !eq(obj, value);
    }

    /**
     * 小于
     *
     * @param obj   对象
     * @param value 价值
     * @return boolean
     * @author lqb
     */
    public static <T extends Comparable<T>> boolean lt(T obj, T value) {
        if (obj == null || value == null) {
            return false;
        }
        return obj.compareTo(value) < 0;
    }

    /**
     * 小于等于
     *
     * @param obj   对象
     * @param value 价值
     * @return boolean
     * @author lqb
     */
    public static <T extends Comparable<T>> boolean le(T obj, T value) {
        if (obj == null || value == null) {
            return false;
        }
        return obj.compareTo(value) < 1;
    }

    /**
     * 大于
     *
     * @param obj   对象
     * @param value 价值
     * @return boolean
     * @author lqb
     */
    public static <T extends Comparable<T>> boolean gt(T obj, T value) {
        if (obj == null || value == null) {
            return false;
        }
        return obj.compareTo(value) > 0;
    }

    /**
     * 大于等于
     *
     * @param obj   对象
     * @param value 价值
     * @return boolean
     * @author lqb
     */
    public static <T extends Comparable<T>> boolean ge(T obj, T value) {
        if (obj == null || value == null) {
            return false;
        }
        return obj.compareTo(value) > -1;
    }

    /**
     * 介于 ~ ~ 之间
     *
     * @param obj    对象
     * @param value1 值1
     * @param value2 值2
     * @return boolean
     * @author lqb
     */
    public static <T extends Comparable<T>> boolean between(T obj, T value1, T value2) {
        if (obj == null || value1 == null || value2 == null) {
            return false;
        }
        if (value1.compareTo(value2) < 0) {
            return obj.compareTo(value1) > 0 && obj.compareTo(value2) < 0;
        } else if (value1.compareTo(value2) > 0) {
            return obj.compareTo(value2) > 0 && obj.compareTo(value1) < 0;
        }
        return false;
    }

    /**
     * 包含
     *
     * @param obj    对象
     * @param values 价值观
     * @return boolean
     * @author lqb
     */
    public static <T> boolean in(T obj, List<T> values) {
        return values.contains(obj);
    }


    /**
     * 不包含
     *
     * @param obj    对象
     * @param values 价值观
     * @return boolean
     * @author lqb
     */
    public static <T> boolean nin(T obj, List<T> values) {
        return !values.contains(obj);
    }

}
