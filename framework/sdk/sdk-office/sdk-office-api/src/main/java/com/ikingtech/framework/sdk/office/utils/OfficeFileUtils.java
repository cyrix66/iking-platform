package com.ikingtech.framework.sdk.office.utils;

import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import cn.hutool.poi.word.Word07Writer;
import cn.hutool.poi.word.WordUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.awt.*;
import java.io.ByteArrayOutputStream;

/**
 * 自定义文件实用工具
 *
 * @author lqb
 * @since 2023/09/22
 */
public class OfficeFileUtils {

    public OfficeFileUtils() {

    }


    /**
     * 创建excel文件字节
     *
     * @author lqb
     * @since 2023/09/25
     */
    public static byte[] createExcelFileByte() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ExcelWriter writer = ExcelUtil.getWriter(true);

        Sheet sheet = writer.getSheet();
        Row row = sheet.createRow(0);
        Cell cell = row.createCell(0);
        cell.setCellValue("这是一个excel模板");
        writer.flush(outputStream);
        writer.close();
        return outputStream.toByteArray();
    }

    /**
     * 创建字文件字节
     *
     * @author lqb
     * @since 2023/09/25
     */
    public static byte[] createWordFileByte() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Word07Writer writer = WordUtil.getWriter();
        writer.addText(new Font("宋体", Font.PLAIN, 22),
                "这是一个WORD模板");
        writer.flush(outputStream);
        writer.close();
        return outputStream.toByteArray();
    }

}
