package com.ikingtech.framework.sdk.office.rpc.api;

import com.ikingtech.framework.sdk.office.api.OfficeDataCustomApi;
import com.ikingtech.framework.sdk.office.rpc.factory.RemoteOfficeDataCustomRpcServiceFactory;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * office自定义数据rpc接口
 *
 * @author lqb
 */
@FeignClient(value = "business", contextId = "OfficeDataCustomRpcApi", path = "/office/data/custom", fallbackFactory = RemoteOfficeDataCustomRpcServiceFactory.class)
public interface OfficeDataCustomRpcApi extends OfficeDataCustomApi {
}
