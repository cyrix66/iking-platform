package com.ikingtech.framework.sdk.office.rpc.api;

import com.ikingtech.framework.sdk.office.api.OfficeDataApi;
import com.ikingtech.framework.sdk.office.rpc.factory.RemoteOfficeDataRpcServiceFactory;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * office数据rpc接口
 *
 * @author lqb
 * @since 2023/09/21
 */
@FeignClient(value = "business", contextId = "OfficeDataRpcApi", path = "/office/data", fallbackFactory = RemoteOfficeDataRpcServiceFactory.class)
public interface OfficeDataRpcApi extends OfficeDataApi {
}
