package com.ikingtech.framework.sdk.office.rpc.api;

import com.ikingtech.framework.sdk.office.api.OfficeTempApi;
import com.ikingtech.framework.sdk.office.rpc.factory.RemoteOfficeTempRpcServiceFactory;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * office模板rpc接口
 *
 * @author lqb
 * @since 2023/09/21
 */
@FeignClient(value = "business", contextId = "OfficeTempRpcApi", path = "/office/temp", fallbackFactory = RemoteOfficeTempRpcServiceFactory.class)
public interface OfficeTempRpcApi extends OfficeTempApi {
}
