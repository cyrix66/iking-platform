package com.ikingtech.framework.sdk.office.rpc.factory;

import com.ikingtech.framework.sdk.office.api.OfficeDataCustomApi;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import org.springframework.cloud.openfeign.FallbackFactory;

/**
 * 远程调用office自定义数据服务降级处理类
 *
 * @author lqb
 */
public class RemoteOfficeDataCustomRpcServiceFactory implements FallbackFactory<OfficeDataCustomApi> {
    @Override
    public OfficeDataCustomApi create(Throwable cause) {
        throw new FrameworkException("远程调用office自定义数据服务失败");
    }
}
