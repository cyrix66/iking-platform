package com.ikingtech.framework.sdk.office.rpc.factory;

import com.ikingtech.framework.sdk.office.api.OfficeDataApi;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import org.springframework.cloud.openfeign.FallbackFactory;

/**
 * 远程调用office数据服务降级处理类
 *
 * @author lqb
 */
public class RemoteOfficeDataRpcServiceFactory implements FallbackFactory<OfficeDataApi> {
    @Override
    public OfficeDataApi create(Throwable cause) {
        throw new FrameworkException("远程调用office数据服务失败");
    }
}
