package com.ikingtech.framework.sdk.office.rpc.factory;

import com.ikingtech.framework.sdk.office.api.OfficeTempApi;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import org.springframework.cloud.openfeign.FallbackFactory;

/**
 * 远程调用office模板服务降级处理工厂
 *
 * @author lqb
 */
public class RemoteOfficeTempRpcServiceFactory implements FallbackFactory<OfficeTempApi> {
    @Override
    public OfficeTempApi create(Throwable cause) {
        throw new FrameworkException("调用office模板服务失败");
    }
}
