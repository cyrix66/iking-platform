package com.ikingtech.framework.sdk.oss.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@Schema(name = "OssFileDTO", description = "文件信息")
public class OssFileDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 4755664223505494572L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "originName", description = "原始文件名称")
    private String originName;

    @Schema(name = "fileSize", description = "文件大小")
    private Long fileSize;

    @Schema(name = "suffix", description = "文件后缀")
    private String suffix;

    @Schema(name = "dirName", description = "文件服务器目录")
    private String dirName;

    @Schema(name = "path", description = "文件服务器路径")
    private String path;

    @Schema(name = "url", description = "文件访问相对路径")
    private String url;

    @Schema(name = "createTime", description = "创建时间")
    private LocalDateTime createTime;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateTime", description = "更新时间")
    private LocalDateTime updateTime;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;
}
