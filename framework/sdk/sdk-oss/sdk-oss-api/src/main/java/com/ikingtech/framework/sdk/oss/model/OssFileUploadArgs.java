package com.ikingtech.framework.sdk.oss.model;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class OssFileUploadArgs implements Serializable {

    @Serial
    private static final long serialVersionUID = -5043546110631927310L;

    private Long fileSize;

    private String filePath;
}
