package com.ikingtech.framework.sdk.oss.embedded.core;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.InputStream;

/**
 * @author tie yan
 */
@Data
@AllArgsConstructor
public class OssResponse {

    private String contentType;

    private InputStream objectStream;
}
