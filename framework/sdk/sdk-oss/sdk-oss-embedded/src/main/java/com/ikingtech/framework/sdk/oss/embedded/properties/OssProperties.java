package com.ikingtech.framework.sdk.oss.embedded.properties;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@Data
@ConfigurationProperties(prefix = GLOBAL_CONFIG_PREFIX + ".oss")
public class OssProperties {

    /**
     * 是否开启文件服务<br />
     * 默认不开启
     */
    private Boolean enabled;

    /**
     * 文件存储类型
     */
    private StorageTypeEnum storageType;

    /**
     * 文件存储地址
     */
    private String url;

    /**
     * 文件重定向地址
     */
    private String redirectUrl;

    /**
     * 文件存储accessKey
     */
    private String accessKey;

    /**
     * 文件存储secretKey
     */
    private String secretKey;

    /**
     * 文件存储用户名
     */
    private String username;

    /**
     * 文件存储密码
     */
    private String password;

    /**
     * 默认桶名
     */
    private String defaultBucketName;

    private Boolean autoCreateBucket;

    /**
     * 区域
     */
    private String region;

    @Getter
    @RequiredArgsConstructor
    public enum StorageTypeEnum {

        /**
         * 本地存储
         */
        LOCAL("本地存储"),

        /**
         * Minio存储
         */
        MINIO("Minio存储"),

        /**
         * Amazon S3存储
         */
        AWS_S3("Amazon S3存储"),

        /**
         * 阿里云存储
         */
        ALI_OSS("阿里云存储"),

        /**
         * 阿里云存储
         */
        TENCENT_COS("腾讯云存储");

        private final String description;
    }
}
