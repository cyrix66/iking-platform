package com.ikingtech.framework.sdk.oss.rpc.api;

import com.ikingtech.framework.sdk.oss.api.OssApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "OssRpcApi", path = "/oss")
public interface OssRpcApi extends OssApi {
}
