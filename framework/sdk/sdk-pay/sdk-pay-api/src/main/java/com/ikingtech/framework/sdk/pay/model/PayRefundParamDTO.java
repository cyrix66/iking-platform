package com.ikingtech.framework.sdk.pay.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "PayRefundParamDTO", description = "退款参数")
public class PayRefundParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -7360673974576324381L;

    @Schema(name = "merchantId", description = "商户编号")
    private String merchantId;

    @Schema(name = "amount", description = "支付金额")
    private Double amount;

    @Schema(name = "requestId", description = "支付流水号")
    private String requestId;
}
