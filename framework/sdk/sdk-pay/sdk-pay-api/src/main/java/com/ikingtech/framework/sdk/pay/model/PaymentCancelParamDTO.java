package com.ikingtech.framework.sdk.pay.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "PaymentCancelParamDTO", description = "支付参数")
public class PaymentCancelParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 8043618403225675762L;

    @Schema(name = "amount", description = "支付金额")
    private Double amount;

    @Schema(name = "requestId", description = "支付流水号")
    private String requestId;
}
