package com.ikingtech.framework.sdk.pay.model;

import com.ikingtech.framework.sdk.enums.pay.PayTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Map;

/**
 * @author tie yan
 */
@Data
@Schema(name = "PaymentDTO", description = "支付参数")
public class PaymentDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 8043618403225675762L;

    @Schema(name = "merchantId", description = "商户编号")
    private String merchantId;

    @Schema(name = "supplierId", description = "支付平台编号")
    private String supplierId;

    @Schema(name = "payType", description = "支付类型")
    private PayTypeEnum payType;

    @Schema(name = "amount", description = "支付金额")
    private Double amount;

    @Schema(name = "requestId", description = "支付流水号")
    private String requestId;

    @Schema(name = "title", description = "订单标题")
    private String title;

    @Schema(name = "channelUserId", description = "支付渠道用户标识，用户的微信openid/用户user_id/用户小程序的openid等")
    private String channelUserId;

    @Schema(name = "channelAppId", description = "支付渠道AppId，微信小程序/微信公众号/APP的appid")
    private String channelAppId;

    @Schema(name = "option", description = "扩展参数")
    private Map<String, String> option;
}
