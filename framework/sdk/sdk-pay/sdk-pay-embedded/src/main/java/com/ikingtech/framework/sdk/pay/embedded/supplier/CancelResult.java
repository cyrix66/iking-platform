package com.ikingtech.framework.sdk.pay.embedded.supplier;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@AllArgsConstructor
public class CancelResult implements Serializable {

    @Serial
    private static final long serialVersionUID = -8703814586406430486L;

    public CancelResult() {
        this.success = true;
    }

    private Boolean success;

    private String cause;

    public boolean success() {
        return Boolean.TRUE.equals(this.success);
    }
}
