package com.ikingtech.framework.sdk.pay.embedded.supplier;

import com.ikingtech.framework.sdk.enums.pay.CashierSupplierEnum;
import lombok.Data;

/**
 * @author tie yan
 */
@Data
public class CashierSupplierConfig {

    private CashierSupplierEnum type;

    private String customId;

    private String customName;

    private String customSerialNo;

    private String appId;

    private String apiKey;

    private String extraApiKey;

    private String privateKey;

    private String certKey;

    private String publicKey;

    private String notifyUrl;
}
