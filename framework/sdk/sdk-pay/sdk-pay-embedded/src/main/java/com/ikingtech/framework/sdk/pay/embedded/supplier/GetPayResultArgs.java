package com.ikingtech.framework.sdk.pay.embedded.supplier;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
public class GetPayResultArgs implements Serializable {

    @Serial
    private static final long serialVersionUID = -9097400226439871726L;

    public GetPayResultArgs(Builder builder) {
        this.requestId = builder.requestId;
        this.supplierTradeId = builder.supplierTradeId;
        this.channelTradeId = builder.channelTradeId;
    }

    private String requestId;

    private String supplierTradeId;

    private String channelTradeId;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String requestId;

        private String supplierTradeId;

        private String channelTradeId;



        public Builder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public Builder supplierTradeId(String supplierTradeId) {
            this.supplierTradeId = supplierTradeId;
            return this;
        }

        public Builder channelTradeId(String channelTradeId) {
            this.channelTradeId = channelTradeId;
            return this;
        }

        public GetPayResultArgs build() {
            return new GetPayResultArgs(this);
        }
    }
}
