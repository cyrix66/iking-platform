package com.ikingtech.framework.sdk.pay.embedded.supplier;

import com.ikingtech.framework.sdk.enums.pay.PayTypeEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Map;

/**
 * @author tie yan
 */
@Data
public class PayArgs implements Serializable {

    @Serial
    private static final long serialVersionUID = 7172073038921217618L;

    public PayArgs(Builder builder) {
        this.payType = builder.payType;
        this.amount = builder.amount;
        this.subject = builder.subject;
        this.requestId = builder.requestId;
        this.channelUserId = builder.channelUserId;
        this.channelAppId = builder.channelAppId;
        this.option = builder.option;
    }

    private PayTypeEnum payType;

    private Double amount;

    private String subject;

    private String requestId;

    private String channelUserId;

    private String channelAppId;

    private Map<String, String> option;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private PayTypeEnum payType;

        private Double amount;

        private String subject;

        private String requestId;

        private String channelUserId;

        private String channelAppId;

        private Map<String, String> option;

        public Builder payType(PayTypeEnum payType) {
            this.payType = payType;
            return this;
        }

        public Builder amount(Double amount) {
            this.amount = amount;
            return this;
        }

        public Builder subject(String subject) {
            this.subject = subject;
            return this;
        }

        public Builder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public Builder channelUserId(String channelUserId) {
            this.channelUserId = channelUserId;
            return this;
        }

        public Builder channelAppId(String channelAppId) {
            this.channelAppId = channelAppId;
            return this;
        }

        public Builder option(Map<String, String> option) {
            this.option = option;
            return this;
        }

        public PayArgs build() {
            return new PayArgs(this);
        }
    }
}
