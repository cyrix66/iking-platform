package com.ikingtech.framework.sdk.pay.embedded.supplier;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class RedPackResult implements Serializable {

    @Serial
    private static final long serialVersionUID = 5070601065820706085L;

    private String supplierTradeId;

    private String channelTradeId;

    private String requestId;

    private Boolean success;

    private Boolean sending;

    private String cause;

    public boolean success() {
        return Boolean.TRUE.equals(this.success);
    }
}
