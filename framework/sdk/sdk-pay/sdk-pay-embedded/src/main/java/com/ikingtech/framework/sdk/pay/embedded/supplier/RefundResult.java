package com.ikingtech.framework.sdk.pay.embedded.supplier;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@AllArgsConstructor
public class RefundResult implements Serializable {

    @Serial
    private static final long serialVersionUID = 3451399526929479462L;

    public RefundResult() {
        this.success = true;
    }

    public RefundResult(String cause) {
        this.success = false;
        this.cause = cause;
    }

    private Boolean success;

    private String cause;
}
