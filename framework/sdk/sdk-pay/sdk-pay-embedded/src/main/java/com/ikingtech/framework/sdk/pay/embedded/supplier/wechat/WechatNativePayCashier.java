package com.ikingtech.framework.sdk.pay.embedded.supplier.wechat;

import com.ikingtech.framework.sdk.enums.pay.CashierSupplierEnum;
import com.ikingtech.framework.sdk.pay.embedded.supplier.CashierSupplierConfig;
import com.ikingtech.framework.sdk.pay.embedded.supplier.PayArgs;
import com.ikingtech.framework.sdk.pay.embedded.supplier.PayOrder;
import com.wechat.pay.java.core.RSAAutoCertificateConfig;
import com.wechat.pay.java.service.payments.nativepay.NativePayService;
import com.wechat.pay.java.service.payments.nativepay.model.Amount;
import com.wechat.pay.java.service.payments.nativepay.model.CloseOrderRequest;
import com.wechat.pay.java.service.payments.nativepay.model.PrepayRequest;
import com.wechat.pay.java.service.payments.nativepay.model.PrepayResponse;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * @author tie yan
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class WechatNativePayCashier {

    public static PayOrder prepay(PayArgs payArgs, CashierSupplierConfig config, RSAAutoCertificateConfig rsaConfig) {
        PrepayRequest request = new PrepayRequest();
        Amount amount = new Amount();
        amount.setTotal((int) (payArgs.getAmount() * 100));
        request.setAmount(amount);
        request.setMchid(config.getCustomId());
        request.setAppid(config.getAppId());
        request.setDescription(payArgs.getSubject());
        request.setNotifyUrl(config.getNotifyUrl() + "/" + CashierSupplierEnum.WECHAT_PAY.name() + "/" + payArgs.getRequestId());
        request.setOutTradeNo(payArgs.getRequestId());
        NativePayService payService = new NativePayService.Builder().config(rsaConfig).build();
        PrepayResponse prepayRes = payService.prepay(request);
        PayOrder payOrder = new PayOrder();
        payOrder.setRequestId(payArgs.getRequestId());
        payOrder.setPayInfo(prepayRes.getCodeUrl());
        return payOrder;
    }

    public static void closeOrder(String requestId, CashierSupplierConfig config, RSAAutoCertificateConfig rsaConfig) {
        CloseOrderRequest request = new CloseOrderRequest();
        request.setMchid(config.getCustomId());
        request.setOutTradeNo(requestId);
        NativePayService payService = new NativePayService.Builder().config(rsaConfig).build();
        payService.closeOrder(request);
    }
}
