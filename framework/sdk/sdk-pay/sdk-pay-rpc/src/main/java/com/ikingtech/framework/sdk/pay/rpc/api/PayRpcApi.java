package com.ikingtech.framework.sdk.pay.rpc.api;

import com.ikingtech.framework.sdk.pay.api.PayApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "PayRpcApi", path = "/pay")
public interface PayRpcApi extends PayApi {
}
