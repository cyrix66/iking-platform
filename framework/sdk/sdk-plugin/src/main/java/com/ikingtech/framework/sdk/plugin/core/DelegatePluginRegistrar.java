package com.ikingtech.framework.sdk.plugin.core;

import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class DelegatePluginRegistrar implements PluginRegistrar {

    private final List<PluginRegistrar> registrars;

    public void register(PluginRegistrarArgs args) {
        for (PluginRegistrar registrar : registrars) {
            if (registrar.type().equals(args.getType())) {
                registrar.register(args);
            }
        }
    }

    public void unregister(PluginRegistrarArgs args) {
        for (PluginRegistrar registrar : registrars) {
            if (registrar.type().equals(args.getType())) {
                registrar.unregister(args);
            }
        }
    }
}
