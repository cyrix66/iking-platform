package com.ikingtech.framework.sdk.post.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 岗位信息查询参数
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "PostQueryParamDTO", description = "岗位信息查询参数")
public class PostQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 8767303210649373544L;

    @Schema(name = "name", description = "岗位名称")
    private String name;

    @Schema(name = "remark", description = "岗位描述")
    private String remark;

    @Schema(name = "createName", description = "创建人")
    private String createName;
}
