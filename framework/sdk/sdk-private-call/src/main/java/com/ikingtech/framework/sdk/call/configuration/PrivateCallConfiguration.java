package com.ikingtech.framework.sdk.call.configuration;

import com.ikingtech.framework.sdk.call.PrivateCall;
import com.ikingtech.framework.sdk.call.huawei.HuaweiPrivateCall;
import com.ikingtech.framework.sdk.call.properties.PrivateCallProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@EnableConfigurationProperties({PrivateCallProperties.class})
public class PrivateCallConfiguration {

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".private-call", name = "platform", havingValue = "Huawei")
    public PrivateCall huaweiPrivateCall(PrivateCallProperties properties) {
        return new HuaweiPrivateCall(properties);
    }
}
