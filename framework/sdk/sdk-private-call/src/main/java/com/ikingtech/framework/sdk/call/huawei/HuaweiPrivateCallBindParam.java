package com.ikingtech.framework.sdk.call.huawei;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class HuaweiPrivateCallBindParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -3534882967857421525L;

    private String callerNum;

    private String relationNum;

    private String areaCode;

    private String areaMatchMode;

    private String calleeNum;

    private Integer callDirection;

    private Integer duration;

    private String recordFlag;

    private String recordHintTone;

    private ApiPayInfo preVoice;

    private Integer maxDuration;

    private String lastMinVoice;

    private String privateSms;

    private String userData;

    @Data
    public static class ApiPayInfo implements Serializable{

        @Serial
    private static final long serialVersionUID = -1000852992730644601L;

        private String callerHintTone;

        private String calleeHintTone;
    }
}
