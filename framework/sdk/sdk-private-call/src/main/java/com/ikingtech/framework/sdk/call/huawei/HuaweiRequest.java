package com.ikingtech.framework.sdk.call.huawei;

import com.ikingtech.framework.sdk.utils.Tools;

import java.util.HashMap;
import java.util.Map;

/**
 * @author tie yan
 */
public class HuaweiRequest {

    private HuaweiRequest() {
        // do nothing
    }

    public static String post(String url, String param, String appKey, String secretKey) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "AKSK realm=\"SDP\",profile=\"UsernameToken\",type=\"Appkey\"");
        headers.put("X-AKSK", Signature.build(appKey, secretKey));
        return Tools.Http.post(url, param, headers);
    }

    public static String delete(String url, String param, String appKey, String secretKey) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "AKSK realm=\"SDP\",profile=\"UsernameToken\",type=\"Appkey\"");
        headers.put("X-AKSK", Signature.build(appKey, secretKey));
        return Tools.Http.delete(url, param, headers);
    }
}
