package com.ikingtech.framework.sdk.call.huawei;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class HuaweiResponse implements Serializable {

    @Serial
    private static final long serialVersionUID = -4060103279609625248L;

    private String resultcode;

    private String resultdesc;

    public boolean fail() {
        return !"0".equals(this.resultcode);
    }
}
