package com.ikingtech.framework.sdk.role.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.role.model.RoleDTO;
import com.ikingtech.framework.sdk.role.model.RoleMenuUpdateParamDTO;
import com.ikingtech.framework.sdk.role.model.RoleQueryParamDTO;
import com.ikingtech.framework.sdk.role.model.RoleUserUpdateParamDTO;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * 角色管理模块
 *
 * @author tie yan
 */
public interface RoleApi {

    /**
     * 添加角色信息
     *
     * @param role 角色信息
     * @return 返回角色信息
     */
    @PostRequest(order = 1, value = "/add", summary = "添加角色信息", description = "添加角色信息")
    R<String> add(@Parameter(name = "role", description = "角色信息")
                  @RequestBody RoleDTO role);

    /**
     * 删除角色信息
     *
     * @param id 编号
     * @return 返回删除结果
     */
    @PostRequest(order = 2, value = "/delete", summary = "删除角色信息", description = "删除角色信息")
    R<Object> delete(@Parameter(name = "id", description = "编号")
                     @RequestBody String id);

    /**
     * 更新角色信息
     *
     * @param role 角色信息
     * @return 返回更新结果
     */
    @PostRequest(order = 3, value = "/update", summary = "更新角色信息", description = "更新角色信息")
    R<Object> update(@Parameter(name = "role", description = "角色信息")
                     @RequestBody RoleDTO role);

    /**
     * 分页查询角色信息
     *
     * @param queryParam 查询条件
     * @return 返回角色信息列表
     */
    @PostRequest(order = 4, value = "/list/page", summary = "分页查询角色信息", description = "分页查询角色信息")
    R<List<RoleDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                          @RequestBody RoleQueryParamDTO queryParam);

    /**
     * 查询所有角色信息
     *
     * @return 返回角色信息列表
     */
    @PostRequest(order = 5, value = "/list/all", summary = "查询所有角色信息", description = "查询所有角色信息")
    R<List<RoleDTO>> all();

    /**
     * 获取角色信息
     *
     * @param id 编号
     * @return 返回角色信息
     */
    @PostRequest(order = 6, value = "/detail/id", summary = "获取角色信息", description = "获取角色信息")
    R<RoleDTO> detail(@Parameter(name = "id", description = "编号")
                      @RequestBody String id);

    /**
     * 更新角色菜单分配信息
     *
     * @param roleMenu 角色菜单分配信息
     * @return 返回更新结果
     */
    @PostRequest(order = 7, value = "/menu/update", summary = "更新角色菜单分配信息", description = "更新角色菜单分配信息")
    R<Object> updateRoleMenu(@Parameter(name = "roleMenu", description = "角色菜单分配信息")
                             @RequestBody RoleMenuUpdateParamDTO roleMenu);

    /**
     * 添加角色用户
     *
     * @param updateParam 角色用户更新信息
     * @return 返回添加结果
     */
    @PostRequest(order = 8, value = "/user/add", summary = "添加角色用户", description = "添加角色用户")
    R<Object> addRoleUser(@Parameter(name = "updateParam", description = "角色用户更新信息")
                          @RequestBody RoleUserUpdateParamDTO updateParam);

    /**
     * 删除角色用户
     *
     * @param updateParam 角色用户更新信息
     * @return 返回删除结果
     */
    @PostRequest(order = 9, value = "/user/remove", summary = "删除角色用户", description = "删除角色用户")
    R<Object> removeRoleUser(@Parameter(name = "updateParam", description = "角色用户更新信息")
                             @RequestBody RoleUserUpdateParamDTO updateParam);
}
