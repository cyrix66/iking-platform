package com.ikingtech.framework.sdk.role.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.system.role.DataScopeTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 角色信息
 *
 * @author tie yan
 */
@Data
@Schema(name = "RoleDTO", description = "角色信息")
public class RoleDTO implements Serializable {

	@Serial
    private static final long serialVersionUID = -9209450223801107852L;

	@Schema(name = "id", description = "角色编号")
	private String id;

	@Schema(name = "name", description = "角色名称")
	private String name;

	@Schema(name = "domainCode", description = "租户code")
	private String domainCode;

	@Schema(name = "tenantCode", description = "租户code")
	private String tenantCode;

	@Schema(name = "appCode", description = "租户code")
	private String appCode;

	@Schema(name = "dataScopeType", description = "数据权限类型")
	private DataScopeTypeEnum dataScopeType;

	@Schema(name = "dataScopeTypeName", description = "数据权限类型名称")
	private String dataScopeTypeName;

	@Schema(name = "dataScopeCodes", description = "数据权限码集合")
	private List<String> dataScopeCodes;

	@Schema(name = "remark", description = "角色描述")
	private String remark;

	@Schema(name = "sortOrder", description = "排序值")
	private Integer sortOrder;

	@Schema(name = "deletable", description = "是否可以删除")
	private Boolean deletable;

	@Schema(name = "createBy", description = "创建人编号")
	private String createBy;

	@Schema(name = "createName", description = "创建人姓名")
	private String createName;

	@Schema(name = "updateBy", description = "更新人编号")
	private String updateBy;

	@Schema(name = "updateName", description = "更新人姓名")
	private String updateName;

	@Schema(name = "createTime", description = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private LocalDateTime createTime;

	@Schema(name = "updateTime", description = "更新时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private LocalDateTime updateTime;
}
