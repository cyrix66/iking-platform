package com.ikingtech.framework.sdk.role.model;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class RoleMenuDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 2917374090288788165L;

    /**
     * 角色id
     */
    private String roleId;

    /**
     * 菜单id
     */
    private String menuId;

    /**
     * 租户code
     */
    private String tenantCode;
}
