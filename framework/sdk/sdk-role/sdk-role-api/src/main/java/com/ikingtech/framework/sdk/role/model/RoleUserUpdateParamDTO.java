package com.ikingtech.framework.sdk.role.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "RoleUserUpdateParamDTO", description = "更新角色用户参数")
public class RoleUserUpdateParamDTO implements Serializable {

	@Serial
    private static final long serialVersionUID = -4376997976920569359L;

	@Schema(name = "roleId", description = "角色编号")
	private String roleId;

	@Schema(name = "userIds", description = "用户编号集合")
	private List<String> userIds;
}
