package com.ikingtech.framework.sdk.rpc.configuration;

import com.ikingtech.framework.sdk.rpc.decoder.DefaultFeignReason;
import com.ikingtech.framework.sdk.rpc.decoder.FeignErrorDecoder;
import com.ikingtech.framework.sdk.rpc.decoder.FeignErrorReason;
import com.ikingtech.framework.sdk.rpc.interceptor.FeignClientInterceptor;
import feign.RequestInterceptor;
import feign.codec.ErrorDecoder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@EnableFeignClients(basePackages = "**.**.rpc.**")
@ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
public class FeignConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public FeignErrorReason feignErrorReason() {
        return new DefaultFeignReason();
    }

    @Bean
    public ErrorDecoder errorDecoder(FeignErrorReason reason) {
        return new FeignErrorDecoder(reason);
    }

    @Bean
    public RequestInterceptor requestInterceptor() {
        return new FeignClientInterceptor();
    }
}
