package com.ikingtech.framework.sdk.rpc.decoder;

import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class FeignErrorDecoder implements ErrorDecoder {

    private final FeignErrorReason feignErrorReason;

    @Override
    public Exception decode(String methodKey, Response response) {
        return new FrameworkException(feignErrorReason.is(methodKey, response));
    }
}
