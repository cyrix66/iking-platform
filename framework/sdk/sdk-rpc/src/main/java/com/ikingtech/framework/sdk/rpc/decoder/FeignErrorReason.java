package com.ikingtech.framework.sdk.rpc.decoder;

import feign.Response;

/**
 * @author tie yan
 */
public interface FeignErrorReason {

    /**
     * 获取失败原因
     *
     * @param methodKey 方法名
     * @param response 响应体
     * @return 失败圆心
     */
    String is(String methodKey, Response response);
}
