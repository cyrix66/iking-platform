package com.ikingtech.framework.sdk.rpc.interceptor;

import com.ikingtech.framework.sdk.context.security.Identity;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static com.ikingtech.framework.sdk.context.constant.SecurityConstants.HEADER_CALLER;
import static com.ikingtech.framework.sdk.context.constant.SecurityConstants.HEADER_INNER_IDENTITY;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class FeignClientInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header(HEADER_CALLER, "INNER");
        requestTemplate.header(HEADER_INNER_IDENTITY, Identity.inner());
    }
}
