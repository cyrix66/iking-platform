package com.ikingtech.framework.sdk.security.configuration;

import com.ikingtech.framework.sdk.authenticate.operation.IdentityValidator;
import com.ikingtech.framework.sdk.security.filter.SecurityFilter;
import com.ikingtech.framework.sdk.security.properties.SecurityProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.StringRedisTemplate;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;
import static com.ikingtech.framework.sdk.context.constant.CommonConstants.SECURITY_FILTER_ORDER;

/**
 * @author tie yan
 */
@EnableConfigurationProperties({SecurityProperties.class})
@ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".security", name = "enable", havingValue = "true", matchIfMissing = true)
public class SecurityConfiguration {

    @Bean("securityFilterRegistrationBean")
    public FilterRegistrationBean<SecurityFilter> securityFilterRegistrationBean(IdentityValidator identityValidator,
                                                                                 SecurityProperties properties,
                                                                                 StringRedisTemplate redisTemplate) {
        FilterRegistrationBean<SecurityFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new SecurityFilter(identityValidator, properties, redisTemplate));
        registrationBean.setName("securityFilter");
        registrationBean.addUrlPatterns("/*");
        registrationBean.setOrder(SECURITY_FILTER_ORDER);
        return registrationBean;
    }
}
