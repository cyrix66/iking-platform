package com.ikingtech.framework.sdk.sms.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.sms.model.SmsDTO;
import com.ikingtech.framework.sdk.sms.model.SmsQueryParamDTO;
import com.ikingtech.framework.sdk.sms.model.SmsSendMessageParamDTO;
import com.ikingtech.framework.sdk.sms.model.SmsTemplateDTO;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author tie yan
 */
public interface SmsApi {

    /**
     * 添加短信平台信息
     *
     * @param sms 短信平台信息
     * @return 返回添加结果
     */
    @PostRequest(order = 1,value = "/add", summary = "添加短信平台信息", description = "添加短信平台信息")
    R<String> add(@Parameter(name = "sms", description = "短信平台信息")
                  @RequestBody SmsDTO sms);

    /**
     * 删除短信平台信息
     *
     * @param id 编号
     * @return 返回删除结果
     */
    @PostRequest(order = 2,value = "/delete", summary = "删除短信平台信息", description = "删除短信平台信息")
    R<Object> delete(@Parameter(name = "id", description = "编号")
                     @RequestBody String id);

    /**
     * 更新短信平台信息
     *
     * @param sms 短信平台信息
     * @return 返回更新结果
     */
    @PostRequest(order = 3,value = "/update", summary = "更新短信平台信息", description = "更新短信平台信息")
    R<Object> update(@Parameter(name = "sms", description = "短信平台信息")
                     @RequestBody SmsDTO sms);

    /**
     * 分页查询短信平台信息
     *
     * @param queryParam 查询条件
     * @return 返回分页查询结果
     */
    @PostRequest(order = 4,value = "/list/page", summary = "分页查询短信平台信息", description = "分页查询短信平台信息")
    R<List<SmsDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                         @RequestBody SmsQueryParamDTO queryParam);

    /**
     * 查询所有短信平台信息
     *
     * @return 返回所有短信平台信息
     */
    @PostRequest(order = 5,value = "/list/all", summary = "查询所有短信平台信息", description = "查询所有短信平台信息")
    R<List<SmsDTO>> all();

    /**
     * 查询指定编号的短信平台信息
     *
     * @param id 编号
     * @return 返回指定编号的短信平台信息
     */
    @PostRequest(order = 6,value = "/detail/id", summary = "查询指定编号的短信平台信息", description = "查询指定编号的短信平台信息")
    R<SmsDTO> detail(@RequestBody String id);

    /**
     * 发送短信
     *
     * @param sendMessageParam 发送短信参数
     * @return 返回发送结果
     */
    @PostRequest(order = 7,value = "/send", summary = "发送短信", description = "发送短信")
    R<Object> send(@RequestBody SmsSendMessageParamDTO sendMessageParam);

    /**
     * 查询短信平台模板列表
     *
     * @param smsId 短信平台ID
     * @return 返回短信平台模板列表
     */
    @PostRequest(order = 8,value = "/template/list", summary = "查询短信平台模板列表", description = "查询短信平台模板列表")
    R<List<SmsTemplateDTO>> listTemplate(@RequestBody String smsId);
}
