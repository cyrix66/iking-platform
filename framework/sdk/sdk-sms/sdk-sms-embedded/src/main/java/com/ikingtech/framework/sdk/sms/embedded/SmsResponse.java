package com.ikingtech.framework.sdk.sms.embedded;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author Administrator
 */
@Data
public class SmsResponse implements Serializable {

    @Serial
    private static final long serialVersionUID = 2369292196889622607L;


}
