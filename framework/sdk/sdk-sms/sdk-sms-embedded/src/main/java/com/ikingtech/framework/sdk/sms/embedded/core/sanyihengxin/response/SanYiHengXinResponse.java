package com.ikingtech.framework.sdk.sms.embedded.core.sanyihengxin.response;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class SanYiHengXinResponse implements Serializable {

    @Serial
    private static final long serialVersionUID = 4083400784871204323L;

    private Integer code;

    private String message;

    private List<ResponseData> data;

    @Data
    public static class ResponseData implements Serializable{

        @Serial
    private static final long serialVersionUID = 7232238612270619083L;

        private String code;

        private String message;

        private String msgId;

        private String phone;
    }

    public boolean fail() {
        return 0 != this.code;
    }
}
