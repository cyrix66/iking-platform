package com.ikingtech.framework.sdk.tenant.api;

import java.util.List;

/**
 * @author tie yan
 */
public interface InitializerTenantApi {

    List<String> loadAllCodes();
}
