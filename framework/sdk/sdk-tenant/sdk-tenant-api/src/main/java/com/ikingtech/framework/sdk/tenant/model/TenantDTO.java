package com.ikingtech.framework.sdk.tenant.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.system.tenant.TenantStatusEnum;
import com.ikingtech.framework.sdk.enums.system.tenant.TenantTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 租户信息
 *
 * @author tie yan
 */
@Data
@Schema(name = "TenantDTO", description = "租户信息")
public class TenantDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -5606985776069675794L;

    @Schema(name = "id", description = "租户编号")
    private String id;

    @Schema(name = "name", description = "租户名称")
    private String name;

    @Length(max = 64, message = "{tenant.codeLength}")
    @Schema(name = "code", description = "租户标识")
    private String code;

    @Schema(name = "type", description = "租户类别")
    private TenantTypeEnum type;

    @Schema(name = "typeName", description = "租户类别名称")
    private String typeName;

    @Schema(name = "tenantDomain", description = "租户域名")
    private String tenantDomain;

    @Schema(name = "startDate", description = "租户开始日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @Schema(name = "endDate", description = "租户结束日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @Schema(name = "favicon", description = "浏览器页签图标")
    private String favicon;

    @Schema(name = "faviconName", description = "浏览器页签图标名称")
    private String faviconName;

    @Schema(name = "element", description = "元素")
    private String element;

    @Schema(name = "arrange", description = "排列方式")
    private String arrange;

    @Schema(name = "alignment", description = "水平对齐")
    private String alignment;

    @Schema(name = "menuName", description = "名称（默认为租户名称，可修改，建议名称在8个字内）")
    private String menuName;

    @Schema(name = "logo", description = "租户Logo")
    private String logo;

    @Schema(name = "icon", description = "租户ICON")
    private String icon;

    @Schema(name = "iconOpen", description = "图标（左侧菜单展开）")
    private String iconOpen;

    @Schema(name = "iconClose", description = "图标（左侧菜单收起）")
    private String iconClose;

    @Schema(name = "homePage", description = "租户登录后默认跳转页面地址")
    private String homePage;

    @Schema(name = "status", description = "租户状态")
    private TenantStatusEnum status;

    @Schema(name = "statusName", description = "租户状态名称")
    private String statusName;

    @Length(max = 200, message = "tenantRemark")
    @Schema(name = "remark", description = "租户描述")
    private String remark;

    @Schema(name = "sortOrder", description = "排序值")
    private Integer sortOrder;

    @Schema(name = "delFlag", description = "是否默认租户")
    private Boolean defaultFlag;

    @Schema(name = "delFlag", description = "是否已删除")
    private Boolean delFlag;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
