package com.ikingtech.framework.sdk.tenant.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * 租户信息查询参数
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "TenantQueryParamDTO", description = "租户信息查询参数")
public class TenantQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 8767303210649373544L;

    @Schema(name = "name", description = "租户名称")
    private String name;

    @Schema(name = "adminUsername", description = "管理员用户名")
    private String adminUsername;

    @Schema(name = "code", description = "租户标识")
    private String code;

    @Schema(name = "type", description = "租户类型", allowableValues = "MASTER, NORMAL")
    private String type;

    @Schema(name = "tenantDomain", description = "租户域名")
    private String tenantDomain;

    @Schema(name = "status", description = "租户状态", allowableValues = "NORMAL, DATA_MIGRATING, DATA_MIGRATE_FAIL, FREEZE")
    private String status;

    @Schema(name = "startTimeStartDate", description = "租户开始日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate startTimeStartDate;

    @Schema(name = "startTimeEndDate", description = "租户开始日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate startTimeEndDate;

    @Schema(name = "endDateTimeStartDate", description = "租户结束日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate endTimeStartDate;

    @Schema(name = "endDateTimeEndDate", description = "租户结束日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate endTimeEndDate;

    @Schema(name = "createTimeStartDate", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate createTimeStartDate;

    @Schema(name = "createTimeEndDate", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate createTimeEndDate;

    @Schema(name = "updateTimeStartDate", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate updateTimeStartDate;

    @Schema(name = "updateTimeEndDate", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate updateTimeEndDate;
}
