package com.ikingtech.framework.sdk.tenant.rpc.api;

import com.ikingtech.framework.sdk.tenant.api.TenantApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "TenantRpcApi", path = "/system/tenant")
public interface TenantRpcApi extends TenantApi {
}
