package com.ikingtech.framework.sdk.user.api;

import com.ikingtech.framework.sdk.user.model.UserDeptDTO;

import java.util.List;

/**
 * @author tie yan
 */
public interface UserDeptApi {

    List<UserDeptDTO> loadByIds(List<String> deptIds);

    List<String> loadSubAllId(String deptId);
}
