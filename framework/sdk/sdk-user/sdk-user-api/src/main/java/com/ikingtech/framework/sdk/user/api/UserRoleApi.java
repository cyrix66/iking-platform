package com.ikingtech.framework.sdk.user.api;

import com.ikingtech.framework.sdk.user.model.UserRoleDTO;

import java.util.List;

/**
 * @author tie yan
 */
public interface UserRoleApi {

    List<UserRoleDTO> loadByIds(List<String> roleIds);

    List<String> loadIdByMenuIds(List<String> menuIds, String tenantCode);

    List<String> loadIdByMenuId(String menuId, String tenantCode);

}
