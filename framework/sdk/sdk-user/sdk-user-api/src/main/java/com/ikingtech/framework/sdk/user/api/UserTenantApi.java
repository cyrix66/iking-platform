package com.ikingtech.framework.sdk.user.api;

import com.ikingtech.framework.sdk.user.model.UserTenantDTO;

/**
 * @author tie yan
 */
public interface UserTenantApi {

    /**
     * 根据代码加载用户租户信息
     *
     * @param code 用户租户代码
     * @return 用户租户信息
     */
    UserTenantDTO loadByCode(String code);
}
