package com.ikingtech.framework.sdk.user.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.system.user.UserLockTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 用户基本信息
 *
 * @author tie yan
 */
@Data
@Schema(name = "UserBasicDTO", description = "用户基本信息")
public class UserBasicDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -6732465235885297150L;

    @Schema(name = "id", description = "用户编号")
    private String id;

    @NotBlank(message = "{user.usernameNotBlank}")
    @Length(max = 45, message = "{user.usernameLength}")
    @Schema(name = "username", description = "用户名")
    private String username;

    @Length(max = 128, message = "{user.passwordLength}")
    @Schema(name = "password", description = "用户密码")
    private String password;

    @Schema(name = "passwordModifyTime", description = "密码修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime passwordModifyTime;

    @Length(max = 64, message = "{user.nicknameLength}")
    @Schema(name = "nickname", description = "昵称")
    private String nickname;

    @Length(max = 64, message = "{user.nameLength}")
    @Schema(name = "name", description = "姓名")
    private String name;

    @Length(max = 64, message = "{user.nameLength}")
    @Schema(name = "phone", description = "手机号")
    private String phone;

    @Length(max = 64, message = "{user.nameLength}")
    @Schema(name = "email", description = "邮箱")
    private String email;

    @Schema(name = "avatar", description = "头像地址")
    private String avatar;

    @Length(max = 64, message = "{user.identityNoLength}")
    @Schema(name = "identityNo", description = "身份证号")
    private String identityNo;

    @Schema(name = "locked", description = "是否已锁定")
    private Boolean locked;

    @Schema(name = "lockType", description = "锁定类型")
    private UserLockTypeEnum lockType;

    @Schema(name = "lockTypeName", description = "锁定类型名称")
    private String lockTypeName;

    @Schema(name = "platformUser", description = "是否是平台用户")
    private Boolean platformUser;

    @Schema(name = "adminUser", description = "是否是主管理员")
    private Boolean adminUser;

    @Schema(name = "birthday", description = "生日")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate birthday;

    @Schema(name = "sex", description = "性别")
    private String sex;

    @Schema(name = "socials", description = "用户第三方平台身份标识信息")
    private List<UserSocialDTO> socials;

    @Schema(name = "categoryCodes", description = "用户类别标识集合")
    private List<String> categoryCodes;

    @Schema(name = "categories", description = "用户类别信息集合")
    private List<UserCategoryDTO> categories;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
