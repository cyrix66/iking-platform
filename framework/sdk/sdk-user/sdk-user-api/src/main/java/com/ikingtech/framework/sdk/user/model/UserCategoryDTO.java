package com.ikingtech.framework.sdk.user.model;

import com.ikingtech.framework.sdk.enums.system.user.UserCategoryEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "UserCategoryDTO", description = "用户身份信息")
public class UserCategoryDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -1863139098295488720L;

    @Schema(name = "categoryCode", description = "用户身份")
    private UserCategoryEnum categoryCode;

    @Schema(name = "categoryName", description = "用户身份名称")
    private String categoryName;
}
