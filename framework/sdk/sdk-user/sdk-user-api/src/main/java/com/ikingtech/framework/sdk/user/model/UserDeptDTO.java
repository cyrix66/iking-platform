package com.ikingtech.framework.sdk.user.model;

import com.ikingtech.framework.sdk.enums.system.department.DeptTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "UserDeptDTO", description = "用户组织架构")
public class UserDeptDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -4472789713561231877L;

    @Schema(name = "deptId", description = "组织架构编号")
    private String deptId;

    @Schema(name = "deptName", description = "组织架构名称")
    private String deptName;

    @Schema(name = "deptType", description = "组织架构类型")
    private DeptTypeEnum deptType;

    @Schema(name = "deptTypeName", description = "组织架构类型名称")
    private String deptTypeName;

    @Schema(name = "deptFullPath", description = "组织架构全路径")
    private String deptFullPath;

    @Schema(name = "deptGrade", description = "组织架构级别")
    private Integer deptGrade;

    @Schema(name = "deptCode", description = "组织架构编码")
    private String deptCode;

    @Schema(name = "managerId", description = "主管编号")
    private String managerId;

    @Schema(name = "managerName", description = "主管姓名")
    private String managerName;

    @Schema(name = "managerAvatar", description = "主管头像")
    private String managerAvatar;

    @Schema(name = "unity", description = "所属组织")
    private UserDeptDTO unity;
}
