package com.ikingtech.framework.sdk.user.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "UserDeptUpdateBatchParamDTO", description = "批量更新用户单位参数")
public class UserDeptUpdateBatchParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -126971954259533967L;

    @Schema(name = "userIds", description = "用户编号集合")
    private List<String> userIds;

    @Schema(name = "oldDeptId", description = "原单位编号")
    private String oldDeptId;

    @Schema(name = "newDeptId", description = "新单位编号")
    private String newDeptId;

    @Schema(name = "deptIds", description = "部门编号集合")
    private List<String> deptIds;
}
