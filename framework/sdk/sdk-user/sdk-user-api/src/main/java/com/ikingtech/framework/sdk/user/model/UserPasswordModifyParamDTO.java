package com.ikingtech.framework.sdk.user.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 用户修改密码参数
 * @author tie yan
 */
@Data
@Schema(name = "UserPasswordModifyParamDTO", description = "用户修改密码参数")
public class UserPasswordModifyParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 2326945576535965077L;

    @Schema(name = "userId", description = "用户编号")
    private String userId;

    @Schema(name = "oldPassword", description = "原密码")
    private String oldPassword;

    @Schema(name = "newPassword", description = "新密码")
    private String newPassword;
}
