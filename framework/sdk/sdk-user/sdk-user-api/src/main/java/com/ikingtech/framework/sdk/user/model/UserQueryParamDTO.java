package com.ikingtech.framework.sdk.user.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * 用户信息查询参数
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "UserQueryParamDTO", description = "用户信息查询参数")
public class UserQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 4844729214591339996L;

    @Schema(name = "userIds", description = "用户编号集合")
    private List<String> userIds;

    @Schema(name = "nickname", description = "昵称")
    private String nickname;

    @Schema(name = "name", description = "姓名")
    private String name;

    @Schema(name = "username", description = "姓名")
    private String username;

    @Schema(name = "phone", description = "联系电话")
    private String phone;

    @Schema(name = "phones", description = "联系电话集合")
    private List<String> phones;

    @Schema(name = "email", description = "邮件地址")
    private String email;

    @Schema(name = "locked", description = "是否已锁定")
    private Boolean locked;

    @Schema(name = "lockType", description = "锁定类型")
    private String lockType;

    @Schema(name = "admin", description = "是否是管理员")
    private Boolean admin;

    @Schema(name = "sex", description = "性别")
    private String sex;

    @Schema(name = "deptId", description = "部门编号")
    private String deptId;

    @Schema(name = "currentDeptGradeOnly", description = "是否仅展示本级部门用户")
    private Boolean currentDeptGradeOnly;

    @Schema(name = "postId", description = "岗位编号")
    private String postId;

    @Schema(name = "postIds", description = "部门编号集合")
    private List<String> postIds;

    @Schema(name = "roleId", description = "角色编号")
    private String roleId;

    @Schema(name = "roleIds", description = "角色编号集合")
    private List<String> roleIds;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate updateTime;

    @Schema(name = "keyword", description = "关键字模糊查询")
    private String keyword;

    @Schema(name = "extQueryParam", description = "扩展查询字段")
    private Object extQueryParam;
}
