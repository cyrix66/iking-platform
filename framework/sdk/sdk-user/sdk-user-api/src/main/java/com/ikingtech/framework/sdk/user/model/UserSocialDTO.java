package com.ikingtech.framework.sdk.user.model;

import com.ikingtech.framework.sdk.enums.system.user.UserSocialTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "UserSocialDTO", description = "用户社交号信息")
public class UserSocialDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 4779452977843920485L;

    @Schema(name = "userId", description = "用户编号")
    private String userId;

    @Schema(name = "userName", description = "用户姓名")
    private String userName;

    @Schema(name = "socialType", description = "社交号类型")
    private UserSocialTypeEnum socialType;

    @Schema(name = "socialTypeName", description = "社交号类型名称")
    private String socialTypeName;

    @Schema(name = "socialId", description = "社交平台编号")
    private String socialId;

    @Schema(name = "socialNo", description = "社交号")
    private String socialNo;
}
