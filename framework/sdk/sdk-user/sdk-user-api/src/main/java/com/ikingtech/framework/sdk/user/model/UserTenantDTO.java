package com.ikingtech.framework.sdk.user.model;

import com.ikingtech.framework.sdk.enums.system.tenant.TenantStatusEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "UserTenantDTO", description = "用户租户")
public class UserTenantDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -7882962846164465315L;

    @Schema(name = "tenantId", description = "租户编号")
    private String tenantId;

    @Schema(name = "tenantCode", description = "租户标识")
    private String tenantCode;

    @Schema(name = "tenantName", description = "租户名称")
    private String tenantName;

    @Schema(name = "tenantLogo", description = "租户Logo")
    private String tenantLogo;

    @Schema(name = "tenantIcon", description = "租户图标")
    private String tenantIcon;

    @Schema(name = "tenantStatus", description = "租户状态")
    private TenantStatusEnum tenantStatus;

    @Schema(name = "tenantStatusName", description = "租户状态名称")
    private String tenantStatusName;
}
