package com.ikingtech.framework.sdk.user.extension.model;

import com.ikingtech.framework.sdk.enums.system.user.UserLockTypeEnum;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class UserInfoEvent implements Serializable {

    @Serial
    private static final long serialVersionUID = -4407697794454708401L;

    private UserInfoEventTypeEnum type;

    private String id;

    private String username;

    private String name;

    private String nickname;

    private String phone;

    private String email;

    private String avatar;

    private String identityNo;

    private Boolean adminUser;

    private String tenantCode;

    private String sex;

    private Boolean locked;

    private UserLockTypeEnum lockType;

    @RequiredArgsConstructor
    public enum UserInfoEventTypeEnum {

        ADD("新增"),

        DELETE("删除"),

        UPDATE("更新");

        public final String description;
    }
}
