package com.ikingtech.framework.sdk.variable.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 参数信息查询参数
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "VariableQueryParamDTO", description = "参数信息查询参数")
public class VariableQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 7827749630055561398L;

    @Schema(name = "name", description = "参数名称")
    private String name;

    @Schema(name = "variableKey", description = "参数键")
    private String variableKey;

    @Schema(name = "variableValue", description = "参数值")
    private String variableValue;

    @Schema(name = "type", description = "参数类型")
    private String type;
}
