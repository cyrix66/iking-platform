package com.ikingtech.framework.sdk.web.annotation;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.annotation.*;

/**
 * @author tie yan
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RequestMapping(method = RequestMethod.DELETE)
@Operation
public @interface DeleteRequest {

    /**
     * Alias for {@link RequestMapping#value}.
     */
    @AliasFor(annotation = RequestMapping.class)
    String[] value() default {};

    /**
     * Alias for {@link Operation#summary}.
     */
    @AliasFor(annotation = Operation.class)
    String summary() default "";

    /**
     * Alias for {@link Operation#description}.
     */
    @AliasFor(annotation = Operation.class)
    String description() default "";
}
