package com.ikingtech.framework.sdk.web.annotation;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.annotation.*;

/**
 * @author tie yan
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RequestMapping(method = RequestMethod.POST)
@Operation
public @interface PostRequest {

    /**
     * 请求映射的URL路径。
     * 该属性与@RequestMapping注解中的value属性相同，用于指定该方法处理的HTTP请求的URL路径。
     * 默认值为空数组，表示没有指定URL路径。
     */
    @AliasFor(annotation = RequestMapping.class)
    String[] value() default {};

    /**
     * 请求的简要说明。
     * 该属性与Operation注解中的summary属性相同，用于为处理方法提供一个简短的描述。
     * 默认值为空字符串，表示没有提供简要说明。
     */
    @AliasFor(annotation = Operation.class)
    String summary() default "";

    /**
     * 请求的详细描述。
     * 该属性与Operation注解中的description属性相同，用于为处理方法提供一个详细的说明。
     * 默认值为空字符串，表示没有提供详细描述。
     */
    @AliasFor(annotation = Operation.class)
    String description() default "";

    int order() default Integer.MAX_VALUE;
}

