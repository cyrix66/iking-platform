package com.ikingtech.framework.sdk.web.configuration;

import io.undertow.server.DefaultByteBufferPool;
import io.undertow.server.handlers.DisallowedMethodsHandler;
import io.undertow.util.HttpString;
import io.undertow.websockets.jsr.WebSocketDeploymentInfo;
import org.springframework.boot.web.embedded.undertow.UndertowServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;

/**
 * 解决undertow容器配置
 *
 * @author tie yan
 */
public class UndertowConfiguration implements WebServerFactoryCustomizer<UndertowServletWebServerFactory> {

    /**
     * 解决undertow容器启动时警告
     *
     * @param factory UndertowServletWebServerFactory
     */
    @Override
    public void customize(UndertowServletWebServerFactory factory) {
        factory.addDeploymentInfoCustomizers(deploymentInfo -> {
            WebSocketDeploymentInfo webSocketDeploymentInfo = new WebSocketDeploymentInfo();
            webSocketDeploymentInfo.setBuffers(new DefaultByteBufferPool(false, 1024));
            deploymentInfo.addServletContextAttribute("io.undertow.websockets.jsr.WebSocketDeploymentInfo", webSocketDeploymentInfo);
            deploymentInfo.addInitialHandlerChainWrapper(handler -> {
                HttpString[] disAllowHttpMethods = {HttpString.tryFromString("TRACE"), HttpString.tryFromString("TRACK")
                };
                return new DisallowedMethodsHandler(handler, disAllowHttpMethods);
            });
        });
    }
}
