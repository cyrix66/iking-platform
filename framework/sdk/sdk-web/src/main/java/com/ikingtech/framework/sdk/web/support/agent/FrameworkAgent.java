package com.ikingtech.framework.sdk.web.support.agent;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;

/**
 * @author tie yan
 */
public interface FrameworkAgent {

    /**
     * 客户端发起请求
     *
     * @param agentType 客户端类型
     * @param data 数据
     * @return 执行结果
     */
    R<Object> execute(FrameworkAgentTypeEnum agentType, Object data);
}
