package com.ikingtech.framework.sdk.web.support.agent;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;

/**
 * @author tie yan
 */
public interface FrameworkAgentCaller {

    R<Object> call(Object data);

    FrameworkAgentTypeEnum type();
}
