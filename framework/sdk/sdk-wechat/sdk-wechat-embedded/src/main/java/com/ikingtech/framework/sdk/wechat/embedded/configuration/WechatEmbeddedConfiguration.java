package com.ikingtech.framework.sdk.wechat.embedded.configuration;

import com.ikingtech.framework.sdk.wechat.embedded.mini.WechatMiniRequest;
import com.ikingtech.framework.sdk.wechat.embedded.mini.WechatMiniRequestBuilder;
import com.ikingtech.framework.sdk.wechat.embedded.properties.WechatProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * @author tie yan
 */
@EnableConfigurationProperties({WechatProperties.class})
public class WechatEmbeddedConfiguration {

    @Bean
    public WechatMiniRequest wechatMiniRequest() {
        return new WechatMiniRequest();
    }

    @Bean
    public WechatMiniRequestBuilder wechatMiniRequestBuilder(WechatProperties properties, WechatMiniRequest wechatMiniRequest) {
        return new WechatMiniRequestBuilder(properties, wechatMiniRequest);
    }
}
