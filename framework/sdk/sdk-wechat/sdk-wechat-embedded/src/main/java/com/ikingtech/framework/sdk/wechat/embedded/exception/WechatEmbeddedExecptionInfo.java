package com.ikingtech.framework.sdk.wechat.embedded.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;


/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum WechatEmbeddedExecptionInfo implements FrameworkExceptionInfo {

    WECHAT_MINI_NOT_FOUND("wechatMiniNotFound");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "iking-framework-sdk-wechat-embedded";
    }
}
