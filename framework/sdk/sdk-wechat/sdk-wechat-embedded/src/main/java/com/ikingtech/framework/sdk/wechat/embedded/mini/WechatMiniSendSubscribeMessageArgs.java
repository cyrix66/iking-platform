package com.ikingtech.framework.sdk.wechat.embedded.mini;

import lombok.Data;

import java.util.Map;

/**
 * @author tie yan
 */
@Data
public class WechatMiniSendSubscribeMessageArgs {

    public WechatMiniSendSubscribeMessageArgs(Builder builder) {
        this.templateId = builder.templateId;
        this.redirectTo = builder.redirectTo;
        this.openId = builder.openId;
        this.params = builder.params;
    }

    private String templateId;

    private String openId;

    private String redirectTo;

    private Map<String, Object> params;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String templateId;

        private String openId;

        private String redirectTo;

        private Map<String, Object> params;

        public Builder templateId(String templateId) {
            this.templateId = templateId;
            return this;
        }

        public Builder openId(String openId) {
            this.openId = openId;
            return this;
        }

        public Builder redirectTo(String redirectTo) {
            this.redirectTo = redirectTo;
            return this;
        }

        public Builder params(Map<String, Object> params) {
            this.params = params;
            return this;
        }

        public WechatMiniSendSubscribeMessageArgs build() {
            return new WechatMiniSendSubscribeMessageArgs(this);
        }
    }
}
