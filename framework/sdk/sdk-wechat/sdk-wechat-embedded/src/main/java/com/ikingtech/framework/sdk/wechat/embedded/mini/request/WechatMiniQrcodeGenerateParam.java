package com.ikingtech.framework.sdk.wechat.embedded.mini.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class WechatMiniQrcodeGenerateParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -6417821915044776412L;

    private String page;

    private String scene;

    @JsonProperty(value = "env_version")
    private String envVersion;

    private Integer width;

    @JsonProperty(value = "check_path")
    private Boolean checkPath;
}
