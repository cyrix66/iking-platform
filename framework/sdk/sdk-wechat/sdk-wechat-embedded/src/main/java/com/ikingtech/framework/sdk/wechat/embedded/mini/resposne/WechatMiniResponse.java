package com.ikingtech.framework.sdk.wechat.embedded.mini.resposne;

import lombok.Data;

/**
 * @author tie yan
 */
@Data
public class WechatMiniResponse<T> {

    /**
     * 错误码
     */
    private Integer errcode;

    /**
     * 错误信息
     */
    private String errmsg;

    private T data;

    public boolean fail() {
        return this.errcode != 0;
    }
}
