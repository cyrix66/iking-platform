package com.ikingtech.framework.sdk.wechat.embedded.mini.resposne;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WechatMiniResponseUrlLink extends WechatMiniResponse<Object> implements Serializable {

    private static final long serialVersionUID = 7195918589542398228L;

    @JsonProperty(value = "url_link")
    private String urlLink;
}
