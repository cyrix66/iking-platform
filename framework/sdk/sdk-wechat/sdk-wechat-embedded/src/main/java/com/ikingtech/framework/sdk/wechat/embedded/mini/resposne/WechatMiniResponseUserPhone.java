package com.ikingtech.framework.sdk.wechat.embedded.mini.resposne;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WechatMiniResponseUserPhone extends WechatMiniResponse<Object> implements Serializable {

    @Serial
    private static final long serialVersionUID = 396216450242850523L;

    @JsonProperty(value = "phone_info")
    private PhoneInfo phoneInfo;

    @Data
    public static class PhoneInfo implements Serializable {

        @Serial
    private static final long serialVersionUID = -4565306950070731267L;

        private String phoneNumber;

        private String purePhoneNumber;

        private String countryCode;
    }
}
