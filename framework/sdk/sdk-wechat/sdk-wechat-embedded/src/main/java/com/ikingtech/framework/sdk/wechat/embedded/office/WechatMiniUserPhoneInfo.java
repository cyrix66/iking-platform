package com.ikingtech.framework.sdk.wechat.embedded.office;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class WechatMiniUserPhoneInfo implements Serializable {

    @Serial
    private static final long serialVersionUID = 7753674793745473950L;

    private String phoneNumber;

    private String purePhoneNumber;

    private String countryCode;
}
