package com.ikingtech.framework.sdk.wechat.embedded.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author Administrator
 */
@Data
@ConfigurationProperties(GLOBAL_CONFIG_PREFIX + ".wechat")
public class WechatProperties {

    private List<WechatMini> mini;

    private List<WechatOffice> office;

    @Data
    public static class WechatMini {

        /**
         * 小程序标识，平台定义
         */
        private String id;

        /**
         * 小程序名称
         */
        private String name;

        /**
         * 小程序AppId
         */
        private String appId;

        /**
         * 小程序AppSecret
         */
        private String appSecret;
    }

    @Data
    public static class WechatOffice {

        /**
         * 公众号标识，平台定义
         */
        private String id;

        /**
         * 公众号名称
         */
        private String name;

        /**
         * 公众号AppId
         */
        private String appId;

        /**
         * 公众号AppSecret
         */
        private String appSecret;
    }
}
