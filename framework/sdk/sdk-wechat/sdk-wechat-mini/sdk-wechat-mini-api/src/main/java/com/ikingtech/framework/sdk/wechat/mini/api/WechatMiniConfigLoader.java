package com.ikingtech.framework.sdk.wechat.mini.api;

import com.ikingtech.framework.sdk.wechat.mini.model.WechatMiniDTO;

/**
 * @author tie yan
 */
public interface WechatMiniConfigLoader {

    WechatMiniDTO load(String wechatMiniId);
}
