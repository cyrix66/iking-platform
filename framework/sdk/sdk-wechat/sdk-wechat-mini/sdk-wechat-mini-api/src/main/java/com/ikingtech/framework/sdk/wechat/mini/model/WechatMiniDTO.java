package com.ikingtech.framework.sdk.wechat.mini.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "WechatMiniDTO", description = "微信小程序信息")
public class WechatMiniDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 2402389288901552429L;

    @Schema(name = "id", description = "小程序唯一标识")
    private String id;

    @Schema(name = "tenantCode", description = "租户标识")
    private String tenantCode;

    @Schema(name = "name", description = "名称")
    private String name;

    @Schema(name = "appId", description = "AppId")
    private String appId;

    @Schema(name = "appSecret", description = "AppSecret")
    private String appSecret;
}
