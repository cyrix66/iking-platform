package com.ikingtech.framework.sdk.wechat.mini.model;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class WechatMiniPhoneDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 8521224914229245348L;

    private String phoneNo;

    private String purePhoneNo;

    private String countryCode;
}
