package com.ikingtech.framework.sdk.wechat.mini.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.wechat.WechatMiniSubscribeMessageTemplateTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "WechatMiniMessageTemplateDTO", description = "微信小程序订阅消息模板信息")
public class WechatMiniSubscribeMessageTemplateDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -270255548079801980L;

    @Schema(name = "id", description = "模板编号")
    private String id;

    @Schema(name = "name", description = "消息模板名称")
    private String name;

    @Schema(name = "wechatMiniId", description = "消息模板所属小程序编号")
    private String wechatMiniId;

    @Schema(name = "title", description = "模版标题")
    private String title;

    @Schema(name = "content", description = "模版内容")
    private String content;

    @Schema(name = "type", description = "模版类型，2 为一次性订阅，3 为长期订阅")
    private WechatMiniSubscribeMessageTemplateTypeEnum type;

    @Schema(name = "typeName", description = "模版类型名称")
    private String typeName;

    @Schema(name = "params", description = "模版参数集合")
    private List<String> params;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
