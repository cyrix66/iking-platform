package com.ikingtech.framework.sdk.wechat.mini.rpc.api;

import com.ikingtech.framework.sdk.wechat.mini.api.WechatMiniApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "WechatMiniRpcApi", path = "/wechat/mini")
public interface WechatMiniRpcApi extends WechatMiniApi {
}
