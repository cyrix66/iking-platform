package com.ikingtech.framework.sdk.wechat.office.model;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class WechatOfficeExchangeCodeParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -7888555682416005364L;

    private String wechatOfficeId;

    private String code;
}
