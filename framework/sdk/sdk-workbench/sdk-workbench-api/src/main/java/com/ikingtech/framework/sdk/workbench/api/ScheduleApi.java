package com.ikingtech.framework.sdk.workbench.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.workbench.model.ScheduleDTO;
import com.ikingtech.framework.sdk.workbench.model.ScheduleQueryParamDTO;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public interface ScheduleApi {

    /**
     * 添加日程
     *
     * @param schedule 日程参数
     * @return 返回添加结果
     */
    @PostRequest(order = 1, value = "/add", summary = "添加日程", description = "添加日程")
    R<String> add(@Parameter(name = "schedule", description = "新增参数")
                  @RequestBody ScheduleDTO schedule);

    /**
     * 删除日程
     *
     * @param id 日程主键编号
     * @return 返回删除结果
     */
    @PostRequest(order = 2, value = "/delete", summary = "删除日程", description = "删除日程")
    R<Object> delete(@Parameter(name = "id", description = "删除参数")
                     @RequestBody String id);

    /**
     * 更新日程
     *
     * @param schedule 日程参数
     * @return 返回更新结果
     */
    @PostRequest(order = 3, value = "/update", summary = "更新日程", description = "更新日程")
    R<Boolean> update(@Parameter(name = "schedule", description = "更新参数")
                      @RequestBody ScheduleDTO schedule);

    /**
     * 获取日程详情
     *
     * @param id 日程主键编号
     * @return 返回日程详情
     */
    @PostRequest(order = 4, value = "/detail", summary = "获取日程详情", description = "获取日程详情")
    R<ScheduleDTO> detail(@Parameter(name = "id", description = "主键编号")
                          @RequestBody String id);

    /**
     * 获取分页日程列表
     *
     * @param queryParam 查询参数
     * @return 返回分页日程列表
     */
    @PostRequest(order = 5, value = "/list/page", summary = "获取分页日程列表", description = "获取分页日程列表")
    R<List<ScheduleDTO>> page(@Parameter(name = "queryParam", description = "查询参数")
                              @RequestBody ScheduleQueryParamDTO queryParam);

    /**
     * 获取所有日程列表
     *
     * @return 返回所有日程列表
     */
    @PostRequest(order = 6, value = "/list/all", summary = "获取所有日程列表", description = "获取所有日程列表")
    R<List<ScheduleDTO>> all();

    /**
     * 根据日期统计日程数量
     *
     * @param queryParam 查询参数
     * @return 返回日程数量统计结果
     */
    @PostRequest(order = 7, value = "/count/date", summary = "根据日期统计日程数量", description = "根据日期统计日程数量")
    R<Map<String, Integer>> countByDate(@Parameter(name = "queryParam", description = "查询参数")
                                        @RequestBody ScheduleQueryParamDTO queryParam);
}
