package com.ikingtech.framework.sdk.workbench.api;

import com.ikingtech.framework.sdk.workbench.model.ScheduleUser;

/**
 * @author tie yan
 */
public interface ScheduleUserApi {

    ScheduleUser loadById(String userId);
}
