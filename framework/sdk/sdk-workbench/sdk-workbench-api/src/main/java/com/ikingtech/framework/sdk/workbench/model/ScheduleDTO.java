package com.ikingtech.framework.sdk.workbench.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.workbench.ScheduleStatusEnum;
import com.ikingtech.framework.sdk.enums.workbench.ScheduleTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ScheduleDTO", description = "日程")
public class ScheduleDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -771358204796978667L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "businessId", description = "业务编号")
    private String businessId;

    @Schema(name = "title", description = "标题")
    private String title;

    @Schema(name = "type", description = "类型")
    private ScheduleTypeEnum type;

    @Schema(name = "typeName", description = "类型名称")
    private String typeName;

    @Schema(name = "status", description = "状态")
    private ScheduleStatusEnum status;

    @Schema(name = "statusName", description = "状态名称")
    private String statusName;

    @Schema(name = "estimateEndTime", description = "计划结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime estimateEndTime;

    @Schema(name = "remark", description = "说明")
    private String remark;

    @Schema(name = "estimateStartTime", description = "计划开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime estimateStartTime;

    @Schema(name = "redirectUrl", description = "跳转地址")
    private String redirectUrl;

    @Schema(name = "allDay", description = "是否为全天日程")
    private Boolean allDay;

    @Schema(name = "executors", description = "日程执行人")
    private List<ScheduleExecutorDTO> executors;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
