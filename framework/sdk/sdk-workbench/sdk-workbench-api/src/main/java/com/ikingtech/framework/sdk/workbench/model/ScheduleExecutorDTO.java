package com.ikingtech.framework.sdk.workbench.model;

import com.ikingtech.framework.sdk.enums.common.ElementTypeEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class ScheduleExecutorDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -5481975882155607685L;

    private String id;

    private String scheduleId;

    private String executorId;

    private ElementTypeEnum executorType;

    private String executorTypeName;

    private String executorName;

    private String executorAvatar;
}
