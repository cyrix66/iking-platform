package com.ikingtech.framework.sdk.workbench.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ScheduleQueryParamDTO", description = "日程查询参数")
public class ScheduleQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -1621749193883015806L;

    @Schema(name = "ids", description = "编号集合")
    private List<String> ids;

    @Schema(name = "status", description = "状态")
    private String status;

    @Schema(name = "title", description = "标题")
    private String title;

    @Schema(name = "type", description = "类型")
    private String type;

    @Schema(name = "startTime", description = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime startTime;

    @Schema(name = "endTime", description = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime endTime;

    @Schema(name = "remark", description = "说明")
    private String remark;

    @Schema(name = "loginUser", description = "是否仅查询当前登录用户")
    private Boolean loginUser;
}
