package com.ikingtech.framework.sdk.workbench.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ScheduleUser", description = "日程用户信息")
public class ScheduleUser implements Serializable {

    @Serial
    private static final long serialVersionUID = 1258136714614838309L;

    private String userId;

    private String userName;

    private String userAvatar;
    
    private List<String> userDeptIds;

    private List<String> userPostIds;

    private List<String> userRoleIds;
}
