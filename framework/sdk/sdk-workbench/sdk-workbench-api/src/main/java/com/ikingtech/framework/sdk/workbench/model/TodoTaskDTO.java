package com.ikingtech.framework.sdk.workbench.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@Schema(name = "TodoTaskDTO", description = "待办任务信息")
public class TodoTaskDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 7916878737133341023L;

    @Schema(name = "id", description = "编号")
    private String id;

    @Schema(name = "tenantCode", description = "租户编号")
    private String tenantCode;

    @Schema(name = "appCode", description = "应用标识")
    private String appCode;

    @Schema(name = "businessName", description = "业务名称")
    private String businessName;

    @Schema(name = "businessId", description = "业务数据编号")
    private String businessId;

    @Schema(name = "summary", description = "待办事项摘要")
    private String summary;

    @Schema(name = "userId", description = "用户编号")
    private String userId;

    @Schema(name = "status", description = "业务状态")
    private String status;

    @Schema(name = "redirect", description = "跳转链接")
    private String redirect;

    @Schema(name = "delFlag", description = "是否删除")
    private Boolean delFlag;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
