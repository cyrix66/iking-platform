package com.ikingtech.framework.sdk.workbench.embedded.annotation;

import java.lang.annotation.*;

/**
 * @author tie yan
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Todo {

    String businessName() default "";

    String businessId() default "";

    String appCode() default "";

    String summary() default "";

    String status();

    TaskUser appendUser();

    TaskUser completeUser() default @TaskUser(loginUser = true);

    Redirect redirect() default @Redirect;
}
