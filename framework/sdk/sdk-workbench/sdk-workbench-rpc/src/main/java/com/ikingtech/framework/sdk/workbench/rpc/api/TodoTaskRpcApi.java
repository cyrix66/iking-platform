package com.ikingtech.framework.sdk.workbench.rpc.api;

import com.ikingtech.framework.sdk.workbench.api.TodoTaskApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "PostApi", path = "/workbench/todo-task")
public interface TodoTaskRpcApi extends TodoTaskApi {
}
