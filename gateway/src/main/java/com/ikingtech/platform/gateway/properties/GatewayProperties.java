package com.ikingtech.platform.gateway.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@Data
@ConfigurationProperties(GLOBAL_CONFIG_PREFIX + ".gateway")
public class GatewayProperties implements Serializable {

    @Serial
    private static final long serialVersionUID = -8180688641692627163L;

    private List<String> ignore;
}
