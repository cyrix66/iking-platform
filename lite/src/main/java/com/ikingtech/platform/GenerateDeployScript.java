package com.ikingtech.platform;

import com.ikingtech.framework.sdk.utils.Tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * @author tie yan
 */
public class GenerateDeployScript {

    public static final String MAVEN_MODULE = """
            [INFO] parent ............................................. SUCCESS [  0.074 s]
            [INFO] sdk-utils .......................................... SUCCESS [  0.019 s]
            [INFO] sdk-enums .......................................... SUCCESS [  0.050 s]
            [INFO] sdk-context ........................................ SUCCESS [  0.020 s]
            [INFO] sdk-base ........................................... SUCCESS [  0.016 s]
            [INFO] sdk-core ........................................... SUCCESS [  0.019 s]
            [INFO] sdk-web ............................................ SUCCESS [  0.023 s]
            [INFO] sdk-oss-api ........................................ SUCCESS [  0.017 s]
            [INFO] sdk-oss-embedded ................................... SUCCESS [  0.019 s]
            [INFO] sdk-log-api ........................................ SUCCESS [  0.014 s]
            [INFO] sdk-log-rpc ........................................ SUCCESS [  0.013 s]
            [INFO] sdk-log-embedded ................................... SUCCESS [  0.020 s]
            [INFO] sdk-data ........................................... SUCCESS [  0.031 s]
            [INFO] rt-spring-boot-starter ............................. SUCCESS [  0.004 s]
            [INFO] service-oss ........................................ SUCCESS [  0.003 s]
            [INFO] sdk-approve-api .................................... SUCCESS [  0.025 s]
            [INFO] sdk-cache .......................................... SUCCESS [  0.014 s]
            [INFO] sdk-approve-callback ............................... SUCCESS [  0.016 s]
            [INFO] sdk-message-api .................................... SUCCESS [  0.018 s]
            [INFO] sdk-message-rpc .................................... SUCCESS [  0.013 s]
            [INFO] sdk-message-embedded ............................... SUCCESS [  0.018 s]
            [INFO] sdk-role-api ....................................... SUCCESS [  0.015 s]
            [INFO] sdk-menu-api ....................................... SUCCESS [  0.016 s]
            [INFO] sdk-post-api ....................................... SUCCESS [  0.015 s]
            [INFO] sdk-workbench-api .................................. SUCCESS [  0.012 s]
            [INFO] sdk-workbench-embedded ............................. SUCCESS [  0.014 s]
            [INFO] business-approve ................................... SUCCESS [  0.155 s]
            [INFO] sdk-user-api ....................................... SUCCESS [  0.031 s]
            [INFO] push-common ........................................ SUCCESS [  0.003 s]
            [INFO] business-message ................................... SUCCESS [  0.061 s]
            [INFO] sdk-component ...................................... SUCCESS [  0.013 s]
            [INFO] business-component ................................. SUCCESS [  0.016 s]
            [INFO] sdk-authenticate-operation ......................... SUCCESS [  0.014 s]
            [INFO] sdk-security ....................................... SUCCESS [  0.013 s]
            [INFO] business-workbench ................................. SUCCESS [  0.016 s]
            [INFO] business ........................................... SUCCESS [  0.002 s]
            [INFO] sdk-rpc ............................................ SUCCESS [  0.014 s]
            [INFO] sdk-log ............................................ SUCCESS [  0.004 s]
            [INFO] sdk-job-api ........................................ SUCCESS [  0.014 s]
            [INFO] sdk-job-rpc ........................................ SUCCESS [  0.012 s]
            [INFO] sdk-job-embedded ................................... SUCCESS [  0.019 s]
            [INFO] sdk-job-scheduler .................................. SUCCESS [  0.669 s]
            [INFO] sdk-job ............................................ SUCCESS [  0.003 s]
            [INFO] sdk-limit-api ...................................... SUCCESS [  0.014 s]
            [INFO] sdk-limit-embedded ................................. SUCCESS [  0.014 s]
            [INFO] sdk-limit-config ................................... SUCCESS [  0.014 s]
            [INFO] sdk-limit-mvc ...................................... SUCCESS [  0.013 s]
            [INFO] sdk-limit-gateway .................................. SUCCESS [  0.013 s]
            [INFO] sdk-limit .......................................... SUCCESS [  0.003 s]
            [INFO] sdk-lock ........................................... SUCCESS [  0.012 s]
            [INFO] sdk-wechat-mini-api ................................ SUCCESS [  0.014 s]
            [INFO] sdk-wechat-mini-rpc ................................ SUCCESS [  0.013 s]
            [INFO] sdk-wechat-mini .................................... SUCCESS [  0.002 s]
            [INFO] sdk-wechat-office-api .............................. SUCCESS [  0.014 s]
            [INFO] sdk-wechat-office-rpc .............................. SUCCESS [  0.013 s]
            [INFO] sdk-wechat-office .................................. SUCCESS [  0.003 s]
            [INFO] sdk-wechat-embedded ................................ SUCCESS [  0.023 s]
            [INFO] sdk-wechat ......................................... SUCCESS [  0.002 s]
            [INFO] sdk-gray ........................................... SUCCESS [  0.012 s]
            [INFO] sdk-message ........................................ SUCCESS [  0.003 s]
            [INFO] sdk-user-rpc ....................................... SUCCESS [  0.012 s]
            [INFO] sdk-user ........................................... SUCCESS [  0.003 s]
            [INFO] sdk-menu-rpc ....................................... SUCCESS [  0.011 s]
            [INFO] sdk-menu ........................................... SUCCESS [  0.003 s]
            [INFO] sdk-dict-api ....................................... SUCCESS [  0.014 s]
            [INFO] sdk-dict-rpc ....................................... SUCCESS [  0.012 s]
            [INFO] sdk-dict ........................................... SUCCESS [  0.003 s]
            [INFO] sdk-department-api ................................. SUCCESS [  0.015 s]
            [INFO] sdk-department-rpc ................................. SUCCESS [  0.011 s]
            [INFO] sdk-department ..................................... SUCCESS [  0.003 s]
            [INFO] sdk-post-rpc ....................................... SUCCESS [  0.011 s]
            [INFO] sdk-post ........................................... SUCCESS [  0.003 s]
            [INFO] sdk-role-rpc ....................................... SUCCESS [  0.011 s]
            [INFO] sdk-role ........................................... SUCCESS [  0.003 s]
            [INFO] sdk-tenant-api ..................................... SUCCESS [  0.012 s]
            [INFO] sdk-tenant-rpc ..................................... SUCCESS [  0.012 s]
            [INFO] sdk-tenant ......................................... SUCCESS [  0.002 s]
            [INFO] sdk-variable-api ................................... SUCCESS [  0.014 s]
            [INFO] sdk-variable-rpc ................................... SUCCESS [  0.013 s]
            [INFO] sdk-variable ....................................... SUCCESS [  0.003 s]
            [INFO] sdk-country-api .................................... SUCCESS [  0.015 s]
            [INFO] sdk-country-rpc .................................... SUCCESS [  0.012 s]
            [INFO] sdk-country ........................................ SUCCESS [  0.005 s]
            [INFO] sdk-division-api ................................... SUCCESS [  0.018 s]
            [INFO] sdk-division-rpc ................................... SUCCESS [  0.015 s]
            [INFO] sdk-division ....................................... SUCCESS [  0.003 s]
            [INFO] sdk-oss-rpc ........................................ SUCCESS [  0.015 s]
            [INFO] sdk-oss ............................................ SUCCESS [  0.003 s]
            [INFO] sdk-authenticate-extension ......................... SUCCESS [  0.015 s]
            [INFO] sdk-authenticate-embedded .......................... SUCCESS [  0.019 s]
            [INFO] sdk-authenticate ................................... SUCCESS [  0.002 s]
            [INFO] sdk-datasource-api ................................. SUCCESS [  0.014 s]
            [INFO] sdk-datasource-rpc ................................. SUCCESS [  0.013 s]
            [INFO] sdk-datasource ..................................... SUCCESS [  0.002 s]
            [INFO] sdk-pay-api ........................................ SUCCESS [  0.013 s]
            [INFO] sdk-pay-embedded ................................... SUCCESS [  0.022 s]
            [INFO] sdk-pay-rpc ........................................ SUCCESS [  0.012 s]
            [INFO] sdk-pay ............................................ SUCCESS [  0.002 s]
            [INFO] sdk-private-call ................................... SUCCESS [  0.015 s]
            [INFO] sdk-sms-api ........................................ SUCCESS [  0.012 s]
            [INFO] sdk-sms-embedded ................................... SUCCESS [  0.020 s]
            [INFO] sdk-sms-rpc ........................................ SUCCESS [  0.011 s]
            [INFO] sdk-sms ............................................ SUCCESS [  0.002 s]
            [INFO] sdk-excel .......................................... SUCCESS [  0.012 s]
            [INFO] sdk-approve-rpc .................................... SUCCESS [  0.013 s]
            [INFO] sdk-approve-embedded ............................... SUCCESS [  0.020 s]
            [INFO] sdk-approve ........................................ SUCCESS [  0.003 s]
            [INFO] sdk-plugin ......................................... SUCCESS [  0.015 s]
            [INFO] sdk-cluster ........................................ SUCCESS [  0.018 s]
            [INFO] sdk-workbench-rpc .................................. SUCCESS [  0.011 s]
            [INFO] sdk-workbench ...................................... SUCCESS [  0.003 s]
            [INFO] sdk-application-api ................................ SUCCESS [  0.016 s]
            [INFO] sdk-application-rpc ................................ SUCCESS [  0.012 s]
            [INFO] sdk-application .................................... SUCCESS [  0.003 s]
            [INFO] sdk-label-api ...................................... SUCCESS [  0.012 s]
            [INFO] sdk-label-rpc ...................................... SUCCESS [  0.011 s]
            [INFO] sdk-label-embedded ................................. SUCCESS [  0.011 s]
            [INFO] sdk-label .......................................... SUCCESS [  0.003 s]
            [INFO] sdk-cmd ............................................ SUCCESS [  0.014 s]
            [INFO] sdk-liteflow ....................................... SUCCESS [  0.011 s]
            [INFO] sdk-attachment-api ................................. SUCCESS [  0.012 s]
            [INFO] sdk-attachment-rpc ................................. SUCCESS [  0.011 s]
            [INFO] sdk-attachment-embedded ............................ SUCCESS [  0.013 s]
            [INFO] sdk-attachment ..................................... SUCCESS [  0.002 s]
            [INFO] sdk-office-api ..................................... SUCCESS [  0.027 s]
            [INFO] sdk-office-rpc ..................................... SUCCESS [  0.014 s]
            [INFO] sdk-office ......................................... SUCCESS [  0.002 s]
            [INFO] sdk-authorization-api .............................. SUCCESS [  0.013 s]
            [INFO] sdk-authorization .................................. SUCCESS [  0.003 s]
            [INFO] sdk-charts-common .................................. SUCCESS [  0.013 s]
            [INFO] sdk-charts-api ..................................... SUCCESS [  0.016 s]
            [INFO] sdk-charts ......................................... SUCCESS [  0.003 s]
            [INFO] sdk-extension ...................................... SUCCESS [  0.018 s]
            [INFO] sdk ................................................ SUCCESS [  0.002 s]
            [INFO] framework .......................................... SUCCESS [  0.002 s]
            [INFO] service-job ........................................ SUCCESS [  0.003 s]
            [INFO] service-log ........................................ SUCCESS [  0.003 s]
            [INFO] service-magic-api .................................. SUCCESS [  0.002 s]
            [INFO] push-websocket ..................................... SUCCESS [  0.003 s]
            [INFO] push-wechat ........................................ SUCCESS [  0.002 s]
            [INFO] push-sms ........................................... SUCCESS [  0.003 s]
            [INFO] push-dingTalk ...................................... SUCCESS [  0.002 s]
            [INFO] service-push ....................................... SUCCESS [  0.003 s]
            [INFO] system-user ........................................ SUCCESS [  0.002 s]
            [INFO] system-department .................................. SUCCESS [  0.003 s]
            [INFO] system-menu ........................................ SUCCESS [  0.003 s]
            [INFO] system-role ........................................ SUCCESS [  0.002 s]
            [INFO] system-post ........................................ SUCCESS [  0.002 s]
            [INFO] system-variable .................................... SUCCESS [  0.003 s]
            [INFO] system-dict ........................................ SUCCESS [  0.002 s]
            [INFO] system-division .................................... SUCCESS [  0.003 s]
            [INFO] system-country ..................................... SUCCESS [  0.002 s]
            [INFO] system-config ...................................... SUCCESS [  0.002 s]
            [INFO] service-system ..................................... SUCCESS [  0.002 s]
            [INFO] service-authorization .............................. SUCCESS [  0.002 s]
            [INFO] service-sms ........................................ SUCCESS [  0.003 s]
            [INFO] service-wechat-mini ................................ SUCCESS [  0.003 s]
            [INFO] service-wechat-office .............................. SUCCESS [  0.002 s]
            [INFO] service-wechat ..................................... SUCCESS [  0.002 s]
            [INFO] service-datasource ................................. SUCCESS [  0.003 s]
            [INFO] service-authentication ............................. SUCCESS [  0.002 s]
            [INFO] service-pay ........................................ SUCCESS [  0.002 s]
            [INFO] service-cluster .................................... SUCCESS [  0.002 s]
            [INFO] service-application ................................ SUCCESS [  0.002 s]
            [INFO] service-tenant ..................................... SUCCESS [  0.002 s]
            [INFO] service-domain ..................................... SUCCESS [  0.002 s]
            [INFO] service-initializer ................................ SUCCESS [  0.002 s]
            [INFO] service-plugin ..................................... SUCCESS [  0.002 s]
            [INFO] service-attachment ................................. SUCCESS [  0.002 s]
            [INFO] service-label ...................................... SUCCESS [  0.003 s]
            [INFO] service-office ..................................... SUCCESS [  0.002 s]
            [INFO] service-oidc ....................................... SUCCESS [  0.002 s]
            [INFO] service-license .................................... SUCCESS [  0.002 s]
            [INFO] service ............................................ SUCCESS [  0.002 s]
            [INFO] admin .............................................. SUCCESS [  0.257 s]
            [INFO] server ............................................. SUCCESS [  0.002 s]
            [INFO] gateway ............................................ SUCCESS [  0.041 s]
            [INFO] auth ............................................... SUCCESS [  0.013 s]
            [INFO] lite ............................................... SUCCESS [  0.011 s]
            [INFO] iking-platform ................. SUCCESS [  0.056 s]
            """;

    public static void main(String[] args) {
        File rootDir = new File("../iking-platform");
        Map<String, String> targetDirectories = findSubDir(rootDir);

        List<String> modules = Tools.Coll.filter(Tools.Str.split(MAVEN_MODULE, "\n"), Tools.Str::isNotBlank);

        List<String> result = new ArrayList<>();
        modules.forEach(module -> {
            String moduleName = module.substring(module.indexOf(" ") + 1);
            moduleName = moduleName.substring(0, moduleName.indexOf(" "));
            if (targetDirectories.containsKey(moduleName)) {
                String modulePath = targetDirectories.get(moduleName);
                result.add("cd $pwd/$work_dir" + modulePath.replace("..", "").replace("\\", "/") + "\n" + "mvn -Dmaven.repo.local=$pwd/maven_repo clean deploy central-publishing:publish -f pom.xml\n\n");
            }
        });
        result.forEach(System.out::printf);
    }

    private static Map<String, String> findSubDir(File rootDir) {
        Map<String, String> result = new HashMap<>();
        for (File file : Objects.requireNonNull(rootDir.listFiles())) {
            if (!file.isDirectory() ||
                    file.getName().startsWith(".") ||
                    "gateway".equals(file.getName()) ||
                    "auth".equals(file.getName()) ||
                    "lite".equals(file.getName()) ||
                    "server".equals(file.getName())) {
                continue;
            }
            File[] subFiles = file.listFiles();
            if (null == subFiles || subFiles.length == 0) {
                continue;
            }
            boolean isTargetDir = false;
            for (File subFile : subFiles) {
                if (!"pom.xml".equals(subFile.getName())) {
                    continue;
                }
                try (BufferedReader reader = new BufferedReader(new FileReader(subFile))) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        if (line.contains("<packaging>jar</packaging>")) {
                            isTargetDir = true;
                        }
                    }
                } catch (IOException e) {
                    System.err.println("An error occurred while reading the file:");
                }
            }
            if (isTargetDir) {
                result.put(file.getName(), file.getPath());
            } else {
                result.putAll(findSubDir(file));
            }
        }
        return result;
    }
}
