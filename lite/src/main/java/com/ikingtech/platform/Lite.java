package com.ikingtech.platform;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author tie yan
 */
@EnableScheduling
@EnableAsync
@MapperScan(basePackages = "**.**.mapper")
@SpringBootApplication
public class Lite {

    public static void main(String[] args) {
        SpringApplication.run(Lite.class, args);
    }
}