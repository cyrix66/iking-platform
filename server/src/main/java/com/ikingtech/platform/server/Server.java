package com.ikingtech.platform.server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author tie yan
 */
@EnableScheduling
@EnableAsync
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan(basePackages = "**.**.mapper")
public class Server {
    public static void main(String[] args) {
        SpringApplication.run(Server.class, args);
    }
}
