package com.ikingtech.platform.service.attachment.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.attachment.entity.AttachmentDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface AttachmentMapper extends BaseMapper<AttachmentDO> {
}