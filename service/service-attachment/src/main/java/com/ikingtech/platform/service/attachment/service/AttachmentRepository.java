package com.ikingtech.platform.service.attachment.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.attachment.entity.AttachmentDO;
import com.ikingtech.platform.service.attachment.mapper.AttachmentMapper;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class AttachmentRepository extends ServiceImpl<AttachmentMapper, AttachmentDO> {
}
