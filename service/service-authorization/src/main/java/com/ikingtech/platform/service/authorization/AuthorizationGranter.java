package com.ikingtech.platform.service.authorization;

/**
 * @author tie yan
 */
public interface AuthorizationGranter {

    void grant(String userId);
}
