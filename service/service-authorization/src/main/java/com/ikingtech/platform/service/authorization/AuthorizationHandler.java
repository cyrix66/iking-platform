package com.ikingtech.platform.service.authorization;

import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
@ApiController(value = "/authorization", name = "授权服务", description = "授权服务")
public class AuthorizationHandler {

    private final AuthorizationService service;

    /**
     * 授权指定用户
     *
     * @param id 用户编号
     * @return 授权结果
     */
    @PostRequest(order = 1, value = "/grant/id", summary = "授权指定用户", description = "授权指定用户")
    R<Object> grant(@Parameter(name = "id", description = "用户编号")
                    @RequestBody String id) {
        return R.ok(service.grant(id));
    }

    /**
     * 授权登录用户
     *
     * @return 授权结果
     */
    @PostRequest(order = 2, value = "/grant/login-user", summary = "授权登录用户", description = "授权登录用户")
    R<String> grantLoginUser() {
        return R.ok(service.grant(Me.id()));
    }
}
