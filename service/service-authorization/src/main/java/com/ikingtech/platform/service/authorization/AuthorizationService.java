package com.ikingtech.platform.service.authorization;

import com.iking.framework.sdk.authorization.api.AuthorizationDepartmentApi;
import com.iking.framework.sdk.authorization.api.AuthorizationRoleApi;
import com.iking.framework.sdk.authorization.api.AuthorizationUserApi;
import com.iking.framework.sdk.authorization.model.AuthorizationRoleMenu;
import com.iking.framework.sdk.authorization.model.AuthorizationUser;
import com.iking.framework.sdk.authorization.model.AuthorizationUserDepartment;
import com.iking.framework.sdk.authorization.model.AuthorizationUserRole;
import com.ikingtech.framework.sdk.cache.constants.CacheConstants;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.enums.domain.DomainEnum;
import com.ikingtech.framework.sdk.enums.system.role.DataScopeTypeEnum;
import com.ikingtech.framework.sdk.enums.system.user.UserCategoryEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.ikingtech.framework.sdk.context.constant.SecurityConstants.GLOBAL_MENU_ID;
import static com.ikingtech.framework.sdk.enums.system.role.DataScopeTypeEnum.*;

/**
 * @author tie yan
 */
@Service
@RequiredArgsConstructor
public class AuthorizationService {

    private final StringRedisTemplate redisTemplate;

    private final AuthorizationUserApi userApi;

    private final AuthorizationRoleApi roleApi;

    private final AuthorizationDepartmentApi deptApi;

    /**
     * 用户授权
     *
     * @param userId 用户ID
     * @return 用户ID
     */
    public String grant(String userId) {
        // 加载用户信息
        AuthorizationUser user = this.userApi.loadUser(userId);
        // 如果用户不存在，则抛出异常
        if (null == user) {
            throw new FrameworkException("userNotFound");
        }
        // 授权用户
        this.grant(user.getUsername(), user.getRoles(), user.getDepartments());
        // 返回用户ID
        return userId;
    }


    /**
     * 授权
     *
     * @param username    用户名
     * @param roles       角色列表
     * @param departments 部门列表
     */
    private void grant(String username,
                       List<AuthorizationUserRole> roles,
                       List<AuthorizationUserDepartment> departments) {
        // key是menuId,value是部门全路径集合的map
        Map<String, List<String>> dataScopeMap = new HashMap<>(roles.size());
        if (DomainEnum.PLATFORM.name().equals(Me.domainCode())) {
            return;
        } else if (DomainEnum.TENANT.name().equals(Me.domainCode()) &&
                   Me.categoryCodes().contains(UserCategoryEnum.TENANT_ADMINISTRATOR.name())) {
            List<String> deptFullPaths = Tools.Coll.distinct(this.deptApi.loadFullPathAll());
            dataScopeMap.put(GLOBAL_MENU_ID, deptFullPaths);
        } else {
            // 根据角色id查询每个角色关联的菜单id
            List<AuthorizationRoleMenu> roleMenus = this.roleApi.loadMenu(
                    Tools.Coll.convertList(roles, AuthorizationUserRole::getRoleId));
            if (Tools.Coll.isBlank(roleMenus)) {
                return;
            }
            Map<String, List<String>> menuIdMap = Tools.Coll.convertGroup(roleMenus,
                    AuthorizationRoleMenu::getRoleId,
                    AuthorizationRoleMenu::getMenuId);
            //如果角色列表中有一个角色的数据范围是“全部数据”,则给每个菜单都赋予所有单位权限
            if (roles.stream().anyMatch(role -> role.getDataScopeType().equals(DataScopeTypeEnum.ALL))) {
                List<String> deptFullPaths = Tools.Coll.distinct(this.deptApi.loadFullPathAll());
                Tools.Coll.distinct(Tools.Coll.flatMap(menuIdMap.values(), Collection::stream)).forEach(menuId -> dataScopeMap.put(menuId, deptFullPaths));
            } else if (Tools.Coll.isNotBlank(departments)) {
                // 遍历所有的角色
                roles.forEach(role -> {
                    // 根据角色信息和用户所属单位获取该角色的单位权限
                    List<String> currentRoleDataScopeCodes = this.parseDataScopeCode(role.getDataScopeType(), role.getDataScopeCodes(), departments);
                    if (Tools.Coll.isNotBlank(currentRoleDataScopeCodes) && menuIdMap.containsKey(role.getRoleId())) {
                        // 轮询该角色对应的菜单
                        menuIdMap.get(role.getRoleId()).forEach(menuId -> dataScopeMap.computeIfAbsent(menuId, k -> new ArrayList<>()).addAll(currentRoleDataScopeCodes));
                    }
                });
            } else {
                return;
            }
        }
        if (Tools.Coll.isNotBlankMap(dataScopeMap)) {
            //缓存菜单信息到redis
            Map<String, String> cacheDataScopeMap = new HashMap<>(dataScopeMap.size());
            dataScopeMap.forEach((menuId, dataScopeCodes) -> cacheDataScopeMap.put(menuId, Tools.Coll.join(dataScopeCodes)));
            this.redisTemplate.opsForHash().putAll(CacheConstants.userAuthDetailsFormat(username, Me.tenantCode()), cacheDataScopeMap);
        }
    }

    private List<String> parseDataScopeCode(DataScopeTypeEnum dataScopeType,
                                            List<String> roleDataScopeCodes,
                                            List<AuthorizationUserDepartment> departments) {
        List<String> dataScopeCodes = new ArrayList<>();
        if (DEFINE.equals(dataScopeType) && Tools.Coll.isNotBlank(roleDataScopeCodes)) {
            List<String> fullPaths = this.deptApi.loadFullPath(roleDataScopeCodes);
            if (Tools.Coll.isNotBlank(fullPaths)) {
                dataScopeCodes.addAll(fullPaths);
            }
        }
        if (UNITY_WITH_CHILD.equals(dataScopeType) && Tools.Coll.isNotBlank(departments) ||
            SECTION_WITH_CHILD.equals(dataScopeType) && Tools.Coll.isNotBlank(departments)) {
            List<String> fullPaths = this.deptApi.loadSubFullPath(departments, dataScopeType);
            if (Tools.Coll.isNotBlank(fullPaths)) {
                dataScopeCodes.addAll(fullPaths);
            }
        }
        if (UNITY.equals(dataScopeType) && Tools.Coll.isNotBlank(departments) ||
            SECTION.equals(dataScopeType) && Tools.Coll.isNotBlank(departments)) {
            List<String> fullPaths = this.deptApi.loadFullPath(departments, dataScopeType);
            if (Tools.Coll.isNotBlank(fullPaths)) {
                dataScopeCodes.addAll(fullPaths);
            }
        }
        return dataScopeCodes;
    }

}
