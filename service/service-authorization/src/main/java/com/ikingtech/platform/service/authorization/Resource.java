package com.ikingtech.platform.service.authorization;

import com.ikingtech.framework.sdk.enums.authorization.ResourceTypeEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class Resource implements Serializable {

    @Serial
    private static final long serialVersionUID = -1797850771742164209L;

    /**
     * 名称
     */
    private String name;

    /**
     * 标识
     */
    private String code;

    /**
     * 资源类型
     */
    private ResourceTypeEnum type;

    /**
     * 资源类型名称
     */
    private String typeName;

    /**
     * 权限列表
     */
    private List<ResourcePermission> permissions;

}
