package com.ikingtech.platform.service.authorization;

import com.ikingtech.framework.sdk.enums.authorization.ResourcePermissionEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class ResourcePermission implements Serializable {

    @Serial
    private static final long serialVersionUID = -8218951372083364710L;

    /**
     * 权限
     */
    private ResourcePermissionEnum permission;

    /**
     * 权限名称
     */
    private String permissionName;

    /**
     * 权限查询码
     */
    private List<String> permissionCodes;

}
