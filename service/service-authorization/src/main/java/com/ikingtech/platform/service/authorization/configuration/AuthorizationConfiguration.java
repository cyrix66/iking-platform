package com.ikingtech.platform.service.authorization.configuration;

import com.iking.framework.sdk.authorization.api.AuthorizationDepartmentApi;
import com.iking.framework.sdk.authorization.api.AuthorizationRoleApi;
import com.iking.framework.sdk.authorization.api.AuthorizationUserApi;
import com.ikingtech.platform.service.authorization.service.DefaultAuthorizationDepartmentService;
import com.ikingtech.platform.service.authorization.service.DefaultAuthorizationRoleService;
import com.ikingtech.platform.service.authorization.service.DefaultAuthorizationUserService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.context.annotation.Bean;

/**
 * @author tie yan
 */
public class AuthorizationConfiguration {

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.user.service.AuthorizationUserService"})
    @ConditionalOnMissingBean(AuthorizationUserApi.class)
    public AuthorizationUserApi defaultAuthorizationUserService() {
        return new DefaultAuthorizationUserService();
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.dept.service.AuthorizationDepartmentService"})
    @ConditionalOnMissingBean(AuthorizationDepartmentApi.class)
    public AuthorizationDepartmentApi defaultAuthorizationDepartmentService() {
        return new DefaultAuthorizationDepartmentService();
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.role.service.AuthorizationRoleService"})
    @ConditionalOnMissingBean(AuthorizationRoleApi.class)
    public AuthorizationRoleApi defaultAuthorizationRoleService() {
        return new DefaultAuthorizationRoleService();
    }
}
