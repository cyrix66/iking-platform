package com.ikingtech.platform.service.authorization.service;

import com.iking.framework.sdk.authorization.api.AuthorizationDepartmentApi;
import com.iking.framework.sdk.authorization.model.AuthorizationUserDepartment;
import com.ikingtech.framework.sdk.enums.system.role.DataScopeTypeEnum;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class DefaultAuthorizationDepartmentService implements AuthorizationDepartmentApi {

    @Override
    public List<String> loadFullPathAll() {
        return Collections.emptyList();
    }

    @Override
    public List<String> loadFullPath(List<String> ids) {
        return Collections.emptyList();
    }

    @Override
    public List<String> loadFullPath(List<AuthorizationUserDepartment> departments,
                                     DataScopeTypeEnum dataScopeType) {
        return Collections.emptyList();
    }

    @Override
    public List<String> loadSubFullPath(List<AuthorizationUserDepartment> departments,
                                             DataScopeTypeEnum dataScopeType) {
        return Collections.emptyList();
    }
}
