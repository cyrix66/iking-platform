package com.ikingtech.platform.service.authorization.service;

import com.iking.framework.sdk.authorization.api.AuthorizationRoleApi;
import com.iking.framework.sdk.authorization.model.AuthorizationRoleMenu;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class DefaultAuthorizationRoleService implements AuthorizationRoleApi {

    @Override
    public List<AuthorizationRoleMenu> loadMenu(List<String> roleIds) {
        return Collections.emptyList();
    }
}
