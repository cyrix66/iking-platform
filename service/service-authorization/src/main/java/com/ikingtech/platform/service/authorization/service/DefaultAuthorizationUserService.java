package com.ikingtech.platform.service.authorization.service;

import com.iking.framework.sdk.authorization.api.AuthorizationUserApi;
import com.iking.framework.sdk.authorization.model.AuthorizationUser;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class DefaultAuthorizationUserService implements AuthorizationUserApi {

    @Override
    public AuthorizationUser loadUser(String userId) {
        return null;
    }
}
