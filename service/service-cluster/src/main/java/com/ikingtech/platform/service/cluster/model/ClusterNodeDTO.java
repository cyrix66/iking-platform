package com.ikingtech.platform.service.cluster.model;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class ClusterNodeDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -6569840375268972595L;

    private String id;

    private String address;

    private Integer port;

    private Boolean alive;
}
