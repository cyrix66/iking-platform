package com.ikingtech.platform.service.datasource.configuration;

import com.ikingtech.platform.service.datasource.service.DatasourceExecutor;
import com.ikingtech.platform.service.datasource.service.DatasourceRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class DatasourceConfiguration {

    @Bean
    public DatasourceRepository datasourceRepository() {
        return new DatasourceRepository();
    }

    @Bean
    public DatasourceExecutor datasourceExecutor() {
        return new DatasourceExecutor();
    }
}
