package com.ikingtech.platform.service.datasource.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p>
 * 数据源
 * </p>
 *
 * @author mlk
 * @since 2023-02-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("datasource")
public class DatasourceDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @TableField("tenant_code")
    private String tenantCode;

    @TableField("name")
    private String name;

    @TableField("type")
    private String type;

    @TableField("ip")
    private String ip;

    @TableField("port")
    private String port;

    @TableField("jdbc_url")
    private String jdbcUrl;

    @TableField("service_name")
    private String serviceName;

    @TableField("database_name")
    private String databaseName;

    @TableField("schema_name")
    private String schemaName;

    @TableField("username")
    private String username;

    @TableField("password")
    private String password;

    @TableField("remark")
    private String remark;

    @TableField("default_datasource")
    private Boolean defaultDatasource;
}
