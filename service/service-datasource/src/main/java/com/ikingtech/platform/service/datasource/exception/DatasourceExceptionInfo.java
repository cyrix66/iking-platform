package com.ikingtech.platform.service.datasource.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DatasourceExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 指定数据源不存在
     */
    DATASOURCE_NOT_FOUND("datasourceNotFound"),

    /**
     * 库名不能为空
     */
    SCHEMA_NAME_NOT_NULL("schemaNameNotNull"),
    /**
     * 表名不能为空
     */
    TABLE_NAME_NOT_NULL("tableNameNotNull");


    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "iking-framework-datasource";
    }
}
