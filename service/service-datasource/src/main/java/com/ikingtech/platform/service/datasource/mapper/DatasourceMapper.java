package com.ikingtech.platform.service.datasource.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.datasource.entity.DatasourceDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface DatasourceMapper extends BaseMapper<DatasourceDO> {
}
