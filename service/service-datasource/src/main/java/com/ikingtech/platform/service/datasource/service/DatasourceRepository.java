package com.ikingtech.platform.service.datasource.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.datasource.model.DatasourceQueryParamDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.datasource.entity.DatasourceDO;
import com.ikingtech.platform.service.datasource.mapper.DatasourceMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class DatasourceRepository extends ServiceImpl<DatasourceMapper, DatasourceDO> {

    public static LambdaQueryWrapper<DatasourceDO> createWrapper(DatasourceQueryParamDTO queryParam, String tenantCode) {
        return Wrappers.<DatasourceDO>lambdaQuery()
                .like(Tools.Str.isNotBlank(queryParam.getName()), DatasourceDO::getName, queryParam.getName())
                .eq(Tools.Str.isNotBlank(queryParam.getType()), DatasourceDO::getType, queryParam.getType())
                .like(Tools.Str.isNotBlank(queryParam.getIp()), DatasourceDO::getIp, queryParam.getIp())
                .like(Tools.Str.isNotBlank(queryParam.getPort()), DatasourceDO::getPort, queryParam.getPort())
                .like(Tools.Str.isNotBlank(queryParam.getSchemaName()), DatasourceDO::getSchemaName, queryParam.getSchemaName())
                .like(Tools.Str.isNotBlank(queryParam.getRemark()), DatasourceDO::getRemark, queryParam.getRemark())
                .eq(null != queryParam.getCreateTime(), DatasourceDO::getCreateTime, queryParam.getCreateTime())
                .eq(null != queryParam.getUpdateTime(), DatasourceDO::getUpdateTime, queryParam.getUpdateTime())
                .eq(Tools.Str.isNotBlank(tenantCode), DatasourceDO::getTenantCode, tenantCode);
    }
}
