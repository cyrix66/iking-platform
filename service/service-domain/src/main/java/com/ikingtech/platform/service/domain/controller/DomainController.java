package com.ikingtech.platform.service.domain.controller;

import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.system.user.UserCategoryEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import com.ikingtech.platform.service.domain.model.DomainDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
@ApiController(value = "/domain", name = "域管理", description = "域管理")
public class DomainController {

    /**
     * 根据登录用户获取域列表
     *
     * @return 权限列表
     */
    @PostRequest(order = 1, value = "/list/login-user", summary = "根据登录用户获取域列表", description = "根据登录用户获取域列表")
    public R<List<DomainDTO>> listByLoginUser() {
        return R.ok(Tools.Coll.convertList(Me.categoryCodes(),
                categoryCode -> !UserCategoryEnum.NORMAL_USER.name().equals(categoryCode),
                categoryCode -> {
                    DomainDTO domain = new DomainDTO();
                    domain.setType(UserCategoryEnum.valueOf(categoryCode).domain);
                    domain.setName(domain.getType().description);
                    return domain;
                }));
    }
}
