package com.ikingtech.platform.service.domain.model;

import com.ikingtech.framework.sdk.enums.domain.DomainEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "DomainDTO", description = "域")
public class DomainDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -6137950301511519061L;

    @Schema(name = "name", description = "域名称")
    private String name;

    @Schema(name = "type", description = "域类型")
    private DomainEnum type;
}
