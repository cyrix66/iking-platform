package com.ikingtech.platform.service;

import com.ikingtech.framework.sdk.context.event.SystemInitEvent;
import com.ikingtech.framework.sdk.tenant.api.InitializerTenantApi;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;

/**
 * @author zhangqiang
 */
@Slf4j
@Order(1)
@RequiredArgsConstructor
public class Initializer implements ApplicationRunner {

    private final ApplicationContext applicationContext;

    private final InitializerTenantApi initializerTenantApi;

    @Override
    public void run(ApplicationArguments args) {
        SystemInitEvent systemInitEvent = new SystemInitEvent(this.initializerTenantApi.loadAllCodes(), this);
        this.applicationContext.publishEvent(systemInitEvent);
    }
}
