CREATE TABLE public.application
(
    id              VARCHAR(32) NOT NULL,
    tenant_code     VARCHAR(64),
    name            VARCHAR(64),
    code            VARCHAR(64),
    icon            VARCHAR(256),
    icon_background VARCHAR(256),
    link            VARCHAR(256),
    remark          VARCHAR(500),
    index_path      VARCHAR(512),
    type            VARCHAR(64),
    status          VARCHAR(64),
    sort_order      INT4,
    create_time     DATE,
    create_by       VARCHAR(32),
    create_name     VARCHAR(64),
    update_time     DATE,
    update_by       VARCHAR(32),
    update_name     VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.application IS '应用';
COMMENT ON COLUMN public.application.id IS '主键';
COMMENT ON COLUMN public.application.tenant_code IS '租户标识';
COMMENT ON COLUMN public.application.name IS '应用名称';
COMMENT ON COLUMN public.application.code IS '应用标识';
COMMENT ON COLUMN public.application.icon IS '应用图标';
COMMENT ON COLUMN public.application.icon_background IS '图标背景色';
COMMENT ON COLUMN public.application.link IS '菜单链接';
COMMENT ON COLUMN public.application.remark IS '备注';
COMMENT ON COLUMN public.application.index_path IS '应用首页地址';
COMMENT ON COLUMN public.application.type IS '应用类型';
COMMENT ON COLUMN public.application.status IS '应用状态';
COMMENT ON COLUMN public.application.sort_order IS '排序值';
COMMENT ON COLUMN public.application.create_time IS '创建时间';
COMMENT ON COLUMN public.application.create_by IS '创建人编号';
COMMENT ON COLUMN public.application.create_name IS '创建人姓名';
COMMENT ON COLUMN public.application.update_time IS '更新时间';
COMMENT ON COLUMN public.application.update_by IS '更新人编号';
COMMENT ON COLUMN public.application.update_name IS '更新人姓名';

CREATE TABLE public.application_assign
(
    id          VARCHAR(32) NOT NULL,
    app_code    VARCHAR(64),
    tenant_code VARCHAR(64),
    enabled     TINYINT,
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.application_assign IS '应用分配';
COMMENT ON COLUMN public.application_assign.id IS '主键';
COMMENT ON COLUMN public.application_assign.app_code IS '应用标识';
COMMENT ON COLUMN public.application_assign.tenant_code IS '租户标识';
COMMENT ON COLUMN public.application_assign.enabled IS '是否启用';
COMMENT ON COLUMN public.application_assign.create_time IS '创建时间';
COMMENT ON COLUMN public.application_assign.create_by IS '创建人编号';
COMMENT ON COLUMN public.application_assign.create_name IS '创建人姓名';
COMMENT ON COLUMN public.application_assign.update_time IS '更新时间';
COMMENT ON COLUMN public.application_assign.update_by IS '更新人编号';
COMMENT ON COLUMN public.application_assign.update_name IS '更新人姓名';

CREATE TABLE public.application_general_data
(
    id            VARCHAR(32) NOT NULL,
    tenant_code   VARCHAR(64),
    page_id       VARCHAR(32),
    business_code VARCHAR(64),
    data          JSON,
    create_time   DATE,
    create_by     VARCHAR(32),
    create_name   VARCHAR(64),
    update_time   DATE,
    update_by     VARCHAR(32),
    update_name   VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.application_general_data IS '页面模型';
COMMENT ON COLUMN public.application_general_data.id IS '主键';
COMMENT ON COLUMN public.application_general_data.tenant_code IS '租户标识';
COMMENT ON COLUMN public.application_general_data.page_id IS '页面编号';
COMMENT ON COLUMN public.application_general_data.business_code IS '业务标识';
COMMENT ON COLUMN public.application_general_data.data IS '业务数据';
COMMENT ON COLUMN public.application_general_data.create_time IS '创建时间';
COMMENT ON COLUMN public.application_general_data.create_by IS '创建人编号';
COMMENT ON COLUMN public.application_general_data.create_name IS '创建人姓名';
COMMENT ON COLUMN public.application_general_data.update_time IS '更新时间';
COMMENT ON COLUMN public.application_general_data.update_by IS '更新人编号';
COMMENT ON COLUMN public.application_general_data.update_name IS '更新人姓名';

CREATE TABLE public.application_model
(
    id                VARCHAR(32) NOT NULL,
    app_code          VARCHAR(64),
    name              VARCHAR(64),
    code              VARCHAR(64),
    type              VARCHAR(64),
    status            VARCHAR(64),
    creation_type     VARCHAR(64),
    datasource_id     VARCHAR(32),
    sync_table_name   VARCHAR(64),
    base_package_name VARCHAR(256),
    remark            VARCHAR(500),
    create_time       DATE,
    create_by         VARCHAR(32),
    create_name       VARCHAR(64),
    update_time       DATE,
    update_by         VARCHAR(32),
    update_name       VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.application_model IS '业务模型';
COMMENT ON COLUMN public.application_model.id IS '主键';
COMMENT ON COLUMN public.application_model.app_code IS '应用标识';
COMMENT ON COLUMN public.application_model.name IS '模型名称';
COMMENT ON COLUMN public.application_model.code IS '模型标识';
COMMENT ON COLUMN public.application_model.type IS '模型类型';
COMMENT ON COLUMN public.application_model.status IS '模型状态';
COMMENT ON COLUMN public.application_model.creation_type IS '模型创建类型';
COMMENT ON COLUMN public.application_model.datasource_id IS '数据源编号';
COMMENT ON COLUMN public.application_model.sync_table_name IS '同步表名称';
COMMENT ON COLUMN public.application_model.base_package_name IS '基础包名';
COMMENT ON COLUMN public.application_model.remark IS '说明';
COMMENT ON COLUMN public.application_model.create_time IS '创建时间';
COMMENT ON COLUMN public.application_model.create_by IS '创建人编号';
COMMENT ON COLUMN public.application_model.create_name IS '创建人姓名';
COMMENT ON COLUMN public.application_model.update_time IS '更新时间';
COMMENT ON COLUMN public.application_model.update_by IS '更新人编号';
COMMENT ON COLUMN public.application_model.update_name IS '更新人姓名';

CREATE TABLE public.application_model_field
(
    id                   CHAR(32) NOT NULL,
    tenant_code          VARCHAR(64),
    model_id             CHAR(32),
    name                 VARCHAR(128),
    type                 VARCHAR(64),
    generic_type         VARCHAR(64),
    remark               VARCHAR(500),
    db_table_field       TINYINT,
    length               VARCHAR(32),
    db_table_field_type  VARCHAR(64),
    nullable             TINYINT,
    query_field          TINYINT,
    compare_type         VARCHAR(64),
    sync_with_datasource TINYINT,
    sort_order           INT4,
    create_time          DATE,
    create_by            VARCHAR(32),
    create_name          VARCHAR(64),
    update_time          DATE,
    update_by            VARCHAR(32),
    update_name          VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.application_model_field IS '业务模型字段';
COMMENT ON COLUMN public.application_model_field.id IS '主键';
COMMENT ON COLUMN public.application_model_field.tenant_code IS '租户标识';
COMMENT ON COLUMN public.application_model_field.model_id IS '所属模型编号';
COMMENT ON COLUMN public.application_model_field.name IS '模型字段名称';
COMMENT ON COLUMN public.application_model_field.type IS '模型字段类型';
COMMENT ON COLUMN public.application_model_field.generic_type IS '模型字段泛型';
COMMENT ON COLUMN public.application_model_field.remark IS '说明';
COMMENT ON COLUMN public.application_model_field.db_table_field IS '是否是数据库表字段';
COMMENT ON COLUMN public.application_model_field.length IS '数据库表字段长度';
COMMENT ON COLUMN public.application_model_field.db_table_field_type IS '数据库表字段类型';
COMMENT ON COLUMN public.application_model_field.nullable IS '是否可以为空';
COMMENT ON COLUMN public.application_model_field.query_field IS '是否为查询字段';
COMMENT ON COLUMN public.application_model_field.compare_type IS '查询比较类型';
COMMENT ON COLUMN public.application_model_field.sync_with_datasource IS '是否与数据源保持同步';
COMMENT ON COLUMN public.application_model_field.sort_order IS '排序值';
COMMENT ON COLUMN public.application_model_field.create_time IS '创建时间';
COMMENT ON COLUMN public.application_model_field.create_by IS '创建人编号';
COMMENT ON COLUMN public.application_model_field.create_name IS '创建人姓名';
COMMENT ON COLUMN public.application_model_field.update_time IS '更新时间';
COMMENT ON COLUMN public.application_model_field.update_by IS '更新人编号';
COMMENT ON COLUMN public.application_model_field.update_name IS '更新人姓名';

CREATE TABLE public.application_model_relation
(
    id                   CHAR(32) NOT NULL,
    tenant_code          VARCHAR(64),
    model_id             CHAR(32),
    master_model_id      CHAR(32),
    foreign_key_field_id CHAR(32),
    create_time          DATE,
    create_by            VARCHAR(32),
    create_name          VARCHAR(64),
    update_time          DATE,
    update_by            VARCHAR(32),
    update_name          VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.application_model_relation IS '业务模型关系';
COMMENT ON COLUMN public.application_model_relation.id IS '主键';
COMMENT ON COLUMN public.application_model_relation.tenant_code IS '租户标识';
COMMENT ON COLUMN public.application_model_relation.model_id IS '附属模型编号';
COMMENT ON COLUMN public.application_model_relation.master_model_id IS '主模型编号';
COMMENT ON COLUMN public.application_model_relation.foreign_key_field_id IS '外键字段编号';
COMMENT ON COLUMN public.application_model_relation.create_time IS '创建时间';
COMMENT ON COLUMN public.application_model_relation.create_by IS '创建人编号';
COMMENT ON COLUMN public.application_model_relation.create_name IS '创建人姓名';
COMMENT ON COLUMN public.application_model_relation.update_time IS '更新时间';
COMMENT ON COLUMN public.application_model_relation.update_by IS '更新人编号';
COMMENT ON COLUMN public.application_model_relation.update_name IS '更新人姓名';

CREATE TABLE public.application_page
(
    id                 VARCHAR(32) NOT NULL,
    code               VARCHAR(64),
    app_code           VARCHAR(64),
    app_id             VARCHAR(32),
    parent_id          VARCHAR(32),
    name               VARCHAR(64),
    icon               VARCHAR(256),
    category           VARCHAR(64) DEFAULT 'GENERAL',
    type               VARCHAR(64),
    link               VARCHAR(256),
    sidebar            TINYINT,
    register_menu      TINYINT,
    immediately_render TINYINT,
    sort_order         INT4,
    create_time        DATE,
    create_by          VARCHAR(32),
    create_name        VARCHAR(64),
    update_time        DATE,
    update_by          VARCHAR(32),
    update_name        VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.application_page IS '应用页面';
COMMENT ON COLUMN public.application_page.id IS '主键';
COMMENT ON COLUMN public.application_page.app_code IS '应用标识';
COMMENT ON COLUMN public.application_page.app_id IS '应用编号';
COMMENT ON COLUMN public.application_page.parent_id IS '父页面编号';
COMMENT ON COLUMN public.application_page.name IS '页面名称';
COMMENT ON COLUMN public.application_page.icon IS '页面图标';
COMMENT ON COLUMN public.application_page.category IS '页面类别';
COMMENT ON COLUMN public.application_page.type IS '页面类型';
COMMENT ON COLUMN public.application_page.link IS '菜单链接';
COMMENT ON COLUMN public.application_page.sidebar IS '是否在侧边栏展示';
COMMENT ON COLUMN public.application_page.register_menu IS '是否注册为菜单';
COMMENT ON COLUMN public.application_page.immediately_render IS '是否立即更新渲染JSON';
COMMENT ON COLUMN public.application_page.sort_order IS '页面排序值';
COMMENT ON COLUMN public.application_page.create_time IS '创建时间';
COMMENT ON COLUMN public.application_page.create_by IS '创建人编号';
COMMENT ON COLUMN public.application_page.create_name IS '创建人姓名';
COMMENT ON COLUMN public.application_page.update_time IS '更新时间';
COMMENT ON COLUMN public.application_page.update_by IS '更新人编号';
COMMENT ON COLUMN public.application_page.update_name IS '更新人姓名';

CREATE TABLE public.application_page_item
(
    id             VARCHAR(32) NOT NULL,
    page_id        VARCHAR(32),
    page_parent_id VARCHAR(32),
    app_code       VARCHAR(64),
    app_id         VARCHAR(32),
    icon           VARCHAR(256),
    type           VARCHAR(64),
    link           VARCHAR(256),
    json           JSON,
    render_json    JSON,
    component      VARCHAR(512),
    sidebar        TINYINT,
    register_menu  TINYINT,
    sort_order     INT4,
    create_time    DATE,
    create_by      VARCHAR(32),
    create_name    VARCHAR(64),
    update_time    DATE,
    update_by      VARCHAR(32),
    update_name    VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.application_page_item IS '应用页面元素';
COMMENT ON COLUMN public.application_page_item.id IS '主键';
COMMENT ON COLUMN public.application_page_item.page_id IS '页面编号';
COMMENT ON COLUMN public.application_page_item.page_parent_id IS '父页面编号';
COMMENT ON COLUMN public.application_page_item.app_code IS '应用标识';
COMMENT ON COLUMN public.application_page_item.app_id IS '应用编号';
COMMENT ON COLUMN public.application_page_item.icon IS '页面图标';
COMMENT ON COLUMN public.application_page_item.type IS '页面元素类型';
COMMENT ON COLUMN public.application_page_item.link IS '页面链接';
COMMENT ON COLUMN public.application_page_item.json IS '页面JSON';
COMMENT ON COLUMN public.application_page_item.render_json IS '页面渲染JSON';
COMMENT ON COLUMN public.application_page_item.component IS '页面组件路径';
COMMENT ON COLUMN public.application_page_item.sidebar IS '是否侧边栏展示';
COMMENT ON COLUMN public.application_page_item.register_menu IS '是否注册为菜单';
COMMENT ON COLUMN public.application_page_item.sort_order IS '页面排序值';
COMMENT ON COLUMN public.application_page_item.create_time IS '创建时间';
COMMENT ON COLUMN public.application_page_item.create_by IS '创建人编号';
COMMENT ON COLUMN public.application_page_item.create_name IS '创建人姓名';
COMMENT ON COLUMN public.application_page_item.update_time IS '更新时间';
COMMENT ON COLUMN public.application_page_item.update_by IS '更新人编号';
COMMENT ON COLUMN public.application_page_item.update_name IS '更新人姓名';

CREATE TABLE public.application_page_model
(
    id          VARCHAR(32) NOT NULL,
    page_id     VARCHAR(32),
    tenant_code VARCHAR(64),
    model_id    VARCHAR(32),
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.application_page_model IS '应用页面关联模型';
COMMENT ON COLUMN public.application_page_model.id IS '主键';
COMMENT ON COLUMN public.application_page_model.page_id IS '页面编号';
COMMENT ON COLUMN public.application_page_model.tenant_code IS '租户标识';
COMMENT ON COLUMN public.application_page_model.model_id IS '模型编号';
COMMENT ON COLUMN public.application_page_model.create_time IS '创建时间';
COMMENT ON COLUMN public.application_page_model.create_by IS '创建人编号';
COMMENT ON COLUMN public.application_page_model.create_name IS '创建人姓名';
COMMENT ON COLUMN public.application_page_model.update_time IS '更新时间';
COMMENT ON COLUMN public.application_page_model.update_by IS '更新人编号';
COMMENT ON COLUMN public.application_page_model.update_name IS '更新人姓名';

CREATE TABLE public.application_page_template
(
    id                VARCHAR(32) NOT NULL,
    title             VARCHAR(128),
    json_data         JSON,
    description       VARCHAR(1024),
    internal_template TINYINT,
    type              VARCHAR(64),
    create_time       DATE,
    create_by         VARCHAR(32),
    create_name       VARCHAR(64),
    update_time       DATE,
    update_by         VARCHAR(32),
    update_name       VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.application_page_template IS '应用页面模板信息';
COMMENT ON COLUMN public.application_page_template.id IS '主键';
COMMENT ON COLUMN public.application_page_template.title IS '名称';
COMMENT ON COLUMN public.application_page_template.json_data IS '模板定义';
COMMENT ON COLUMN public.application_page_template.description IS '描述';
COMMENT ON COLUMN public.application_page_template.internal_template IS '是否为内置模板';
COMMENT ON COLUMN public.application_page_template.type IS '模板类别';
COMMENT ON COLUMN public.application_page_template.create_time IS '创建时间';
COMMENT ON COLUMN public.application_page_template.create_by IS '创建人编号';
COMMENT ON COLUMN public.application_page_template.create_name IS '创建人姓名';
COMMENT ON COLUMN public.application_page_template.update_time IS '更新时间';
COMMENT ON COLUMN public.application_page_template.update_by IS '更新人编号';
COMMENT ON COLUMN public.application_page_template.update_name IS '更新人姓名';

CREATE TABLE public.application_quick_entry
(
    id          VARCHAR(32) NOT NULL,
    app_code    VARCHAR(64),
    tenant_code VARCHAR(64),
    user_id     VARCHAR(32),
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.application_quick_entry IS '应用快捷入口';
COMMENT ON COLUMN public.application_quick_entry.id IS '主键';
COMMENT ON COLUMN public.application_quick_entry.app_code IS '应用标识';
COMMENT ON COLUMN public.application_quick_entry.tenant_code IS '租户标识';
COMMENT ON COLUMN public.application_quick_entry.user_id IS '用户编号';
COMMENT ON COLUMN public.application_quick_entry.create_time IS '创建时间';
COMMENT ON COLUMN public.application_quick_entry.create_by IS '创建人编号';
COMMENT ON COLUMN public.application_quick_entry.create_name IS '创建人姓名';
COMMENT ON COLUMN public.application_quick_entry.update_time IS '更新时间';
COMMENT ON COLUMN public.application_quick_entry.update_by IS '更新人编号';
COMMENT ON COLUMN public.application_quick_entry.update_name IS '更新人姓名';

CREATE TABLE public.approve_cond_comp
(
    id                 VARCHAR(32) NOT NULL,
    form_id            VARCHAR(32),
    process_id         VARCHAR(32),
    node_id            VARCHAR(32),
    group_id           VARCHAR(32),
    widget_code        VARCHAR(128),
    widget_name        VARCHAR(64),
    widget_description VARCHAR(64),
    comparator         VARCHAR(32),
    compare_to_values  TEXT,
    date_pattern       VARCHAR(64),
    applicable_type    VARCHAR(64),
    create_time        DATE,
    create_by          VARCHAR(32),
    create_name        VARCHAR(64),
    update_time        DATE,
    update_by          VARCHAR(32),
    update_name        VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_cond_comp IS '表单流程节点分支条件';
COMMENT ON COLUMN public.approve_cond_comp.id IS '主键';
COMMENT ON COLUMN public.approve_cond_comp.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_cond_comp.process_id IS '流程编号';
COMMENT ON COLUMN public.approve_cond_comp.node_id IS '流程节点编号';
COMMENT ON COLUMN public.approve_cond_comp.group_id IS '流程条件组编号';
COMMENT ON COLUMN public.approve_cond_comp.widget_name IS '表单控件名称';
COMMENT ON COLUMN public.approve_cond_comp.widget_description IS '表单控件描述';
COMMENT ON COLUMN public.approve_cond_comp.comparator IS '条件比较式';
COMMENT ON COLUMN public.approve_cond_comp.compare_to_values IS '条件比较值';
COMMENT ON COLUMN public.approve_cond_comp.date_pattern IS '日期格式';
COMMENT ON COLUMN public.approve_cond_comp.applicable_type IS '适用类型';
COMMENT ON COLUMN public.approve_cond_comp.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_cond_comp.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_cond_comp.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_cond_comp.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_cond_comp.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_cond_comp.update_name IS '更新人姓名';

CREATE TABLE public.approve_cond_group
(
    id              VARCHAR(32) NOT NULL,
    form_id         VARCHAR(32),
    process_id      VARCHAR(32),
    node_id         VARCHAR(32),
    name            VARCHAR(64),
    applicable_type VARCHAR(64),
    create_time     DATE,
    create_by       VARCHAR(32),
    create_name     VARCHAR(64),
    update_time     DATE,
    update_by       VARCHAR(32),
    update_name     VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_cond_group IS '表单流程节点分支条件组';
COMMENT ON COLUMN public.approve_cond_group.id IS '主键';
COMMENT ON COLUMN public.approve_cond_group.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_cond_group.process_id IS '流程编号';
COMMENT ON COLUMN public.approve_cond_group.node_id IS '流程节点编号';
COMMENT ON COLUMN public.approve_cond_group.name IS '分支条件组名称';
COMMENT ON COLUMN public.approve_cond_group.applicable_type IS '适用类型';
COMMENT ON COLUMN public.approve_cond_group.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_cond_group.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_cond_group.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_cond_group.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_cond_group.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_cond_group.update_name IS '更新人姓名';

CREATE TABLE public.approve_form
(
    id                  VARCHAR(32) NOT NULL,
    name                VARCHAR(64),
    code                VARCHAR(64),
    process_code        VARCHAR(64),
    business_type       VARCHAR(64),
    type                VARCHAR(64),
    icon                VARCHAR(512),
    icon_background     VARCHAR(64),
    group_id            VARCHAR(64),
    remark              VARCHAR(500),
    configured          TINYINT,
    report              TINYINT,
    customize_initiator TINYINT,
    dept_id             VARCHAR(32),
    status              VARCHAR(64),
    latest_version      TINYINT,
    version_no          INT4,
    sort_order          INT4,
    visible             TINYINT,
    create_time         DATE,
    create_by           VARCHAR(32),
    create_name         VARCHAR(64),
    update_time         DATE,
    update_by           VARCHAR(32),
    update_name         VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_form IS '表单';
COMMENT ON COLUMN public.approve_form.id IS '主键';
COMMENT ON COLUMN public.approve_form.name IS '表单名称';
COMMENT ON COLUMN public.approve_form.process_code IS '消息渠道自定义模板';
COMMENT ON COLUMN public.approve_form.business_type IS '表单业务类型';
COMMENT ON COLUMN public.approve_form.type IS '表单分类';
COMMENT ON COLUMN public.approve_form.icon IS '表单图标';
COMMENT ON COLUMN public.approve_form.icon_background IS '表单图标背景色';
COMMENT ON COLUMN public.approve_form.group_id IS '表单分组编号';
COMMENT ON COLUMN public.approve_form.remark IS '表单说明';
COMMENT ON COLUMN public.approve_form.configured IS '表单是否已配置';
COMMENT ON COLUMN public.approve_form.report IS '是否是注册表单';
COMMENT ON COLUMN public.approve_form.customize_initiator IS '表单是否支持配置发起人';
COMMENT ON COLUMN public.approve_form.dept_id IS '所属部门编号';
COMMENT ON COLUMN public.approve_form.status IS '表单状态';
COMMENT ON COLUMN public.approve_form.latest_version IS '是否最新版本';
COMMENT ON COLUMN public.approve_form.version_no IS '表单版本号';
COMMENT ON COLUMN public.approve_form.sort_order IS '排序值';
COMMENT ON COLUMN public.approve_form.visible IS '表单在审批中心是否可见';
COMMENT ON COLUMN public.approve_form.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_form.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_form.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_form.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_form.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_form.update_name IS '更新人姓名';

CREATE TABLE public.approve_form_advance_config
(
    id                                VARCHAR(32) NOT NULL,
    form_id                           VARCHAR(32),
    approve_auto_distinct             TINYINT,
    approve_distinct_type             VARCHAR(32),
    allow_append_approve              TINYINT,
    allow_revoke_passed_approve       TINYINT,
    passed_approve_revoke_expire      INT4,
    allow_revoke_approving            TINYINT,
    allow_modify_passed_approve       TINYINT,
    passed_approve_modify_expire      INT4,
    approve_comment_required          TINYINT,
    approve_comment_missing_tip       VARCHAR(128),
    approve_comment_invisible         TINYINT,
    allow_submit_by_other             TINYINT,
    title_template                    VARCHAR(256),
    summary_field_names               VARCHAR(1024),
    notify_day                        DATE,
    notify_time                       TIME,
    approve_form_config_invisible     TINYINT,
    trigger_condition_enabled         TINYINT,
    specified_initiator_dept_required TINYINT,
    allow_trigger_multi_process       TINYINT,
    report_template_id                VARCHAR(64),
    create_time                       DATE,
    create_by                         VARCHAR(32),
    create_name                       VARCHAR(64),
    update_time                       DATE,
    update_by                         VARCHAR(32),
    update_name                       VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_form_advance_config IS '表单高级配置';
COMMENT ON COLUMN public.approve_form_advance_config.id IS '主键';
COMMENT ON COLUMN public.approve_form_advance_config.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_form_advance_config.approve_auto_distinct IS '是否自动去重审批人';
COMMENT ON COLUMN public.approve_form_advance_config.approve_distinct_type IS '审批人去重类型';
COMMENT ON COLUMN public.approve_form_advance_config.allow_append_approve IS '是否允许新增审批人';
COMMENT ON COLUMN public.approve_form_advance_config.allow_revoke_passed_approve IS '是否允许撤销审批通过的审批单';
COMMENT ON COLUMN public.approve_form_advance_config.passed_approve_revoke_expire IS '已通过审批单可撤销最大天数';
COMMENT ON COLUMN public.approve_form_advance_config.allow_revoke_approving IS '是否允许撤销审批中的审批单';
COMMENT ON COLUMN public.approve_form_advance_config.allow_modify_passed_approve IS '是否允许修改审批通过的审批单';
COMMENT ON COLUMN public.approve_form_advance_config.passed_approve_modify_expire IS '修改已通过的审批天数';
COMMENT ON COLUMN public.approve_form_advance_config.approve_comment_required IS '审批意见是否必填';
COMMENT ON COLUMN public.approve_form_advance_config.approve_comment_missing_tip IS '审批意见填写提示';
COMMENT ON COLUMN public.approve_form_advance_config.approve_comment_invisible IS '是否显示审批意见';
COMMENT ON COLUMN public.approve_form_advance_config.allow_submit_by_other IS '是否允许他人代提交';
COMMENT ON COLUMN public.approve_form_advance_config.title_template IS '审批标题模板';
COMMENT ON COLUMN public.approve_form_advance_config.summary_field_names IS '摘要字段名';
COMMENT ON COLUMN public.approve_form_advance_config.notify_day IS '审批提醒日期';
COMMENT ON COLUMN public.approve_form_advance_config.notify_time IS '审批提醒时间';
COMMENT ON COLUMN public.approve_form_advance_config.approve_form_config_invisible IS '是否仅管理员可见';
COMMENT ON COLUMN public.approve_form_advance_config.report_template_id IS '报表模板编号';
COMMENT ON COLUMN public.approve_form_advance_config.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_form_advance_config.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_form_advance_config.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_form_advance_config.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_form_advance_config.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_form_advance_config.update_name IS '更新人姓名';

CREATE TABLE public.approve_form_group
(
    id          VARCHAR(32) NOT NULL,
    name        VARCHAR(64),
    remark      VARCHAR(500),
    renamable   TINYINT,
    deletable   TINYINT,
    sortable    TINYINT,
    sort_order  INT4,
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_form_group IS '表单分组';
COMMENT ON COLUMN public.approve_form_group.id IS '主键';
COMMENT ON COLUMN public.approve_form_group.name IS '表单分组名称';
COMMENT ON COLUMN public.approve_form_group.remark IS '备注';
COMMENT ON COLUMN public.approve_form_group.renamable IS '是否可重命名';
COMMENT ON COLUMN public.approve_form_group.deletable IS '是否可删除';
COMMENT ON COLUMN public.approve_form_group.sortable IS '是否可排序';
COMMENT ON COLUMN public.approve_form_group.sort_order IS '排序值';
COMMENT ON COLUMN public.approve_form_group.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_form_group.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_form_group.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_form_group.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_form_group.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_form_group.update_name IS '更新人姓名';

CREATE TABLE public.approve_form_initiator
(
    id             VARCHAR(32) NOT NULL,
    form_id        VARCHAR(32),
    initiator_id   VARCHAR(32),
    initiator_type VARCHAR(64),
    create_time    DATE,
    create_by      VARCHAR(32),
    create_name    VARCHAR(64),
    update_time    DATE,
    update_by      VARCHAR(32),
    update_name    VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_form_initiator IS '表单发起人';
COMMENT ON COLUMN public.approve_form_initiator.id IS '主键';
COMMENT ON COLUMN public.approve_form_initiator.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_form_initiator.initiator_id IS '表单发起人编号';
COMMENT ON COLUMN public.approve_form_initiator.initiator_type IS '表单发起人类型';
COMMENT ON COLUMN public.approve_form_initiator.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_form_initiator.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_form_initiator.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_form_initiator.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_form_initiator.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_form_initiator.update_name IS '更新人姓名';

CREATE TABLE public.approve_form_instance
(
    id                VARCHAR(32) NOT NULL,
    serial_no         VARCHAR(32),
    form_id           VARCHAR(32),
    form_visible      TINYINT,
    business_type     VARCHAR(64),
    business_data_id  VARCHAR(32),
    form_name         VARCHAR(128),
    form_type         VARCHAR(32),
    title             VARCHAR(128),
    initiator_id      VARCHAR(32),
    initiate_time     DATE,
    form_data         JSON,
    form_data_summary TEXT,
    process_status    VARCHAR(64),
    visible           TINYINT,
    draft             TINYINT,
    create_time       DATE,
    create_by         VARCHAR(32),
    create_name       VARCHAR(64),
    update_time       DATE,
    update_by         VARCHAR(32),
    update_name       VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_form_instance IS '表单实例';
COMMENT ON COLUMN public.approve_form_instance.id IS '主键';
COMMENT ON COLUMN public.approve_form_instance.serial_no IS '审批流水号';
COMMENT ON COLUMN public.approve_form_instance.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_form_instance.form_visible IS '所属表单是否可见';
COMMENT ON COLUMN public.approve_form_instance.business_type IS '表单业务类型';
COMMENT ON COLUMN public.approve_form_instance.business_data_id IS '业务数据编号';
COMMENT ON COLUMN public.approve_form_instance.form_name IS '表单名称';
COMMENT ON COLUMN public.approve_form_instance.form_type IS '表单类型';
COMMENT ON COLUMN public.approve_form_instance.title IS '表单标题';
COMMENT ON COLUMN public.approve_form_instance.initiator_id IS '发起人编号';
COMMENT ON COLUMN public.approve_form_instance.initiate_time IS '发起时间';
COMMENT ON COLUMN public.approve_form_instance.form_data IS '表单数据';
COMMENT ON COLUMN public.approve_form_instance.form_data_summary IS '表单数据摘要';
COMMENT ON COLUMN public.approve_form_instance.process_status IS '流程状态';
COMMENT ON COLUMN public.approve_form_instance.visible IS '表单在审批中心是否可见';
COMMENT ON COLUMN public.approve_form_instance.draft IS '是否草稿';
COMMENT ON COLUMN public.approve_form_instance.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_form_instance.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_form_instance.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_form_instance.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_form_instance.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_form_instance.update_name IS '更新人姓名';

CREATE TABLE public.approve_form_instance_share
(
    id               VARCHAR(32) NOT NULL,
    form_instance_id VARCHAR(32),
    user_id          VARCHAR(32),
    create_time      DATE,
    create_by        VARCHAR(32),
    create_name      VARCHAR(64),
    update_time      DATE,
    update_by        VARCHAR(32),
    update_name      VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_form_instance_share IS '表单实例分享';
COMMENT ON COLUMN public.approve_form_instance_share.id IS '主键';
COMMENT ON COLUMN public.approve_form_instance_share.form_instance_id IS '表单实例编号';
COMMENT ON COLUMN public.approve_form_instance_share.user_id IS '用户编号';
COMMENT ON COLUMN public.approve_form_instance_share.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_form_instance_share.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_form_instance_share.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_form_instance_share.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_form_instance_share.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_form_instance_share.update_name IS '更新人姓名';

CREATE TABLE public.approve_form_manager
(
    id           VARCHAR(32) NOT NULL,
    form_id      VARCHAR(32),
    manager_id   VARCHAR(32),
    manager_type VARCHAR(64),
    sort_order   INT4,
    create_time  DATE,
    create_by    VARCHAR(32),
    create_name  VARCHAR(64),
    update_time  DATE,
    update_by    VARCHAR(32),
    update_name  VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_form_manager IS '表单管理者';
COMMENT ON COLUMN public.approve_form_manager.id IS '主键';
COMMENT ON COLUMN public.approve_form_manager.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_form_manager.manager_id IS '表单管理者编号';
COMMENT ON COLUMN public.approve_form_manager.manager_type IS '表单管理者类型';
COMMENT ON COLUMN public.approve_form_manager.sort_order IS '排序值';
COMMENT ON COLUMN public.approve_form_manager.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_form_manager.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_form_manager.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_form_manager.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_form_manager.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_form_manager.update_name IS '更新人姓名';

CREATE TABLE public.approve_form_view
(
    id           VARCHAR(32) NOT NULL,
    form_id      VARCHAR(32),
    property     JSON,
    submit_label VARCHAR(64),
    refuse_label VARCHAR(64),
    create_time  DATE,
    create_by    VARCHAR(32),
    create_name  VARCHAR(64),
    update_time  DATE,
    update_by    VARCHAR(32),
    update_name  VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_form_view IS '表单页面配置';
COMMENT ON COLUMN public.approve_form_view.id IS '主键';
COMMENT ON COLUMN public.approve_form_view.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_form_view.property IS '表单定义信息';
COMMENT ON COLUMN public.approve_form_view.submit_label IS '提交按钮文字';
COMMENT ON COLUMN public.approve_form_view.refuse_label IS '拒绝按钮文字';
COMMENT ON COLUMN public.approve_form_view.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_form_view.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_form_view.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_form_view.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_form_view.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_form_view.update_name IS '更新人姓名';

CREATE TABLE public.approve_form_widget
(
    id           VARCHAR(32) NOT NULL,
    form_id      VARCHAR(32),
    widget_code  VARCHAR(128),
    widget_name  VARCHAR(64),
    widget_label VARCHAR(64),
    widget_type  VARCHAR(32),
    required     TINYINT,
    hidden       TINYINT,
    remain       TINYINT,
    create_time  DATE,
    create_by    VARCHAR(32),
    create_name  VARCHAR(64),
    update_time  DATE,
    update_by    VARCHAR(32),
    update_name  VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_form_widget IS '表单页面组件';
COMMENT ON COLUMN public.approve_form_widget.id IS '主键';
COMMENT ON COLUMN public.approve_form_widget.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_form_widget.widget_name IS '表单控件名称';
COMMENT ON COLUMN public.approve_form_widget.widget_label IS '表单控件字段标签';
COMMENT ON COLUMN public.approve_form_widget.widget_type IS '表单控件字段类型';
COMMENT ON COLUMN public.approve_form_widget.required IS '字段是否必填';
COMMENT ON COLUMN public.approve_form_widget.remain IS '是否为前一节点保留字段';
COMMENT ON COLUMN public.approve_form_widget.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_form_widget.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_form_widget.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_form_widget.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_form_widget.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_form_widget.update_name IS '更新人姓名';

CREATE TABLE public.approve_process
(
    id          VARCHAR(32) NOT NULL,
    form_id     VARCHAR(32),
    name        VARCHAR(64),
    property    JSON,
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_process IS '表单流程';
COMMENT ON COLUMN public.approve_process.id IS '主键';
COMMENT ON COLUMN public.approve_process.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_process.name IS '流程名称';
COMMENT ON COLUMN public.approve_process.property IS '审批流定义信息';
COMMENT ON COLUMN public.approve_process.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_process.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_process.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_process.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_process.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_process.update_name IS '更新人姓名';

CREATE TABLE public.approve_process_executor
(
    id                         VARCHAR(32) NOT NULL,
    form_id                    VARCHAR(32),
    process_id                 VARCHAR(32),
    node_id                    VARCHAR(32),
    executor_id                VARCHAR(32),
    executor_type              VARCHAR(64),
    executor_dept_id           VARCHAR(32),
    executor_parent_dept_level INT4,
    executor_category          VARCHAR(64),
    sort_order                 INT4,
    create_time                DATE,
    create_by                  VARCHAR(32),
    create_name                VARCHAR(64),
    update_time                DATE,
    update_by                  VARCHAR(32),
    update_name                VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_process_executor IS '表单流程节点执行者配置信息';
COMMENT ON COLUMN public.approve_process_executor.id IS '主键';
COMMENT ON COLUMN public.approve_process_executor.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_process_executor.process_id IS '流程节编号';
COMMENT ON COLUMN public.approve_process_executor.node_id IS '流程节节点编号';
COMMENT ON COLUMN public.approve_process_executor.executor_id IS '流程节执行者编号';
COMMENT ON COLUMN public.approve_process_executor.executor_type IS '流程节执行者类型';
COMMENT ON COLUMN public.approve_process_executor.executor_category IS '流程节执行者类别';
COMMENT ON COLUMN public.approve_process_executor.sort_order IS '排序值';
COMMENT ON COLUMN public.approve_process_executor.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_process_executor.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_process_executor.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_process_executor.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_process_executor.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_process_executor.update_name IS '更新人姓名';

CREATE TABLE public.approve_process_instance
(
    id               VARCHAR(32) NOT NULL,
    name             VARCHAR(64),
    form_id          VARCHAR(32),
    process_id       VARCHAR(32),
    form_instance_id VARCHAR(32),
    status           VARCHAR(64),
    create_time      DATE,
    create_by        VARCHAR(32),
    create_name      VARCHAR(64),
    update_time      DATE,
    update_by        VARCHAR(32),
    update_name      VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_process_instance IS '表单流程实例';
COMMENT ON COLUMN public.approve_process_instance.id IS '主键';
COMMENT ON COLUMN public.approve_process_instance.name IS '流程实例名称';
COMMENT ON COLUMN public.approve_process_instance.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_process_instance.process_id IS '流程配置编号';
COMMENT ON COLUMN public.approve_process_instance.form_instance_id IS '表单实例编号';
COMMENT ON COLUMN public.approve_process_instance.status IS '流程状态';
COMMENT ON COLUMN public.approve_process_instance.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_process_instance.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_process_instance.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_process_instance.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_process_instance.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_process_instance.update_name IS '更新人姓名';

CREATE TABLE public.approve_process_instance_node
(
    id                      VARCHAR(32) NOT NULL,
    form_id                 VARCHAR(32),
    node_form_id            VARCHAR(32),
    process_id              VARCHAR(32),
    node_id                 VARCHAR(32),
    form_instance_id        VARCHAR(32),
    process_instance_id     VARCHAR(32),
    name                    VARCHAR(64),
    type                    VARCHAR(64),
    approve_type            VARCHAR(64),
    approval_category       VARCHAR(64),
    single_approval         TINYINT,
    multi_executor_type     VARCHAR(64),
    re_submit_to_back       TINYINT,
    back_to_initiator       TINYINT,
    executor_empty_strategy VARCHAR(64),
    auto_execute_comment    VARCHAR(500),
    status                  VARCHAR(64),
    sort_order              INT4,
    create_time             DATE,
    create_by               VARCHAR(32),
    create_name             VARCHAR(64),
    update_time             DATE,
    update_by               VARCHAR(32),
    update_name             VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_process_instance_node IS '表单流程实例节点';
COMMENT ON COLUMN public.approve_process_instance_node.id IS '主键';
COMMENT ON COLUMN public.approve_process_instance_node.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_process_instance_node.node_form_id IS '节点表单编号';
COMMENT ON COLUMN public.approve_process_instance_node.process_id IS '流程配置编号';
COMMENT ON COLUMN public.approve_process_instance_node.node_id IS '流程配置节点编号';
COMMENT ON COLUMN public.approve_process_instance_node.form_instance_id IS '表单实例编号';
COMMENT ON COLUMN public.approve_process_instance_node.process_instance_id IS '流程实例编号';
COMMENT ON COLUMN public.approve_process_instance_node.name IS '流程节点名称';
COMMENT ON COLUMN public.approve_process_instance_node.type IS '流程节点类型';
COMMENT ON COLUMN public.approve_process_instance_node.approve_type IS '审批方式';
COMMENT ON COLUMN public.approve_process_instance_node.approval_category IS '流程节点执行者类型';
COMMENT ON COLUMN public.approve_process_instance_node.single_approval IS '发起人自选-是否为自选单人';
COMMENT ON COLUMN public.approve_process_instance_node.multi_executor_type IS '多人审批方式';
COMMENT ON COLUMN public.approve_process_instance_node.re_submit_to_back IS '重新发起时是否有退回人直接审批';
COMMENT ON COLUMN public.approve_process_instance_node.back_to_initiator IS '退回时是否直接退回到发起节点';
COMMENT ON COLUMN public.approve_process_instance_node.executor_empty_strategy IS '节点执行对象为空时的策略';
COMMENT ON COLUMN public.approve_process_instance_node.auto_execute_comment IS '节点自动执行时的审批意见';
COMMENT ON COLUMN public.approve_process_instance_node.status IS '节点执行状态';
COMMENT ON COLUMN public.approve_process_instance_node.sort_order IS '排序值';
COMMENT ON COLUMN public.approve_process_instance_node.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_process_instance_node.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_process_instance_node.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_process_instance_node.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_process_instance_node.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_process_instance_node.update_name IS '更新人姓名';

CREATE TABLE public.approve_process_instance_user
(
    id                       VARCHAR(32) NOT NULL,
    form_id                  VARCHAR(32),
    process_id               VARCHAR(32),
    node_id                  VARCHAR(32),
    form_instance_id         VARCHAR(32),
    process_instance_id      VARCHAR(32),
    process_instance_node_id VARCHAR(32),
    user_id                  VARCHAR(32),
    status                   VARCHAR(64),
    sort_order               INT4,
    create_time              DATE,
    create_by                VARCHAR(32),
    create_name              VARCHAR(64),
    update_time              DATE,
    update_by                VARCHAR(32),
    update_name              VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_process_instance_user IS '表单流程实例节点执行人';
COMMENT ON COLUMN public.approve_process_instance_user.id IS '主键';
COMMENT ON COLUMN public.approve_process_instance_user.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_process_instance_user.process_id IS '流程配置编号';
COMMENT ON COLUMN public.approve_process_instance_user.node_id IS '流程配置节点编号';
COMMENT ON COLUMN public.approve_process_instance_user.form_instance_id IS '表单实例编号';
COMMENT ON COLUMN public.approve_process_instance_user.process_instance_id IS '流程实例编号';
COMMENT ON COLUMN public.approve_process_instance_user.process_instance_node_id IS '流程实例节点编号';
COMMENT ON COLUMN public.approve_process_instance_user.user_id IS '流程执行用户编号';
COMMENT ON COLUMN public.approve_process_instance_user.status IS '流程执行用户状态';
COMMENT ON COLUMN public.approve_process_instance_user.sort_order IS '排序值';
COMMENT ON COLUMN public.approve_process_instance_user.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_process_instance_user.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_process_instance_user.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_process_instance_user.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_process_instance_user.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_process_instance_user.update_name IS '更新人姓名';

CREATE TABLE public.approve_process_node
(
    id                             VARCHAR(32) NOT NULL,
    form_id                        VARCHAR(32),
    node_form_id                   VARCHAR(32),
    process_id                     VARCHAR(32),
    parent_id                      VARCHAR(32),
    name                           VARCHAR(64),
    type                           VARCHAR(64),
    approve_type                   VARCHAR(64),
    approval_category              VARCHAR(64),
    dept_widget_name               VARCHAR(64),
    user_widget_name               VARCHAR(64),
    initiator_specified_scope_type VARCHAR(64),
    role_specified_scope_type      VARCHAR(64),
    single_approval                TINYINT,
    executor_empty_strategy        VARCHAR(64),
    auto_execute_comment           VARCHAR(500),
    multi_executor_type            VARCHAR(64),
    re_submit_to_back              TINYINT,
    back_to_initiator              TINYINT,
    initiator_specify_carbon_copy  TINYINT,
    condition_order                INT4,
    create_time                    DATE,
    create_by                      VARCHAR(32),
    create_name                    VARCHAR(64),
    update_time                    DATE,
    update_by                      VARCHAR(32),
    update_name                    VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_process_node IS '表单流程节点';
COMMENT ON COLUMN public.approve_process_node.id IS '主键';
COMMENT ON COLUMN public.approve_process_node.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_process_node.node_form_id IS '节点表单编号';
COMMENT ON COLUMN public.approve_process_node.process_id IS '流程配置编号';
COMMENT ON COLUMN public.approve_process_node.parent_id IS '前一流程节点编号';
COMMENT ON COLUMN public.approve_process_node.name IS '流程节点名称';
COMMENT ON COLUMN public.approve_process_node.type IS '流程节点类型';
COMMENT ON COLUMN public.approve_process_node.approve_type IS '流程节点审批方式';
COMMENT ON COLUMN public.approve_process_node.approval_category IS '流程节点执行者类型';
COMMENT ON COLUMN public.approve_process_node.dept_widget_name IS '部门选择控件名称';
COMMENT ON COLUMN public.approve_process_node.user_widget_name IS '联系人控件名称';
COMMENT ON COLUMN public.approve_process_node.initiator_specified_scope_type IS '发起人范围类型';
COMMENT ON COLUMN public.approve_process_node.single_approval IS '发起人自选-是否为自选单人';
COMMENT ON COLUMN public.approve_process_node.executor_empty_strategy IS '执行对象为空时的策略';
COMMENT ON COLUMN public.approve_process_node.auto_execute_comment IS '自动执行时的审批意见';
COMMENT ON COLUMN public.approve_process_node.multi_executor_type IS '多人审批方式';
COMMENT ON COLUMN public.approve_process_node.re_submit_to_back IS '重新发起时是否由退回人直接审批';
COMMENT ON COLUMN public.approve_process_node.back_to_initiator IS '退回时是否直接退回到发起节点';
COMMENT ON COLUMN public.approve_process_node.initiator_specify_carbon_copy IS '是否允许申请人自选抄送人';
COMMENT ON COLUMN public.approve_process_node.condition_order IS '条件节点优先级';
COMMENT ON COLUMN public.approve_process_node.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_process_node.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_process_node.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_process_node.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_process_node.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_process_node.update_name IS '更新人姓名';

CREATE TABLE public.approve_process_permission
(
    id              VARCHAR(32) NOT NULL,
    form_id         VARCHAR(32),
    process_id      VARCHAR(32),
    node_id         VARCHAR(32),
    widget_code     VARCHAR(64),
    widget_name     VARCHAR(32),
    permission_type VARCHAR(64),
    create_time     DATE,
    create_by       VARCHAR(32),
    create_name     VARCHAR(64),
    update_time     DATE,
    update_by       VARCHAR(32),
    update_name     VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_process_permission IS '表单流程节点字段权限';
COMMENT ON COLUMN public.approve_process_permission.id IS '主键';
COMMENT ON COLUMN public.approve_process_permission.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_process_permission.process_id IS '流程配置编号';
COMMENT ON COLUMN public.approve_process_permission.node_id IS '流程配置节点编号';
COMMENT ON COLUMN public.approve_process_permission.widget_name IS '表单控件名称';
COMMENT ON COLUMN public.approve_process_permission.permission_type IS '字段权限类型';
COMMENT ON COLUMN public.approve_process_permission.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_process_permission.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_process_permission.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_process_permission.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_process_permission.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_process_permission.update_name IS '更新人姓名';

CREATE TABLE public.approve_record
(
    id                  VARCHAR(32) NOT NULL,
    form_id             VARCHAR(32),
    process_id          VARCHAR(32),
    form_instance_id    VARCHAR(32),
    process_instance_id VARCHAR(32),
    name                VARCHAR(64),
    create_time         DATE,
    create_by           VARCHAR(32),
    create_name         VARCHAR(64),
    update_time         DATE,
    update_by           VARCHAR(32),
    update_name         VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_record IS '审批记录';
COMMENT ON COLUMN public.approve_record.id IS '主键';
COMMENT ON COLUMN public.approve_record.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_record.process_id IS '流程配置编号';
COMMENT ON COLUMN public.approve_record.form_instance_id IS '表单实例编号';
COMMENT ON COLUMN public.approve_record.process_instance_id IS '流程实例编号';
COMMENT ON COLUMN public.approve_record.name IS '审批记录名称';
COMMENT ON COLUMN public.approve_record.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_record.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_record.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_record.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_record.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_record.update_name IS '更新人姓名';

CREATE TABLE public.approve_record_node
(
    id                       VARCHAR(32) NOT NULL,
    record_id                VARCHAR(32),
    form_id                  VARCHAR(32),
    process_id               VARCHAR(32),
    node_id                  VARCHAR(32),
    form_instance_id         VARCHAR(32),
    process_instance_id      VARCHAR(32),
    process_instance_node_id VARCHAR(32),
    name                     VARCHAR(64),
    type                     VARCHAR(64),
    approve_type             VARCHAR(64),
    approval_category        VARCHAR(64),
    single_approval          TINYINT,
    re_submit_to_back        TINYINT,
    back_to_initiator        TINYINT,
    executor_empty_strategy  VARCHAR(64),
    multi_executor_type      VARCHAR(64),
    status                   VARCHAR(64),
    append_executor_user_id  VARCHAR(32),
    back_to_record_node_id   VARCHAR(32),
    back_to_record_node_name VARCHAR(64),
    approve_comment          VARCHAR(500),
    form_instance_comment    VARCHAR(500),
    attachment               VARCHAR(512),
    images                   VARCHAR(512),
    form_data                JSON,
    sort_order               INT4,
    create_time              DATE,
    create_by                VARCHAR(32),
    create_name              VARCHAR(64),
    update_time              DATE,
    update_by                VARCHAR(32),
    update_name              VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_record_node IS '审批记录节点';
COMMENT ON COLUMN public.approve_record_node.id IS '主键';
COMMENT ON COLUMN public.approve_record_node.record_id IS '审批记录编号';
COMMENT ON COLUMN public.approve_record_node.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_record_node.process_id IS '流程配置编号';
COMMENT ON COLUMN public.approve_record_node.node_id IS '流程配置节点编号';
COMMENT ON COLUMN public.approve_record_node.form_instance_id IS '表单实例编号';
COMMENT ON COLUMN public.approve_record_node.process_instance_id IS '流程实例编号';
COMMENT ON COLUMN public.approve_record_node.process_instance_node_id IS '流程实例节点编号';
COMMENT ON COLUMN public.approve_record_node.name IS '流程节点名称';
COMMENT ON COLUMN public.approve_record_node.type IS '流程节点类型';
COMMENT ON COLUMN public.approve_record_node.approve_type IS '审批方式';
COMMENT ON COLUMN public.approve_record_node.approval_category IS '工作流节点执行者类型';
COMMENT ON COLUMN public.approve_record_node.single_approval IS '发起人自选-是否为自选单人';
COMMENT ON COLUMN public.approve_record_node.re_submit_to_back IS '重新发起时是否有退回人直接审批';
COMMENT ON COLUMN public.approve_record_node.back_to_initiator IS '退回时是否直接退回到发起节点';
COMMENT ON COLUMN public.approve_record_node.executor_empty_strategy IS '节点执行对象为空时的策略';
COMMENT ON COLUMN public.approve_record_node.multi_executor_type IS '多人审批方式';
COMMENT ON COLUMN public.approve_record_node.status IS '节点状态';
COMMENT ON COLUMN public.approve_record_node.append_executor_user_id IS '追加执行者用户编号';
COMMENT ON COLUMN public.approve_record_node.back_to_record_node_id IS '退回节点编号';
COMMENT ON COLUMN public.approve_record_node.back_to_record_node_name IS '退回节点名称';
COMMENT ON COLUMN public.approve_record_node.approve_comment IS '审批意见';
COMMENT ON COLUMN public.approve_record_node.form_instance_comment IS '评论';
COMMENT ON COLUMN public.approve_record_node.attachment IS '文件附件';
COMMENT ON COLUMN public.approve_record_node.images IS '图片附件';
COMMENT ON COLUMN public.approve_record_node.sort_order IS '排序值';
COMMENT ON COLUMN public.approve_record_node.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_record_node.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_record_node.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_record_node.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_record_node.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_record_node.update_name IS '更新人姓名';

CREATE TABLE public.approve_record_node_attachment
(
    id                       VARCHAR(32) NOT NULL,
    form_id                  VARCHAR(32),
    process_id               VARCHAR(32),
    node_id                  VARCHAR(32),
    form_instance_id         VARCHAR(32),
    process_instance_id      VARCHAR(32),
    process_instance_node_id VARCHAR(32),
    record_id                VARCHAR(32),
    record_node_id           VARCHAR(32),
    attachment_id            VARCHAR(32),
    attachment_name          VARCHAR(256),
    attachment_size          VARCHAR(64),
    attachment_suffix        VARCHAR(32),
    sort_order               INT4,
    create_time              DATE,
    create_by                VARCHAR(32),
    create_name              VARCHAR(64),
    update_time              DATE,
    update_by                VARCHAR(32),
    update_name              VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_record_node_attachment IS '审批记录附件';
COMMENT ON COLUMN public.approve_record_node_attachment.id IS '主键';
COMMENT ON COLUMN public.approve_record_node_attachment.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_record_node_attachment.process_id IS '流程配置编号';
COMMENT ON COLUMN public.approve_record_node_attachment.node_id IS '流程配置节点编号';
COMMENT ON COLUMN public.approve_record_node_attachment.form_instance_id IS '表单实例编号';
COMMENT ON COLUMN public.approve_record_node_attachment.process_instance_id IS '流程实例编号';
COMMENT ON COLUMN public.approve_record_node_attachment.process_instance_node_id IS '流程实例节点编号';
COMMENT ON COLUMN public.approve_record_node_attachment.record_id IS '审批记录编号';
COMMENT ON COLUMN public.approve_record_node_attachment.record_node_id IS '审批记录节点编号';
COMMENT ON COLUMN public.approve_record_node_attachment.attachment_id IS '附件编号';
COMMENT ON COLUMN public.approve_record_node_attachment.attachment_name IS '附件名称';
COMMENT ON COLUMN public.approve_record_node_attachment.attachment_size IS '附件大小';
COMMENT ON COLUMN public.approve_record_node_attachment.attachment_suffix IS '附件后缀名';
COMMENT ON COLUMN public.approve_record_node_attachment.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_record_node_attachment.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_record_node_attachment.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_record_node_attachment.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_record_node_attachment.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_record_node_attachment.update_name IS '更新人姓名';

CREATE TABLE public.approve_record_node_user
(
    id                       VARCHAR(32) NOT NULL,
    form_id                  VARCHAR(32),
    process_id               VARCHAR(32),
    node_id                  VARCHAR(32),
    form_instance_id         VARCHAR(32),
    process_instance_id      VARCHAR(32),
    process_instance_node_id VARCHAR(32),
    record_id                VARCHAR(32),
    record_node_id           VARCHAR(32),
    user_id                  VARCHAR(32),
    status                   VARCHAR(64),
    append_user              TINYINT,
    sort_order               INT4,
    create_time              DATE,
    create_by                VARCHAR(32),
    create_name              VARCHAR(64),
    update_time              DATE,
    update_by                VARCHAR(32),
    update_name              VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.approve_record_node_user IS '审批记录节点执行人';
COMMENT ON COLUMN public.approve_record_node_user.id IS '主键';
COMMENT ON COLUMN public.approve_record_node_user.form_id IS '表单编号';
COMMENT ON COLUMN public.approve_record_node_user.process_id IS '流程配置编号';
COMMENT ON COLUMN public.approve_record_node_user.node_id IS '流程配置节点编号';
COMMENT ON COLUMN public.approve_record_node_user.form_instance_id IS '表单实例编号';
COMMENT ON COLUMN public.approve_record_node_user.process_instance_id IS '流程实例编号';
COMMENT ON COLUMN public.approve_record_node_user.process_instance_node_id IS '流程实例节点编号';
COMMENT ON COLUMN public.approve_record_node_user.record_id IS '审批记录编号';
COMMENT ON COLUMN public.approve_record_node_user.record_node_id IS '审批记录节点编号';
COMMENT ON COLUMN public.approve_record_node_user.user_id IS '用户编号';
COMMENT ON COLUMN public.approve_record_node_user.status IS '用户状态';
COMMENT ON COLUMN public.approve_record_node_user.append_user IS '是否为加签用户';
COMMENT ON COLUMN public.approve_record_node_user.sort_order IS '排序值';
COMMENT ON COLUMN public.approve_record_node_user.create_time IS '创建时间';
COMMENT ON COLUMN public.approve_record_node_user.create_by IS '创建人编号';
COMMENT ON COLUMN public.approve_record_node_user.create_name IS '创建人姓名';
COMMENT ON COLUMN public.approve_record_node_user.update_time IS '更新时间';
COMMENT ON COLUMN public.approve_record_node_user.update_by IS '更新人编号';
COMMENT ON COLUMN public.approve_record_node_user.update_name IS '更新人姓名';

CREATE TABLE public.attachment
(
    id             VARCHAR(32) NOT NULL,
    business_id    VARCHAR(32),
    business_group VARCHAR(32),
    oss_file_id    VARCHAR(32),
    type           VARCHAR(64),
    create_time    DATE,
    create_by      VARCHAR(32),
    create_name    VARCHAR(64),
    update_time    DATE,
    update_by      VARCHAR(32),
    update_name    VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.attachment IS '附件';
COMMENT ON COLUMN public.attachment.id IS '主键';
COMMENT ON COLUMN public.attachment.business_id IS '业务数据编号';
COMMENT ON COLUMN public.attachment.business_group IS '业务附件分组';
COMMENT ON COLUMN public.attachment.oss_file_id IS '文件编号';
COMMENT ON COLUMN public.attachment.type IS '附件类型';
COMMENT ON COLUMN public.attachment.create_time IS '创建时间';
COMMENT ON COLUMN public.attachment.create_by IS '创建人编号';
COMMENT ON COLUMN public.attachment.create_name IS '创建人姓名';
COMMENT ON COLUMN public.attachment.update_time IS '更新时间';
COMMENT ON COLUMN public.attachment.update_by IS '更新人编号';
COMMENT ON COLUMN public.attachment.update_name IS '更新人姓名';

CREATE TABLE public.auth_log
(
    id          VARCHAR(32) NOT NULL,
    domain_code VARCHAR(64),
    tenant_code VARCHAR(64),
    app_code    VARCHAR(64),
    user_id     CHAR(32),
    username    VARCHAR(64),
    type        VARCHAR(32),
    ip          VARCHAR(64),
    location    VARCHAR(256),
    browser     VARCHAR(256),
    os          VARCHAR(256),
    success     TINYINT,
    message     TEXT,
    sign_time   DATE,
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.auth_log IS '登录日志';
COMMENT ON COLUMN public.auth_log.id IS '主键';
COMMENT ON COLUMN public.auth_log.domain_code IS '域标识';
COMMENT ON COLUMN public.auth_log.tenant_code IS '租户标识';
COMMENT ON COLUMN public.auth_log.app_code IS '应用标识';
COMMENT ON COLUMN public.auth_log.user_id IS '用户编号';
COMMENT ON COLUMN public.auth_log.username IS '用户名';
COMMENT ON COLUMN public.auth_log.type IS '登录类型';
COMMENT ON COLUMN public.auth_log.ip IS '客户端ip';
COMMENT ON COLUMN public.auth_log.location IS '客户端地区';
COMMENT ON COLUMN public.auth_log.browser IS '浏览器';
COMMENT ON COLUMN public.auth_log.os IS '操作系统';
COMMENT ON COLUMN public.auth_log.success IS '是否成功';
COMMENT ON COLUMN public.auth_log.message IS '返回信息';
COMMENT ON COLUMN public.auth_log.sign_time IS '登录/登出时间';
COMMENT ON COLUMN public.auth_log.create_time IS '创建时间';
COMMENT ON COLUMN public.auth_log.create_by IS '创建人编号';
COMMENT ON COLUMN public.auth_log.create_name IS '创建人姓名';
COMMENT ON COLUMN public.auth_log.update_time IS '更新时间';
COMMENT ON COLUMN public.auth_log.update_by IS '更新人编号';
COMMENT ON COLUMN public.auth_log.update_name IS '更新人姓名';

CREATE TABLE public.cashier_supplier
(
    id               VARCHAR(32) NOT NULL,
    type             VARCHAR(32),
    name             VARCHAR(64),
    custom_id        VARCHAR(256),
    custom_name      VARCHAR(256),
    custom_serial_no VARCHAR(256),
    app_id           VARCHAR(256),
    app_secret       VARCHAR(256),
    api_key          VARCHAR(256),
    extra_api_key    VARCHAR(256),
    private_key      VARCHAR(512),
    public_key       VARCHAR(512),
    cert_key         VARCHAR(512),
    notify_url       VARCHAR(512),
    create_time      DATE,
    create_by        VARCHAR(32),
    create_name      VARCHAR(64),
    update_time      DATE,
    update_by        VARCHAR(32),
    update_name      VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.cashier_supplier IS '支付平台';
COMMENT ON COLUMN public.cashier_supplier.id IS '主键';
COMMENT ON COLUMN public.cashier_supplier.type IS '支付平台类型';
COMMENT ON COLUMN public.cashier_supplier.name IS '支付平台名称';
COMMENT ON COLUMN public.cashier_supplier.custom_id IS '支付平台商户编号';
COMMENT ON COLUMN public.cashier_supplier.custom_name IS '支付平台商户名称';
COMMENT ON COLUMN public.cashier_supplier.custom_serial_no IS '支付平台商户SerialNo';
COMMENT ON COLUMN public.cashier_supplier.app_id IS '支付平台AppId';
COMMENT ON COLUMN public.cashier_supplier.app_secret IS '短信平台AppSecret';
COMMENT ON COLUMN public.cashier_supplier.api_key IS '支付平台AppKey';
COMMENT ON COLUMN public.cashier_supplier.extra_api_key IS '支付平台旧版本AppKey';
COMMENT ON COLUMN public.cashier_supplier.private_key IS '支付平台私钥';
COMMENT ON COLUMN public.cashier_supplier.public_key IS '支付平台公钥';
COMMENT ON COLUMN public.cashier_supplier.cert_key IS '支付平台证书';
COMMENT ON COLUMN public.cashier_supplier.notify_url IS '支付结果通知地址';
COMMENT ON COLUMN public.cashier_supplier.create_time IS '创建时间';
COMMENT ON COLUMN public.cashier_supplier.create_by IS '创建人编号';
COMMENT ON COLUMN public.cashier_supplier.create_name IS '创建人姓名';
COMMENT ON COLUMN public.cashier_supplier.update_time IS '更新时间';
COMMENT ON COLUMN public.cashier_supplier.update_by IS '更新人编号';
COMMENT ON COLUMN public.cashier_supplier.update_name IS '更新人姓名';

CREATE TABLE public.country
(
    id          VARCHAR(32) NOT NULL,
    code        VARCHAR(64),
    full_code   VARCHAR(64),
    tenant_code VARCHAR(32),
    name        VARCHAR(128),
    eu_name     VARCHAR(512),
    continent   VARCHAR(32),
    sort_order  INT4,
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.country IS '国家';
COMMENT ON COLUMN public.country.id IS '主键';
COMMENT ON COLUMN public.country.code IS '国家编码';
COMMENT ON COLUMN public.country.full_code IS '国家完整编码';
COMMENT ON COLUMN public.country.tenant_code IS '国家完整编码';
COMMENT ON COLUMN public.country.name IS '国家名称';
COMMENT ON COLUMN public.country.eu_name IS '国家英文名称';
COMMENT ON COLUMN public.country.continent IS '国家所属大洲';
COMMENT ON COLUMN public.country.sort_order IS '排序值';
COMMENT ON COLUMN public.country.create_time IS '创建时间';
COMMENT ON COLUMN public.country.create_by IS '创建人编号';
COMMENT ON COLUMN public.country.create_name IS '创建人姓名';
COMMENT ON COLUMN public.country.update_time IS '更新时间';
COMMENT ON COLUMN public.country.update_by IS '更新人编号';
COMMENT ON COLUMN public.country.update_name IS '更新人姓名';

CREATE TABLE public.database_backup_record
(
    id            CHAR(32) NOT NULL,
    strategy_id   CHAR(32),
    record_code   VARCHAR(64),
    sql_file_path VARCHAR(256),
    log_file_path VARCHAR(256),
    success       TINYINT,
    expire_time   DATE,
    create_time   DATE,
    create_by     CHAR(32),
    create_name   VARCHAR(64),
    update_time   DATE,
    update_by     CHAR(32),
    update_name   VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.database_backup_record IS '数据库备份记录';
COMMENT ON COLUMN public.database_backup_record.id IS '主键';
COMMENT ON COLUMN public.database_backup_record.strategy_id IS '数据库备份策略id';
COMMENT ON COLUMN public.database_backup_record.record_code IS '编号';
COMMENT ON COLUMN public.database_backup_record.sql_file_path IS 'sql文件路径';
COMMENT ON COLUMN public.database_backup_record.log_file_path IS 'log文件路劲';
COMMENT ON COLUMN public.database_backup_record.success IS '是否执行成功';
COMMENT ON COLUMN public.database_backup_record.expire_time IS '备份文件到期时间';
COMMENT ON COLUMN public.database_backup_record.create_time IS '创建时间';
COMMENT ON COLUMN public.database_backup_record.create_by IS '创建人编号';
COMMENT ON COLUMN public.database_backup_record.create_name IS '创建人姓名';
COMMENT ON COLUMN public.database_backup_record.update_time IS '更新时间';
COMMENT ON COLUMN public.database_backup_record.update_by IS '更新人编号';
COMMENT ON COLUMN public.database_backup_record.update_name IS '更新人姓名';

CREATE TABLE public.database_backup_strategy
(
    id            CHAR(32) NOT NULL,
    name          VARCHAR(64),
    datasource_id CHAR(32),
    schema_name   VARCHAR(64),
    type          TINYINT,
    table_names   VARCHAR(1024),
    cron          VARCHAR(64),
    expire_type   INT4,
    parent_path   VARCHAR(256),
    running       TINYINT,
    remark        VARCHAR(256),
    del_flag      TINYINT,
    create_time   DATE,
    create_by     CHAR(32),
    create_name   VARCHAR(64),
    update_time   DATE,
    update_by     CHAR(32),
    update_name   VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.database_backup_strategy IS '数据库备份策略';
COMMENT ON COLUMN public.database_backup_strategy.id IS '主键';
COMMENT ON COLUMN public.database_backup_strategy.name IS '备份策略名称（字母下划线数字）';
COMMENT ON COLUMN public.database_backup_strategy.datasource_id IS '数据源id';
COMMENT ON COLUMN public.database_backup_strategy.schema_name IS '数据库名称';
COMMENT ON COLUMN public.database_backup_strategy.type IS '备份对象类型（0自定义选表；1全表)';
COMMENT ON COLUMN public.database_backup_strategy.table_names IS '备份对象（全表为null,自定义为表名字符串）';
COMMENT ON COLUMN public.database_backup_strategy.cron IS 'cron表达式';
COMMENT ON COLUMN public.database_backup_strategy.expire_type IS '过期策略';
COMMENT ON COLUMN public.database_backup_strategy.parent_path IS '备份根目录';
COMMENT ON COLUMN public.database_backup_strategy.running IS '状态（开启/暂停）';
COMMENT ON COLUMN public.database_backup_strategy.remark IS '描述';
COMMENT ON COLUMN public.database_backup_strategy.del_flag IS '是否已删除';
COMMENT ON COLUMN public.database_backup_strategy.create_time IS '创建时间';
COMMENT ON COLUMN public.database_backup_strategy.create_by IS '创建人编号';
COMMENT ON COLUMN public.database_backup_strategy.create_name IS '创建人姓名';
COMMENT ON COLUMN public.database_backup_strategy.update_time IS '更新时间';
COMMENT ON COLUMN public.database_backup_strategy.update_by IS '更新人编号';
COMMENT ON COLUMN public.database_backup_strategy.update_name IS '更新人姓名';

CREATE TABLE public.datasource
(
    id                 VARCHAR(32) NOT NULL,
    tenant_code        VARCHAR(64),
    name               VARCHAR(64),
    type               VARCHAR(64),
    ip                 VARCHAR(64),
    port               VARCHAR(16),
    jdbc_url           VARCHAR(512),
    service_name       VARCHAR(128),
    username           VARCHAR(64),
    password           VARCHAR(64),
    driver_class_name  VARCHAR(256),
    schema_name        VARCHAR(64),
    database_name      VARCHAR(128),
    remark             VARCHAR(500),
    default_datasource TINYINT,
    create_time        DATE,
    create_by          VARCHAR(32),
    create_name        VARCHAR(64),
    update_time        DATE,
    update_by          VARCHAR(32),
    update_name        VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.datasource IS '数据源';
COMMENT ON COLUMN public.datasource.id IS '主键';
COMMENT ON COLUMN public.datasource.tenant_code IS '租户标识';
COMMENT ON COLUMN public.datasource.name IS '数据源名称';
COMMENT ON COLUMN public.datasource.type IS '数据源类型';
COMMENT ON COLUMN public.datasource.ip IS '数据源IP地址';
COMMENT ON COLUMN public.datasource.port IS '数据源端口';
COMMENT ON COLUMN public.datasource.jdbc_url IS 'jdbc链接';
COMMENT ON COLUMN public.datasource.username IS '用户名';
COMMENT ON COLUMN public.datasource.password IS '密码';
COMMENT ON COLUMN public.datasource.driver_class_name IS '驱动类名';
COMMENT ON COLUMN public.datasource.schema_name IS '数据库名';
COMMENT ON COLUMN public.datasource.remark IS '备注';
COMMENT ON COLUMN public.datasource.default_datasource IS '是否默认';
COMMENT ON COLUMN public.datasource.create_time IS '创建时间';
COMMENT ON COLUMN public.datasource.create_by IS '创建人编号';
COMMENT ON COLUMN public.datasource.create_name IS '创建人姓名';
COMMENT ON COLUMN public.datasource.update_time IS '更新时间';
COMMENT ON COLUMN public.datasource.update_by IS '更新人编号';
COMMENT ON COLUMN public.datasource.update_name IS '更新人姓名';

CREATE TABLE public.department
(
    id                           VARCHAR(32) NOT NULL,
    full_path                    VARCHAR(600),
    parent_id                    VARCHAR(32),
    tenant_code                  VARCHAR(64),
    name                         VARCHAR(64),
    grade                        INT4,
    code                         VARCHAR(64),
    short_name                   VARCHAR(64),
    manager_id                   VARCHAR(32),
    juridical_person_name        VARCHAR(64),
    juridical_person_phone       VARCHAR(64),
    juridical_person_identity_no VARCHAR(64),
    type                         VARCHAR(64),
    category                     VARCHAR(64),
    establish_date               DATE,
    status                       VARCHAR(64),
    equity_ratio                 VARCHAR(32),
    province_code                VARCHAR(64),
    province_name                VARCHAR(64),
    city_code                    VARCHAR(64),
    city_name                    VARCHAR(64),
    district_code                VARCHAR(64),
    town_code                    VARCHAR(128),
    town_name                    VARCHAR(128),
    district_name                VARCHAR(64),
    street_code                  VARCHAR(64),
    street_name                  VARCHAR(64),
    address                      VARCHAR(500),
    social_credit_identity_code  VARCHAR(128),
    bank_name                    VARCHAR(128),
    bank_account                 VARCHAR(128),
    home_page                    VARCHAR(500),
    postal_code                  VARCHAR(64),
    phone                        VARCHAR(64),
    fax                          VARCHAR(256),
    introduction_type            VARCHAR(64),
    introduction                 LONGTEXT,
    default_flag                 TINYINT,
    sort_order                   INT4,
    del_flag                     TINYINT,
    create_time                  DATE,
    create_by                    VARCHAR(32),
    create_name                  VARCHAR(64),
    update_time                  DATE,
    update_by                    VARCHAR(32),
    update_name                  VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.department IS '部门';
COMMENT ON COLUMN public.department.id IS '主键';
COMMENT ON COLUMN public.department.full_path IS '组织架构全路径';
COMMENT ON COLUMN public.department.parent_id IS '父组织架构编号';
COMMENT ON COLUMN public.department.tenant_code IS '租户标识';
COMMENT ON COLUMN public.department.name IS '组织架构名称';
COMMENT ON COLUMN public.department.grade IS '级别';
COMMENT ON COLUMN public.department.code IS '编码';
COMMENT ON COLUMN public.department.short_name IS '简称';
COMMENT ON COLUMN public.department.manager_id IS '主管领导';
COMMENT ON COLUMN public.department.type IS '组织架构类型';
COMMENT ON COLUMN public.department.category IS '组织架构类别';
COMMENT ON COLUMN public.department.establish_date IS '成立日期';
COMMENT ON COLUMN public.department.status IS '状态';
COMMENT ON COLUMN public.department.equity_ratio IS '股权比例';
COMMENT ON COLUMN public.department.province_code IS '省份代码';
COMMENT ON COLUMN public.department.province_name IS '省份名称';
COMMENT ON COLUMN public.department.city_code IS '市代码';
COMMENT ON COLUMN public.department.city_name IS '市名称';
COMMENT ON COLUMN public.department.district_code IS '区代码';
COMMENT ON COLUMN public.department.district_name IS '区名称';
COMMENT ON COLUMN public.department.street_code IS '街道代码';
COMMENT ON COLUMN public.department.street_name IS '街道名称';
COMMENT ON COLUMN public.department.address IS '详细住址';
COMMENT ON COLUMN public.department.home_page IS '网址';
COMMENT ON COLUMN public.department.postal_code IS '邮编';
COMMENT ON COLUMN public.department.phone IS '联系电话';
COMMENT ON COLUMN public.department.fax IS '传真号';
COMMENT ON COLUMN public.department.introduction_type IS '简介类型';
COMMENT ON COLUMN public.department.introduction IS '简介内容';
COMMENT ON COLUMN public.department.default_flag IS '是否默认部门';
COMMENT ON COLUMN public.department.sort_order IS '排序值';
COMMENT ON COLUMN public.department.del_flag IS '是否已删除';
COMMENT ON COLUMN public.department.create_time IS '创建时间';
COMMENT ON COLUMN public.department.create_by IS '创建人编号';
COMMENT ON COLUMN public.department.create_name IS '创建人姓名';
COMMENT ON COLUMN public.department.update_time IS '更新时间';
COMMENT ON COLUMN public.department.update_by IS '更新人编号';
COMMENT ON COLUMN public.department.update_name IS '更新人姓名';

CREATE TABLE public.dict
(
    id          VARCHAR(32) NOT NULL,
    name        VARCHAR(64),
    code        VARCHAR(64),
    tenant_code VARCHAR(64),
    remark      VARCHAR(500),
    type        VARCHAR(64),
    del_flag    TINYINT,
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.dict IS '字典';
COMMENT ON COLUMN public.dict.id IS '主键';
COMMENT ON COLUMN public.dict.name IS '字典名称';
COMMENT ON COLUMN public.dict.code IS '字典标识';
COMMENT ON COLUMN public.dict.tenant_code IS '租户标识';
COMMENT ON COLUMN public.dict.remark IS '备注';
COMMENT ON COLUMN public.dict.type IS '字典类型，system:系统字典 business:业务字典';
COMMENT ON COLUMN public.dict.del_flag IS '是否已删除';
COMMENT ON COLUMN public.dict.create_time IS '创建时间';
COMMENT ON COLUMN public.dict.create_by IS '创建人编号';
COMMENT ON COLUMN public.dict.create_name IS '创建人姓名';
COMMENT ON COLUMN public.dict.update_time IS '更新时间';
COMMENT ON COLUMN public.dict.update_by IS '更新人编号';
COMMENT ON COLUMN public.dict.update_name IS '更新人姓名';

CREATE TABLE public.dict_item
(
    id          VARCHAR(32) NOT NULL,
    parent_id   VARCHAR(32),
    full_path   VARCHAR(600),
    dict_id     VARCHAR(32),
    dict_code   VARCHAR(32),
    tenant_code VARCHAR(64),
    label       VARCHAR(64),
    value       VARCHAR(64),
    remark      VARCHAR(500),
    preset      TINYINT,
    sort_order  INT4,
    del_flag    TINYINT,
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.dict_item IS '字典项';
COMMENT ON COLUMN public.dict_item.id IS '主键';
COMMENT ON COLUMN public.dict_item.parent_id IS '父字典项编号';
COMMENT ON COLUMN public.dict_item.full_path IS '字典项全路径';
COMMENT ON COLUMN public.dict_item.dict_id IS '所属字典编号';
COMMENT ON COLUMN public.dict_item.dict_code IS '所属字典标识';
COMMENT ON COLUMN public.dict_item.tenant_code IS '租户标识';
COMMENT ON COLUMN public.dict_item.label IS '字典项名称';
COMMENT ON COLUMN public.dict_item.value IS '字典项数据';
COMMENT ON COLUMN public.dict_item.remark IS '备注';
COMMENT ON COLUMN public.dict_item.preset IS '是否预置字典项';
COMMENT ON COLUMN public.dict_item.sort_order IS '排序值';
COMMENT ON COLUMN public.dict_item.del_flag IS '是否已删除';
COMMENT ON COLUMN public.dict_item.create_time IS '创建时间';
COMMENT ON COLUMN public.dict_item.create_by IS '创建人编号';
COMMENT ON COLUMN public.dict_item.create_name IS '创建人姓名';
COMMENT ON COLUMN public.dict_item.update_time IS '更新时间';
COMMENT ON COLUMN public.dict_item.update_by IS '更新人编号';
COMMENT ON COLUMN public.dict_item.update_name IS '更新人姓名';

CREATE TABLE public.job
(
    id                 VARCHAR(32) NOT NULL,
    tenant_code        VARCHAR(64),
    name               VARCHAR(256),
    description        VARCHAR(256),
    type               VARCHAR(64),
    executor_client_id VARCHAR(512),
    executor_handler   VARCHAR(512),
    param              TEXT,
    cron               VARCHAR(128),
    next_time          DATE,
    status             VARCHAR(64),
    create_time        DATE,
    create_by          VARCHAR(32),
    create_name        VARCHAR(64),
    update_time        DATE,
    update_by          VARCHAR(32),
    update_name        VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.job IS '任务';
COMMENT ON COLUMN public.job.id IS '主键';
COMMENT ON COLUMN public.job.tenant_code IS '租户标识';
COMMENT ON COLUMN public.job.name IS '任务名称';
COMMENT ON COLUMN public.job.description IS '任务描述';
COMMENT ON COLUMN public.job.type IS '任务类型';
COMMENT ON COLUMN public.job.executor_client_id IS '执行器客户端标识';
COMMENT ON COLUMN public.job.executor_handler IS '执行器标识';
COMMENT ON COLUMN public.job.param IS '任务参数';
COMMENT ON COLUMN public.job.cron IS 'cron表达式';
COMMENT ON COLUMN public.job.next_time IS '下一次任务执行时间';
COMMENT ON COLUMN public.job.status IS '任务状态';
COMMENT ON COLUMN public.job.create_time IS '创建时间';
COMMENT ON COLUMN public.job.create_by IS '创建人编号';
COMMENT ON COLUMN public.job.create_name IS '创建人姓名';
COMMENT ON COLUMN public.job.update_time IS '更新时间';
COMMENT ON COLUMN public.job.update_by IS '更新人编号';
COMMENT ON COLUMN public.job.update_name IS '更新人姓名';

CREATE TABLE public.label
(
    id            VARCHAR(32) NOT NULL,
    tenant_code   VARCHAR(64),
    business_key  VARCHAR(64),
    business_name VARCHAR(64),
    label         VARCHAR(64),
    value         VARCHAR(64),
    create_time   DATE,
    create_by     VARCHAR(32),
    create_name   VARCHAR(64),
    update_time   DATE,
    update_by     VARCHAR(32),
    update_name   VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.label IS '标签';
COMMENT ON COLUMN public.label.id IS '主键';
COMMENT ON COLUMN public.label.tenant_code IS '租户标识';
COMMENT ON COLUMN public.label.business_key IS '业务标识';
COMMENT ON COLUMN public.label.business_name IS '业务名称';
COMMENT ON COLUMN public.label.label IS '标签名称';
COMMENT ON COLUMN public.label.value IS '标签值';
COMMENT ON COLUMN public.label.create_time IS '创建时间';
COMMENT ON COLUMN public.label.create_by IS '创建人编号';
COMMENT ON COLUMN public.label.create_name IS '创建人姓名';
COMMENT ON COLUMN public.label.update_time IS '更新时间';
COMMENT ON COLUMN public.label.update_by IS '更新人编号';
COMMENT ON COLUMN public.label.update_name IS '更新人姓名';

CREATE TABLE public.label_assign
(
    id          VARCHAR(32) NOT NULL,
    tenant_code VARCHAR(64),
    label_id    VARCHAR(32),
    business_id VARCHAR(256),
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.label_assign IS '标签分配';
COMMENT ON COLUMN public.label_assign.id IS '主键';
COMMENT ON COLUMN public.label_assign.tenant_code IS '租户标识';
COMMENT ON COLUMN public.label_assign.label_id IS '标签名称';
COMMENT ON COLUMN public.label_assign.business_id IS '业务数据编号';
COMMENT ON COLUMN public.label_assign.create_time IS '创建时间';
COMMENT ON COLUMN public.label_assign.create_by IS '创建人编号';
COMMENT ON COLUMN public.label_assign.create_name IS '创建人姓名';
COMMENT ON COLUMN public.label_assign.update_time IS '更新时间';
COMMENT ON COLUMN public.label_assign.update_by IS '更新人编号';
COMMENT ON COLUMN public.label_assign.update_name IS '更新人姓名';

CREATE TABLE public.menu
(
    id              VARCHAR(32) NOT NULL,
    full_path       VARCHAR(600),
    parent_id       VARCHAR(32),
    name            VARCHAR(64),
    domain_code     VARCHAR(64),
    tenant_code     VARCHAR(64),
    app_code        VARCHAR(64),
    page_path       VARCHAR(512),
    component       VARCHAR(512),
    framework       TINYINT,
    eu_name         VARCHAR(64),
    permission_code VARCHAR(64),
    icon            VARCHAR(256),
    active_icon     VARCHAR(256),
    logo            VARCHAR(256),
    link            VARCHAR(256),
    iframe          VARCHAR(256),
    default_opened  TINYINT,
    permanent       TINYINT,
    sidebar         TINYINT,
    breadcrumb      TINYINT,
    active_menu     VARCHAR(256),
    cache           VARCHAR(256),
    no_cache        VARCHAR(256),
    badge           VARCHAR(256),
    copyright       TINYINT,
    padding_bottom  VARCHAR(256),
    whitelist       VARCHAR(256),
    menu_view_type  VARCHAR(64),
    menu_type       VARCHAR(64),
    jump_type       VARCHAR(64),
    keep_alive      TINYINT,
    visible         TINYINT,
    auto_render     TINYINT,
    remark          VARCHAR(500),
    preset          TINYINT,
    sort_order      INT4,
    create_time     DATE,
    create_by       VARCHAR(32),
    create_name     VARCHAR(64),
    update_time     DATE,
    update_by       VARCHAR(32),
    update_name     VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.menu IS '菜单';
COMMENT ON COLUMN public.menu.id IS '主键';
COMMENT ON COLUMN public.menu.full_path IS '菜单全路径';
COMMENT ON COLUMN public.menu.parent_id IS '父菜单编号';
COMMENT ON COLUMN public.menu.name IS '菜单名称';
COMMENT ON COLUMN public.menu.domain_code IS '域标识';
COMMENT ON COLUMN public.menu.tenant_code IS '租户标识';
COMMENT ON COLUMN public.menu.app_code IS '应用标识';
COMMENT ON COLUMN public.menu.page_path IS '菜单图标';
COMMENT ON COLUMN public.menu.component IS '文件地址';
COMMENT ON COLUMN public.menu.framework IS '是否为框架路由';
COMMENT ON COLUMN public.menu.eu_name IS '菜单英文名称';
COMMENT ON COLUMN public.menu.permission_code IS '菜单权限码';
COMMENT ON COLUMN public.menu.icon IS '菜单图标';
COMMENT ON COLUMN public.menu.active_icon IS '菜单激活时显示的图标';
COMMENT ON COLUMN public.menu.logo IS '应用图标';
COMMENT ON COLUMN public.menu.link IS '外部网页跳转链接';
COMMENT ON COLUMN public.menu.iframe IS '内嵌网页链接';
COMMENT ON COLUMN public.menu.default_opened IS '是否默认展开';
COMMENT ON COLUMN public.menu.permanent IS '是否常驻标签页';
COMMENT ON COLUMN public.menu.sidebar IS '是否在侧边栏导航中展示';
COMMENT ON COLUMN public.menu.breadcrumb IS '是否在面包屑导航中展示';
COMMENT ON COLUMN public.menu.active_menu IS '侧边栏高亮路由完整地址';
COMMENT ON COLUMN public.menu.cache IS '页面缓存，内容为空时表示不缓存，内容为permanent时表示永久缓存，内容为其他时表示跳转到指定页面是缓存当前页面';
COMMENT ON COLUMN public.menu.no_cache IS '清除页面缓存';
COMMENT ON COLUMN public.menu.badge IS '导航徽标，内容为空时表示无徽标，内容为point时表示点形徽标，内容为其他时表示徽标文本内容，如果是数字字符串，则为0时不显示徽标';
COMMENT ON COLUMN public.menu.copyright IS '是否显示版权信息';
COMMENT ON COLUMN public.menu.padding_bottom IS '页面和底部的距离';
COMMENT ON COLUMN public.menu.whitelist IS '路由白名单，白名单内的路由不受权限控制';
COMMENT ON COLUMN public.menu.menu_view_type IS '菜单可视类型';
COMMENT ON COLUMN public.menu.menu_type IS '菜单类型';
COMMENT ON COLUMN public.menu.jump_type IS '跳转方式';
COMMENT ON COLUMN public.menu.keep_alive IS '路由缓冲';
COMMENT ON COLUMN public.menu.visible IS '菜单是否可见';
COMMENT ON COLUMN public.menu.auto_render IS '是否自动渲染页面';
COMMENT ON COLUMN public.menu.remark IS '菜单描述';
COMMENT ON COLUMN public.menu.preset IS '是否为预置菜单';
COMMENT ON COLUMN public.menu.sort_order IS '排序值';
COMMENT ON COLUMN public.menu.create_time IS '创建时间';
COMMENT ON COLUMN public.menu.create_by IS '创建人编号';
COMMENT ON COLUMN public.menu.create_name IS '创建人姓名';
COMMENT ON COLUMN public.menu.update_time IS '更新时间';
COMMENT ON COLUMN public.menu.update_by IS '更新人编号';
COMMENT ON COLUMN public.menu.update_name IS '更新人姓名';

CREATE TABLE public.message
(
    id                   VARCHAR(32) NOT NULL,
    tenant_code          VARCHAR(64),
    template_id          VARCHAR(32),
    channel              VARCHAR(32),
    business_name        VARCHAR(64),
    business_key         VARCHAR(32),
    message_template_key VARCHAR(32),
    message_title        VARCHAR(256),
    message_content      VARCHAR(1024),
    redirect_to          TEXT,
    deliver_status       VARCHAR(64),
    deliver_time         DATE,
    receiver_id          VARCHAR(32),
    read_status          VARCHAR(64),
    read_time            DATE,
    cause                VARCHAR(1024),
    create_time          DATE,
    create_by            VARCHAR(32),
    create_name          VARCHAR(64),
    update_time          DATE,
    update_by            VARCHAR(32),
    update_name          VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.message IS '消息';
COMMENT ON COLUMN public.message.id IS '主键';
COMMENT ON COLUMN public.message.tenant_code IS '租户标识';
COMMENT ON COLUMN public.message.template_id IS '消息模板编号';
COMMENT ON COLUMN public.message.channel IS '发送渠道';
COMMENT ON COLUMN public.message.business_name IS '业务类型名称';
COMMENT ON COLUMN public.message.business_key IS '业务类型标识';
COMMENT ON COLUMN public.message.message_template_key IS '消息模板标识';
COMMENT ON COLUMN public.message.message_title IS '消息标题';
COMMENT ON COLUMN public.message.message_content IS '消息内容';
COMMENT ON COLUMN public.message.redirect_to IS '跳转地址';
COMMENT ON COLUMN public.message.deliver_status IS '发送状态';
COMMENT ON COLUMN public.message.deliver_time IS '发送时间';
COMMENT ON COLUMN public.message.receiver_id IS '接收者编号';
COMMENT ON COLUMN public.message.read_status IS '消息读取状态';
COMMENT ON COLUMN public.message.read_time IS '消息读取时间';
COMMENT ON COLUMN public.message.cause IS '发送失败原因';
COMMENT ON COLUMN public.message.create_time IS '创建时间';
COMMENT ON COLUMN public.message.create_by IS '创建人编号';
COMMENT ON COLUMN public.message.create_name IS '创建人姓名';
COMMENT ON COLUMN public.message.update_time IS '更新时间';
COMMENT ON COLUMN public.message.update_by IS '更新人编号';
COMMENT ON COLUMN public.message.update_name IS '更新人姓名';

CREATE TABLE public.message_channel_definition
(
    id                  VARCHAR(32) NOT NULL,
    tenant_code         VARCHAR(64),
    template_id         VARCHAR(32),
    business_key        VARCHAR(64),
    business_name       VARCHAR(128),
    message_title       VARCHAR(64),
    channel             VARCHAR(32),
    channel_description VARCHAR(64),
    channel_id          VARCHAR(32),
    channel_template_id VARCHAR(64),
    content_type        VARCHAR(64),
    content             LONGTEXT,
    customize_content   LONGTEXT,
    voice               TINYINT,
    redirect_layout     VARCHAR(32),
    status              VARCHAR(64),
    support_template    TINYINT,
    show_notification   TINYINT,
    sort_order          INT4,
    create_time         DATE,
    create_by           VARCHAR(32),
    create_name         VARCHAR(64),
    update_time         DATE,
    update_by           VARCHAR(32),
    update_name         VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.message_channel_definition IS '消息发送渠道定义';
COMMENT ON COLUMN public.message_channel_definition.id IS '主键';
COMMENT ON COLUMN public.message_channel_definition.tenant_code IS '租户标识';
COMMENT ON COLUMN public.message_channel_definition.template_id IS '消息模板编号';
COMMENT ON COLUMN public.message_channel_definition.business_key IS '业务标识';
COMMENT ON COLUMN public.message_channel_definition.business_name IS '业务类型名称';
COMMENT ON COLUMN public.message_channel_definition.message_title IS '消息标题';
COMMENT ON COLUMN public.message_channel_definition.channel IS '消息发送渠道';
COMMENT ON COLUMN public.message_channel_definition.channel_description IS '消息发送渠道描述';
COMMENT ON COLUMN public.message_channel_definition.channel_id IS '消息发送渠道标识';
COMMENT ON COLUMN public.message_channel_definition.channel_template_id IS '消息发送渠道模板编号';
COMMENT ON COLUMN public.message_channel_definition.content_type IS '消息内容模板类型';
COMMENT ON COLUMN public.message_channel_definition.content IS '消息模板内容';
COMMENT ON COLUMN public.message_channel_definition.customize_content IS '消息渠道自定义模板';
COMMENT ON COLUMN public.message_channel_definition.voice IS '是否播放声音';
COMMENT ON COLUMN public.message_channel_definition.redirect_layout IS '重定向链接样式';
COMMENT ON COLUMN public.message_channel_definition.status IS '消息发送渠道状态';
COMMENT ON COLUMN public.message_channel_definition.support_template IS '消息渠道是否支持模板';
COMMENT ON COLUMN public.message_channel_definition.show_notification IS '是否展示消息提醒';
COMMENT ON COLUMN public.message_channel_definition.sort_order IS '排序值';
COMMENT ON COLUMN public.message_channel_definition.create_time IS '创建时间';
COMMENT ON COLUMN public.message_channel_definition.create_by IS '创建人编号';
COMMENT ON COLUMN public.message_channel_definition.create_name IS '创建人姓名';
COMMENT ON COLUMN public.message_channel_definition.update_time IS '更新时间';
COMMENT ON COLUMN public.message_channel_definition.update_by IS '更新人编号';
COMMENT ON COLUMN public.message_channel_definition.update_name IS '更新人姓名';

CREATE TABLE public.message_param_definition
(
    id                    VARCHAR(32) NOT NULL,
    tenant_code           VARCHAR(64),
    template_id           VARCHAR(32),
    channel_definition_id VARCHAR(32),
    pre_definition        TINYINT,
    param_name            VARCHAR(64),
    param_description     VARCHAR(64),
    mapped_param_name     VARCHAR(64),
    sort_order            INT4,
    create_time           DATE,
    create_by             VARCHAR(32),
    create_name           VARCHAR(64),
    update_time           DATE,
    update_by             VARCHAR(32),
    update_name           VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.message_param_definition IS '消息参数定义';
COMMENT ON COLUMN public.message_param_definition.id IS '主键';
COMMENT ON COLUMN public.message_param_definition.tenant_code IS '租户标识';
COMMENT ON COLUMN public.message_param_definition.template_id IS '消息模板编号';
COMMENT ON COLUMN public.message_param_definition.channel_definition_id IS '消息发送渠道定义编号';
COMMENT ON COLUMN public.message_param_definition.pre_definition IS '是否预定义参数';
COMMENT ON COLUMN public.message_param_definition.param_name IS '参数名称';
COMMENT ON COLUMN public.message_param_definition.param_description IS '参数描述';
COMMENT ON COLUMN public.message_param_definition.mapped_param_name IS '映射参数名称';
COMMENT ON COLUMN public.message_param_definition.sort_order IS '排序值';
COMMENT ON COLUMN public.message_param_definition.create_time IS '创建时间';
COMMENT ON COLUMN public.message_param_definition.create_by IS '创建人编号';
COMMENT ON COLUMN public.message_param_definition.create_name IS '创建人姓名';
COMMENT ON COLUMN public.message_param_definition.update_time IS '更新时间';
COMMENT ON COLUMN public.message_param_definition.update_by IS '更新人编号';
COMMENT ON COLUMN public.message_param_definition.update_name IS '更新人姓名';

CREATE TABLE public.message_receiver_definition
(
    id                  VARCHAR(32) NOT NULL,
    tenant_code         VARCHAR(64),
    template_id         VARCHAR(32),
    receiver_type       VARCHAR(64),
    receiver_param_name VARCHAR(64),
    sort_order          INT4,
    create_time         DATE,
    create_by           VARCHAR(32),
    create_name         VARCHAR(64),
    update_time         DATE,
    update_by           VARCHAR(32),
    update_name         VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.message_receiver_definition IS '消息接收者定义';
COMMENT ON COLUMN public.message_receiver_definition.id IS '主键';
COMMENT ON COLUMN public.message_receiver_definition.tenant_code IS '租户标识';
COMMENT ON COLUMN public.message_receiver_definition.template_id IS '消息模板编号';
COMMENT ON COLUMN public.message_receiver_definition.receiver_type IS '接收者类型';
COMMENT ON COLUMN public.message_receiver_definition.receiver_param_name IS '接收者参数名称';
COMMENT ON COLUMN public.message_receiver_definition.sort_order IS '排序值';
COMMENT ON COLUMN public.message_receiver_definition.create_time IS '创建时间';
COMMENT ON COLUMN public.message_receiver_definition.create_by IS '创建人编号';
COMMENT ON COLUMN public.message_receiver_definition.create_name IS '创建人姓名';
COMMENT ON COLUMN public.message_receiver_definition.update_time IS '更新时间';
COMMENT ON COLUMN public.message_receiver_definition.update_by IS '更新人编号';
COMMENT ON COLUMN public.message_receiver_definition.update_name IS '更新人姓名';

CREATE TABLE public.message_redirect_definition
(
    id                    VARCHAR(32) NOT NULL,
    tenant_code           VARCHAR(64),
    template_id           VARCHAR(32),
    channel_definition_id VARCHAR(32),
    redirect_to           VARCHAR(256),
    redirect_name         VARCHAR(64),
    sort_order            INT4,
    create_time           DATE,
    create_by             VARCHAR(32),
    create_name           VARCHAR(64),
    update_time           DATE,
    update_by             VARCHAR(32),
    update_name           VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.message_redirect_definition IS '消息重定向定义';
COMMENT ON COLUMN public.message_redirect_definition.id IS '主键';
COMMENT ON COLUMN public.message_redirect_definition.tenant_code IS '租户标识';
COMMENT ON COLUMN public.message_redirect_definition.template_id IS '消息模板编号';
COMMENT ON COLUMN public.message_redirect_definition.channel_definition_id IS '消息发送渠道定义编号';
COMMENT ON COLUMN public.message_redirect_definition.redirect_to IS '重定向地址';
COMMENT ON COLUMN public.message_redirect_definition.redirect_name IS '重定向名称';
COMMENT ON COLUMN public.message_redirect_definition.sort_order IS '排序值';
COMMENT ON COLUMN public.message_redirect_definition.create_time IS '创建时间';
COMMENT ON COLUMN public.message_redirect_definition.create_by IS '创建人编号';
COMMENT ON COLUMN public.message_redirect_definition.create_name IS '创建人姓名';
COMMENT ON COLUMN public.message_redirect_definition.update_time IS '更新时间';
COMMENT ON COLUMN public.message_redirect_definition.update_by IS '更新人编号';
COMMENT ON COLUMN public.message_redirect_definition.update_name IS '更新人姓名';

CREATE TABLE public.message_subscriber
(
    id          VARCHAR(32) NOT NULL,
    tenant_code VARCHAR(64),
    name        VARCHAR(64) DEFAULT '1',
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.message_subscriber IS '消息订阅方';
COMMENT ON COLUMN public.message_subscriber.id IS '主键';
COMMENT ON COLUMN public.message_subscriber.tenant_code IS '租户标识';
COMMENT ON COLUMN public.message_subscriber.name IS '订阅方名称';
COMMENT ON COLUMN public.message_subscriber.create_time IS '创建时间';
COMMENT ON COLUMN public.message_subscriber.create_by IS '创建人编号';
COMMENT ON COLUMN public.message_subscriber.create_name IS '创建人姓名';
COMMENT ON COLUMN public.message_subscriber.update_time IS '更新时间';
COMMENT ON COLUMN public.message_subscriber.update_by IS '更新人编号';
COMMENT ON COLUMN public.message_subscriber.update_name IS '更新人姓名';

CREATE TABLE public.message_subscriber_data_definition
(
    id            VARCHAR(32) NOT NULL,
    tenant_code   VARCHAR(64),
    subscriber_id VARCHAR(64),
    send_url_id   VARCHAR(64),
    field_name    VARCHAR(64),
    create_time   DATE,
    create_by     VARCHAR(32),
    create_name   VARCHAR(64),
    update_time   DATE,
    update_by     VARCHAR(32),
    update_name   VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.message_subscriber_data_definition IS '消息订阅方数据结构';
COMMENT ON COLUMN public.message_subscriber_data_definition.id IS '主键';
COMMENT ON COLUMN public.message_subscriber_data_definition.tenant_code IS '租户标识';
COMMENT ON COLUMN public.message_subscriber_data_definition.subscriber_id IS '订阅方编号';
COMMENT ON COLUMN public.message_subscriber_data_definition.send_url_id IS '推送地址编号';
COMMENT ON COLUMN public.message_subscriber_data_definition.field_name IS '订阅方数据字段名';
COMMENT ON COLUMN public.message_subscriber_data_definition.create_time IS '创建时间';
COMMENT ON COLUMN public.message_subscriber_data_definition.create_by IS '创建人编号';
COMMENT ON COLUMN public.message_subscriber_data_definition.create_name IS '创建人姓名';
COMMENT ON COLUMN public.message_subscriber_data_definition.update_time IS '更新时间';
COMMENT ON COLUMN public.message_subscriber_data_definition.update_by IS '更新人编号';
COMMENT ON COLUMN public.message_subscriber_data_definition.update_name IS '更新人姓名';

CREATE TABLE public.message_subscriber_send_url
(
    id            VARCHAR(32) NOT NULL,
    tenant_code   VARCHAR(64),
    subscriber_id VARCHAR(64),
    send_url      VARCHAR(512),
    api_key       VARCHAR(64),
    create_time   DATE,
    create_by     VARCHAR(32),
    create_name   VARCHAR(64),
    update_time   DATE,
    update_by     VARCHAR(32),
    update_name   VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.message_subscriber_send_url IS '消息订阅方';
COMMENT ON COLUMN public.message_subscriber_send_url.id IS '主键';
COMMENT ON COLUMN public.message_subscriber_send_url.tenant_code IS '租户标识';
COMMENT ON COLUMN public.message_subscriber_send_url.subscriber_id IS '订阅方编号';
COMMENT ON COLUMN public.message_subscriber_send_url.send_url IS '推送地址';
COMMENT ON COLUMN public.message_subscriber_send_url.api_key IS 'API Key';
COMMENT ON COLUMN public.message_subscriber_send_url.create_time IS '创建时间';
COMMENT ON COLUMN public.message_subscriber_send_url.create_by IS '创建人编号';
COMMENT ON COLUMN public.message_subscriber_send_url.create_name IS '创建人姓名';
COMMENT ON COLUMN public.message_subscriber_send_url.update_time IS '更新时间';
COMMENT ON COLUMN public.message_subscriber_send_url.update_by IS '更新人编号';
COMMENT ON COLUMN public.message_subscriber_send_url.update_name IS '更新人姓名';

CREATE TABLE public.message_template
(
    id                     VARCHAR(32) NOT NULL,
    tenant_code            VARCHAR(64),
    business_name          VARCHAR(64),
    business_key           VARCHAR(32),
    message_template_key   VARCHAR(32),
    message_template_title VARCHAR(256),
    configured             TINYINT,
    configurable           TINYINT,
    sort_order             INT4,
    del_flag               TINYINT,
    create_time            DATE,
    create_by              VARCHAR(32),
    create_name            VARCHAR(64),
    update_time            DATE,
    update_by              VARCHAR(32),
    update_name            VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.message_template IS '消息模板';
COMMENT ON COLUMN public.message_template.id IS '主键';
COMMENT ON COLUMN public.message_template.tenant_code IS '租户标识';
COMMENT ON COLUMN public.message_template.business_name IS '业务名称';
COMMENT ON COLUMN public.message_template.business_key IS '业务名称';
COMMENT ON COLUMN public.message_template.message_template_key IS '消息标识';
COMMENT ON COLUMN public.message_template.message_template_title IS '消息标题';
COMMENT ON COLUMN public.message_template.configured IS '是否已配置';
COMMENT ON COLUMN public.message_template.configurable IS '是否可配置';
COMMENT ON COLUMN public.message_template.sort_order IS '排序值';
COMMENT ON COLUMN public.message_template.del_flag IS '是否已删除';
COMMENT ON COLUMN public.message_template.create_time IS '创建时间';
COMMENT ON COLUMN public.message_template.create_by IS '创建人编号';
COMMENT ON COLUMN public.message_template.create_name IS '创建人姓名';
COMMENT ON COLUMN public.message_template.update_time IS '更新时间';
COMMENT ON COLUMN public.message_template.update_by IS '更新人编号';
COMMENT ON COLUMN public.message_template.update_name IS '更新人姓名';

CREATE TABLE public.oidc_client
(
    id          VARCHAR(32) NOT NULL,
    secret      VARCHAR(128),
    name        VARCHAR(64),
    logo        VARCHAR(512),
    website     VARCHAR(512),
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.oidc_client IS 'oidc客户端';
COMMENT ON COLUMN public.oidc_client.id IS '主键';
COMMENT ON COLUMN public.oidc_client.secret IS '客户端secret';
COMMENT ON COLUMN public.oidc_client.name IS '客户端名称';
COMMENT ON COLUMN public.oidc_client.logo IS '客户端logo';
COMMENT ON COLUMN public.oidc_client.website IS '客户端网址';
COMMENT ON COLUMN public.oidc_client.create_time IS '创建时间';
COMMENT ON COLUMN public.oidc_client.create_by IS '创建人编号';
COMMENT ON COLUMN public.oidc_client.create_name IS '创建人姓名';
COMMENT ON COLUMN public.oidc_client.update_time IS '更新时间';
COMMENT ON COLUMN public.oidc_client.update_by IS '更新人编号';
COMMENT ON COLUMN public.oidc_client.update_name IS '更新人姓名';

CREATE TABLE public.operation_log
(
    id               VARCHAR(32) NOT NULL,
    domain_code      VARCHAR(64),
    tenant_code      VARCHAR(64),
    app_code         VARCHAR(64),
    module           VARCHAR(256),
    operation        VARCHAR(256),
    method           VARCHAR(256),
    request_method   VARCHAR(64),
    request_url      VARCHAR(512),
    request_param    TEXT,
    response_body    TEXT,
    operate_user_id  VARCHAR(32),
    operate_username VARCHAR(64),
    ip               VARCHAR(64),
    location         VARCHAR(256),
    success          TINYINT,
    message          TEXT,
    operation_time   DATE,
    create_time      DATE,
    create_by        VARCHAR(32),
    create_name      VARCHAR(64),
    update_time      DATE,
    update_by        VARCHAR(32),
    update_name      VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.operation_log IS '操作日志';
COMMENT ON COLUMN public.operation_log.id IS '主键';
COMMENT ON COLUMN public.operation_log.domain_code IS '域标识';
COMMENT ON COLUMN public.operation_log.tenant_code IS '租户标识';
COMMENT ON COLUMN public.operation_log.app_code IS '应用标识';
COMMENT ON COLUMN public.operation_log.module IS '业务模块';
COMMENT ON COLUMN public.operation_log.operation IS '操作内容';
COMMENT ON COLUMN public.operation_log.method IS '方法名';
COMMENT ON COLUMN public.operation_log.request_method IS '请求方法';
COMMENT ON COLUMN public.operation_log.request_url IS '请求地址';
COMMENT ON COLUMN public.operation_log.request_param IS '请求参数';
COMMENT ON COLUMN public.operation_log.response_body IS '响应体';
COMMENT ON COLUMN public.operation_log.operate_user_id IS '操作用户编号';
COMMENT ON COLUMN public.operation_log.operate_username IS '操作用户账户';
COMMENT ON COLUMN public.operation_log.ip IS '客户端ip';
COMMENT ON COLUMN public.operation_log.location IS '客户端地区';
COMMENT ON COLUMN public.operation_log.success IS '是否成功';
COMMENT ON COLUMN public.operation_log.message IS '返回信息';
COMMENT ON COLUMN public.operation_log.operation_time IS '操作时间';
COMMENT ON COLUMN public.operation_log.create_time IS '创建时间';
COMMENT ON COLUMN public.operation_log.create_by IS '创建人编号';
COMMENT ON COLUMN public.operation_log.create_name IS '创建人姓名';
COMMENT ON COLUMN public.operation_log.update_time IS '更新时间';
COMMENT ON COLUMN public.operation_log.update_by IS '更新人编号';
COMMENT ON COLUMN public.operation_log.update_name IS '更新人姓名';

CREATE TABLE public.oss_file
(
    id          VARCHAR(32) NOT NULL,
    domain_code VARCHAR(64),
    tenant_code VARCHAR(64),
    app_code    VARCHAR(64),
    origin_name VARCHAR(256),
    file_size   VARCHAR(64),
    suffix      VARCHAR(32),
    url         VARCHAR(256),
    dir_name    VARCHAR(64),
    path        VARCHAR(256),
    md5         VARCHAR(256),
    use_count   VARCHAR(64) DEFAULT '0',
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.oss_file IS '文件';
COMMENT ON COLUMN public.oss_file.id IS '主键';
COMMENT ON COLUMN public.oss_file.domain_code IS '域标识';
COMMENT ON COLUMN public.oss_file.tenant_code IS '租户标识';
COMMENT ON COLUMN public.oss_file.app_code IS '应用标识';
COMMENT ON COLUMN public.oss_file.origin_name IS '原始文件名称';
COMMENT ON COLUMN public.oss_file.file_size IS '文件大小';
COMMENT ON COLUMN public.oss_file.suffix IS '文件后缀';
COMMENT ON COLUMN public.oss_file.url IS '文件访问相对路径';
COMMENT ON COLUMN public.oss_file.dir_name IS '文件服务器目录';
COMMENT ON COLUMN public.oss_file.path IS '文件服务器路径';
COMMENT ON COLUMN public.oss_file.md5 IS '文件md5';
COMMENT ON COLUMN public.oss_file.use_count IS '文件使用次数';
COMMENT ON COLUMN public.oss_file.create_time IS '创建时间';
COMMENT ON COLUMN public.oss_file.create_by IS '创建人编号';
COMMENT ON COLUMN public.oss_file.create_name IS '创建人姓名';
COMMENT ON COLUMN public.oss_file.update_time IS '更新时间';
COMMENT ON COLUMN public.oss_file.update_by IS '更新人编号';
COMMENT ON COLUMN public.oss_file.update_name IS '更新人姓名';

CREATE TABLE public.pay_record
(
    id                VARCHAR(32) NOT NULL,
    request_id        VARCHAR(64),
    amount            NUMERIC(22, 2),
    supplier_id       VARCHAR(32),
    supplier_type     VARCHAR(64),
    pay_type          VARCHAR(64),
    supplier_trade_id VARCHAR(64),
    channel_trade_id  VARCHAR(64),
    trade_time        DATE,
    cause             VARCHAR(256),
    status            VARCHAR(64),
    props             VARCHAR(512),
    create_time       DATE,
    create_by         VARCHAR(32),
    create_name       VARCHAR(64),
    update_time       DATE,
    update_by         VARCHAR(32),
    update_name       VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.pay_record IS '支付记录';
COMMENT ON COLUMN public.pay_record.id IS '主键';
COMMENT ON COLUMN public.pay_record.request_id IS '请求编号';
COMMENT ON COLUMN public.pay_record.amount IS '支付金额';
COMMENT ON COLUMN public.pay_record.supplier_id IS '支付平台编号';
COMMENT ON COLUMN public.pay_record.supplier_type IS '支付平台类型';
COMMENT ON COLUMN public.pay_record.pay_type IS '支付类型';
COMMENT ON COLUMN public.pay_record.supplier_trade_id IS '支付平台交易编号';
COMMENT ON COLUMN public.pay_record.channel_trade_id IS '支付渠道交易编号';
COMMENT ON COLUMN public.pay_record.trade_time IS '交易时间';
COMMENT ON COLUMN public.pay_record.cause IS '交易详情';
COMMENT ON COLUMN public.pay_record.status IS '交易状态';
COMMENT ON COLUMN public.pay_record.props IS '交易参数';
COMMENT ON COLUMN public.pay_record.create_time IS '创建时间';
COMMENT ON COLUMN public.pay_record.create_by IS '创建人编号';
COMMENT ON COLUMN public.pay_record.create_name IS '创建人姓名';
COMMENT ON COLUMN public.pay_record.update_time IS '更新时间';
COMMENT ON COLUMN public.pay_record.update_by IS '更新人编号';
COMMENT ON COLUMN public.pay_record.update_name IS '更新人姓名';

CREATE TABLE public.post
(
    id          VARCHAR(32) NOT NULL,
    name        VARCHAR(64),
    tenant_code VARCHAR(64),
    remark      VARCHAR(500),
    sort_order  INT4,
    del_flag    TINYINT,
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.post IS '岗位';
COMMENT ON COLUMN public.post.id IS '主键';
COMMENT ON COLUMN public.post.name IS '岗位名称';
COMMENT ON COLUMN public.post.tenant_code IS '租户标识';
COMMENT ON COLUMN public.post.remark IS '备注';
COMMENT ON COLUMN public.post.sort_order IS '排序值';
COMMENT ON COLUMN public.post.del_flag IS '是否已删除';
COMMENT ON COLUMN public.post.create_time IS '创建时间';
COMMENT ON COLUMN public.post.create_by IS '创建人编号';
COMMENT ON COLUMN public.post.create_name IS '创建人姓名';
COMMENT ON COLUMN public.post.update_time IS '更新时间';
COMMENT ON COLUMN public.post.update_by IS '更新人编号';
COMMENT ON COLUMN public.post.update_name IS '更新人姓名';

CREATE TABLE public.role
(
    id              VARCHAR(32) NOT NULL,
    name            VARCHAR(64),
    domain_code     VARCHAR(64),
    tenant_code     VARCHAR(64),
    app_code        VARCHAR(64),
    data_scope_type VARCHAR(64),
    remark          VARCHAR(500),
    sort_order      INT4,
    del_flag        TINYINT,
    create_time     DATE,
    create_by       VARCHAR(32),
    create_name     VARCHAR(64),
    update_time     DATE,
    update_by       VARCHAR(32),
    update_name     VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.role IS '角色';
COMMENT ON COLUMN public.role.id IS '主键';
COMMENT ON COLUMN public.role.name IS '角色名称';
COMMENT ON COLUMN public.role.domain_code IS '域标识';
COMMENT ON COLUMN public.role.tenant_code IS '租户标识';
COMMENT ON COLUMN public.role.app_code IS '应用标识';
COMMENT ON COLUMN public.role.data_scope_type IS '数据权限类型';
COMMENT ON COLUMN public.role.remark IS '备注';
COMMENT ON COLUMN public.role.sort_order IS '排序值';
COMMENT ON COLUMN public.role.del_flag IS '是否已删除';
COMMENT ON COLUMN public.role.create_time IS '创建时间';
COMMENT ON COLUMN public.role.create_by IS '创建人编号';
COMMENT ON COLUMN public.role.create_name IS '创建人姓名';
COMMENT ON COLUMN public.role.update_time IS '更新时间';
COMMENT ON COLUMN public.role.update_by IS '更新人编号';
COMMENT ON COLUMN public.role.update_name IS '更新人姓名';

CREATE TABLE public.role_data_scope
(
    id              VARCHAR(32) NOT NULL,
    role_id         VARCHAR(32),
    tenant_code     VARCHAR(64),
    data_scope_code VARCHAR(32),
    create_time     DATE,
    create_by       VARCHAR(32),
    create_name     VARCHAR(64),
    update_time     DATE,
    update_by       VARCHAR(32),
    update_name     VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.role_data_scope IS '角色数据权限';
COMMENT ON COLUMN public.role_data_scope.id IS '编号';
COMMENT ON COLUMN public.role_data_scope.role_id IS '角色编号';
COMMENT ON COLUMN public.role_data_scope.tenant_code IS '租户标识';
COMMENT ON COLUMN public.role_data_scope.data_scope_code IS '权限码';
COMMENT ON COLUMN public.role_data_scope.create_time IS '创建时间';
COMMENT ON COLUMN public.role_data_scope.create_by IS '创建人编号';
COMMENT ON COLUMN public.role_data_scope.create_name IS '创建人姓名';
COMMENT ON COLUMN public.role_data_scope.update_time IS '更新时间';
COMMENT ON COLUMN public.role_data_scope.update_by IS '更新人编号';
COMMENT ON COLUMN public.role_data_scope.update_name IS '更新人姓名';

CREATE TABLE public.role_menu
(
    id          VARCHAR(32) NOT NULL,
    role_id     VARCHAR(32),
    domain_code VARCHAR(64),
    tenant_code VARCHAR(64),
    app_code    VARCHAR(64),
    menu_id     VARCHAR(32),
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.role_menu IS '角色菜单';
COMMENT ON COLUMN public.role_menu.id IS '主键';
COMMENT ON COLUMN public.role_menu.role_id IS '角色编号';
COMMENT ON COLUMN public.role_menu.domain_code IS '域标识';
COMMENT ON COLUMN public.role_menu.tenant_code IS '租户标识';
COMMENT ON COLUMN public.role_menu.app_code IS '应用标识';
COMMENT ON COLUMN public.role_menu.menu_id IS '菜单编号';
COMMENT ON COLUMN public.role_menu.create_time IS '创建时间';
COMMENT ON COLUMN public.role_menu.create_by IS '创建人编号';
COMMENT ON COLUMN public.role_menu.create_name IS '创建人姓名';
COMMENT ON COLUMN public.role_menu.update_time IS '更新时间';
COMMENT ON COLUMN public.role_menu.update_by IS '更新人编号';
COMMENT ON COLUMN public.role_menu.update_name IS '更新人姓名';

CREATE TABLE public.schedule
(
    id                  VARCHAR(32) NOT NULL,
    business_id         VARCHAR(128),
    title               VARCHAR(128),
    type                VARCHAR(64),
    status              VARCHAR(64),
    tenant_code         VARCHAR(64),
    remark              VARCHAR(500),
    redirect_url        VARCHAR(512),
    all_day             TINYINT,
    estimate_end_time   DATE,
    estimate_start_time DATE,
    create_by           CHAR(32),
    create_name         VARCHAR(64),
    create_time         DATE,
    update_by           CHAR(32),
    update_name         VARCHAR(64),
    update_time         DATE,
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.schedule IS '日程';
COMMENT ON COLUMN public.schedule.id IS '主键';
COMMENT ON COLUMN public.schedule.business_id IS '业务编号';
COMMENT ON COLUMN public.schedule.title IS '标题';
COMMENT ON COLUMN public.schedule.type IS '类型';
COMMENT ON COLUMN public.schedule.status IS '状态';
COMMENT ON COLUMN public.schedule.remark IS '说明';
COMMENT ON COLUMN public.schedule.redirect_url IS '跳转地址';
COMMENT ON COLUMN public.schedule.all_day IS '是否全天';
COMMENT ON COLUMN public.schedule.estimate_end_time IS '计划结束时间';
COMMENT ON COLUMN public.schedule.estimate_start_time IS '计划开始时间';
COMMENT ON COLUMN public.schedule.create_by IS '创建人编号';
COMMENT ON COLUMN public.schedule.create_name IS '创建人姓名';
COMMENT ON COLUMN public.schedule.create_time IS '创建时间';
COMMENT ON COLUMN public.schedule.update_by IS '更新人编号';
COMMENT ON COLUMN public.schedule.update_name IS '更新人姓名';
COMMENT ON COLUMN public.schedule.update_time IS '更新时间';

CREATE TABLE public.schedule_executor
(
    id            VARCHAR(32) NOT NULL,
    tenant_code   VARCHAR(64),
    schedule_id   VARCHAR(32),
    executor_id   VARCHAR(32),
    executor_type VARCHAR(64),
    create_time   DATE,
    create_by     VARCHAR(32),
    create_name   VARCHAR(64),
    update_time   DATE,
    update_by     VARCHAR(32),
    update_name   VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.schedule_executor IS '日程执行者';
COMMENT ON COLUMN public.schedule_executor.id IS '主键';
COMMENT ON COLUMN public.schedule_executor.tenant_code IS '租户标识';
COMMENT ON COLUMN public.schedule_executor.schedule_id IS '日程编号';
COMMENT ON COLUMN public.schedule_executor.executor_id IS '执行者编号';
COMMENT ON COLUMN public.schedule_executor.executor_type IS '执行者类型';
COMMENT ON COLUMN public.schedule_executor.create_time IS '创建时间';
COMMENT ON COLUMN public.schedule_executor.create_by IS '创建人编号';
COMMENT ON COLUMN public.schedule_executor.create_name IS '创建人姓名';
COMMENT ON COLUMN public.schedule_executor.update_time IS '更新时间';
COMMENT ON COLUMN public.schedule_executor.update_by IS '更新人编号';
COMMENT ON COLUMN public.schedule_executor.update_name IS '更新人姓名';

CREATE TABLE public.sms
(
    id               VARCHAR(32) NOT NULL,
    code             VARCHAR(64),
    name             VARCHAR(64),
    type             VARCHAR(64),
    app_key          VARCHAR(256),
    app_secret       VARCHAR(256),
    extend_code      VARCHAR(256),
    endpoint         VARCHAR(256),
    signature        VARCHAR(256),
    support_template TINYINT,
    create_time      DATE,
    create_by        VARCHAR(32),
    create_name      VARCHAR(64),
    update_time      DATE,
    update_by        VARCHAR(32),
    update_name      VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.sms IS '短信平台';
COMMENT ON COLUMN public.sms.id IS '主键';
COMMENT ON COLUMN public.sms.code IS '短信平台标识';
COMMENT ON COLUMN public.sms.name IS '短信平台名称';
COMMENT ON COLUMN public.sms.type IS '短信平台类型';
COMMENT ON COLUMN public.sms.app_key IS '短信平台AppKey';
COMMENT ON COLUMN public.sms.app_secret IS '短信平台AppSecret';
COMMENT ON COLUMN public.sms.extend_code IS '短信平台ExtendCode';
COMMENT ON COLUMN public.sms.endpoint IS '短信平台地址';
COMMENT ON COLUMN public.sms.signature IS '短信签名';
COMMENT ON COLUMN public.sms.support_template IS '是否支持模板';
COMMENT ON COLUMN public.sms.create_time IS '创建时间';
COMMENT ON COLUMN public.sms.create_by IS '创建人编号';
COMMENT ON COLUMN public.sms.create_name IS '创建人姓名';
COMMENT ON COLUMN public.sms.update_time IS '更新时间';
COMMENT ON COLUMN public.sms.update_by IS '更新人编号';
COMMENT ON COLUMN public.sms.update_name IS '更新人姓名';

CREATE TABLE public.sql_api_definition
(
    file_path    VARCHAR(512) NOT NULL,
    file_content MEDIUMTEXT,
    PRIMARY KEY (file_path)
);

COMMENT ON TABLE public.sql_api_definition IS '';

CREATE TABLE public.sql_api_definition_backup
(
    id          VARCHAR(32) NOT NULL,
    create_date BIGINT      NOT NULL,
    tag         VARCHAR(32),
    type        VARCHAR(32),
    name        VARCHAR(64),
    content BYTEA,
    create_by   VARCHAR(64),
    PRIMARY KEY (id, create_date)
);

COMMENT ON TABLE public.sql_api_definition_backup IS '';
COMMENT ON COLUMN public.sql_api_definition_backup.id IS '原对象id';
COMMENT ON COLUMN public.sql_api_definition_backup.create_date IS '备份时间';
COMMENT ON COLUMN public.sql_api_definition_backup.tag IS '标签';
COMMENT ON COLUMN public.sql_api_definition_backup.type IS '类型';
COMMENT ON COLUMN public.sql_api_definition_backup.name IS '原名称';
COMMENT ON COLUMN public.sql_api_definition_backup.content IS '备份内容';
COMMENT ON COLUMN public.sql_api_definition_backup.create_by IS '操作人';

CREATE TABLE public.sys_config
(
    id             VARCHAR(32) NOT NULL,
    tenant_code    VARCHAR(64),
    type           VARCHAR(64),
    data_id        VARCHAR(32),
    parent_key     VARCHAR(64),
    property_key   VARCHAR(128),
    property_value LONGTEXT,
    create_time    DATE,
    create_by      VARCHAR(32),
    create_name    VARCHAR(64),
    update_time    DATE,
    update_by      VARCHAR(32),
    update_name    VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.sys_config IS '平台配置';
COMMENT ON COLUMN public.sys_config.id IS '主键';
COMMENT ON COLUMN public.sys_config.tenant_code IS '租户标识';
COMMENT ON COLUMN public.sys_config.type IS '配置类型';
COMMENT ON COLUMN public.sys_config.data_id IS '配置编号';
COMMENT ON COLUMN public.sys_config.parent_key IS '父配置项';
COMMENT ON COLUMN public.sys_config.property_key IS '配置项';
COMMENT ON COLUMN public.sys_config.property_value IS '配置内容';
COMMENT ON COLUMN public.sys_config.create_time IS '创建时间';
COMMENT ON COLUMN public.sys_config.create_by IS '创建人编号';
COMMENT ON COLUMN public.sys_config.create_name IS '创建人姓名';
COMMENT ON COLUMN public.sys_config.update_time IS '更新时间';
COMMENT ON COLUMN public.sys_config.update_by IS '更新人编号';
COMMENT ON COLUMN public.sys_config.update_name IS '更新人姓名';

CREATE TABLE public.sys_user
(
    id                   VARCHAR(32) NOT NULL,
    username             VARCHAR(64),
    password             VARCHAR(128),
    password_modify_time DATE,
    nickname             VARCHAR(64),
    name                 VARCHAR(64),
    avatar               VARCHAR(256),
    birth_day            DATE,
    phone                VARCHAR(64),
    email                VARCHAR(64),
    identity_no          VARCHAR(64),
    sex                  VARCHAR(64) DEFAULT 'MALE',
    locked               TINYINT,
    lock_type            VARCHAR(64) DEFAULT 'NO_LOCK',
    admin_user           TINYINT,
    platform_user        TINYINT,
    del_flag             TINYINT,
    create_time          DATE,
    create_by            VARCHAR(32),
    create_name          VARCHAR(64),
    update_time          DATE,
    update_by            VARCHAR(32),
    update_name          VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.sys_user IS '用户';
COMMENT ON COLUMN public.sys_user.id IS '主键';
COMMENT ON COLUMN public.sys_user.username IS '账户';
COMMENT ON COLUMN public.sys_user.password IS '账户密码';
COMMENT ON COLUMN public.sys_user.password_modify_time IS '密码修改时间';
COMMENT ON COLUMN public.sys_user.nickname IS '昵称';
COMMENT ON COLUMN public.sys_user.name IS '名称';
COMMENT ON COLUMN public.sys_user.avatar IS '头像';
COMMENT ON COLUMN public.sys_user.birth_day IS '出生日期';
COMMENT ON COLUMN public.sys_user.phone IS '联系电话';
COMMENT ON COLUMN public.sys_user.email IS '邮件地址';
COMMENT ON COLUMN public.sys_user.identity_no IS '身份证号';
COMMENT ON COLUMN public.sys_user.sex IS '性别';
COMMENT ON COLUMN public.sys_user.locked IS '是否被锁定';
COMMENT ON COLUMN public.sys_user.lock_type IS '锁定类型';
COMMENT ON COLUMN public.sys_user.admin_user IS '是否管理员';
COMMENT ON COLUMN public.sys_user.platform_user IS '是否平台用户';
COMMENT ON COLUMN public.sys_user.del_flag IS '是否已删除';
COMMENT ON COLUMN public.sys_user.create_time IS '创建时间';
COMMENT ON COLUMN public.sys_user.create_by IS '创建人编号';
COMMENT ON COLUMN public.sys_user.create_name IS '创建人姓名';
COMMENT ON COLUMN public.sys_user.update_time IS '更新时间';
COMMENT ON COLUMN public.sys_user.update_by IS '更新人编号';
COMMENT ON COLUMN public.sys_user.update_name IS '更新人姓名';

CREATE TABLE public.system_log
(
    id              VARCHAR(32) NOT NULL,
    tenant_code     VARCHAR(64),
    trace_id        VARCHAR(32),
    package_name    VARCHAR(256),
    method          VARCHAR(256),
    message         TEXT,
    exception_stack TEXT,
    execute_time    DATE,
    create_time     DATE,
    create_by       VARCHAR(32),
    create_name     VARCHAR(64),
    update_time     DATE,
    update_by       VARCHAR(32),
    update_name     VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.system_log IS '系统日志';
COMMENT ON COLUMN public.system_log.id IS '主键';
COMMENT ON COLUMN public.system_log.tenant_code IS '租户标识';
COMMENT ON COLUMN public.system_log.trace_id IS '追踪编号';
COMMENT ON COLUMN public.system_log.package_name IS '包名';
COMMENT ON COLUMN public.system_log.method IS '方法名';
COMMENT ON COLUMN public.system_log.message IS '返回信息';
COMMENT ON COLUMN public.system_log.exception_stack IS '异常堆栈';
COMMENT ON COLUMN public.system_log.execute_time IS '执行时间';
COMMENT ON COLUMN public.system_log.create_time IS '创建时间';
COMMENT ON COLUMN public.system_log.create_by IS '创建人编号';
COMMENT ON COLUMN public.system_log.create_name IS '创建人姓名';
COMMENT ON COLUMN public.system_log.update_time IS '更新时间';
COMMENT ON COLUMN public.system_log.update_by IS '更新人编号';
COMMENT ON COLUMN public.system_log.update_name IS '更新人姓名';

CREATE TABLE public.tenant
(
    id            VARCHAR(32) NOT NULL,
    name          VARCHAR(64),
    code          VARCHAR(64),
    type          VARCHAR(64),
    tenant_domain VARCHAR(256),
    logo          VARCHAR(256),
    icon          VARCHAR(256),
    element       VARCHAR(64) DEFAULT 'icon_name',
    arrange       VARCHAR(64) DEFAULT 'left_icon_right_name',
    alignment     VARCHAR(64) DEFAULT 'left',
    icon_open     VARCHAR(512),
    icon_close    VARCHAR(512),
    menu_name     VARCHAR(64),
    favicon       VARCHAR(512),
    favicon_name  VARCHAR(64),
    start_date    DATE,
    end_date      DATE,
    status        VARCHAR(64),
    remark        VARCHAR(500),
    sort_order    INT4,
    del_flag      TINYINT,
    create_time   DATE,
    create_by     VARCHAR(32),
    create_name   VARCHAR(64),
    update_time   DATE,
    update_by     VARCHAR(32),
    update_name   VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.tenant IS '租户';
COMMENT ON COLUMN public.tenant.id IS '编号/租户编号';
COMMENT ON COLUMN public.tenant.name IS '租户名称';
COMMENT ON COLUMN public.tenant.code IS '租户标识';
COMMENT ON COLUMN public.tenant.type IS '租户类型';
COMMENT ON COLUMN public.tenant.tenant_domain IS '租户域名';
COMMENT ON COLUMN public.tenant.logo IS '租户logo';
COMMENT ON COLUMN public.tenant.icon IS '租户icon';
COMMENT ON COLUMN public.tenant.element IS '元素';
COMMENT ON COLUMN public.tenant.arrange IS '排列方式';
COMMENT ON COLUMN public.tenant.alignment IS '水平对齐';
COMMENT ON COLUMN public.tenant.icon_open IS '图标（左侧菜单展开）';
COMMENT ON COLUMN public.tenant.icon_close IS '图标（左侧菜单收起）';
COMMENT ON COLUMN public.tenant.menu_name IS '名称（默认为租户名称，可修改，建议名称在8个字内）';
COMMENT ON COLUMN public.tenant.favicon IS '浏览器页签图标';
COMMENT ON COLUMN public.tenant.favicon_name IS '浏览器页签图标名称';
COMMENT ON COLUMN public.tenant.start_date IS '租户有效期开始日期';
COMMENT ON COLUMN public.tenant.end_date IS '租户有效期结束日期';
COMMENT ON COLUMN public.tenant.status IS '租户状态';
COMMENT ON COLUMN public.tenant.remark IS '租户描述';
COMMENT ON COLUMN public.tenant.sort_order IS '排序值';
COMMENT ON COLUMN public.tenant.del_flag IS '是否已删除';
COMMENT ON COLUMN public.tenant.create_time IS '创建时间';
COMMENT ON COLUMN public.tenant.create_by IS '创建人编号';
COMMENT ON COLUMN public.tenant.create_name IS '创建人姓名';
COMMENT ON COLUMN public.tenant.update_time IS '更新时间';
COMMENT ON COLUMN public.tenant.update_by IS '更新人编号';
COMMENT ON COLUMN public.tenant.update_name IS '更新人姓名';

CREATE TABLE public.todo_task
(
    id            CHAR(32) NOT NULL,
    domain_code   VARCHAR(64),
    tenant_code   VARCHAR(64),
    app_code      VARCHAR(64),
    business_name VARCHAR(128),
    business_id   VARCHAR(128),
    summary       VARCHAR(1024),
    user_id       CHAR(32),
    user_name     VARCHAR(64),
    status        VARCHAR(64),
    redirect      VARCHAR(256),
    create_time   DATE,
    create_by     CHAR(32),
    create_name   VARCHAR(64),
    update_time   DATE,
    update_by     CHAR(32),
    update_name   VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.todo_task IS '待办任务表';
COMMENT ON COLUMN public.todo_task.id IS '主键';
COMMENT ON COLUMN public.todo_task.domain_code IS '域标识';
COMMENT ON COLUMN public.todo_task.tenant_code IS '租户标识';
COMMENT ON COLUMN public.todo_task.app_code IS '应用标识';
COMMENT ON COLUMN public.todo_task.business_name IS '业务名称';
COMMENT ON COLUMN public.todo_task.business_id IS '业务数据编号';
COMMENT ON COLUMN public.todo_task.summary IS '摘要';
COMMENT ON COLUMN public.todo_task.user_id IS '用户编号';
COMMENT ON COLUMN public.todo_task.user_name IS '用户名';
COMMENT ON COLUMN public.todo_task.status IS '业务状态';
COMMENT ON COLUMN public.todo_task.redirect IS '跳转链接';
COMMENT ON COLUMN public.todo_task.create_time IS '创建时间';
COMMENT ON COLUMN public.todo_task.create_by IS '创建人编号';
COMMENT ON COLUMN public.todo_task.create_name IS '创建人姓名';
COMMENT ON COLUMN public.todo_task.update_time IS '更新时间';
COMMENT ON COLUMN public.todo_task.update_by IS '更新人编号';
COMMENT ON COLUMN public.todo_task.update_name IS '更新人姓名';

CREATE TABLE public.user_category
(
    id            VARCHAR(32) NOT NULL,
    tenant_code   VARCHAR(64),
    user_id       VARCHAR(32),
    category_code VARCHAR(64),
    create_time   DATE,
    create_by     VARCHAR(32),
    create_name   VARCHAR(64),
    update_time   DATE,
    update_by     VARCHAR(32),
    update_name   VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.user_category IS '用户身份';
COMMENT ON COLUMN public.user_category.id IS '主键';
COMMENT ON COLUMN public.user_category.user_id IS '用户编号';
COMMENT ON COLUMN public.user_category.category_code IS '身份标识';
COMMENT ON COLUMN public.user_category.create_time IS '创建时间';
COMMENT ON COLUMN public.user_category.create_by IS '创建人编号';
COMMENT ON COLUMN public.user_category.create_name IS '创建人姓名';
COMMENT ON COLUMN public.user_category.update_time IS '更新时间';
COMMENT ON COLUMN public.user_category.update_by IS '更新人编号';
COMMENT ON COLUMN public.user_category.update_name IS '更新人姓名';

CREATE TABLE public.user_config
(
    id          VARCHAR(32) NOT NULL,
    tenant_code VARCHAR(64),
    user_id     VARCHAR(32),
    type        VARCHAR(32),
    value       LONGTEXT,
    global      TINYINT,
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.user_config IS '用户配置';
COMMENT ON COLUMN public.user_config.id IS '主键';
COMMENT ON COLUMN public.user_config.tenant_code IS '租户标识';
COMMENT ON COLUMN public.user_config.user_id IS '用户id';
COMMENT ON COLUMN public.user_config.type IS '配置类型';
COMMENT ON COLUMN public.user_config.value IS '配置信息';
COMMENT ON COLUMN public.user_config.global IS '是否全局配置';
COMMENT ON COLUMN public.user_config.create_time IS '创建时间';
COMMENT ON COLUMN public.user_config.create_by IS '创建人编号';
COMMENT ON COLUMN public.user_config.create_name IS '创建人姓名';
COMMENT ON COLUMN public.user_config.update_time IS '更新时间';
COMMENT ON COLUMN public.user_config.update_by IS '更新人编号';
COMMENT ON COLUMN public.user_config.update_name IS '更新人姓名';

CREATE TABLE public.user_dept
(
    id          VARCHAR(32) NOT NULL,
    tenant_code VARCHAR(64),
    user_id     VARCHAR(32),
    dept_id     VARCHAR(32),
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.user_dept IS '用户部门';
COMMENT ON COLUMN public.user_dept.id IS '主键';
COMMENT ON COLUMN public.user_dept.tenant_code IS '租户标识';
COMMENT ON COLUMN public.user_dept.user_id IS '用户编号';
COMMENT ON COLUMN public.user_dept.dept_id IS '部门编号';
COMMENT ON COLUMN public.user_dept.create_time IS '创建时间';
COMMENT ON COLUMN public.user_dept.create_by IS '创建人编号';
COMMENT ON COLUMN public.user_dept.create_name IS '创建人姓名';
COMMENT ON COLUMN public.user_dept.update_time IS '更新时间';
COMMENT ON COLUMN public.user_dept.update_by IS '更新人编号';
COMMENT ON COLUMN public.user_dept.update_name IS '更新人姓名';

CREATE TABLE public.user_post
(
    id          VARCHAR(32) NOT NULL,
    tenant_code VARCHAR(64),
    user_id     VARCHAR(32),
    post_id     VARCHAR(32),
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.user_post IS '用户岗位';
COMMENT ON COLUMN public.user_post.id IS '主键';
COMMENT ON COLUMN public.user_post.tenant_code IS '租户标识';
COMMENT ON COLUMN public.user_post.user_id IS '用户编号';
COMMENT ON COLUMN public.user_post.post_id IS '岗位编号';
COMMENT ON COLUMN public.user_post.create_time IS '创建时间';
COMMENT ON COLUMN public.user_post.create_by IS '创建人编号';
COMMENT ON COLUMN public.user_post.create_name IS '创建人姓名';
COMMENT ON COLUMN public.user_post.update_time IS '更新时间';
COMMENT ON COLUMN public.user_post.update_by IS '更新人编号';
COMMENT ON COLUMN public.user_post.update_name IS '更新人姓名';

CREATE TABLE public.user_role
(
    id          VARCHAR(32) NOT NULL,
    domain_code VARCHAR(64),
    tenant_code VARCHAR(64),
    app_code    VARCHAR(64),
    user_id     VARCHAR(32),
    role_id     VARCHAR(32),
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.user_role IS '用户角色';
COMMENT ON COLUMN public.user_role.id IS '主键';
COMMENT ON COLUMN public.user_role.domain_code IS '域标识';
COMMENT ON COLUMN public.user_role.tenant_code IS '租户标识';
COMMENT ON COLUMN public.user_role.app_code IS '应用标识';
COMMENT ON COLUMN public.user_role.user_id IS '用户编号';
COMMENT ON COLUMN public.user_role.role_id IS '角色编号';
COMMENT ON COLUMN public.user_role.create_time IS '创建时间';
COMMENT ON COLUMN public.user_role.create_by IS '创建人编号';
COMMENT ON COLUMN public.user_role.create_name IS '创建人姓名';
COMMENT ON COLUMN public.user_role.update_time IS '更新时间';
COMMENT ON COLUMN public.user_role.update_by IS '更新人编号';
COMMENT ON COLUMN public.user_role.update_name IS '更新人姓名';

CREATE TABLE public.user_social
(
    id          VARCHAR(32) NOT NULL,
    user_id     VARCHAR(32),
    social_id   VARCHAR(256),
    social_type VARCHAR(64),
    social_no   VARCHAR(128),
    create_time DATE,
    create_by   VARCHAR(32),
    create_name VARCHAR(64),
    update_time DATE,
    update_by   VARCHAR(32),
    update_name VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.user_social IS '用户第三方平台身份标识';
COMMENT ON COLUMN public.user_social.id IS '主键';
COMMENT ON COLUMN public.user_social.user_id IS '用户编号';
COMMENT ON COLUMN public.user_social.social_id IS '第三方平台编号';
COMMENT ON COLUMN public.user_social.social_type IS '第三方平台类型';
COMMENT ON COLUMN public.user_social.social_no IS '第三方平台身份标识';
COMMENT ON COLUMN public.user_social.create_time IS '创建时间';
COMMENT ON COLUMN public.user_social.create_by IS '创建人编号';
COMMENT ON COLUMN public.user_social.create_name IS '创建人姓名';
COMMENT ON COLUMN public.user_social.update_time IS '更新时间';
COMMENT ON COLUMN public.user_social.update_by IS '更新人编号';
COMMENT ON COLUMN public.user_social.update_name IS '更新人姓名';

CREATE TABLE public.user_tenant
(
    id           VARCHAR(32) NOT NULL,
    user_id      VARCHAR(32),
    tenant_code  VARCHAR(64),
    recent_login TINYINT,
    create_time  DATE,
    create_by    VARCHAR(32),
    create_name  VARCHAR(64),
    update_time  DATE,
    update_by    VARCHAR(32),
    update_name  VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.user_tenant IS '用户租户';
COMMENT ON COLUMN public.user_tenant.id IS '主键';
COMMENT ON COLUMN public.user_tenant.user_id IS '用户编号';
COMMENT ON COLUMN public.user_tenant.tenant_code IS '租户标识';
COMMENT ON COLUMN public.user_tenant.recent_login IS '是否最近登录';
COMMENT ON COLUMN public.user_tenant.create_time IS '创建时间';
COMMENT ON COLUMN public.user_tenant.create_by IS '创建人编号';
COMMENT ON COLUMN public.user_tenant.create_name IS '创建人姓名';
COMMENT ON COLUMN public.user_tenant.update_time IS '更新时间';
COMMENT ON COLUMN public.user_tenant.update_by IS '更新人编号';
COMMENT ON COLUMN public.user_tenant.update_name IS '更新人姓名';

CREATE TABLE public.user_track
(
    id               VARCHAR(32) NOT NULL,
    tenant_code      VARCHAR(64),
    business_data_id VARCHAR(64),
    user_id          VARCHAR(32),
    user_name        VARCHAR(64),
    operation        LONGTEXT,
    operation_props  JSON,
    create_time      DATE,
    create_by        VARCHAR(32),
    create_name      VARCHAR(64),
    update_time      DATE,
    update_by        VARCHAR(32),
    update_name      VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.user_track IS '用户埋点信息';
COMMENT ON COLUMN public.user_track.id IS '主键';
COMMENT ON COLUMN public.user_track.tenant_code IS '租户标识';
COMMENT ON COLUMN public.user_track.business_data_id IS '业务数据编号';
COMMENT ON COLUMN public.user_track.user_id IS '用户编号';
COMMENT ON COLUMN public.user_track.user_name IS '用户姓名';
COMMENT ON COLUMN public.user_track.operation IS '操作内容';
COMMENT ON COLUMN public.user_track.operation_props IS '订阅方数据字段名';
COMMENT ON COLUMN public.user_track.create_time IS '创建时间';
COMMENT ON COLUMN public.user_track.create_by IS '创建人编号';
COMMENT ON COLUMN public.user_track.create_name IS '创建人姓名';
COMMENT ON COLUMN public.user_track.update_time IS '更新时间';
COMMENT ON COLUMN public.user_track.update_by IS '更新人编号';
COMMENT ON COLUMN public.user_track.update_name IS '更新人姓名';

CREATE TABLE public.variable
(
    id             VARCHAR(32) NOT NULL,
    name           VARCHAR(64),
    variable_key   VARCHAR(128),
    variable_value VARCHAR(128),
    type           VARCHAR(64),
    create_time    DATE,
    create_by      VARCHAR(32),
    create_name    VARCHAR(64),
    update_time    DATE,
    update_by      VARCHAR(32),
    update_name    VARCHAR(64),
    PRIMARY KEY (id)
);

COMMENT ON TABLE public.variable IS '系统参数';
COMMENT ON COLUMN public.variable.id IS '编号/参数编号';
COMMENT ON COLUMN public.variable.name IS '参数名称';
COMMENT ON COLUMN public.variable.variable_key IS '参数键名';
COMMENT ON COLUMN public.variable.variable_value IS '参数值';
COMMENT ON COLUMN public.variable.type IS '参数类型';
COMMENT ON COLUMN public.variable.create_time IS '创建时间';
COMMENT ON COLUMN public.variable.create_by IS '创建人编号';
COMMENT ON COLUMN public.variable.create_name IS '创建人姓名';
COMMENT ON COLUMN public.variable.update_time IS '更新时间';
COMMENT ON COLUMN public.variable.update_by IS '更新人编号';
COMMENT ON COLUMN public.variable.update_name IS '更新人姓名';

