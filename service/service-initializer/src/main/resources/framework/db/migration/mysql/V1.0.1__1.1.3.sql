ALTER TABLE `application_page` ADD COLUMN `category` varchar(64) NULL DEFAULT 'GENERAL' COMMENT '页面类别' AFTER `icon`;

ALTER TABLE `application_page` ADD COLUMN `render_json` json NULL COMMENT '页面渲染JSON' AFTER `json`;

ALTER TABLE `application_page` ADD COLUMN `sort_order` int NULL DEFAULT NULL COMMENT '页面排序值' AFTER `component`;

ALTER TABLE `application_page_template` ADD COLUMN `type` varchar(64) NULL DEFAULT NULL COMMENT '模板类别' AFTER `internal_template`;

ALTER TABLE `application_page` DROP COLUMN `app_id`;

ALTER TABLE `application_page` DROP COLUMN `tenant_code`;