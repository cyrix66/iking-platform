ALTER TABLE `application`
    MODIFY COLUMN `id` varchar(32) NOT NULL COMMENT '主键' FIRST;

ALTER TABLE `application`
    MODIFY COLUMN `icon` varchar(256) NULL DEFAULT '' COMMENT '应用图标' AFTER `code`;

ALTER TABLE `application`
    MODIFY COLUMN `icon_background` varchar(256) NULL DEFAULT '' COMMENT '图标背景色' AFTER `icon`;

ALTER TABLE `application`
    MODIFY COLUMN `index_path` varchar(512) NULL DEFAULT '' COMMENT '应用首页地址' AFTER `remark`;

ALTER TABLE `application`
    MODIFY COLUMN `type` varchar(64) NULL DEFAULT '' COMMENT '应用类型' AFTER `index_path`;

ALTER TABLE `application`
    MODIFY COLUMN `status` varchar(64) NULL DEFAULT '' COMMENT '应用状态' AFTER `type`;

ALTER TABLE `application_model`
    MODIFY COLUMN `status` varchar(64) NULL DEFAULT '' COMMENT '模型状态' AFTER `type`;

ALTER TABLE `application_model`
    MODIFY COLUMN `creation_type` varchar(64) NULL DEFAULT '' COMMENT '模型创建类型' AFTER `status`;

ALTER TABLE `application_model_field`
    MODIFY COLUMN `db_table_field` tinyint NULL DEFAULT 0 COMMENT '是否是数据库表字段' AFTER `remark`;

ALTER TABLE `application_model_field`
    MODIFY COLUMN `nullable` tinyint NULL DEFAULT 1 COMMENT '是否可以为空' AFTER `db_table_field_type`;

ALTER TABLE `application_model_field`
    MODIFY COLUMN `query_field` tinyint NULL DEFAULT 0 COMMENT '是否为查询字段' AFTER `nullable`;

ALTER TABLE `application_model_field`
    MODIFY COLUMN `sync_with_datasource` tinyint NULL DEFAULT 0 COMMENT '是否与数据源保持同步' AFTER `compare_type`;

ALTER TABLE `application_model_field`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `sync_with_datasource`;

ALTER TABLE `application_page_template`
    MODIFY COLUMN `internal_template` tinyint NULL DEFAULT 1 COMMENT '是否为内置模板' AFTER `description`;

ALTER TABLE `application_page_template`
    MODIFY COLUMN `type` varchar(64) NULL DEFAULT '' COMMENT '模板类别' AFTER `internal_template`;

CREATE TABLE `approve_cond_comp`
(
    `id`                 varchar(32) NOT NULL COMMENT '主键',
    `form_id`            varchar(32) NULL DEFAULT '' COMMENT '表单编号',
    `process_id`         varchar(32) NULL DEFAULT '' COMMENT '流程编号',
    `node_id`            varchar(32) NULL DEFAULT '' COMMENT '流程节点编号',
    `group_id`           varchar(32) NULL DEFAULT '' COMMENT '流程条件组编号',
    `widget_name`        varchar(64) NULL DEFAULT '' COMMENT '表单控件名称',
    `widget_description` varchar(64) NULL DEFAULT '' COMMENT '表单控件描述',
    `comparator`         varchar(32) NULL DEFAULT '' COMMENT '条件比较式',
    `compare_to_values`  text        NULL COMMENT '条件比较值',
    `date_pattern`       varchar(64) NULL DEFAULT '' COMMENT '日期格式',
    `applicable_type`    varchar(64) NULL DEFAULT '' COMMENT '适用类型',
    `create_time`        datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`          varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`        varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`        datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`          varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`        varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT = '表单流程节点分支条件';

CREATE TABLE `approve_cond_group`
(
    `id`              varchar(32) NOT NULL COMMENT '主键',
    `form_id`         varchar(32) NULL DEFAULT '' COMMENT '表单编号',
    `process_id`      varchar(32) NULL DEFAULT '' COMMENT '流程编号',
    `node_id`         varchar(32) NULL DEFAULT '' COMMENT '流程节点编号',
    `name`            varchar(64) NULL DEFAULT '' COMMENT '分支条件组名称',
    `applicable_type` varchar(64) NULL DEFAULT '' COMMENT '适用类型',
    `create_time`     datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`       varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`     varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`     datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`       varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`     varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT = '表单流程节点分支条件组';

ALTER TABLE `approve_form`
    ADD COLUMN `dept_id` varchar(32) NULL DEFAULT '' COMMENT '所属部门编号' AFTER `customize_initiator`;

ALTER TABLE `approve_form`
    MODIFY COLUMN `configured` tinyint NULL DEFAULT 0 COMMENT '表单是否已配置' AFTER `remark`;

ALTER TABLE `approve_form`
    MODIFY COLUMN `report` tinyint NULL DEFAULT 0 COMMENT '是否是注册表单' AFTER `configured`;

ALTER TABLE `approve_form`
    MODIFY COLUMN `customize_initiator` tinyint NULL DEFAULT 0 COMMENT '表单是否支持配置发起人' AFTER `report`;

ALTER TABLE `approve_form`
    MODIFY COLUMN `latest_version` tinyint NULL DEFAULT 1 COMMENT '是否最新版本' AFTER `status`;

ALTER TABLE `approve_form`
    MODIFY COLUMN `version_no` int NULL DEFAULT 1 COMMENT '表单版本号' AFTER `latest_version`;

ALTER TABLE `approve_form`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `version_no`;

ALTER TABLE `approve_form`
    MODIFY COLUMN `visible` tinyint NULL DEFAULT 1 COMMENT '表单在审批中心是否可见' AFTER `sort_order`;

ALTER TABLE `approve_form_advance_config`
    ADD COLUMN `approve_form_config_invisible` tinyint NULL DEFAULT 0 COMMENT '是否仅管理员可见' AFTER `notify_time`;

ALTER TABLE `approve_form_group`
    MODIFY COLUMN `renamable` tinyint NULL DEFAULT 1 COMMENT '是否可重命名' AFTER `remark`;

ALTER TABLE `approve_form_group`
    MODIFY COLUMN `deletable` tinyint NULL DEFAULT 1 COMMENT '是否可删除' AFTER `renamable`;

ALTER TABLE `approve_form_group`
    MODIFY COLUMN `sortable` tinyint NULL DEFAULT 1 COMMENT '是否可排序' AFTER `deletable`;

ALTER TABLE `approve_form_group`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `sortable`;

ALTER TABLE `approve_form_instance`
    MODIFY COLUMN `visible` tinyint NULL DEFAULT 1 COMMENT '表单在审批中心是否可见' AFTER `process_status`;

ALTER TABLE `approve_form_instance`
    MODIFY COLUMN `draft` tinyint NULL DEFAULT 0 COMMENT '是否草稿' AFTER `visible`;

ALTER TABLE `approve_form_manager`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `manager_type`;

ALTER TABLE `approve_form_view`
    MODIFY COLUMN `process_node_id` varchar(32) NULL DEFAULT '' COMMENT '所属流程节点编号' AFTER `form_id`;

ALTER TABLE `approve_form_widget`
    MODIFY COLUMN `process_node_id` varchar(32) NULL DEFAULT '' COMMENT '流程节点编号' AFTER `form_id`;

ALTER TABLE `approve_form_widget`
    MODIFY COLUMN `widget_type` varchar(32) NULL DEFAULT '' COMMENT '表单控件字段类型' AFTER `widget_label`;

ALTER TABLE `approve_form_widget`
    MODIFY COLUMN `required` tinyint NULL DEFAULT 0 COMMENT '字段是否必填' AFTER `widget_type`;

ALTER TABLE `approve_form_widget`
    MODIFY COLUMN `remain` tinyint NULL DEFAULT 0 COMMENT '是否为前一节点保留字段' AFTER `required`;

ALTER TABLE `approve_process_executor`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `executor_category`;

ALTER TABLE `approve_process_instance_node`
    MODIFY COLUMN `node_form_id` varchar(32) NULL DEFAULT '' COMMENT '节点表单编号' AFTER `form_id`;

ALTER TABLE `approve_process_instance_node`
    MODIFY COLUMN `single_approval` tinyint NULL DEFAULT 0 COMMENT '发起人自选-是否为自选单人' AFTER `approval_category`;

ALTER TABLE `approve_process_instance_node`
    MODIFY COLUMN `re_submit_to_back` tinyint NULL DEFAULT 0 COMMENT '重新发起时是否有退回人直接审批' AFTER `multi_executor_type`;

ALTER TABLE `approve_process_instance_node`
    MODIFY COLUMN `back_to_initiator` tinyint NULL DEFAULT 0 COMMENT '退回时是否直接退回到发起节点' AFTER `re_submit_to_back`;

ALTER TABLE `approve_process_instance_node`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `form_data`;

ALTER TABLE `approve_process_instance_user`
    MODIFY COLUMN `node_id` varchar(32) NULL DEFAULT '' COMMENT '流程配置节点编号' AFTER `process_id`;

ALTER TABLE `approve_process_instance_user`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `status`;

ALTER TABLE `approve_process_node`
    MODIFY COLUMN `node_form_id` varchar(32) NULL DEFAULT '' COMMENT '节点表单编号' AFTER `form_id`;

ALTER TABLE `approve_process_node`
    MODIFY COLUMN `single_approval` tinyint NULL DEFAULT 0 COMMENT '发起人自选-是否为自选单人' AFTER `initiator_specified_scope_type`;

ALTER TABLE `approve_process_node`
    MODIFY COLUMN `re_submit_to_back` tinyint NULL DEFAULT 0 COMMENT '重新发起时是否由退回人直接审批' AFTER `multi_executor_type`;

ALTER TABLE `approve_process_node`
    MODIFY COLUMN `back_to_initiator` tinyint NULL DEFAULT 0 COMMENT '退回时是否直接退回到发起节点' AFTER `re_submit_to_back`;

ALTER TABLE `approve_process_node`
    MODIFY COLUMN `initiator_specify_carbon_copy` tinyint NULL DEFAULT 0 COMMENT '是否允许申请人自选抄送人' AFTER `back_to_initiator`;

ALTER TABLE `approve_process_node`
    MODIFY COLUMN `condition_order` int NULL DEFAULT 1 COMMENT '条件节点优先级' AFTER `initiator_specify_carbon_copy`;

ALTER TABLE `approve_record_node`
    MODIFY COLUMN `single_approval` tinyint NULL DEFAULT 0 COMMENT '发起人自选-是否为自选单人' AFTER `approval_category`;

ALTER TABLE `approve_record_node`
    MODIFY COLUMN `re_submit_to_back` tinyint NULL DEFAULT 0 COMMENT '重新发起时是否有退回人直接审批' AFTER `single_approval`;

ALTER TABLE `approve_record_node`
    MODIFY COLUMN `back_to_initiator` tinyint NULL DEFAULT 0 COMMENT '退回时是否直接退回到发起节点' AFTER `re_submit_to_back`;

ALTER TABLE `approve_record_node`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `images`;

ALTER TABLE `approve_record_node_attachment`
    COMMENT = '审批记录附件';

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `id` varchar(32) NOT NULL COMMENT '主键' FIRST;

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `form_id` varchar(32) NULL DEFAULT '' COMMENT '表单编号' AFTER `id`;

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `process_id` varchar(32) NULL DEFAULT '' COMMENT '流程配置编号' AFTER `form_id`;

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `node_id` varchar(32) NULL DEFAULT '' COMMENT '流程配置节点编号' AFTER `process_id`;

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `form_instance_id` varchar(32) NULL DEFAULT '' COMMENT '表单实例编号' AFTER `node_id`;

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `process_instance_id` varchar(32) NULL DEFAULT '' COMMENT '流程实例编号' AFTER `form_instance_id`;

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `process_instance_node_id` varchar(32) NULL DEFAULT '' COMMENT '流程实例节点编号' AFTER `process_instance_id`;

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `record_id` varchar(32) NULL DEFAULT '' COMMENT '审批记录编号' AFTER `process_instance_node_id`;

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `record_node_id` varchar(32) NULL DEFAULT '' COMMENT '审批记录节点编号' AFTER `record_id`;

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `attachment_id` varchar(32) NULL DEFAULT '' COMMENT '附件编号' AFTER `record_node_id`;

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `attachment_name` varchar(256) NULL DEFAULT '' COMMENT '附件名称' AFTER `attachment_id`;

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `attachment_size` varchar(64) NULL DEFAULT '' COMMENT '附件大小' AFTER `attachment_name`;

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `attachment_suffix` varchar(32) NULL DEFAULT '' COMMENT '附件后缀名' AFTER `attachment_size`;

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 AFTER `attachment_suffix`;

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `create_by` varchar(32) NULL DEFAULT '' COMMENT '创建人编号' AFTER `create_time`;

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `create_name` varchar(64) NULL DEFAULT '' COMMENT '创建人姓名' AFTER `create_by`;

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `update_by` varchar(32) NULL DEFAULT '' COMMENT '更新人编号' AFTER `update_time`;

ALTER TABLE `approve_record_node_attachment`
    MODIFY COLUMN `update_name` varchar(64) NULL DEFAULT '' COMMENT '更新人姓名' AFTER `update_by`;

ALTER TABLE `approve_record_node_user`
    MODIFY COLUMN `append_user` tinyint NULL DEFAULT 0 COMMENT '是否为加签用户' AFTER `status`;

ALTER TABLE `approve_record_node_user`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `append_user`;

ALTER TABLE `auth_log`
    MODIFY COLUMN `success` tinyint NULL DEFAULT 1 COMMENT '是否成功' AFTER `os`;

ALTER TABLE `cashier_supplier`
    MODIFY COLUMN `type` varchar(32) NULL DEFAULT '' COMMENT '支付平台类型' AFTER `id`;

ALTER TABLE `country`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `continent`;

ALTER TABLE `database_backup_record`
    MODIFY COLUMN `record_code` varchar(64) NULL DEFAULT '' COMMENT '编号' AFTER `strategy_id`;

ALTER TABLE `database_backup_record`
    MODIFY COLUMN `log_file_path` varchar(256) NULL DEFAULT '' COMMENT 'log文件路劲' AFTER `sql_file_path`;

ALTER TABLE `database_backup_record`
    MODIFY COLUMN `success` tinyint NULL DEFAULT 1 COMMENT '是否执行成功' AFTER `log_file_path`;

ALTER TABLE `database_backup_strategy`
    MODIFY COLUMN `type` tinyint NULL DEFAULT 1 COMMENT '备份对象类型（0自定义选表；1全表)' AFTER `schema_name`;

ALTER TABLE `database_backup_strategy`
    MODIFY COLUMN `expire_type` int NULL DEFAULT NULL COMMENT '过期策略' AFTER `cron`;

ALTER TABLE `database_backup_strategy`
    MODIFY COLUMN `running` tinyint NULL DEFAULT 0 COMMENT '状态（开启/暂停）' AFTER `parent_path`;

ALTER TABLE `database_backup_strategy`
    MODIFY COLUMN `del_flag` tinyint NULL DEFAULT 0 COMMENT '是否已删除' AFTER `remark`;

ALTER TABLE `datasource`
    MODIFY COLUMN `default_datasource` tinyint NULL DEFAULT 0 COMMENT '是否默认' AFTER `remark`;

ALTER TABLE `department`
    ADD COLUMN `type` varchar(64) NULL DEFAULT '' COMMENT '组织架构类型' AFTER `manager_id`;

ALTER TABLE `department`
    ADD COLUMN `home_page` varchar(500) NULL DEFAULT '' COMMENT '网址' AFTER `address`;

ALTER TABLE `department`
    MODIFY COLUMN `full_path` varchar(600) NULL DEFAULT '' COMMENT '组织架构全路径' AFTER `id`;

ALTER TABLE `department`
    MODIFY COLUMN `parent_id` varchar(32) NULL DEFAULT '' COMMENT '父组织架构编号' AFTER `full_path`;

ALTER TABLE `department`
    MODIFY COLUMN `name` varchar(64) NULL DEFAULT '' COMMENT '组织架构名称' AFTER `tenant_code`;

ALTER TABLE `department`
    MODIFY COLUMN `grade` int NULL DEFAULT 1 COMMENT '级别' AFTER `name`;

ALTER TABLE `department`
    MODIFY COLUMN `equity_ratio` varchar(32) NULL DEFAULT '' COMMENT '股权比例' AFTER `status`;

ALTER TABLE `department`
    MODIFY COLUMN `default_flag` tinyint NULL DEFAULT 0 COMMENT '是否默认部门' AFTER `introduction`;

ALTER TABLE `department`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `default_flag`;

ALTER TABLE `department`
    MODIFY COLUMN `del_flag` tinyint NULL DEFAULT 0 COMMENT '是否已删除' AFTER `sort_order`;

ALTER TABLE `department`
    DROP COLUMN `website`;

ALTER TABLE `department`
    DROP COLUMN `org_id`;

ALTER TABLE `dict`
    MODIFY COLUMN `del_flag` tinyint NULL DEFAULT 0 COMMENT '是否已删除' AFTER `type`;

ALTER TABLE `dict_item`
    MODIFY COLUMN `preset` tinyint NULL DEFAULT 0 COMMENT '是否预置字典项' AFTER `remark`;

ALTER TABLE `dict_item`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `preset`;

ALTER TABLE `dict_item`
    MODIFY COLUMN `del_flag` tinyint NULL DEFAULT 0 COMMENT '是否已删除' AFTER `sort_order`;

ALTER TABLE `division`
    MODIFY COLUMN `division_level` varchar(32) NULL DEFAULT '' COMMENT '行政区划级别' AFTER `parent_no`;

ALTER TABLE `division`
    MODIFY COLUMN `provincial_capital` tinyint NULL DEFAULT 0 COMMENT '是否为省会城市' AFTER `division_level`;

ALTER TABLE `division`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `provincial_capital`;

ALTER TABLE `division`
    MODIFY COLUMN `del_flag` tinyint NULL DEFAULT 0 COMMENT '是否已删除' AFTER `sort_order`;

ALTER TABLE `menu`
    MODIFY COLUMN `full_path` varchar(600) NULL DEFAULT '' COMMENT '菜单全路径' AFTER `id`;

ALTER TABLE `menu`
    MODIFY COLUMN `framework` tinyint NULL DEFAULT 0 COMMENT '是否为框架路由' AFTER `component`;

ALTER TABLE `menu`
    MODIFY COLUMN `default_opened` tinyint NULL DEFAULT 0 COMMENT '是否默认展开' AFTER `iframe`;

ALTER TABLE `menu`
    MODIFY COLUMN `permanent` tinyint NULL DEFAULT 0 COMMENT '是否常驻标签页' AFTER `default_opened`;

ALTER TABLE `menu`
    MODIFY COLUMN `sidebar` tinyint NULL DEFAULT 0 COMMENT '是否在侧边栏导航中展示' AFTER `permanent`;

ALTER TABLE `menu`
    MODIFY COLUMN `breadcrumb` tinyint NULL DEFAULT 0 COMMENT '是否在面包屑导航中展示' AFTER `sidebar`;

ALTER TABLE `menu`
    MODIFY COLUMN `copyright` tinyint NULL DEFAULT 0 COMMENT '是否显示版权信息' AFTER `badge`;

ALTER TABLE `menu`
    MODIFY COLUMN `keep_alive` tinyint NULL DEFAULT 0 COMMENT '路由缓冲' AFTER `jump_type`;

ALTER TABLE `menu`
    MODIFY COLUMN `visible` tinyint NULL DEFAULT 0 COMMENT '菜单是否可见' AFTER `keep_alive`;

ALTER TABLE `menu`
    MODIFY COLUMN `auto_render` tinyint NULL DEFAULT 0 COMMENT '是否自动渲染页面' AFTER `visible`;

ALTER TABLE `menu`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `remark`;

ALTER TABLE `message_channel_definition`
    MODIFY COLUMN `voice` tinyint NULL DEFAULT 0 COMMENT '是否播放声音' AFTER `content`;

ALTER TABLE `message_channel_definition`
    MODIFY COLUMN `support_template` tinyint NULL DEFAULT 1 COMMENT '消息渠道是否支持模板' AFTER `status`;

ALTER TABLE `message_channel_definition`
    MODIFY COLUMN `show_notification` tinyint NULL DEFAULT 1 COMMENT '是否展示消息提醒' AFTER `support_template`;

ALTER TABLE `message_channel_definition`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `show_notification`;

ALTER TABLE `message_param_definition`
    MODIFY COLUMN `pre_definition` tinyint NULL DEFAULT 0 COMMENT '是否预定义参数' AFTER `channel_definition_id`;

ALTER TABLE `message_param_definition`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `mapped_param_name`;

ALTER TABLE `message_receiver_definition`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `receiver_param_name`;

ALTER TABLE `message_redirect_definition`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `redirect_name`;

ALTER TABLE `message_template`
    MODIFY COLUMN `configured` tinyint NULL DEFAULT 0 COMMENT '是否已配置' AFTER `message_template_title`;

ALTER TABLE `message_template`
    MODIFY COLUMN `configurable` tinyint NULL DEFAULT 1 COMMENT '是否可配置' AFTER `configured`;

ALTER TABLE `message_template`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `configurable`;

ALTER TABLE `message_template`
    MODIFY COLUMN `del_flag` tinyint NULL DEFAULT 0 COMMENT '是否已删除' AFTER `sort_order`;

ALTER TABLE `operation_log`
    MODIFY COLUMN `success` tinyint NULL DEFAULT 1 COMMENT '是否成功' AFTER `location`;

ALTER TABLE `oss_file`
    MODIFY COLUMN `origin_name` varchar(256) NULL DEFAULT '' COMMENT '原始文件名称' AFTER `app_code`;

ALTER TABLE `post`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `remark`;

ALTER TABLE `post`
    MODIFY COLUMN `del_flag` tinyint NULL DEFAULT 0 COMMENT '是否已删除' AFTER `sort_order`;

ALTER TABLE `role`
    MODIFY COLUMN `name` varchar(64) NULL DEFAULT '' COMMENT '角色名称' AFTER `id`;

ALTER TABLE `role`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `remark`;

ALTER TABLE `role`
    MODIFY COLUMN `del_flag` tinyint NULL DEFAULT 0 COMMENT '是否已删除' AFTER `sort_order`;

ALTER TABLE `sms`
    MODIFY COLUMN `support_template` tinyint NULL DEFAULT 0 COMMENT '是否支持模板' AFTER `signature`;

ALTER TABLE `sql_api_definition_backup`
    MODIFY COLUMN `tag` varchar(32) NULL DEFAULT '' COMMENT '标签' AFTER `create_date`;

ALTER TABLE `sql_api_definition_backup`
    MODIFY COLUMN `type` varchar(32) NULL DEFAULT '' COMMENT '类型' AFTER `tag`;

ALTER TABLE `sql_api_definition_backup`
    MODIFY COLUMN `name` varchar(64) NULL DEFAULT '' COMMENT '原名称' AFTER `type`;

ALTER TABLE `sql_api_definition_backup`
    MODIFY COLUMN `create_by` varchar(64) NULL DEFAULT '' COMMENT '操作人' AFTER `content`;

ALTER TABLE `sys_user`
    MODIFY COLUMN `locked` tinyint NULL DEFAULT 0 COMMENT '是否被锁定' AFTER `sex`;

ALTER TABLE `sys_user`
    MODIFY COLUMN `admin_user` tinyint NULL DEFAULT 0 COMMENT '是否管理员' AFTER `lock_type`;

ALTER TABLE `sys_user`
    MODIFY COLUMN `platform_user` tinyint NULL DEFAULT 1 COMMENT '是否平台用户' AFTER `admin_user`;

ALTER TABLE `sys_user`
    MODIFY COLUMN `del_flag` tinyint NULL DEFAULT 0 COMMENT '是否已删除' AFTER `platform_user`;

ALTER TABLE `tenant`
    MODIFY COLUMN `sort_order` int NULL DEFAULT 1 COMMENT '排序值' AFTER `remark`;

ALTER TABLE `tenant`
    MODIFY COLUMN `del_flag` tinyint NULL DEFAULT 0 COMMENT '是否已删除' AFTER `sort_order`;

ALTER TABLE `user_config`
    MODIFY COLUMN `global` tinyint NULL DEFAULT 0 COMMENT '是否全局配置' AFTER `value`;

ALTER TABLE `user_social`
    MODIFY COLUMN `user_id` varchar(32) NULL DEFAULT '' COMMENT '用户编号' AFTER `id`;

ALTER TABLE `user_tenant`
    MODIFY COLUMN `recent_login` tinyint NULL DEFAULT 0 COMMENT '是否最近登录' AFTER `tenant_code`;

DROP TABLE IF EXISTS `approve_process_cond_comp`;

DROP TABLE IF EXISTS `approve_process_cond_group`;

DROP TABLE IF EXISTS `jimu_dict`;

DROP TABLE IF EXISTS `jimu_dict_item`;

DROP TABLE IF EXISTS `jimu_report`;

DROP TABLE IF EXISTS `jimu_report_data_source`;

DROP TABLE IF EXISTS `jimu_report_db`;

DROP TABLE IF EXISTS `jimu_report_db_field`;

DROP TABLE IF EXISTS `jimu_report_db_param`;

DROP TABLE IF EXISTS `jimu_report_link`;

DROP TABLE IF EXISTS `jimu_report_map`;

DROP TABLE IF EXISTS `jimu_report_share`;

DROP TABLE IF EXISTS `organization`;