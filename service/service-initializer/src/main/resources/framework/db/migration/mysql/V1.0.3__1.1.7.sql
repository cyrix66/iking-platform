alter table approve_form_advance_config
    add allow_trigger_multi_process tinyint default 0 null after approve_form_config_invisible;

alter table user_category
    add tenant_code varchar(64) default '' null after id;