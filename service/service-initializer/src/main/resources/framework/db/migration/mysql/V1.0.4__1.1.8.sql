alter table department
    add juridical_person_name varchar(64) default '' null after manager_id;

alter table department
    add juridical_person_phone varchar(64) default '' null after juridical_person_name;

alter table department
    add juridical_person_identity_no varchar(64) default '' null after juridical_person_phone;

alter table department
    add social_credit_identity_code varchar(128) default '' null after address;

alter table department
    add bank_name varchar(128) default '' null after social_credit_identity_code;

alter table department
    add bank_account varchar(128) default '' null after bank_name;