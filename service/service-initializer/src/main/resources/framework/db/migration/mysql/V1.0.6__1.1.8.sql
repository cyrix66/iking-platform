alter table approve_form
    add column code varchar(64) null default '' after name;

alter table schedule
    add column tenant_code varchar(64) null default '' after status;