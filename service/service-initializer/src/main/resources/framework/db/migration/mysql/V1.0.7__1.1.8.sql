CREATE TABLE `schedule_executor`
(
    `id`            varchar(32) NOT NULL COMMENT '主键',
    `tenant_code`   varchar(64) NULL DEFAULT '' COMMENT '租户标识',
    `schedule_id`   varchar(32) NULL DEFAULT '' COMMENT '日程编号',
    `executor_id`   varchar(32) NULL DEFAULT '' COMMENT '执行者编号',
    `executor_type` varchar(64) NULL DEFAULT '' COMMENT '执行者类型',
    `create_time`   datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`     varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`   varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`   datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`     varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`   varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='日程执行者';