alter table department
    add column town_code varchar(128) null default '' after district_code;

alter table department
    add column town_name varchar(128) null default '' after town_code;