alter table approve_form_widget
    add column widget_code varchar(128) null default '' after process_node_id;

alter table approve_cond_comp
    add column widget_code varchar(128) null default '' after group_id;

ALTER TABLE approve_process_instance_node
    DROP COLUMN form_data;

alter table approve_record_node
    add column form_data json null after images;