CREATE TABLE `message_subscriber`
(
    `id`          varchar(32)  NOT NULL COMMENT '主键',
    `tenant_code` varchar(64)  NULL DEFAULT '' COMMENT '租户标识',
    `name`        varchar(64)  NULL DEFAULT 1 COMMENT '订阅方名称',
    `send_url`    varchar(512) NULL DEFAULT 1 COMMENT '推送地址',
    `api_key`     varchar(64)  NULL DEFAULT 1 COMMENT 'API Key',
    `create_time` datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='消息订阅方';

alter table variable
    drop column tenant_code;