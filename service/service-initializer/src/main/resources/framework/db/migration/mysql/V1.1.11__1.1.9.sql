alter table message_channel_definition
    modify content longtext null comment '消息模板内容';

alter table message_channel_definition
    add customize_content longtext null comment '消息渠道自定义模板' after content;