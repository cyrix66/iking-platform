alter table message_channel_definition
    add business_key varchar(64) null default '' comment '业务标识' after template_id;

alter table message_subscriber_send_url
    alter column subscriber_id set default '';

alter table message_subscriber_send_url
    alter column send_url set default '';

alter table message_subscriber_send_url
    alter column api_key set default '';

CREATE TABLE `message_subscriber_data_definition`
(
    `id`            varchar(32) NOT NULL COMMENT '主键',
    `tenant_code`   varchar(64) NULL DEFAULT '' COMMENT '租户标识',
    `subscriber_id` varchar(64) NULL DEFAULT '' COMMENT '订阅方编号',
    `send_url_id`   varchar(64) NULL DEFAULT '' COMMENT '推送地址编号',
    `field_name`    varchar(64) NULL DEFAULT '' COMMENT '订阅方数据字段名',
    `create_time`   datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`     varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`   varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`   datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`     varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`   varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='消息订阅方数据结构';