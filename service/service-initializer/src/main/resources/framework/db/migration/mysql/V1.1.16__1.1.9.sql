CREATE TABLE `user_track`
(
    `id`               varchar(32) NOT NULL COMMENT '主键',
    `tenant_code`      varchar(64) NULL DEFAULT '' COMMENT '租户标识',
    `business_data_id` varchar(64) NULL DEFAULT '' COMMENT '业务数据编号',
    `user_id`          varchar(32) NULL DEFAULT '' COMMENT '用户编号',
    `user_name`        varchar(64) NULL DEFAULT '' COMMENT '用户姓名',
    `operation`        longtext NULL COMMENT '操作内容',
    `operation_props`  json NULL COMMENT '订阅方数据字段名',
    `create_time`      datetime NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`        varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`      varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`      datetime NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`        varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`      varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='用户埋点信息';