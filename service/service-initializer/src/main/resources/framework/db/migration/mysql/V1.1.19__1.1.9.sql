create table oidc_client_redirect_uri
(
    id           varchar(32)  not null comment '主键',
    client_id    varchar(32) null default '' comment '客户端编号',
    redirect_uri varchar(512)  null default '' comment '客户端跳转地址',
    create_time  datetime     null default null comment '创建时间',
    create_by    varchar(32)  null default '' comment '创建人编号',
    create_name  varchar(64)  null default '' comment '创建人姓名',
    update_time  datetime     null default null comment '更新时间',
    update_by    varchar(32)  null default '' comment '更新人编号',
    update_name  varchar(64)  null default '' comment '更新人姓名',
    primary key (id) using btree
) comment ='oidc客户端跳转地址';