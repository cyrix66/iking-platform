CREATE TABLE `application_quick_entry`
(
    `id`          varchar(32) NOT NULL COMMENT '主键',
    `app_code`    varchar(64) NULL DEFAULT '' COMMENT '应用标识',
    `tenant_code` varchar(64) NULL DEFAULT '' COMMENT '租户标识',
    `user_id`     varchar(32) NULL DEFAULT '' COMMENT '用户编号',
    `create_time` datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='应用快捷入口';

CREATE TABLE `platform_config`
(
    `id`             varchar(32)  NOT NULL COMMENT '主键',
    `tenant_code`    varchar(64)  NULL DEFAULT '' COMMENT '租户标识',
    `type`           varchar(64)  NULL DEFAULT '' COMMENT '配置类型',
    `data_id`         varchar(32)  NULL DEFAULT '' COMMENT '配置编号',
    `parent_key`     varchar(64)  NULL DEFAULT '' COMMENT '父配置项',
    `property_key`   varchar(128) NULL DEFAULT '' COMMENT '配置项',
    `property_value` longtext     NULL COMMENT '配置内容',
    `create_time`    datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`      varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`    varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`    datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`      varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`    varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='平台配置';

drop table wechat_mini;

alter table approve_process_permission
    add column widget_code varchar(64) null default '' after node_id;