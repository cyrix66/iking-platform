alter table approve_process_node
    add column role_specified_scope_type varchar(64) null default '' after initiator_specified_scope_type;

alter table approve_process_executor
    add column executor_dept_id varchar(32) null default '' after executor_type;

alter table approve_process_executor
    add column executor_parent_dept_level int null default -1 after executor_dept_id;

alter table approve_form_advance_config
    add column specified_initiator_dept_required tinyint null default 0 after trigger_condition_enabled;

alter table approve_process_node
    drop column organization_widget_name;

create table oidc_client
(
    id          varchar(32)  not null comment '主键',
    secret      varchar(128) null default '' comment '客户端secret',
    name        varchar(64)  null default '' comment '客户端名称',
    logo        varchar(512) null default '' comment '客户端logo',
    website     varchar(512) null default '' comment '客户端网址',
    create_time datetime     null default null comment '创建时间',
    create_by   varchar(32)  null default '' comment '创建人编号',
    create_name varchar(64)  null default '' comment '创建人姓名',
    update_time datetime     null default null comment '更新时间',
    update_by   varchar(32)  null default '' comment '更新人编号',
    update_name varchar(64)  null default '' comment '更新人姓名',
    primary key (id) using btree
) comment ='oidc客户端';