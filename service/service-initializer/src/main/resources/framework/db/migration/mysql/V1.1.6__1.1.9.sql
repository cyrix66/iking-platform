alter table approve_form_advance_config
    change allow_revoke_pass_approve allow_revoke_passed_approve tinyint default 0 null comment '是否允许撤销审批通过的审批单';

alter table approve_form_advance_config
    change approve_opinion_required approve_comment_required tinyint default 0 null comment '审批意见是否必填';

alter table approve_form_advance_config
    change approve_opinion approve_comment varchar(128) default '' null comment '默认审批意见';

alter table approve_form_advance_config
    change allow_add_approve allow_append_approve tinyint default 0 null comment '是否允许新增审批人';

alter table approve_form_advance_config
    alter column allow_revoke_approving set default 1;

alter table approve_form_advance_config
    change approve_comment approve_comment_missing_tip varchar(128) default '' null comment '审批意见填写提示';

alter table approve_form_instance
    change initiator_time initiate_time datetime null comment '发起时间';

alter table approve_form_advance_config
    add column passed_approve_revoke_expire int null default 30 comment '已通过审批单可撤销最大天数' after allow_revoke_passed_approve;

alter table message_channel_definition
    add content_type varchar(64) default '' null comment '消息内容模板类型' after channel_template_id;

alter table message_channel_definition
    add business_name varchar(128) default '' null comment '业务类型名称' after template_id;

alter table message_channel_definition
    add message_title varchar(64) default '' null comment '消息标题' after business_name;
