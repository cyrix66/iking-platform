create table attachment
(
    id             varchar(32)            not null comment '主键',
    business_id    varchar(32) default '' null comment '业务数据编号',
    business_group varchar(32) default '' null comment '业务附件分组',
    oss_file_id    varchar(32) default '' null comment '文件编号',
    type           varchar(64) default '' null comment '附件类型',
    create_time    datetime               null comment '创建时间',
    create_by      varchar(32) default '' null comment '创建人编号',
    create_name    varchar(64) default '' null comment '创建人姓名',
    update_time    datetime               null comment '更新时间',
    update_by      varchar(32) default '' null comment '更新人编号',
    update_name    varchar(64) default '' null comment '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
)
    comment '附件';
