alter table application
    add link varchar(256) default '' null comment '菜单链接' after icon_background;

alter table application
    add sort_order int default 1 null comment '排序值' after status;

alter table application_page
    add app_id varchar(32) default '' null comment '应用编号' after app_code;

alter table application_page
    modify link varchar(256) default '' null comment '菜单链接';

alter table application_page
    drop column json;

alter table application_page
    drop column render_json;

alter table application_page
    add sidebar tinyint default 0 null comment '是否在侧边栏展示' after link;

alter table application_page
    drop column component;

alter table application_page
    add register_menu tinyint default 0 null comment '是否注册为菜单' after sidebar;

alter table application_page
    add immediately_render tinyint default 0 null comment '是否立即更新渲染JSON' after register_menu;

create table application_page_item
(
    id             varchar(32)             not null comment '主键',
    page_id        varchar(32)  default '' null comment '页面编号',
    page_parent_id varchar(32)  default '' null comment '父页面编号',
    app_code       varchar(64)  default '' null comment '应用标识',
    app_id         varchar(32)  default '' null comment '应用编号',
    icon           varchar(256) default '' null comment '页面图标',
    type           varchar(64)  default '' null comment '页面元素类型',
    link           varchar(256) default '' null comment '页面链接',
    json           json                    null comment '页面JSON',
    render_json    json                    null comment '页面渲染JSON',
    component      varchar(512) default '' null comment '页面组件路径',
    sidebar        tinyint      default 0  null comment '是否侧边栏展示',
    register_menu  tinyint      default 0  null comment '是否注册为菜单',
    sort_order     int          default 1  null comment '页面排序值',
    create_time    datetime                null comment '创建时间',
    create_by      varchar(32)  default '' null comment '创建人编号',
    create_name    varchar(64)  default '' null comment '创建人姓名',
    update_time    datetime                null comment '更新时间',
    update_by      varchar(32)  default '' null comment '更新人编号',
    update_name    varchar(64)  default '' null comment '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
)
    comment '应用页面元素';
