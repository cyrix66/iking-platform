alter table approve_form_advance_config
    add passed_approve_modify_expire int default 30 null comment '修改已通过的审批天数' after allow_modify_passed_approve;

alter table approve_form_view
    drop column process_node_id;

alter table approve_form_widget
    drop column process_node_id;

alter table label
    add business_key varchar(64) default '' null comment '业务标识' after tenant_code;

alter table label
    add business_name varchar(64) default '' null comment '业务名称' after business_key;

drop table division;

rename table platform_config to sys_config;
