package com.ikingtech.platform.service.job.configuration;

import com.ikingtech.framework.sdk.job.scheduler.JobScheduler;
import com.ikingtech.platform.service.job.entity.JobDO;
import com.ikingtech.platform.service.job.service.repository.JobRepository;
import com.ikingtech.platform.service.job.service.JobService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class JobConfiguration {

    @Bean
    public JobRepository jobRepository() {
        return new JobRepository();
    }

    @Bean
    public JobService jobService(JobRepository repo, JobScheduler<JobDO> scheduler) {
        return new JobService(repo, scheduler);
    }
}
