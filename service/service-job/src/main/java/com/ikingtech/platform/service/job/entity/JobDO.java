package com.ikingtech.platform.service.job.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("job")
public class JobDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 2576113804017002503L;

    /**
     * 作业名称
     */
    @TableField("name")
    private String name;

    /**
     * 租户代码
     */
    @TableField("tenant_code")
    private String tenantCode;

    /**
     * 作业类型
     */
    @TableField("type")
    private String type;

    /**
     * 执行器客户端ID
     */
    @TableField("executor_client_id")
    private String executorClientId;

    /**
     * 执行器处理器
     */
    @TableField("executor_handler")
    private String executorHandler;

    /**
     * cron表达式
     */
    @TableField("cron")
    private String cron;

    /**
     * 下一次执行时间
     */
    @TableField("next_time")
    private LocalDateTime nextTime;

    /**
     * 参数
     */
    @TableField("param")
    private String param;

    /**
     * 状态
     */
    @TableField("status")
    private String status;
}
