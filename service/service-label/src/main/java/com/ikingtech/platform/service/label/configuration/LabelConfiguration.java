package com.ikingtech.platform.service.label.configuration;

import com.ikingtech.platform.service.label.service.LabelAssignRepository;
import com.ikingtech.platform.service.label.service.LabelRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class LabelConfiguration {

    @Bean
    public LabelRepository labelRepository() {
        return new LabelRepository();
    }

    @Bean
    public LabelAssignRepository labelAssignRepository() {
        return new LabelAssignRepository();
    }
}
