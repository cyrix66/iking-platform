package com.ikingtech.platform.service.label.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@TableName("label_assign")
@EqualsAndHashCode(callSuper = true)
public class LabelAssignDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -1865838091012034413L;

    @TableField("label_id")
    private String labelId;

    @TableField("business_id")
    private String businessId;
}
