package com.ikingtech.platform.service.label.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@TableName("label")
@EqualsAndHashCode(callSuper = true)
public class LabelDO  extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -7628716592427666L;

    @TableField("name")
    private String name;

    @TableField("tenant_code")
    private String tenantCode;

    @TableField("business_key")
    private String businessKey;

    @TableField("business_name")
    private String businessName;

    @TableLogic
    @TableField("del_flag")
    private Boolean delFlag;
}
