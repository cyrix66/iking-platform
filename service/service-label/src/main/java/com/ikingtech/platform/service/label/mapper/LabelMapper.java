package com.ikingtech.platform.service.label.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.label.entity.LabelDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface LabelMapper extends BaseMapper<LabelDO> {
}
