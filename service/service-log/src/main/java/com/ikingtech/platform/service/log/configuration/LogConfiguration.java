package com.ikingtech.platform.service.log.configuration;

import com.ikingtech.platform.service.log.service.AuthLogRepository;
import com.ikingtech.platform.service.log.service.OperationLogRepository;
import com.ikingtech.platform.service.log.service.SystemLogRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class LogConfiguration {

    @Bean
    public AuthLogRepository authLogRepository() {
        return new AuthLogRepository();
    }

    @Bean
    public OperationLogRepository operationLogRepository() {
        return new OperationLogRepository();
    }

    @Bean
    public SystemLogRepository systemLogRepository() {
        return new SystemLogRepository();
    }
}
