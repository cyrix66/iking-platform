package com.ikingtech.platform.service.log.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 操作日志记录
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "system_log")
public class SystemLogDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 6315937870243240637L;

    @TableField(value = "trace_id")
    private String traceId;

    @TableField(value = "tenant_code")
    private String tenantCode;

    @TableField(value = "package_name")
    private String packageName;

    @TableField(value = "method")
    private String method;

    @TableField(value = "message")
    private String message;

    @TableField(value = "exception_stack")
    private String exceptionStack;

    @TableField(value = "execute_time")
    private LocalDateTime executeTime;
}
