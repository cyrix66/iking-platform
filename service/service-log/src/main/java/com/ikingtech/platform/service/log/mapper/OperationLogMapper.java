package com.ikingtech.platform.service.log.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.log.entity.OperationLogDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface OperationLogMapper extends BaseMapper<OperationLogDO> {
}
