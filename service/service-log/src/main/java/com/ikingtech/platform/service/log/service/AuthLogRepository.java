package com.ikingtech.platform.service.log.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.log.model.AuthLogQueryParamDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.log.entity.AuthLogDO;
import com.ikingtech.platform.service.log.mapper.AuthLogMapper;

/**
 * @author tie yan
 */
public class AuthLogRepository extends ServiceImpl<AuthLogMapper, AuthLogDO> {

    public static LambdaQueryWrapper<AuthLogDO> createWrapper(AuthLogQueryParamDTO queryParam) {
        return Wrappers.<AuthLogDO>lambdaQuery()
                .like(Tools.Str.isNotBlank(queryParam.getUserId()), AuthLogDO::getUserId, queryParam.getUserId())
                .like(Tools.Str.isNotBlank(queryParam.getUsername()), AuthLogDO::getUsername, queryParam.getUsername())
                .like(Tools.Str.isNotBlank(queryParam.getIp()), AuthLogDO::getIp, queryParam.getIp())
                .like(Tools.Str.isNotBlank(queryParam.getOs()), AuthLogDO::getOs, queryParam.getOs())
                .like(Tools.Str.isNotBlank(queryParam.getBrowser()), AuthLogDO::getBrowser, queryParam.getBrowser())
                .eq(Tools.Str.isNotBlank(queryParam.getType()), AuthLogDO::getType, queryParam.getType())
                .like(Tools.Str.isNotBlank(queryParam.getMessage()), AuthLogDO::getMessage,queryParam.getMessage())
                .like(Tools.Str.isNotBlank(queryParam.getLocation()), AuthLogDO::getMessage,queryParam.getLocation())
                .eq(null != queryParam.getSuccess(), AuthLogDO::getSuccess, queryParam.getSuccess())
                .ge(null != queryParam.getSignStartTime(), AuthLogDO::getSignTime, queryParam.getSignStartTime())
                .le(null != queryParam.getSignEndTime(), AuthLogDO::getSignTime, queryParam.getSignEndTime())
                .orderByDesc(AuthLogDO::getCreateTime);
    }
}
