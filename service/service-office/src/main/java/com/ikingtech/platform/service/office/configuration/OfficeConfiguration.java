package com.ikingtech.platform.service.office.configuration;

import com.ikingtech.framework.sdk.office.api.AffixFileApi;
import com.ikingtech.framework.sdk.office.api.OfficeDataApi;
import com.ikingtech.framework.sdk.office.api.OfficeDataCustomApi;
import com.ikingtech.framework.sdk.office.properties.OfficeProperties;
import com.ikingtech.framework.sdk.oss.api.OssApi;
import com.ikingtech.framework.sdk.user.api.UserApi;
import com.ikingtech.platform.service.office.mapper.OfficeDataCustomBranchLogicMapper;
import com.ikingtech.platform.service.office.mapper.OfficeDataFieldsMapper;
import com.ikingtech.platform.service.office.mapper.OfficeDataParamsMapper;
import com.ikingtech.platform.service.office.mapper.OfficeTempRecordMapper;
import com.ikingtech.platform.service.office.service.*;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class OfficeConfiguration {

    @Bean
    public OfficeTempService officeTempService(OssApi ossApi, AffixFileApi affixFileApi, OfficeDataApi officeDataApi, OfficeDataCustomApi officeDataCustomApi, OfficeTempRecordMapper officeTempRecordMapper) {
        return new OfficeTempService(ossApi, affixFileApi, officeDataApi, officeDataCustomApi, officeTempRecordMapper);
    }

    @Bean
    public OfficeDataCustomBranchLogicService officeDataCustomBranchLogicService() {
        return new OfficeDataCustomBranchLogicService();
    }

    @Bean
    public OfficeDataCustomBranchService officeDataCustomBranchService(OfficeDataCustomBranchLogicService officeDataCustomBranchLogicService) {
        return new OfficeDataCustomBranchService(officeDataCustomBranchLogicService);
    }

    @Bean
    public OfficeDataCustomFieldsParamsService officeDataCustomFieldsParamsService() {
        return new OfficeDataCustomFieldsParamsService();
    }

    @Bean
    public OfficeDataCustomFieldsService officeDataCustomFieldsService(OfficeDataManagementService officeDataManagementService, OfficeDataCustomBranchService officeDataCustomBranchService, OfficeDataCustomFieldsParamsService officeDataCustomFieldsParamsService, OfficeDataCustomBranchLogicMapper officeDataCustomBranchLogicMapper) {
        return new OfficeDataCustomFieldsService(officeDataManagementService, officeDataCustomBranchService, officeDataCustomFieldsParamsService, officeDataCustomBranchLogicMapper);
    }

    @Bean
    @ConditionalOnMissingBean({OfficeDataCustomManagementService.class})
    public OfficeDataCustomManagementService officeDataCustomManagementService(OfficeDataCustomFieldsService officeDataCustomFieldsService) {
        return new OfficeDataCustomManagementService(officeDataCustomFieldsService);
    }

    @Bean
    @ConditionalOnMissingBean({OfficeDataManagementService.class})
    public OfficeDataManagementService officeDataManagementService(OfficeDataFieldsMapper officeDataFieldsMapper, OfficeDataParamsMapper officeDataParamsMapper, OfficeProperties officeProperties) {
        return new OfficeDataManagementService(officeDataFieldsMapper, officeDataParamsMapper, officeProperties);
    }

    @Bean
    public OfficeDataParamsTypeService officeDataParamsTypeService() {
        return new OfficeDataParamsTypeService();
    }

    @Bean
    public AffixFileService affixFileService() {
        return new AffixFileService();
    }

    @Bean
    public DocumentHistoryService documentHistoryService(OfficeTempService officeTempService,UserApi userApi,AffixFileApi affixFileApi) {
        return new DocumentHistoryService(officeTempService,userApi,affixFileApi);
    }
}
