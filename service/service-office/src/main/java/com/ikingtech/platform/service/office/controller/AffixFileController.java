package com.ikingtech.platform.service.office.controller;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.office.api.AffixFileApi;
import com.ikingtech.framework.sdk.office.model.dto.AffixFileDto;
import com.ikingtech.framework.sdk.office.model.vo.AffixFileVo;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.platform.service.office.entity.AffixFile;
import com.ikingtech.platform.service.office.service.AffixFileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@ApiController(value = "/common/affix/file", name = "附件", description = "附件")
public class AffixFileController implements AffixFileApi {

    private final AffixFileService affixFileService;

    /**
     * 添加
     *
     * @param dto dto
     * @return 执行结果
     * @author lqb
     */
    @Override
    public R<String> add(AffixFileDto dto) {
        return affixFileService.add(dto);
    }

    /**
     * 删除
     *
     * @param id id
     * @return 执行结果
     * @author lqb
     */
    @Override
    public R<String> delete(String id) {
        return affixFileService.delete(id);
    }

    /**
     * 由业务id查询
     *
     * @param businessId 业务标识
     * @return 执行结果
     * @author lqb
     */
    @Override
    public R<List<AffixFileVo>> queryByBusinessId(String businessId) {
        return affixFileService.queryByBusinessId(businessId);
    }

    /**
     * 批量新增
     *
     * @param affixFiles 签署文件
     * @return 执行结果
     * @author lqb
     */
    @Override
    public R<String> addBatch(List<AffixFileDto> affixFiles) {
        return affixFileService.addBatch(affixFiles);
    }

    /**
     * 删除批处理
     *
     * @param ids id
     * @return 执行结果
     * @author lqb
     */
    @Override
    public R<String> deleteBatch(List<String> ids) {
        return affixFileService.deleteBatch(ids);
    }

    /**
     * 由业务id删除
     *
     * @param businessId 业务标识
     * @return 执行结果
     * @author lqb
     */
    @Override
    public R<String> deleteByBusinessId(String businessId) {
        return affixFileService.deleteByBusinessId(businessId);
    }

    /**
     * 详情
     *
     * @param id id
     * @return 执行结果
     * @author lqb
     */
    @Override
    public R<AffixFileVo> detail(String id) {
        AffixFile file = affixFileService.getById(id);
        if (ObjectUtils.isEmpty(file)) {
            return R.failed("附件不存在");
        }
        return R.ok(file.copy(new AffixFileVo()));
    }

    /**
     * 根据业务ids查询所有文件
     *
     * @param businessIds 业务ids
     * @return 执行结果
     */
    @Override
    public R<Map<String, List<AffixFileVo>>> mapByBusinessIds(List<String> businessIds) {
        return R.ok(this.affixFileService.mapByBusinessIds(businessIds));
    }

    /**
     * 根绝业务ids删除所有文件
     *
     * @param businessIds 业务ids
     * @return 执行结果
     */
    @Override
    public R<String> deleteBatchBusinessIds(List<String> businessIds) {
        if (CollUtil.isNotEmpty(businessIds)) {
            this.affixFileService.remove(new LambdaQueryWrapper<AffixFile>().in(AffixFile::getBusinessId, businessIds));
        }
        return R.ok();
    }
}
