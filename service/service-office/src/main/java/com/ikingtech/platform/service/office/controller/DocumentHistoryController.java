package com.ikingtech.platform.service.office.controller;


import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.office.api.DocumentHistoryApi;
import com.ikingtech.framework.sdk.office.model.dto.OfficeVersionDTO;
import com.ikingtech.framework.sdk.office.model.vo.DocumentHistoryVO;
import com.ikingtech.framework.sdk.office.model.vo.HistoryDataVO;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.platform.service.office.service.DocumentHistoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;

/**
 * <p>
 * office-历史版本
 * </p>
 *
 * @author zhangpan
 */
@Slf4j
@Validated
@RequiredArgsConstructor
@ApiController(value = "/office/history", name = "office-历史版本", description = "office-历史版本")
public class DocumentHistoryController implements DocumentHistoryApi {

    private  final DocumentHistoryService documentHistoryService;

    /**
     * 根据文档模板ID获取所有的版本历史
     * @param id
     * @return
     */
    @Override
    public R<DocumentHistoryVO> getDocumentHistoryByTmpId(String id){
        return   R.ok(documentHistoryService.getDocumentHistoryByTmpId(id));
    }

    /**
     * 获取版本信息
     * @param officeRestoreVersionDTO
     * @return
     */
    public  R<HistoryDataVO> getVersionInfo(OfficeVersionDTO officeRestoreVersionDTO){
        return R.ok(this.documentHistoryService.getVersionInfo(officeRestoreVersionDTO.getId(),officeRestoreVersionDTO.getVersion()));
    }
}
