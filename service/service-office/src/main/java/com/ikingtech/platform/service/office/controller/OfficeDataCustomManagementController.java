package com.ikingtech.platform.service.office.controller;

import cn.hutool.json.JSONObject;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.office.api.OfficeDataCustomApi;
import com.ikingtech.framework.sdk.office.model.dto.OfficeDataCustomFieldsDto;
import com.ikingtech.framework.sdk.office.model.dto.OfficeDataCustomManagementDto;
import com.ikingtech.framework.sdk.office.model.dto.OfficeDataParamsTypeDto;
import com.ikingtech.framework.sdk.office.model.enums.LogicOperator;
import com.ikingtech.framework.sdk.office.model.query.OfficeDataManagementQuery;
import com.ikingtech.framework.sdk.office.model.vo.OfficeDataCustomFieldsVo;
import com.ikingtech.framework.sdk.office.model.vo.OfficeDataCustomManagementVo;
import com.ikingtech.framework.sdk.office.model.vo.OfficeDataParamsTypeVo;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.platform.service.office.entity.OfficeDataCustomFields;
import com.ikingtech.platform.service.office.entity.OfficeDataCustomManagement;
import com.ikingtech.platform.service.office.entity.OfficeDataParamsType;
import com.ikingtech.platform.service.office.service.OfficeDataCustomFieldsService;
import com.ikingtech.platform.service.office.service.OfficeDataCustomManagementService;
import com.ikingtech.platform.service.office.service.OfficeDataParamsTypeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.annotation.Validated;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 办公室数据自定义管理控制器
 *
 * @author lqb
 */
@Slf4j
@Validated
@RequiredArgsConstructor
@ApiController(value = "/office/data/custom", name = "office-自定义数据管理", description = "office-自定义据管理")
public class OfficeDataCustomManagementController implements OfficeDataCustomApi {

    private final OfficeDataCustomManagementService officeDataCustomManagementService;
    private final OfficeDataCustomFieldsService officeDataCustomFieldsService;
    private final OfficeDataParamsTypeService officeDataParamsTypeService;

    /**
     * 新增分组
     *
     * @param dto 提交参数
     * @return 执行结果
     * @author lqb
     */
    @Override
    public R<String> managementAdd(OfficeDataCustomManagementDto dto) {
        return this.officeDataCustomManagementService.add(dto);
    }

    /**
     * 修改分组
     *
     * @param dto 提交参数
     * @return 执行结果
     * @author lqb
     */
    @Override
    public R<String> managementUpdate(OfficeDataCustomManagementDto dto) {
        OfficeDataCustomManagement management = this.officeDataCustomManagementService.getById(dto.getId());
        if (ObjectUtils.isEmpty(management)) {
            throw new FrameworkException("分组数据不存在");
        }
        return this.officeDataCustomManagementService.update(dto);
    }

    /**
     * 分组列表
     *
     * @return 执行结果
     * @author lqb
     */
    @Override
    public R<List<OfficeDataCustomManagementVo>> managementList(OfficeDataManagementQuery query) {
        // 默认查询公共参数
        query.getBusinessTypeList().add("COMMON");
        return this.officeDataCustomManagementService.queryList(query);
    }

    /**
     * 删除分组
     *
     * @param id id
     * @return 执行结果
     * @author lqb
     */
    @Override
    public R<String> managementDelete(String id) {
        return this.officeDataCustomManagementService.delete(id);
    }

    /**
     * 新增自定义字段
     *
     * @param dto 提交参数
     * @return 执行结果
     * @author lqb
     */
    @Override
    public R<String> fieldsAdd(OfficeDataCustomFieldsDto dto) {
        OfficeDataCustomManagement management = this.officeDataCustomManagementService.getById(dto.getOfficeDataCustomManagementId());
        if (ObjectUtils.isEmpty(management)) {
            throw new FrameworkException("自定义分组管理不存在");
        }
        OfficeDataCustomFields field = this.officeDataCustomFieldsService.lambdaQuery()
                .eq(OfficeDataCustomFields::getOfficeDataCustomManagementId, dto.getOfficeDataCustomManagementId())
                .eq(OfficeDataCustomFields::getFieldName, dto.getFieldName())
                .one();
        if (ObjectUtils.isNotEmpty(field)) {
            throw new FrameworkException("该分组下已存在：" + dto.getFieldName() + "字段");
        }
        return this.officeDataCustomFieldsService.add(dto);
    }

    /**
     * 字段编辑
     *
     * @param dto 提交参数
     * @return 执行结果
     * @author lqb
     */
    @Override
    public R<String> fieldsUpdate(OfficeDataCustomFieldsDto dto) {
        OfficeDataCustomFields fields = this.officeDataCustomFieldsService.getById(dto.getId());
        if (ObjectUtils.isEmpty(fields)) {
            throw new FrameworkException("该字段不存在，无法编辑");
        }
        if (StringUtils.isNotBlank(dto.getOfficeDataCustomManagementId())) {
            OfficeDataCustomManagement management = this.officeDataCustomManagementService.getById(dto.getOfficeDataCustomManagementId());
            if (ObjectUtils.isEmpty(management)) {
                throw new FrameworkException("自定义分组管理不存在");
            }
        }
        return this.officeDataCustomFieldsService.update(dto);
    }

    /**
     * 字段详情
     *
     * @param id id
     * @return 执行结果
     * @author lqb
     */
    @Override
    public R<OfficeDataCustomFieldsVo> fieldsDetail(String id) {
        return R.ok(this.officeDataCustomFieldsService.detail(id));
    }

    /**
     * 字段删除
     *
     * @param id id
     * @return 执行结果
     * @author lqb
     */
    @Override
    public R<String> fieldsDelete(String id) {
        return this.officeDataCustomFieldsService.delete(id);
    }

    /**
     * 预览自定义数据
     *
     * @param id     id
     * @param params params
     * @return 执行结果
     * @author lqb
     */
    @Override
    public R<Object> customPreview(String id, Map<String, Object> params) {
        return this.officeDataCustomFieldsService.preview(id, params);
    }

    /**
     * 自定义逻辑运算符
     *
     * @return 执行结果
     * @author lqb
     */
    @Override
    public R<List<LogicOperator>> customLogicOperator() {
        return R.ok(Arrays.stream(LogicOperator.LogicOperatorEnum.values())
                .map(LogicOperator.LogicOperatorEnum::toLogicOperator)
                .collect(Collectors.toList()));
    }

    /**
     * 自定义参数类型
     *
     * @return 执行结果
     */
    @Override
    public R<List<OfficeDataParamsTypeVo>> customParamsType() {
        return R.ok(this.officeDataParamsTypeService.list()
                .stream().map(type -> type.copy(new OfficeDataParamsTypeVo()))
                .collect(Collectors.toList()));
    }

    /**
     * 根据业务类型 获取分组管理信息
     *
     * @param businessType a
     * @return 执行结果
     */
    @Override
    public R<List<OfficeDataCustomManagementVo>> managementListByBusinessType(String businessType) {
        OfficeDataManagementQuery query = new OfficeDataManagementQuery();
        query.getBusinessTypeList().add(businessType);
        return this.managementList(query);
    }

    /**
     * 复制
     *
     * @param id id
     * @return 执行结果
     */
    @Override
    public R<String> copy(String id) {
        return this.officeDataCustomFieldsService.copy(id);
    }

    /**
     * 新增参数类型
     *
     * @param dto 新增参数类型
     * @return 执行结果
     */
    @Override
    public R<String> paramsTypeAdd(OfficeDataParamsTypeDto dto) {
        OfficeDataParamsType paramsType = dto.copy(new OfficeDataParamsType());
        paramsType.setId(Tools.Id.uuid());
        paramsType.insert();
        return R.ok();
    }

    /**
     * 修改参数类型
     *
     * @param dto 修改参数类型
     * @return 执行结果
     */
    @Override
    public R<String> paramsTypeUpdate(OfficeDataParamsTypeDto dto) {
        OfficeDataParamsType type = this.officeDataParamsTypeService.getById(dto.getId());
        if (type == null) {
            throw new FrameworkException("参数类型不存在");
        }
        OfficeDataParamsType paramsType = dto.copy(new OfficeDataParamsType());
        paramsType.updateById();
        return R.ok();
    }

    /**
     * 删除参数类型
     *
     * @param id id
     * @return 执行结果
     */
    @Override
    public R<String> paramsTypeDelete(String id) {
        this.officeDataParamsTypeService.removeById(id);
        return R.ok();
    }

}
