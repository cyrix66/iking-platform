package com.ikingtech.platform.service.office.controller;


import cn.hutool.core.bean.BeanUtil;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.office.api.OfficeDataApi;
import com.ikingtech.framework.sdk.office.model.dto.OfficeDataManagementDto;
import com.ikingtech.framework.sdk.office.model.enums.OfficeDataEnums;
import com.ikingtech.framework.sdk.office.model.query.OfficeDataManagementQuery;
import com.ikingtech.framework.sdk.office.model.vo.OfficeDataAllVo;
import com.ikingtech.framework.sdk.office.model.vo.OfficeDataCustomManagementVo;
import com.ikingtech.framework.sdk.office.model.vo.OfficeDataFieldsAllVo;
import com.ikingtech.framework.sdk.office.model.vo.OfficeDataManagementVo;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.platform.service.office.service.OfficeDataCustomManagementService;
import com.ikingtech.platform.service.office.service.OfficeDataManagementService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * <p>
 * 办公文件管理-基础数据管理 前端控制器
 * </p>
 *
 * @author lqb
 */
@Slf4j
@Validated
@RequiredArgsConstructor
@ApiController(value = "/office/data", name = "office-基础数据管理", description = "office-基础数据管理")
public class OfficeDataManagementController implements OfficeDataApi {

    private final OfficeDataManagementService officeDataManagementService;
    private final OfficeDataCustomManagementService officeDataCustomManagementService;

    /**
     * 新增基础数据
     *
     * @author lqb
     */
    @Override
    public R<String> baseAdd(OfficeDataManagementDto dto) {
        if (OfficeDataEnums.ResourceType.API.equals(dto.getResourceType()) &&
                ObjectUtils.isEmpty(dto.getApiMethod())) {
            throw new FrameworkException("请求方法不能为空");
        }
        return officeDataManagementService.add(dto);
    }

    /**
     * 修改基础数据
     *
     */
    @Override
    public R<String> baseUpdate(OfficeDataManagementDto dto) {
        if (OfficeDataEnums.ResourceType.API.equals(dto.getResourceType()) &&
                ObjectUtils.isEmpty(dto.getApiMethod())) {
            throw new FrameworkException("请求方法不能为空");
        }
        return officeDataManagementService.update(dto);
    }

    /**
     * 根据归属模块查询数据（包含公共模块数据）
     *
     */
    @Override
    public R<List<OfficeDataManagementVo>> baseQueryList(
            OfficeDataManagementQuery query) {
        // 默认查询公共参数
        query.getBusinessTypeList().add("COMMON");
        return officeDataManagementService.queryList(query);
    }

    /**
     * 基础详情
     *
     */
    @Override
    public R<OfficeDataManagementVo> baseDetails(String id) {
        return officeDataManagementService.details(id);
    }

    /**
     * 基本数据预览
     *
     */
    @Override
    public R<Object> basePreview(String id, Map<String, Object> params) {
        return officeDataManagementService.preview(id, params);
    }

    /**
     * 全部
     *
     * @param query 查询参数
     * @return 执行结果
     */
    @Override
    public R<List<OfficeDataAllVo>> all(OfficeDataManagementQuery query) {
        // 默认查询公共参数
        query.getBusinessTypeList().add("COMMON");
        List<OfficeDataAllVo> list = new ArrayList<>();
        // 基础数据
        List<OfficeDataManagementVo> baseDataList = this.officeDataManagementService.queryList(query).getData();
        for (OfficeDataManagementVo management : baseDataList) {
            list.add(BeanUtil.copyProperties(management, OfficeDataAllVo.class, "fields")
                    .setManagementId(management.getId())
                    .setFields(management.getFields().stream().map(f ->
                            BeanUtil.copyProperties(f, OfficeDataFieldsAllVo.class, "fieldType")
                                    .setName(f.getFieldName())
                                    .setManagementId(f.getDataBaseManagementId())
                                    .setFieldType(OfficeDataEnums.FieldType.BASE)
                    ).collect(Collectors.toList()))
                    .setFieldType(OfficeDataEnums.FieldType.BASE));
        }
        // 自定义数据
        List<OfficeDataCustomManagementVo> customList = this.officeDataCustomManagementService.queryList(query).getData();
        for (OfficeDataCustomManagementVo management : customList) {
            list.add(BeanUtil.copyProperties(management, OfficeDataAllVo.class, "fields")
                    .setManagementId(management.getId())
                    .setFields(management.getFields().stream().map(f ->
                            BeanUtil.copyProperties(f, OfficeDataFieldsAllVo.class, "fieldType")
                                    .setName(f.getFieldName())
                                    .setManagementId(f.getOfficeDataCustomManagementId())
                                    .setFieldType(OfficeDataEnums.FieldType.CUSTOM)
                    ).collect(Collectors.toList()))
                    .setFieldType(OfficeDataEnums.FieldType.CUSTOM));
        }
        return R.ok(list);
    }
}

