package com.ikingtech.platform.service.office.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.jwt.JWTUtil;
import cn.hutool.jwt.signers.JWTSigner;
import cn.hutool.jwt.signers.JWTSignerUtil;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.office.api.OfficeTempApi;
import com.ikingtech.framework.sdk.office.model.dto.*;
import com.ikingtech.framework.sdk.office.model.vo.OfficeTempRecordVo;
import com.ikingtech.framework.sdk.office.model.vo.OfficeTempVo;
import com.ikingtech.framework.sdk.office.properties.OfficeProperties;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.platform.service.office.entity.OfficeTemp;
import com.ikingtech.platform.service.office.service.OfficeTempService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.Map;


/**
 * <p>
 * office-模板 前端控制器
 * </p>
 *
 * @author lqb
 * @since 2023-09-22 11:34:02
 */
@Slf4j
@Validated
@RequiredArgsConstructor
@ApiController(value = "/office/temp", name = "office-模板", description = "office-模板")
public class OfficeTempController implements OfficeTempApi {

    private final OfficeTempService officeTempService;

    private final OfficeProperties officeProperties;

    @Override
    public R<String> add(OfficeTempDto docTemplate) {
        return officeTempService.add(docTemplate);
    }

    @Override
    public R<String> update(OfficeTempDto docTemplate) {
        return null;
    }

    @Override
    public R<OfficeTempVo> detail(String id) {
        OfficeTemp temp = this.officeTempService.getById(id);
        if (ObjectUtils.isEmpty(temp)) {
            return R.failed("模板不存在");
        }
        return this.officeTempService.detail(temp);
    }

    @Override
    public R<List<OfficeTempRecordVo>> records(String id) {
        return R.ok(officeTempService.records(id));
    }

    @Override
    public R<OfficeTempRecordVo> getEnableOnlyOffice(String id) {
        return R.ok(officeTempService.getEnableOnlyOffice(id));
    }

    /**
     * onlyOffice 保存回调
     *
     */
    @Override
    public R<String> onlyOfficeCallBack(String id, OfficeTempFileDto dto) {
        String ossId="";
        // 手动保存才算
        if (dto.getStatus() == 6||dto.getStatus() == 2) {
           ossId=officeTempService.onlyOfficeCallBack(id, dto);
        }
        return R.ok(ossId);
    }

    /**
     * 生成office文件
     *
     */
    @Override
    public R<String> make(OfficeMakeDto dto) {
        OfficeTemp temp = this.officeTempService.getById(dto.getOfficeTempId());
        if (ObjectUtils.isEmpty(temp)) {
            throw new FrameworkException("office模板不存在");
        }
        return this.officeTempService.make(temp, dto);
    }

    /**
     * 只获取办公令牌
     *
     * @param dto 到
     * @return 执行结果
     */
    @Override
    public R<String> getOnlyOfficeToken(OfficeTokenDto dto) {
        JWTSigner jwtSigner = JWTSignerUtil.hs256(this.officeProperties.getJwtSecret().getBytes());
        Map<String, Object> stringObjectMap = BeanUtil.beanToMap(dto);
        return R.ok(JWTUtil.createToken(stringObjectMap, jwtSigner));
    }

    /**
     * 复制模板
     *
     * @param id id
     * @return 执行结果
     */
    @Override
    public R<String> copy(String id) {
        OfficeTemp temp = this.officeTempService.getById(id);
        if (ObjectUtils.isEmpty(temp)) {
            throw new FrameworkException("office模板不存在");
        }
        return this.officeTempService.copy(temp);
    }

    /**
     * 删除
     *
     * @param id id
     * @return 执行结果
     */
    @Override
    public R<String> delete(String id) {
        return this.officeTempService.delete(id);
    }

    /**
     * 设置当前版本为最新版本
     * @param id 版本ID
     * @return
     */
    @Override
    public R<String> setDocumentVersion(String id ){
        return this.officeTempService.setDocumentVersion(id);
    }


    /**
     * 还原版本
     * @param officeVersionDTO
     * @return
     */
    public  R<String> restoreVersion(OfficeVersionDTO officeVersionDTO){
        return this.officeTempService.restoreVersion(officeVersionDTO.getId(),officeVersionDTO.getVersion());
    }


    /**
     * 新建模板时复制原有模板
     * @param id
     * @return
     */
    @Override
    public R<String> copyTemp(String id) {
        OfficeTemp temp = this.officeTempService.getById(id);
        if (ObjectUtils.isEmpty(temp)) {
            throw new FrameworkException("office模板不存在");
        }
        return this.officeTempService.copyTemp(temp);
    }

    public R<OfficeTempRecordVo> getLastDocVersion(String id){
        OfficeTempRecordVo officeTempRecord = new OfficeTempRecordVo();
        BeanUtil.copyProperties(officeTempService.getLastDocVersion(id),officeTempRecord,true);
        return R.ok(officeTempRecord);
    }

}

