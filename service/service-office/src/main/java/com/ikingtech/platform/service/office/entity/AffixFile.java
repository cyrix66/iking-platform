package com.ikingtech.platform.service.office.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.ModelEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
@TableName("affix_file")
@EqualsAndHashCode(callSuper = true)
@Schema(name = "AffixFile", description = "附件")
public class AffixFile extends ModelEntity<AffixFile> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "ossFileId", description = "oss文件id")
    @TableField("oss_file_id")
    private String ossFileId;

    @Schema(name = "businessId", description = "归属业务id")
    @TableField("business_id")
    private String businessId;

    @Schema(name = "originName", description = "原始文件名称")
    @TableField("origin_name")
    private String originName;

    @Schema(name = "tenantCode", description = "租户标识")
    @TableField("tenant_code")
    private String tenantCode;

    @Schema(name = "fileSize", description = "文件大小")
    @TableField("file_size")
    private String fileSize;

    @Schema(name = "suffix", description = "文件后缀")
    @TableField("suffix")
    private String suffix;

    @Schema(name = "url", description = "文件访问相对路径")
    @TableField("url")
    private String url;

    @Schema(name = "dirName", description = "文件服务器目录")
    @TableField("dir_name")
    private String dirName;

    @Schema(name = "path", description = "文件服务器路径")
    @TableField("path")
    private String path;

    @Schema(name = "md5", description = "文件md5")
    @TableField("md5")
    private String md5;

    @Schema(name = "sortOrder", description = "排序值")
    @TableField("sort_order")
    private Integer sortOrder;

    @Schema(name = "delFlag", description = "是否已删除")
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    @TableLogic
    private Boolean delFlag;


    @Override
    public Serializable pkVal() {
        return this.getId();
    }

    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }
}
