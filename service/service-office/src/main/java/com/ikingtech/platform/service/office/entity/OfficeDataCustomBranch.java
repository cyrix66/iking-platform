package com.ikingtech.platform.service.office.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.ModelEntity;
import com.ikingtech.framework.sdk.office.model.enums.ConditionsEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * <p>
 * office-自定义条件分支
 * </p>
 *
 * @author lqb
 * @since 2023-10-13 09:28:43
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("office_data_custom_branch")
@Schema(name = "OfficeDataCustomBranch", description = "office-自定义条件分支")
public class OfficeDataCustomBranch extends ModelEntity<OfficeDataCustomBranch> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "officeDataCustomFieldsId", description = "自定义字段id")
    @TableField("office_data_custom_fields_id")
    private String officeDataCustomFieldsId;

    @Schema(name = "branchName", description = "分支名称")
    @TableField("branch_name")
    private String branchName;

    @Schema(name = "conditions", description = "条件关系 AND-满足所有 OR-满足任意")
    @TableField("conditions")
    private ConditionsEnum conditions;

    @Schema(name = "conditionStatement", description = "条件关系表达式")
    @TableField("condition_statement")
    private String conditionStatement;

    @Schema(name = "logicExpression", description = "函数文本表达式")
    @TableField("logic_expression")
    private String logicExpression;

    @Schema(name = "sortOrder", description = "排序值")
    @TableField("sort_order")
    private Integer sortOrder;

    @Override
    public Serializable pkVal() {
        return this.getId();
    }


    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }
}
