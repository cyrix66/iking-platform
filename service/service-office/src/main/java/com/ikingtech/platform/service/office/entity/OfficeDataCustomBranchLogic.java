package com.ikingtech.platform.service.office.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.ModelEntity;
import com.ikingtech.framework.sdk.office.model.enums.LogicOperator;
import com.ikingtech.framework.sdk.office.model.enums.OfficeDataEnums;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * <p>
 * office-自定义分支逻辑运算
 * </p>
 *
 * @author lqb
 * @since 2023-10-13 09:28:43
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("office_data_custom_branch_logic")
@Schema(name = "OfficeDataCustomBranchLogic", description = "office-自定义分支逻辑运算")
public class OfficeDataCustomBranchLogic extends ModelEntity<OfficeDataCustomBranchLogic> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "officeDataCustomFieldsId", description = "自定义字段id")
    @TableField("office_data_custom_fields_id")
    private String officeDataCustomFieldsId;

    @Schema(name = "officeDataCustomBranchId", description = "自定义分支id")
    @TableField("office_data_custom_branch_id")
    private String officeDataCustomBranchId;

    @Schema(name = "leftType", description = "运算符左边参数类型 SYSTEM-系统参数 CUSTOM-自定义")
    @TableField("left_type")
    private OfficeDataEnums.ParamsType leftType;

    @Schema(name = "leftParam", description = "运算符左边参数")
    @TableField("left_param")
    private String leftParam;

    @Schema(name = "logicOperator", description = "运算符 LESS-小于 LESS_EQUAL-小于等于 GREATER-大于 GREATER_EQUAL-大于等于 EQUAL-等于 BETWEEN-介于 IN-在范围内")
    @TableField("logic_operator")
    private LogicOperator.LogicOperatorEnum logicOperator;

    @Schema(name = "rightType", description = "运算符右边参数类型 SYSTEM-系统参数 CUSTOM-自定义")
    @TableField("right_type")
    private OfficeDataEnums.ParamsType rightType;

    @Schema(name = "rightParam", description = "运算符右边参数")
    @TableField("right_param")
    private String rightParam;

    @Schema(name = "sortOrder", description = "排序值")
    @TableField("sort_order")
    private Integer sortOrder;


    @Override
    public Serializable pkVal() {
        return this.getId();
    }


    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }
}
