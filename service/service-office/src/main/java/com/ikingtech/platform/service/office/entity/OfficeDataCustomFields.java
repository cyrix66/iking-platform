package com.ikingtech.platform.service.office.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.ModelEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * <p>
 * office-自定义字段
 * </p>
 *
 * @author lqb
 * @since 2023-10-13 09:28:43
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("office_data_custom_fields")
@Schema(name = "OfficeDataCustomFields", description = "office-自定义字段")
public class OfficeDataCustomFields extends ModelEntity<OfficeDataCustomFields> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "officeDataCustomManagementId", description = "自定义数据管理id")
    @TableField("office_data_custom_management_id")
    private String officeDataCustomManagementId;

    @Schema(name = "fieldName", description = "字段名称")
    @TableField("field_name")
    private String fieldName;

    @Schema(name = "fieldKey", description = "key")
    @TableField("field_key")
    private String fieldKey;

    @Schema(name = "aviatorScript", description = "逻辑文本表达式翻译的可执行aviator执行脚本")
    @TableField("aviator_script")
    private String aviatorScript;

    @Schema(name = "remark", description = "备注")
    @TableField("remark")
    private String remark;

    @Schema(name = "sortOrder", description = "排序值")
    @TableField("sort_order")
    private Integer sortOrder;


    @Override
    public Serializable pkVal() {
        return this.getId();
    }


    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }
}
