package com.ikingtech.platform.service.office.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.ModelEntity;
import com.ikingtech.framework.sdk.office.model.enums.OfficeDataEnums;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * <p>
 * office-自定义字段参数
 * </p>
 *
 * @author lqb
 * @since 2023-10-13 09:28:43
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("office_data_custom_fields_params")
@Schema(name = "OfficeDataCustomFieldsParams", description = "office-自定义字段参数")
public class OfficeDataCustomFieldsParams extends ModelEntity<OfficeDataCustomFieldsParams> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "officeDataCustomFieldsId", description = "自定义字段id")
    @TableField("office_data_custom_fields_id")
    private String officeDataCustomFieldsId;

    @Schema(name = "fieldType", description = "字段类型 BASE-基础参数 CUSTOM-自定义")
    @TableField("field_type")
    private OfficeDataEnums.FieldType fieldType;

    @Schema(name = "officeDataManagementId", description = "系统基础数据管理id BASE时 必填")
    @TableField("office_data_management_id")
    private String officeDataManagementId;

    @Schema(name = "fieldId", description = "字段id")
    @TableField("field_id")
    private String fieldId;

    @Schema(name = "fieldKey", description = "字段key")
    @TableField("field_key")
    private String fieldKey;

    @Schema(name = "fieldName", description = "字段名称")
    @TableField("field_name")
    private String fieldName;

    @Schema(name = "params", description = "筛选参数 例如：#deptId=222&name=陕煤")
    @TableField("params")
    private String params;


    @Override
    public Serializable pkVal() {
        return this.getId();
    }


    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }
}
