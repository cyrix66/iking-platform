package com.ikingtech.platform.service.office.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.ModelEntity;
import com.ikingtech.framework.sdk.office.model.enums.OfficeDataEnums;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * <p>
 * office-自定义数据管理
 * </p>
 *
 * @author lqb
 * @since 2023-10-13 09:28:43
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("office_data_custom_management")
@Schema(name = "OfficeDataCustomManagement", description = "office-自定义数据管理")
public class OfficeDataCustomManagement extends ModelEntity<OfficeDataCustomManagement> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "businessType", description = "数据所属业务模块")
    @TableField("business_type")
    private String businessType;

    @Schema(name = "name", description = "分组名称")
    @TableField("name")
    private String name;

    @Schema(name = "resultType", description = "返回类型 OBJECT-对象 LIST-列表集合")
    @TableField("result_type")
    private OfficeDataEnums.ResultType resultType;

    @Schema(name = "sortOrder", description = "排序值")
    @TableField("sort_order")
    private Integer sortOrder;


    @Override
    public Serializable pkVal() {
        return this.getId();
    }


    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }
}
