package com.ikingtech.platform.service.office.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.ModelEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * <p>
 * 办公文件管理-基础数据api相应字段
 * </p>
 *
 * @author lqb
 * @since 2023-09-19 03:01:06
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("office_data_fields")
@Schema(name = "OfficeDataFields", description = "办公文件管理-基础数据api相应字段")
public class OfficeDataFields extends ModelEntity<OfficeDataFields> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "dataBaseManagementId", description = "数据管理id")
    @TableField("data_base_management_id")
    private String dataBaseManagementId;

    @Schema(name = "fieldName", description = "字段名称")
    @TableField("field_name")
    private String fieldName;

    @Schema(name = "fieldKey", description = "key")
    @TableField("field_key")
    private String fieldKey;

    @Schema(name = "sortOrder", description = "排序值")
    @TableField("sort_order")
    private Integer sortOrder;

    @Schema(name = "delFlag", description = "是否已删除")
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    @TableLogic
    private Boolean delFlag;


    @Override
    public Serializable pkVal() {
        return this.getId();
    }


    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }
}
