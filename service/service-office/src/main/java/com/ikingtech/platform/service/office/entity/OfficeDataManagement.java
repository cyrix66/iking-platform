package com.ikingtech.platform.service.office.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.ModelEntity;
import com.ikingtech.framework.sdk.office.model.enums.OfficeDataEnums;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * <p>
 * 办公文件管理-基础数据管理
 * </p>
 *
 * @author lqb
 * @since 2023-09-19 03:01:06
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("office_data_management")
@Schema(name = "OfficeDataManagement", description = "办公文件管理-基础数据管理")
public class OfficeDataManagement extends ModelEntity<OfficeDataManagement> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "service", description = "调用服务")
    @TableField("service")
    private String service;

    @Schema(name = "businessType", description = "数据所属业务模块")
    @TableField("business_type")
    private String businessType;

    @Schema(name = "name", description = "数据来源名称")
    @TableField("name")
    private String name;

    @Schema(name = "resourceType", description = "数据来源类型 API-接口 JSON-json字符串 ")
    @TableField("resource_type")
    private OfficeDataEnums.ResourceType resourceType;

    @Schema(name = "content", description = "内容 API-接口请求地址 JSON-json内容")
    @TableField("content")
    private String content;

    @Schema(name = "apiMethod", description = "请求方法 POST-post请求 GET-get请求  type=API 必穿")
    @TableField("api_method")
    private OfficeDataEnums.ApiMethod apiMethod;

    @Schema(name = "resultType", description = "返回类型 OBJECT-对象 LIST-列表集合")
    @TableField("result_type")
    private OfficeDataEnums.ResultType resultType;

    @Schema(name = "sortOrder", description = "排序值")
    @TableField("sort_order")
    private Integer sortOrder;

    @Schema(name = "delFlag", description = "是否已删除")
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    @TableLogic
    private Boolean delFlag;

    @Override
    public Serializable pkVal() {
        return this.getId();
    }

    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }
}
