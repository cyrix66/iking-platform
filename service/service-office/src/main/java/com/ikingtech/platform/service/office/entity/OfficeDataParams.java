package com.ikingtech.platform.service.office.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.ModelEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * <p>
 * 办公文件管理-基础数据api请求参数
 * </p>
 *
 * @author lqb
 * @since 2023-09-19 03:01:06
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("office_data_params")
@Schema(name = "OfficeDataParams", description = "办公文件管理-基础数据api请求参数")
public class OfficeDataParams extends ModelEntity<OfficeDataParams> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "dataBaseManagementId", description = "数据管理id")
    @TableField("data_base_management_id")
    private String dataBaseManagementId;

    @Schema(name = "filtration", description = "过滤参数")
    @TableField("filtration")
    private Boolean filtration;

    @Schema(name = "paramName", description = "参数名称")
    @TableField("param_name")
    private String paramName;

    @Schema(name = "paramType", description = "参数类型")
    @TableField("param_type")
    private String paramType;

    @Schema(name = "paramKey", description = "参数key")
    @TableField("param_key")
    private String paramKey;

    @Schema(name = "paramValue", description = "参数值")
    @TableField("param_value")
    private String paramValue;

    @Schema(name = "defaultValue", description = "默认值")
    @TableField("default_value")
    private String defaultValue;

    @Schema(name = "sortOrder", description = "排序值")
    @TableField("sort_order")
    private Integer sortOrder;

    @Schema(name = "delFlag", description = "是否已删除")
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    @TableLogic
    private Boolean delFlag;


    @Override
    public Serializable pkVal() {
        return this.getId();
    }


    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }
}
