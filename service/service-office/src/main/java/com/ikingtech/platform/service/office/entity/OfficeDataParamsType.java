package com.ikingtech.platform.service.office.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.ModelEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * <p>
 * office-参数类型
 * </p>
 *
 * @author lqb
 * @since 2024-01-04 04:25:03
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("office_data_params_type")
@Schema(name = "OfficeDataParamsType", description = "office-参数类型")
public class OfficeDataParamsType extends ModelEntity<OfficeDataParamsType> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "key", description = "key")
    @TableField("`key`")
    private String key;

    @Schema(name = "name", description = "参数类型名称")
    @TableField("`name`")
    private String name;


    @Override
    public Serializable pkVal() {
        return this.getId();
    }


    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }
}
