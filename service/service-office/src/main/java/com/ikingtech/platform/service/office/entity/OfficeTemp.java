package com.ikingtech.platform.service.office.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.ModelEntity;
import com.ikingtech.framework.sdk.office.model.enums.FileTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * <p>
 * office-模板
 * </p>
 *
 * @author lqb
 * @since 2023-09-22 11:34:02
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("office_temp")
@Schema(name = "OfficeTemp", description = "office-模板")
public class OfficeTemp extends ModelEntity<OfficeTemp> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "tempName", description = "模板名称")
    @TableField("temp_name")
    private String tempName;

    @Schema(name = "fileType", description = "文件类型 EXCEL-表格 WORD-文档")
    @TableField("file_type")
    private FileTypeEnum fileType;

    @Schema(name = "officeTempRecordId", description = "启用的记录")
    @TableField("office_temp_record_id")
    private String officeTempRecordId;

    @Schema(name = "delFlag", description = "是否已删除")
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    @TableLogic
    private Boolean delFlag;


    @Override
    public Serializable pkVal() {
        return this.getId();
    }


    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }
}
