package com.ikingtech.platform.service.office.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.ModelEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * <p>
 * office模板文件历史记录
 * </p>
 *
 * @author lqb
 * @since 2023-09-22 11:34:54
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("office_temp_record")
@Schema(name = "OfficeTempRecord", description = "office模板文件历史记录")
public class OfficeTempRecord extends ModelEntity<OfficeTempRecord> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "officeTempId", description = "模板id")
    @TableField("office_temp_id")
    private String officeTempId;

    @Schema(name = "enable", description = "是否启用")
    @TableField("enable")
    private Boolean enable;

    @Schema(name = "affixFileId", description = "附件文件id")
    @TableField("affix_file_id")
    private String affixFileId;

    @Schema(name = "fileName", description = "文件名称")
    @TableField("file_name")
    private String fileName;

    @Schema(name = "delFlag", description = "是否已删除")
    @TableField(value = "del_flag", fill = FieldFill.INSERT)
    @TableLogic
    private Boolean delFlag;

    @Schema(name = "isVersion", description = "是否记录为版本")
    @TableField(value = "is_version")
    private Boolean isVersion;

    @Schema(name = "version", description = "版本")
    @TableField(value = "version")
    private int version;

    @Override
    public Serializable pkVal() {
        return this.getId();
    }


    public <T> T copy(T target, String... ignoreProperties) {
        BeanUtils.copyProperties(this, target, ignoreProperties);
        return target;
    }
}
