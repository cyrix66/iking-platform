package com.ikingtech.platform.service.office.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.framework.sdk.office.model.vo.AffixFileVo;
import com.ikingtech.platform.service.office.entity.AffixFile;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AffixFileMapper extends BaseMapper<AffixFile> {


    /**
     * 根据业务查询文件
     *
     * @param businessId 业务id
     * @return AffixFileVo
     */
    List<AffixFileVo> queryByBusinessId(@Param("businessId") String businessId);



}
