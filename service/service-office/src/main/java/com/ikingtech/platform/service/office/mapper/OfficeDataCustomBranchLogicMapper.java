package com.ikingtech.platform.service.office.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.office.entity.OfficeDataCustomBranchLogic;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * office-自定义分支逻辑运算 Mapper 接口
 * </p>
 *
 * @author lqb
 */
@Mapper
public interface OfficeDataCustomBranchLogicMapper extends BaseMapper<OfficeDataCustomBranchLogic> {

}
