package com.ikingtech.platform.service.office.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.framework.sdk.office.model.vo.OfficeDataCustomBranchVo;
import com.ikingtech.platform.service.office.entity.OfficeDataCustomBranch;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * office-自定义条件分支 Mapper 接口
 * </p>
 *
 * @author lqb
 */
@Mapper
public interface OfficeDataCustomBranchMapper extends BaseMapper<OfficeDataCustomBranch> {

    /**
     * 查询参数列表通过字段id
     *
     * @param fieldsId 字段id
     * @return 执行结果
     */
    List<OfficeDataCustomBranchVo> queryListByFieldsId(String fieldsId);
}
