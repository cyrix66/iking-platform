package com.ikingtech.platform.service.office.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.office.entity.OfficeDataCustomFields;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * office-自定义字段 Mapper 接口
 * </p>
 *
 * @author lqb
 */
@Mapper
public interface OfficeDataCustomFieldsMapper extends BaseMapper<OfficeDataCustomFields> {

    /**
     * 获取最大排序顺序
     *
     * @param managementId 管理id
     * @return 执行结果
     */
    Integer getMaxSortOrder(String managementId);
}
