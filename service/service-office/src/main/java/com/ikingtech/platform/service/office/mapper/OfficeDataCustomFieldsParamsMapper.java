package com.ikingtech.platform.service.office.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.office.entity.OfficeDataCustomFieldsParams;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * office-自定义字段参数 Mapper 接口
 * </p>
 *
 * @author lqb
 */
@Mapper
public interface OfficeDataCustomFieldsParamsMapper extends BaseMapper<OfficeDataCustomFieldsParams> {

}
