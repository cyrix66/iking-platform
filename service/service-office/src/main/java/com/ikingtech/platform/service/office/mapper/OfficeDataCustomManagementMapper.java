package com.ikingtech.platform.service.office.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.framework.sdk.office.model.query.OfficeDataManagementQuery;
import com.ikingtech.framework.sdk.office.model.vo.OfficeDataCustomManagementVo;
import com.ikingtech.platform.service.office.entity.OfficeDataCustomManagement;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * office-自定义数据管理 Mapper 接口
 * </p>
 *
 * @author lqb
 */
@Mapper
public interface OfficeDataCustomManagementMapper extends BaseMapper<OfficeDataCustomManagement> {

    /**
     * 获取最大排序顺序
     *
     * @return 执行结果
     */
    Integer getMaxSortOrder(@Param("businessType") String businessType);

    /**
     * 列表查询
     *
     * @param query 查询参数
     * @return 执行结果
     */
    List<OfficeDataCustomManagementVo> queryList(@Param("query") OfficeDataManagementQuery query);
}
