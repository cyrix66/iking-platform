package com.ikingtech.platform.service.office.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.office.entity.OfficeDataFields;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 办公文件管理-基础数据api相应字段 Mapper 接口
 * </p>
 *
 * @author lqb
 */
@Mapper
public interface OfficeDataFieldsMapper extends BaseMapper<OfficeDataFields> {

}
