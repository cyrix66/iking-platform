package com.ikingtech.platform.service.office.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.framework.sdk.office.model.query.OfficeDataManagementQuery;
import com.ikingtech.framework.sdk.office.model.vo.OfficeDataManagementVo;
import com.ikingtech.platform.service.office.entity.OfficeDataManagement;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 办公文件管理-基础数据管理 Mapper 接口
 * </p>
 *
 * @author lqb
 */
@Mapper
public interface OfficeDataManagementMapper extends BaseMapper<OfficeDataManagement> {

    /**
     * 获取最大排序顺序
     *
     * @param businessType 业务类型
     * @return 执行结果
     * @author lqb
     */
    Integer getMaxSortOrder(@Param("businessType") String businessType);

    /**
     * 查询列表
     *
     * @param query 查询
     * @return 执行结果
     * @author lqb
     */
    List<OfficeDataManagementVo> queryList(@Param("query") OfficeDataManagementQuery query);
}
