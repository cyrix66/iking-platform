package com.ikingtech.platform.service.office.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.office.entity.OfficeDataParams;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 办公文件管理-基础数据api请求参数 Mapper 接口
 * </p>
 *
 * @author lqb
 */
@Mapper
public interface OfficeDataParamsMapper extends BaseMapper<OfficeDataParams> {

}
