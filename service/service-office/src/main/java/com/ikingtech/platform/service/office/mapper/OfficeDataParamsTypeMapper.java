package com.ikingtech.platform.service.office.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.office.entity.OfficeDataParamsType;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * office-参数类型 Mapper 接口
 * </p>
 *
 * @author lqb
 */
@Mapper
public interface OfficeDataParamsTypeMapper extends BaseMapper<OfficeDataParamsType> {

}
