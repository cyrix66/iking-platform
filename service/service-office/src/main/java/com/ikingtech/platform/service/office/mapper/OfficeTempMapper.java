package com.ikingtech.platform.service.office.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.office.entity.OfficeTemp;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * office-模板 Mapper 接口
 * </p>
 *
 * @author lqb
 */
@Mapper
public interface OfficeTempMapper extends BaseMapper<OfficeTemp> {

}
