package com.ikingtech.platform.service.office.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.office.entity.OfficeTempRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * office模板文件历史记录 Mapper 接口
 * </p>
 *
 * @author lqb
 */
@Mapper
public interface OfficeTempRecordMapper extends BaseMapper<OfficeTempRecord> {

}
