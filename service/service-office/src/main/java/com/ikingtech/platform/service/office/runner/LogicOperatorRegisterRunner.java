package com.ikingtech.platform.service.office.runner;

import com.googlecode.aviator.AviatorEvaluator;
import com.ikingtech.framework.sdk.office.utils.LogicOperatorUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

/**
 * 注册逻辑运算符运算方法 到 逻辑引擎
 *
 * @author lqb
 */
@Slf4j
public class LogicOperatorRegisterRunner implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        AviatorEvaluator.addStaticFunctions("logic", LogicOperatorUtil.class);
        log.info("aviator script static function register success : LogicOperatorUtil");
    }
}
