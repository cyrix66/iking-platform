package com.ikingtech.platform.service.office.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.office.api.AffixFileApi;
import com.ikingtech.framework.sdk.office.model.vo.*;
import com.ikingtech.framework.sdk.user.api.UserApi;
import com.ikingtech.framework.sdk.user.model.UserDTO;
import com.ikingtech.framework.sdk.user.model.UserDeptDTO;
import com.ikingtech.platform.service.office.entity.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
public class DocumentHistoryService {

    private final OfficeTempService officeTempService;

    private final UserApi userApi;

    private final AffixFileApi affixFileApi;


    public DocumentHistoryVO getDocumentHistoryByTmpId(String id) {
        List<OfficeTempRecord> officeTempRecordList=  officeTempService.getDocVersionList(id);

        //获取文档最新版本
        OfficeTempRecord officeTempRecordLast = officeTempService.getLastDocVersion(id);
        if (ObjectUtils.isEmpty(officeTempRecordLast)) {
            return  DocumentHistoryVO.builder().build();
        }

        List<HistoryDataVO> historyDataVOS=new ArrayList<>();
        List<HistoryVO>  historyVOS=new ArrayList<>();

        for ( OfficeTempRecord officeTempRecord : officeTempRecordList){

            //获取用户信息
            R<UserDTO> userDTOR= userApi.detail(officeTempRecord.getCreateBy());
            String deptStr =  userDTOR.getData().getDepartments().stream().map(UserDeptDTO::getDeptName).collect(Collectors.joining(","));

            //获取文件信息
            AffixFileVo affixFileVo= affixFileApi.detail(officeTempRecord.getAffixFileId()).getData();

            historyVOS.add( HistoryVO.builder()
                    .key(officeTempRecord.getId())
                    .version(officeTempRecord.getVersion())
                    .created(officeTempRecord.getCreateTime())
                    .user(UserVO.builder()
                            .name("("+deptStr+")"+userDTOR.getData().getName())
                            .id(userDTOR.getData().getId()).build())
                    .build());
            historyDataVOS.add(HistoryDataVO.builder()
                    .key(officeTempRecord.getId())
                    .url(affixFileVo.getUrl())
                    .version(officeTempRecord.getVersion())
                    .fileType(affixFileVo.getSuffix())
                    .build());

        }
       return DocumentHistoryVO.builder()
               .currentVersion(officeTempRecordLast.getVersion())
               .history(historyVOS)
               .historyData(historyDataVOS)
               .build();
    }


    /**
     * 获取版本信息
     * @param tempId
     * @param version
     * @return
     */
    public HistoryDataVO getVersionInfo(String tempId, int version) {
        OfficeTempRecord officeTempRecord = this.officeTempService.getOfficeTempRecordByVersion(tempId,version);
        //获取文件信息
        AffixFileVo affixFileVo= affixFileApi.detail(officeTempRecord.getAffixFileId()).getData();

        return HistoryDataVO.builder()
                .key(officeTempRecord.getId())
                .url(affixFileVo.getUrl())
                .version(officeTempRecord.getVersion())
                .fileType(affixFileVo.getSuffix())
                .build();
    }
}
