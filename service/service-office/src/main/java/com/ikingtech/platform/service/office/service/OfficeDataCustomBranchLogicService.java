package com.ikingtech.platform.service.office.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.office.model.dto.OfficeDataCustomBranchLogicDto;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.office.entity.OfficeDataCustomBranch;
import com.ikingtech.platform.service.office.entity.OfficeDataCustomBranchLogic;
import com.ikingtech.platform.service.office.mapper.OfficeDataCustomBranchLogicMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * office-自定义分支逻辑运算 服务实现类
 * </p>
 *
 * @author lqb
 */
@Slf4j
@RequiredArgsConstructor
public class OfficeDataCustomBranchLogicService extends ServiceImpl<OfficeDataCustomBranchLogicMapper, OfficeDataCustomBranchLogic> {

    /**
     * 批量新增
     *
     * @param branch    树枝
     * @param logicList 逻辑列表
     * @return 执行结果
     */
    @Transactional(rollbackFor = Exception.class)
    public R<String> addBatch(OfficeDataCustomBranch branch, List<OfficeDataCustomBranchLogicDto> logicList) {
        int index = 0;
        for (OfficeDataCustomBranchLogicDto dto : logicList) {
            OfficeDataCustomBranchLogic logic = dto.copy(new OfficeDataCustomBranchLogic())
                    .setOfficeDataCustomBranchId(branch.getId())
                    .setOfficeDataCustomFieldsId(branch.getOfficeDataCustomFieldsId())
                    .setSortOrder(index++);
            logic.setId(Tools.Id.uuid());
            logic.insert();
        }
        return R.ok();
    }
}

