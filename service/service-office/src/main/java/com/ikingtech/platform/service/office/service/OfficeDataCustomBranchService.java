package com.ikingtech.platform.service.office.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.office.model.dto.OfficeDataCustomBranchDto;
import com.ikingtech.framework.sdk.office.model.vo.OfficeDataCustomBranchVo;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.office.entity.OfficeDataCustomBranch;
import com.ikingtech.platform.service.office.entity.OfficeDataCustomFields;
import com.ikingtech.platform.service.office.mapper.OfficeDataCustomBranchMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * office-自定义条件分支 服务实现类
 * </p>
 *
 * @author lqb
 */
@Slf4j
@RequiredArgsConstructor
public class OfficeDataCustomBranchService extends ServiceImpl<OfficeDataCustomBranchMapper, OfficeDataCustomBranch> {

    private final OfficeDataCustomBranchLogicService officeDataCustomBranchLogicService;

    /**
     * 批量新增
     *
     * @param field      领域
     * @param branchList 分支机构列表
     * @return 执行结果
     */
    @Transactional(rollbackFor = Exception.class)
    public R<String> addBatch(OfficeDataCustomFields field, List<OfficeDataCustomBranchDto> branchList) {
        int index = 0;
        for (OfficeDataCustomBranchDto dto : branchList) {
            OfficeDataCustomBranch branch = dto.copy(new OfficeDataCustomBranch(), "logicList")
                    .setOfficeDataCustomFieldsId(field.getId())
                    .setSortOrder(index++);
            branch.setId(Tools.Id.uuid());
            branch.insert();
            this.officeDataCustomBranchLogicService.addBatch(branch, dto.getLogicList());
        }
        return R.ok();
    }

    /**
     * 查询参数列表通过字段id
     *
     * @param fieldsId fieldsId
     * @return 执行结果
     */
    public List<OfficeDataCustomBranchVo> queryListByFieldsId(String fieldsId) {
        return this.baseMapper.queryListByFieldsId(fieldsId);
    }
}

