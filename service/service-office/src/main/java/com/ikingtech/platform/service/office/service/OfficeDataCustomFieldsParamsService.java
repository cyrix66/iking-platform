package com.ikingtech.platform.service.office.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.office.model.dto.OfficeDataCustomFieldsParamsDto;
import com.ikingtech.framework.sdk.office.model.vo.OfficeDataCustomFieldsParamsVo;
import com.ikingtech.platform.service.office.entity.OfficeDataCustomFields;
import com.ikingtech.platform.service.office.entity.OfficeDataCustomFieldsParams;
import com.ikingtech.platform.service.office.mapper.OfficeDataCustomFieldsParamsMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * office-自定义字段参数 服务实现类
 * </p>
 *
 * @author lqb
 */
@Slf4j
@RequiredArgsConstructor
public class OfficeDataCustomFieldsParamsService extends ServiceImpl<OfficeDataCustomFieldsParamsMapper, OfficeDataCustomFieldsParams> {

    /**
     * 批量新增
     *
     * @param field     领域
     * @param paramList 参数列表
     * @return 执行结果
     */
    @Transactional(rollbackFor = Exception.class)
    public R<String> addBatch(OfficeDataCustomFields field, List<OfficeDataCustomFieldsParamsDto> paramList) {
        this.saveBatch(paramList.stream().map(
                p -> p.copy(new OfficeDataCustomFieldsParams())
                        .setOfficeDataCustomFieldsId(field.getId())
        ).collect(Collectors.toList()));
        return R.ok();
    }

    /**
     * 查询参数列表通过字段id
     *
     * @param fieldsId fieldsId
     * @return 执行结果
     * @author lqb
     */
    public List<OfficeDataCustomFieldsParamsVo> queryListByFieldsId(String fieldsId) {
        return this.lambdaQuery()
                .eq(OfficeDataCustomFieldsParams::getOfficeDataCustomFieldsId, fieldsId)
                .list().stream()
                .map(p -> p.copy(new OfficeDataCustomFieldsParamsVo()))
                .collect(Collectors.toList());
    }
}

