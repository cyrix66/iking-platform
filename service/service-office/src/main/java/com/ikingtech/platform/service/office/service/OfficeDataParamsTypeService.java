package com.ikingtech.platform.service.office.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.office.entity.OfficeDataParamsType;
import com.ikingtech.platform.service.office.mapper.OfficeDataParamsTypeMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * office-参数类型 服务实现类
 * </p>
 *
 * @author lqb
 */
@Slf4j
@RequiredArgsConstructor
public class OfficeDataParamsTypeService extends ServiceImpl<OfficeDataParamsTypeMapper, OfficeDataParamsType> {

}

