package com.ikingtech.platform.service.oidc.client.configuration;

import com.ikingtech.platform.service.oidc.client.service.OidcClientRedirectUriRepository;
import com.ikingtech.platform.service.oidc.client.service.OidcClientRepository;
import org.springframework.context.annotation.Bean;

/**
 * @author tie yan
 */
public class OidcClientConfiguration {

    @Bean
    public OidcClientRepository oidcClientRepository() {
        return new OidcClientRepository();
    }

    @Bean
    public OidcClientRedirectUriRepository oidcClientRedirectUriRepository() {
        return new OidcClientRedirectUriRepository();
    }
}
