package com.ikingtech.platform.service.oidc.client.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("oidc_client")
public class OidcClientDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -2033709447338759864L;

    @TableField("secret")
    private String secret;

    @TableField("name")
    private String name;

    @TableField("logo")
    private String logo;

    @TableField("website")
    private String website;
}
