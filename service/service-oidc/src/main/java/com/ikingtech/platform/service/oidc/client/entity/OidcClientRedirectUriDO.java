package com.ikingtech.platform.service.oidc.client.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("oidc_client_redirect_uri")
public class OidcClientRedirectUriDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -2721429383463918648L;

    @TableField("client_id")
    private String clientId;

    @TableField("redirect_uri")
    private String redirectUri;
}
