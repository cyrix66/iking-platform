package com.ikingtech.platform.service.oidc.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.oidc.client.entity.OidcClientDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Administrator
 */
@Mapper
public interface OidcClientMapper extends BaseMapper<OidcClientDO> {
}
