package com.ikingtech.platform.service.oidc.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.oidc.client.entity.OidcClientRedirectUriDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Administrator
 */
@Mapper
public interface OidcClientRedirectUriMapper extends BaseMapper<OidcClientRedirectUriDO> {
}
