package com.ikingtech.platform.service.oidc.client.model;

import com.ikingtech.framework.sdk.base.model.BaseModel;
import com.ikingtech.platform.service.oidc.client.entity.OidcClientDO;
import com.ikingtech.platform.service.oidc.client.entity.OidcClientRedirectUriDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "OidcClientDTO", description = "第三方客户端信息")
public class OidcClientDTO extends BaseModel implements Serializable {

    @Serial
    private static final long serialVersionUID = 5602680598294372991L;

    @Schema(name = "secret", description = "客户端密钥")
    private String secret;

    @Schema(name = "name", description = "名称")
    private String name;

    @Schema(name = "logo", description = "Logo")
    private String logo;

    @Schema(name = "website", description = "主页地址")
    private String website;

    @Schema(name = "redirectUris", description = "跳转地址")
    private List<OidcClientRedirectUriDTO> redirectUris;
}
