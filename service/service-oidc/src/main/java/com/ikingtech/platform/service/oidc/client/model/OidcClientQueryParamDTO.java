package com.ikingtech.platform.service.oidc.client.model;

import com.ikingtech.framework.sdk.base.model.BaseQueryParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class OidcClientQueryParamDTO extends BaseQueryParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -7492377957408759135L;

    String secret;

    String name;

    String redirectUri;
}
