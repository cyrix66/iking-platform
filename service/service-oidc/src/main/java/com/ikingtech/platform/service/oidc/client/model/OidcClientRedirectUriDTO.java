package com.ikingtech.platform.service.oidc.client.model;

import java.io.Serializable;

/**
 * @author tie yan
 */
public record OidcClientRedirectUriDTO(String id, String clientId, String redirectUri) implements Serializable {
}
