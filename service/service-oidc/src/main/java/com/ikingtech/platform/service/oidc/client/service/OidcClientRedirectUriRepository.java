package com.ikingtech.platform.service.oidc.client.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.oidc.client.entity.OidcClientRedirectUriDO;
import com.ikingtech.platform.service.oidc.client.mapper.OidcClientRedirectUriMapper;

/**
 * @author tie yan
 */
public class OidcClientRedirectUriRepository extends ServiceImpl<OidcClientRedirectUriMapper, OidcClientRedirectUriDO> {
}
