package com.ikingtech.platform.service.oidc.client.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.data.mybatisplus.helper.base.BaseRepository;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.oidc.client.entity.OidcClientDO;
import com.ikingtech.platform.service.oidc.client.mapper.OidcClientMapper;
import com.ikingtech.platform.service.oidc.client.model.OidcClientQueryParamDTO;

/**
 * @author tie yan
 */
public class OidcClientRepository extends ServiceImpl<OidcClientMapper, OidcClientDO> implements BaseRepository<OidcClientDO, OidcClientQueryParamDTO> {

    @Override
    public Wrapper<OidcClientDO> conditionalWrapper(OidcClientQueryParamDTO queryParam) {
        return Wrappers.<OidcClientDO>lambdaQuery()
                .like(Tools.Str.isNotBlank(queryParam.getName()), OidcClientDO::getName, queryParam.getName())
                .like(Tools.Str.isNotBlank(queryParam.getSecret()), OidcClientDO::getSecret, queryParam.getSecret());
    }
}
