package com.ikingtech.platform.service.oidc.converter;

import com.ikingtech.platform.service.oidc.web.OidcAuthorizationResponse;
import jakarta.servlet.http.HttpServletResponse;

/**
 * @author tie yan
 */
public interface OidcResponseConverter {

    void convert(OidcAuthorizationResponse oidcResponse, HttpServletResponse httpResponse);

    boolean support(OidcAuthorizationResponse oidcResponse);
}
