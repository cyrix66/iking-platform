package com.ikingtech.platform.service.oidc.endpoint;

import com.ikingtech.platform.service.oidc.web.OidcAuthorizationResponse;
import jakarta.servlet.http.HttpServletRequest;

/**
 * @author tie yan
 */
public interface OidcEndpoint {

    OidcAuthorizationResponse authorization(HttpServletRequest request);

    boolean support(String requestUri);
}
