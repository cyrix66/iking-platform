package com.ikingtech.platform.service.oidc.endpoint.autorize;

import com.ikingtech.platform.service.oidc.web.OidcAuthorizationResponse;
import org.springframework.util.MultiValueMap;

/**
 * @author tie yan
 */
public interface OidcAuthorizeEndpoint {

    OidcAuthorizationResponse authorization(MultiValueMap<String, String> paramMap);

    boolean support(String responseType);
}
