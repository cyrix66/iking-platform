package com.ikingtech.platform.service.oidc.endpoint.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class IdTokenClaims implements Serializable {

    @Serial
    private static final long serialVersionUID = 5475277038524225318L;

    private String iss;

    private String sub;

    private String aud;

    private String nonce;

    private String exp;

    private String iat;

    @JsonProperty("auth_time")
    private long authTime;

    private String acr;
}
