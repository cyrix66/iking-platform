package com.ikingtech.platform.service.oidc.endpoint.model;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class OidcAuthorizationCode implements Serializable {

    @Serial
    private static final long serialVersionUID = -6599788273715798032L;

    private String code;

    private String codeVerifier;
}
