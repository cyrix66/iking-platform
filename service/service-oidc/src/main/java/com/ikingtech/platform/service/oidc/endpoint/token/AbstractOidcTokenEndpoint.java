package com.ikingtech.platform.service.oidc.endpoint.token;

import com.ikingtech.platform.service.oidc.client.service.OidcClientRedirectUriRepository;
import com.ikingtech.platform.service.oidc.client.service.OidcClientRepository;
import com.ikingtech.platform.service.oidc.endpoint.AbstractOidcEndpoint;
import com.ikingtech.platform.service.oidc.web.OidcAuthorizationRequest;
import com.ikingtech.platform.service.oidc.web.OidcAuthorizationResponse;
import org.springframework.util.MultiValueMap;

/**
 * @author tie yan
 */
public abstract class AbstractOidcTokenEndpoint extends AbstractOidcEndpoint implements OidcTokenEndpoint {

    public AbstractOidcTokenEndpoint(OidcClientRepository clientRepo, OidcClientRedirectUriRepository clientRedirectUriRepo) {
        super(clientRepo, clientRedirectUriRepo);
    }

    @Override
    public OidcAuthorizationResponse authorization(MultiValueMap<String, String> paramMap) {
        OidcAuthorizationRequest authorizationRequest = this.convertRequest(paramMap);
        this.authenticateClient(authorizationRequest);
        return this.doAuthorization(authorizationRequest);
    }

    protected abstract OidcAuthorizationResponse doAuthorization(OidcAuthorizationRequest authorizationRequest);
}
