package com.ikingtech.platform.service.oidc.endpoint.token;

import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.oidc.endpoint.OidcEndpoint;
import com.ikingtech.platform.service.oidc.exception.InvalidGrantException;
import com.ikingtech.platform.service.oidc.exception.InvalidRequestException;
import com.ikingtech.platform.service.oidc.web.OidcAuthorizationRequest;
import com.ikingtech.platform.service.oidc.web.OidcAuthorizationResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.util.MultiValueMap;

import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class DelegateOidcTokenEndpoint implements OidcEndpoint {

    private final List<OidcTokenEndpoint> tokenEndpoints;

    @Override
    public OidcAuthorizationResponse authorization(HttpServletRequest request) {
        MultiValueMap<String, String> paramMap;

        if ("POST".equals(request.getMethod())) {
            paramMap = Tools.Http.getFormParameters(request);
        } else {
            throw new InvalidRequestException();
        }
        String grantType = paramMap.getFirst(OidcAuthorizationRequest.AuthenticationRequestFieldName.GRANT_TYPE);
        OidcTokenEndpoint endpoint = null;
        for (OidcTokenEndpoint tokenEndpoint : this.tokenEndpoints) {
            if (tokenEndpoint.support(grantType)) {
                endpoint = tokenEndpoint;
                break;
            }
        }
        if (null == endpoint) {
            throw new InvalidGrantException();
        }
        return endpoint.authorization(paramMap);
    }

    @Override
    public boolean support(String requestUri) {
        return "/oidc/token".equals(requestUri);
    }
}
