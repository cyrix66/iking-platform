package com.ikingtech.platform.service.oidc.endpoint.token;

import com.ikingtech.platform.service.oidc.web.OidcAuthorizationResponse;
import org.springframework.util.MultiValueMap;

/**
 * @author tie yan
 */
public interface OidcTokenEndpoint {

    OidcAuthorizationResponse authorization(MultiValueMap<String, String> paramMap);

    boolean support(String grantType);
}
