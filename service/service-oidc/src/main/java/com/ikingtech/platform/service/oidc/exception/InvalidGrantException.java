package com.ikingtech.platform.service.oidc.exception;

import java.io.Serial;

/**
 * @author tie yan
 */
public class InvalidGrantException extends OidcException{

    @Serial
    private static final long serialVersionUID = -2833015769202833728L;

    public InvalidGrantException() {
        super("invalid_grant", "错误的授权方式", "https://openid.net/specs/openid-connect-core-1_0.html#AuthRequest");
    }
}
