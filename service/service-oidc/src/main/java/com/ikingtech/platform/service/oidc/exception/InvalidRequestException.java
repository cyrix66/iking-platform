package com.ikingtech.platform.service.oidc.exception;

import java.io.Serial;

/**
 * @author tie yan
 */
public class InvalidRequestException extends OidcException{

    @Serial
    private static final long serialVersionUID = 5054458266566195905L;

    public InvalidRequestException() {
        super("invalid_request", "错误的请求方法", "https://openid.net/specs/openid-connect-core-1_0.html#AuthRequest");
    }
}
