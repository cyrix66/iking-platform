package com.ikingtech.platform.service.oidc.exception;

import java.io.Serial;

/**
 * @author tie yan
 */
public class InvalidScopeException extends OidcException{

    @Serial
    private static final long serialVersionUID = 199766216511305571L;

    public InvalidScopeException() {
        super("invalid_scope", "无效的授权范围", "https://openid.net/specs/openid-connect-core-1_0.html#AuthRequest");
    }
}
