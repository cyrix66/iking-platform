package com.ikingtech.platform.service.oidc.exception;

import java.io.Serial;

/**
 * @author tie yan
 */
public class OidcException  extends RuntimeException{

    @Serial
    private static final long serialVersionUID = 8629644129532501194L;

    private final String error;

    private final String errorDescription;

    private final String errorUri;

    public OidcException(String error, String errorDescription, String errorUri) {
        super("invalid_request");
        this.error = error;
        this.errorDescription = errorDescription;
        this.errorUri = errorUri;
    }
}
