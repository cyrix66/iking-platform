package com.ikingtech.platform.service.oidc.exception;

import java.io.Serial;

/**
 * @author tie yan
 */
public class ServerErrorException extends OidcException{

    @Serial
    private static final long serialVersionUID = -2833015769202833728L;

    public ServerErrorException() {
        super("server_error", "服务器异常，请稍后重试", "https://openid.net/specs/openid-connect-core-1_0.html#AuthRequest");
    }
}
