package com.ikingtech.platform.service.oidc.exception;

import java.io.Serial;

/**
 * @author tie yan
 */
public class UnauthorizedClientException extends OidcException{

    @Serial
    private static final long serialVersionUID = 199766216511305571L;

    public UnauthorizedClientException() {
        super("unauthorized_client", "未认证的客户端", "https://openid.net/specs/openid-connect-core-1_0.html#AuthRequest");
    }
}
