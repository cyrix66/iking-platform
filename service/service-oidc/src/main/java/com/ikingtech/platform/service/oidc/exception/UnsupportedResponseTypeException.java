package com.ikingtech.platform.service.oidc.exception;

import java.io.Serial;

/**
 * @author tie yan
 */
public class UnsupportedResponseTypeException extends OidcException{

    @Serial
    private static final long serialVersionUID = 199766216511305571L;

    public UnsupportedResponseTypeException() {
        super("unsupported_response_type", "错误的响应类型", "https://openid.net/specs/openid-connect-core-1_0.html#AuthRequest");
    }
}
