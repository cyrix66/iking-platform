package com.ikingtech.platform.service.oidc.web;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class OidcAuthorizationRequest implements Serializable {

    @Serial
    private static final long serialVersionUID = 1881671719588292888L;

    private String scope;

    private String grantType;

    private String code;

    private String responseType;

    private String clientId;

    private String clientSecret;

    private String redirectUri;

    private String state;

    private String responseMode;

    private String nonce;

    private String display;

    private String prompt;

    private String maxAge;

    private String uiLocales;

    private String idTokenHint;

    private String loginHint;

    private String acrValues;

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class AuthenticationRequestFieldName {

        public static final String SCOPE = "scope";

        public static final String GRANT_TYPE = "grant_type";

        public static final String CODE = "code";

        public static final String RESPONSE_TYPE = "response_type";

        public static final String CLIENT_ID = "client_id";

        public static final String CLIENT_SECRET = "client_secret";

        public static final String REDIRECT_URI = "redirect_uri";

        public static final String STATE = "state";

        public static final String RESPONSE_MODE = "response_mode";

        public static final String NONCE = "nonce";

        public static final String DISPLAY = "display";

        public static final String PROMPT = "prompt";

        public static final String MAX_AGE = "max_age";

        public static final String UI_LOCALES = "ui_locales";

        public static final String ID_TOKEN_HINT = "id_token_hint";

        public static final String LOGIN_HINT = "login_hint";

        public static final String ACR_VALUES = "acr_values";
    }
}
