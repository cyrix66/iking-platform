package com.ikingtech.platform.service.oidc.web;

import com.ikingtech.framework.sdk.utils.Tools;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class OidcAuthorizationResponse implements Serializable {

    @Serial
    private static final long serialVersionUID = -4773959547418549450L;

    private boolean authenticated;

    private String code;

    private String accessToken;

    private String tokenType;

    private String refreshToken;

    private long expiresIn;

    private String idToken;

    private String state;

    private String error;

    private String errorDescription;

    private String errorUri;

    private String interactionRequired;

    private String loginRequired;

    private String accountSelectionRequired;

    private String consentRequired;

    private String invalidRequestUri;

    private String invalidRequestObject;

    private String requestNotSupported;

    private String requestUriNotSupported;

    private String registrationNotSupported;

    private Boolean success;

    private String redirectUri;

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class AuthenticationResponseFieldName {

        public static final String CODE = "code";

        public static final String STATE = "state";

        public static final String ERROR = "error";

        public static final String ERROR_DESCRIPTION = "error_description";

        public static final String ERROR_URI = "error_uri";

        public static final String INTERACTION_REQUIRED = "interaction_required";

        public static final String LOGIN_REQUIRED = "login_required";

        public static final String ACCOUNT_SELECTION_REQUIRED = "account_selection_required";

        public static final String CONSENT_REQUIRED = "consent_required";

        public static final String INVALID_REQUEST_URI = "invalid_request_uri";

        public static final String INVALID_REQUEST_OBJECT = "invalid_request_object";

        public static final String REQUEST_NOT_SUPPORTED = "request_not_supported";

        public static final String REQUEST_URI_NOT_SUPPORTED = "request_uri_not_supported";

        public static final String REGISTRATION_NOT_SUPPORTED = "registration_not_supported";
    }

    public static OidcAuthorizationResponse generateAuthorizationCodeResponse(String redirectUri) {
        OidcAuthorizationResponse authorizationResponse = new OidcAuthorizationResponse();
        authorizationResponse.setCode(Tools.Id.uuid());
        authorizationResponse.setRedirectUri(redirectUri);
        return authorizationResponse;
    }

    public static void commonError(OidcAuthorizationResponse authorizationResponse, String error, String errorDescription) {
        authorizationResponse.setError(error);
        authorizationResponse.setErrorDescription(errorDescription);
        authorizationResponse.setErrorUri("https://openid.net/specs/openid-connect-core-1_0.html");
    }
}
