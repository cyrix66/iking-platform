package com.ikingtech.platform.service.oss.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("oss_file")
public class OssFileDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -5027480092077378181L;

    @TableField("origin_name")
    private String originName;

    @TableField("domain_code")
    private String domainCode;

    @TableField("tenant_code")
    private String tenantCode;

    @TableField("app_code")
    private String appCode;

    @TableField("file_size")
    private Long fileSize;

    @TableField("suffix")
    private String suffix;

    @TableField("dir_name")
    private String dirName;

    @TableField("path")
    private String path;

    @TableField("url")
    private String url;

    @TableField("md5")
    private String md5;

    @TableField("use_count")
    private String useCount;
}
