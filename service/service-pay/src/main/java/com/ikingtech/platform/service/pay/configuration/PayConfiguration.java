package com.ikingtech.platform.service.pay.configuration;

import com.ikingtech.platform.service.pay.service.CashierSupplierRepository;
import com.ikingtech.platform.service.pay.service.PayRecordRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author tie yan
 */
@Configuration
public class PayConfiguration {

    @Bean
    public PayRecordRepository payRepository() {
        return new PayRecordRepository();
    }

    @Bean
    public CashierSupplierRepository cashierSupplierRepository() {
        return new CashierSupplierRepository();
    }
}
