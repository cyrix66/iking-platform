package com.ikingtech.platform.service.pay.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pay_record")
public class PayRecordDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -4005112882839869443L;

    @TableField(value = "request_id")
    private String requestId;

    @TableField(value = "amount")
    private Double amount;

    @TableField(value = "supplier_id")
    private String supplierId;

    @TableField(value = "supplier_type")
    private String supplierType;

    @TableField(value = "pay_type")
    private String payType;

    @TableField(value = "supplier_trade_id")
    private String supplierTradeId;

    @TableField(value = "channel_trade_id")
    private String channelTradeId;

    @TableField(value = "trade_time")
    private LocalDateTime tradeTime;

    @TableField(value = "cause")
    private String cause;

    @TableField(value = "status")
    private String status;

    @TableField(value = "props")
    private String props;
}
