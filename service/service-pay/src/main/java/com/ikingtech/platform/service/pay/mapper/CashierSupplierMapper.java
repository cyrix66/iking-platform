package com.ikingtech.platform.service.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.pay.entity.CashierSupplierDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface CashierSupplierMapper extends BaseMapper<CashierSupplierDO> {
}
