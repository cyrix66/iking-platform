package com.ikingtech.platform.service.plugin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import com.ikingtech.platform.service.plugin.entity.PluginDO;
import com.ikingtech.platform.service.plugin.exception.PluginExceptionInfo;
import com.ikingtech.platform.service.plugin.model.PluginDTO;
import com.ikingtech.platform.service.plugin.model.PluginQueryParamDTO;
import com.ikingtech.platform.service.plugin.service.PluginRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
@ApiController(value = "/plugin", name = "插件管理", description = "插件管理")
public class PluginController {

    private final PluginRepository repo;

    /**
     * 添加插件信息
     *
     * @param plugin 插件信息的DTO（数据传输对象），包含了插件的详细信息
     * @return 返回操作结果
     */
    @PostRequest(order = 1,value = "/add", summary = "添加插件信息", description = "添加插件信息")
    public R<String> add(PluginDTO plugin) {
        // 将插件DTO转换为插件实体对象
        PluginDO entity = Tools.Bean.copy(plugin, PluginDO.class);
        // 为插件实体生成一个唯一标识
        entity.setId(Tools.Id.uuid());
        // 将插件实体保存到数据库
        this.repo.save(entity);
        // 返回新增插件的ID
        return R.ok(entity.getId());
    }

    /**
     * 删除插件信息
     * 
     * @param id 要删除的记录的唯一标识符
     * @return 返回操作结果
     */
    @PostRequest(order = 2,value = "/delete", summary = "删除插件信息", description = "删除插件信息")
    @Transactional(rollbackFor = Exception.class)
    public R<Object> delete(String id) {
        // 通过ID删除记录
        this.repo.removeById(id);
        // 返回操作成功的响应
        return R.ok();
    }

    /**
     * 更新插件信息
     * 
     * @param post 插件信息对象，包含需要更新的插件详情
     * @return 返回操作结果
     * @throws FrameworkException 如果指定的插件不存在，则抛出框架异常
     */
    @PostRequest(order = 3,value = "/update", summary = "更新插件信息", description = "更新插件信息")
    public R<Object> update(PluginDTO post) {
        // 检查插件是否存在
        if (!this.repo.exists(Wrappers.<PluginDO>query().lambda().eq(PluginDO::getId, post.getId()))) {
            throw new FrameworkException(PluginExceptionInfo.PLUGIN_NOT_FOUND);
        }
        // 更新插件信息
        this.repo.updateById(Tools.Bean.copy(post, PluginDO.class));
        return R.ok();
    }

    /**
     * 分页查询插件信息
     * 
     * @param queryParam 包含查询条件的插件查询参数对象，如页码、行数、名称和备注等
     * @return 返回插件信息的列表
     */
    @PostRequest(order = 4,value = "/list/page", summary = "分页查询插件信息", description = "分页查询插件信息")
    public R<List<PluginDTO>> page(PluginQueryParamDTO queryParam) {
        // 构建分页查询，根据查询参数应用过滤条件，并按创建时间降序排序
        return R.ok(PageResult.build(this.repo.page(new Page<>(queryParam.getPage(), queryParam.getRows()), Wrappers.<PluginDO>query().lambda()
                .like(Tools.Str.isNotBlank(queryParam.getName()), PluginDO::getName, queryParam.getName())
                .like(Tools.Str.isNotBlank(queryParam.getRemark()), PluginDO::getRemark, queryParam.getRemark())
                .orderByDesc(PluginDO::getCreateTime))).convert(entity -> Tools.Bean.copy(entity, PluginDTO.class)));
    }

    /**
     * 获取所有插件的列表
     * 
     * @return 返回插件信息的列表
     */
    @PostRequest(order = 5,value = "/list/all", summary = "获取所有插件的列表", description = "获取所有插件的列表")
    public R<List<PluginDTO>> all() {
        // 从仓库中获取所有插件实体的列表，并将其转换为PluginDTO类型的列表
        return R.ok(Tools.Coll.convertList(this.repo.list(), entity -> Tools.Bean.copy(entity, PluginDTO.class)));
    }

    /**
     * 查询插件详情
     *
     * @param id 插件的唯一标识符
     * @return 返回插件详情
     * @throws FrameworkException 如果插件不存在，则抛出框架异常
     */
    @PostRequest(order = 6,value = "/detail", summary = "查询插件详情", description = "查询插件详情")
    public R<PluginDTO> detail(String id) {
        // 通过ID从仓库中获取插件实体
        PluginDO entity = this.repo.getById(id);
        // 如果实体不存在，则抛出插件未找到的异常
        if (null == entity) {
            throw new FrameworkException(PluginExceptionInfo.PLUGIN_NOT_FOUND);
        }
        // 将插件实体转换为DTO，并返回成功的响应
        return R.ok(Tools.Bean.copy(entity, PluginDTO.class));
    }
}
