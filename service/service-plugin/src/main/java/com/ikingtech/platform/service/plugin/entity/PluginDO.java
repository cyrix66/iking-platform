package com.ikingtech.platform.service.plugin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("plugin")
public class PluginDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 809602219241639718L;

    @TableField("name")
    private String name;

    @TableField("icon")
    private String icon;

    @TableField("version")
    private String version;

    @TableField("type")
    private String type;

    @TableField("remark")
    private String remark;

    @TableField("help_file_id")
    private String helpFileId;

    @TableField("jar_file_id")
    private String jarFileId;

    @TableField("depend_db")
    private Boolean dependDb;

    @TableField("depend_redis")
    private Boolean dependRedis;

    @TableField("depend_oss")
    private Boolean dependOss;

    @TableField("depend_rabbit_mq")
    private Boolean dependRabbitMq;
}
