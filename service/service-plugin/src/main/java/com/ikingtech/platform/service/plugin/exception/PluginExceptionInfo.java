package com.ikingtech.platform.service.plugin.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum PluginExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 指定岗位不存在
     */
    PLUGIN_NOT_FOUND("pluginNotFound"),

    /**
     * 已存在相同名称的岗位
     */
    DUPLICATE_PLUGIN_NAME("duplicatePluginName");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "system-post";
    }
}
