package com.ikingtech.platform.service.plugin.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "PluginDTO", description = "插件信息")
public class PluginDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 8142745849783216438L;

    @Schema(name = "id", description = "")
    private String id;

    @Schema(name = "", description = "")
    private String name;

    @Schema(name = "", description = "")
    private String icon;

    @Schema(name = "", description = "")
    private String version;

    @Schema(name = "", description = "")
    private String type;

    @Schema(name = "", description = "")
    private String remark;

    @Schema(name = "", description = "")
    private String helpFileId;

    @Schema(name = "", description = "")
    private String jarFileId;

    @Schema(name = "", description = "")
    private Boolean dependDb;

    @Schema(name = "", description = "")
    private Boolean dependRedis;

    @Schema(name = "", description = "")
    private Boolean dependOss;

    @Schema(name = "", description = "")
    private Boolean dependRabbitMq;
}
