package com.ikingtech.platform.service.plugin.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 岗位信息查询参数
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "PluginQueryParamDTO", description = "插件信息查询参数")
public class PluginQueryParamDTO extends PageParam implements Serializable {

    private static final long serialVersionUID = 4959760741939266595L;

    @Schema(name = "name", description = "插件名称")
    private String name;

    @Schema(name = "remark", description = "插件描述")
    private String remark;
}
