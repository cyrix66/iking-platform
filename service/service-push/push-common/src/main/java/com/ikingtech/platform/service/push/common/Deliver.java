package com.ikingtech.platform.service.push.common;

/**
 * @author tie yan
 */
public interface Deliver {

    DeliverResult send(PushRequest pushRequest, String receiverId);

    DeliverTypeEnum type();
}
