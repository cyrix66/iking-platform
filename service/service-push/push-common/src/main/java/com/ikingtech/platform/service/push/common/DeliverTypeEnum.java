package com.ikingtech.platform.service.push.common;

/**
 * @author tie yan
 */
public enum DeliverTypeEnum {

    WEBSOCKET,

    WEBHOOK,

    WECHAT_MINI,

    DING_TALK,

    SMS
}
