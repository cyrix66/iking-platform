package com.ikingtech.platform.service.push.common;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class MessageBody implements Serializable {

    @Serial
    private static final long serialVersionUID = 1092372830468946137L;

    private String businessKey;

    private String businessName;

    private String messageTitle;

    private Boolean showNotification;

    private List<MessageRedirect> redirectTo;

    private String text;
}
