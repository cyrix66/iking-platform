package com.ikingtech.platform.service.push.common;

import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.InitializingBean;

import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class PushManager implements InitializingBean {

    private static PushManager self = null;

    private final List<Deliver> delivers;

    public static Deliver websocket() {
        for (Deliver deliver : self.delivers) {
            if (DeliverTypeEnum.WEBSOCKET.equals(deliver.type())) {
                return deliver;
            }
        }
        throw new FrameworkException("websocket message deliver not found");
    }

    public static Deliver webhook() {
        for (Deliver deliver : self.delivers) {
            if (DeliverTypeEnum.WEBHOOK.equals(deliver.type())) {
                return deliver;
            }
        }
        throw new FrameworkException("webhook message deliver not found");
    }

    public static Deliver wechatMini() {
        for (Deliver deliver : self.delivers) {
            if (DeliverTypeEnum.WECHAT_MINI.equals(deliver.type())) {
                return deliver;
            }
        }
        throw new FrameworkException("wechatMini message deliver not found");
    }

    public static Deliver sms() {
        for (Deliver deliver : self.delivers) {
            if (DeliverTypeEnum.SMS.equals(deliver.type())) {
                return deliver;
            }
        }
        throw new FrameworkException("websocket message deliver not found");
    }

    private static void setSelf(PushManager manager) {
        self = manager;
    }

    @Override
    public void afterPropertiesSet() {
        if (self == null) {
            setSelf(this);
        }
    }
}
