package com.ikingtech.platform.service.push.common;

import lombok.Data;

import java.util.Map;

/**
 * @author tie yan
 */
@Data
public class PushRequest {

    private String appConfigId;

    private String templateId;

    private MessageBody body;

    private Map<String, Object> templateParams;
}
