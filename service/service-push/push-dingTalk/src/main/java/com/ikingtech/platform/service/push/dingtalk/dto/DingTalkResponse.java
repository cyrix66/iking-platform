package com.ikingtech.platform.service.push.dingtalk.dto;

import lombok.Data;

/**
 * 钉钉接口回执
 *
 * @author tie yan
 */
@Data
public class DingTalkResponse {

    /**
     * 错误码
     */
    private Integer errcode;

    /**
     * 错误信息
     */
    private String errmsg;

    /**
     * 是否成功
     *
     * @return 是否成功
     */
    public boolean success() {
        return this.errcode == 0;
    }
}
