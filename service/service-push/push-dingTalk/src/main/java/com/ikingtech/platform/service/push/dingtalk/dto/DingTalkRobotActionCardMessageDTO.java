package com.ikingtech.platform.service.push.dingtalk.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 卡片消息实体
 *
 * @author tie yan
 */
@Data
public class DingTalkRobotActionCardMessageDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -8212866174443332742L;

    private String title;

    private String text;

    private String btnOrientation;

    private List<Button> btns;

    @Data
    public static class Button implements Serializable {

        @Serial
        private static final long serialVersionUID = -115962089252898225L;

        private String title;

        private String actionURL;
    }
}
