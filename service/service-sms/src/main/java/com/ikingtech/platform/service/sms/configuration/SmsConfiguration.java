package com.ikingtech.platform.service.sms.configuration;

import com.ikingtech.platform.service.sms.service.SmsRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class SmsConfiguration {

    @Bean
    public SmsRepository smsRepository() {
        return new SmsRepository();
    }
}
