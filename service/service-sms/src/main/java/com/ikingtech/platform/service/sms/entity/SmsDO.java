package com.ikingtech.platform.service.sms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sms")
public class SmsDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 1169180175362779861L;

    @TableField(value = "code")
    private String code;

    @TableField(value = "tenant_code")
    private String tenantCode;

    @TableField(value = "name")
    private String name;

    @TableField(value = "type")
    private String type;

    @TableField(value = "app_key")
    private String appKey;

    @TableField(value = "app_secret")
    private String appSecret;

    @TableField(value = "app_id")
    private String appId;

    @TableField(value = "extend_code")
    private String extendCode;

    @TableField(value = "endpoint")
    private String endpoint;

    @TableField(value = "signature")
    private String signature;

    @TableField(value = "support_template")
    private Boolean supportTemplate;
}
