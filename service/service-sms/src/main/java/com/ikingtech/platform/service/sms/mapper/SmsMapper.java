package com.ikingtech.platform.service.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.sms.entity.SmsDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface SmsMapper extends BaseMapper<SmsDO> {
}
