package com.ikingtech.platform.service.system.config.configuration;

import com.ikingtech.framework.sdk.wechat.mini.api.WechatMiniConfigLoader;
import com.ikingtech.platform.service.system.config.service.loader.WechatMiniConfigLoaderImpl;
import com.ikingtech.platform.service.system.config.service.repository.ConfigRepository;
import org.springframework.context.annotation.Bean;

/**
 * @author tie yan
 */
public class SystemConfigConfiguration {

    @Bean
    public ConfigRepository configRepository() {
        return new ConfigRepository();
    }

    @Bean
    public WechatMiniConfigLoader wechatMiniConfigLoader(ConfigRepository repo) {
        return new WechatMiniConfigLoaderImpl(repo);
    }
}
