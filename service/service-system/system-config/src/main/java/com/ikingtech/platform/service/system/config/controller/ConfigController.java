package com.ikingtech.platform.service.system.config.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.base.model.BatchParam;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.integration.PlatformConfigTypeEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.web.annotation.DeleteRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import com.ikingtech.platform.service.system.config.entity.ConfigDO;
import com.ikingtech.platform.service.system.config.model.ConfigDTO;
import com.ikingtech.platform.service.system.config.service.repository.ConfigRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

/**
 * @author wub
 */
@RequiredArgsConstructor
@ApiController(value = "/platform-config", name = "配置管理", description = "配置管理")
public class ConfigController {

    private final ConfigRepository repo;

    @PostRequest(value = "/add", summary = "新增", description = "新增")
    public R<Object> add(@RequestBody ConfigDTO config) {
        this.repo.saveBatch(this.repo.entityConvert(config));
        return R.ok();
    }

    @PostRequest(value = "/add/batch", summary = "批量新增", description = "批量新增")
    public R<Object> addBatch(@RequestBody BatchParam<ConfigDTO> configs) {
        this.repo.saveBatch(Tools.Coll.flatMap(configs.getList(), this.repo::entityConvert, Collection::stream));
        return R.ok();
    }

    @DeleteRequest(value = "/delete", summary = "删除", description = "删除")
    public R<Object> delete(@RequestParam("id") String id) {
        this.repo.remove(Wrappers.<ConfigDO>lambdaQuery().eq(ConfigDO::getDataId, id));
        return R.ok();
    }

    @PostRequest(value = "/update", summary = "更新", description = "更新")
    public R<Object> update(@RequestBody ConfigDTO config) {
        this.repo.remove(Wrappers.<ConfigDO>lambdaQuery().eq(ConfigDO::getDataId, config.getId()));
        this.repo.saveBatch(this.repo.entityConvert(config));
        return R.ok();
    }

    @PostRequest(value = "/update/batch", summary = "批量更新", description = "批量更新")
    public R<Object> updateBatch(@RequestBody BatchParam<ConfigDTO> configs) {
        if (Tools.Coll.isBlank(configs.getList())) {
            return R.ok();
        }
        this.repo.remove(Wrappers.<ConfigDO>lambdaQuery().in(ConfigDO::getDataId, Tools.Coll.convertList(configs.getList(), ConfigDTO::getId)));
        this.repo.saveOrUpdateBatch(Tools.Coll.flatMap(configs.getList(), this.repo::entityConvert, Collection::stream));
        return R.ok();
    }

    @PostRequest(value = "/list/type", summary = "根据类型查询配置", description = "根据类型查询配置")
    public R<List<ConfigDTO>> listByType(@RequestBody String type) {
        List<ConfigDO> entities = this.repo.list(Wrappers.<ConfigDO>lambdaQuery()
                .eq(ConfigDO::getType, type));
        if (Tools.Coll.isBlank(entities)) {
            return R.ok(Collections.emptyList());
        }
        Map<String, List<ConfigDO>> configMap = Tools.Coll.convertGroup(entities, ConfigDO::getDataId);
        List<ConfigDTO> result = new ArrayList<>();
        configMap.forEach((dataId, configEntities) -> {
            ConfigDTO config = new ConfigDTO();
            Map<String, Object> propertyMap = new HashMap<>();
            config.setId(dataId);
            if (Tools.Str.isNotBlank(type)) {
                config.setType(PlatformConfigTypeEnum.valueOf(type));
                config.setTypeName(config.getType().description);
            }
            configEntities.forEach(configEntity -> propertyMap.put(configEntity.getPropertyKey(), configEntity.getPropertyValue()));
            config.setProperties(propertyMap);
            result.add(config);
        });
        return R.ok(result);
    }
}
