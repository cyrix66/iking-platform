package com.ikingtech.platform.service.system.config.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author wub
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_config")
public class ConfigDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -7991538060443195604L;

    @TableField("tenant_code")
    private String tenantCode;

    @TableField("type")
    private String type;

    @TableField("parent_key")
    private String parentKey;

    @TableField("property_key")
    private String propertyKey;

    @TableField("property_value")
    private String propertyValue;

    @TableField("data_id")
    private String dataId;
}
