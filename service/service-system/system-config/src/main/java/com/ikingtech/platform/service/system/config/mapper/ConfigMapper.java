package com.ikingtech.platform.service.system.config.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.system.config.entity.ConfigDO;

/**
 * @author wub
 */

public interface ConfigMapper extends BaseMapper<ConfigDO> {
}
