package com.ikingtech.platform.service.system.config.model;

import com.ikingtech.framework.sdk.enums.integration.PlatformConfigTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author wub
 */

@Data
@Schema(name = "PlatformConfigDTO", description = "平台配置信息")
public class ConfigDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -5921078990351473326L;

    @Schema(name = "id")
    private String id;

    @Schema(name = "type", description = "类型")
    private PlatformConfigTypeEnum type;

    @Schema(name = "typeName", description = "类型名称")
    private String typeName;

    @Schema(name = "properties", description = "配置内容")
    private Object properties;
}
