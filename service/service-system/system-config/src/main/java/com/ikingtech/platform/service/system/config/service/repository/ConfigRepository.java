package com.ikingtech.platform.service.system.config.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.config.entity.ConfigDO;
import com.ikingtech.platform.service.system.config.mapper.ConfigMapper;
import com.ikingtech.platform.service.system.config.model.ConfigDTO;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author wub
 */
@RequiredArgsConstructor
public class ConfigRepository extends ServiceImpl<ConfigMapper, ConfigDO> {

    public List<ConfigDO> entityConvert(ConfigDTO config) {
        String dataId = Tools.Str.isNotBlank(config.getId()) ? config.getId() : Tools.Id.uuid();
        Map<String, Object> propertyMap = Tools.Json.objToMap(config.getProperties());
        List<ConfigDO> entities = new ArrayList<>();
        propertyMap.forEach((key, value) -> {
            ConfigDO entity = new ConfigDO();
            entity.setId(Tools.Id.uuid());
            entity.setType(config.getType().name());
            entity.setDataId(dataId);
            entity.setTenantCode(Me.tenantCode());
            entity.setPropertyKey(key);
            entity.setPropertyValue(String.valueOf(value));
            entities.add(entity);
        });
        return entities;
    }
}
