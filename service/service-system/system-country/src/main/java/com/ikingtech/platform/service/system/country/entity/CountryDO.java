package com.ikingtech.platform.service.system.country.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.SortEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("country")
public class CountryDO extends SortEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 6029782571539770702L;

    @TableField("code")
    private String code;

    @TableField("full_code")
    private String fullCode;

    @TableField("name")
    private String name;

    @TableField("eu_name")
    private String euName;

    @TableField("continent")
    private String continent;
}
