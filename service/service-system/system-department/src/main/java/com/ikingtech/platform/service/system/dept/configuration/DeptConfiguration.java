package com.ikingtech.platform.service.system.dept.configuration;

import com.iking.framework.sdk.authorization.api.AuthorizationDepartmentApi;
import com.ikingtech.framework.sdk.approve.api.ApproveFormDepartmentApi;
import com.ikingtech.framework.sdk.component.api.CompDepartmentApi;
import com.ikingtech.framework.sdk.department.api.DeptDictApi;
import com.ikingtech.framework.sdk.department.api.DeptRoleDataScopeApi;
import com.ikingtech.framework.sdk.department.api.DeptUserApi;
import com.ikingtech.framework.sdk.user.api.UserDeptApi;
import com.ikingtech.platform.service.system.dept.service.*;
import com.ikingtech.platform.service.system.dept.service.repository.DeptRepository;
import com.ikingtech.platform.service.system.dept.service.repository.ModelConverter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class DeptConfiguration {

    @Bean
    public DeptRepository deptRepository() {
        return new DeptRepository();
    }

    @Bean
    @ConditionalOnMissingBean({CompDepartmentApi.class})
    public CompDepartmentApi compDepartmentApi(DeptRepository repo, ModelConverter converter) {
        return new CompDepartmentService(repo, converter);
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.role.service.DeptRoleDataScopeService"})
    @ConditionalOnMissingBean({DeptRoleDataScopeApi.class})
    public DeptRoleDataScopeApi deptRoleDataScopeApi() {
        return new DefaultDeptRoleDataScopeService();
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.user.service.DeptUserService"})
    @ConditionalOnMissingBean({DeptUserApi.class})
    public DeptUserApi deptUserApi() {
        return new DefaultDeptUserService();
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.dict.service.DeptDictService"})
    @ConditionalOnMissingBean({DeptUserApi.class})
    public DeptDictApi deptDictApi() {
        return new DefaultDeptDictService();
    }

    @Bean
    @ConditionalOnMissingBean({AuthorizationDepartmentApi.class})
    public AuthorizationDepartmentApi authorizationDeptApi(DeptRepository repo) {
        return new AuthorizationDepartmentService(repo);
    }

    @Bean
    @ConditionalOnMissingBean({ApproveFormDepartmentApi.class})
    public ApproveFormDepartmentApi approveFormDepartmentApi(DeptRepository repo, DeptUserApi deptUserApi) {
        return new ApproveFormDepartmentService(repo, deptUserApi);
    }

    @Bean
    @ConditionalOnMissingBean({UserDeptApi.class})
    public UserDeptApi userDeptApi(DeptRepository repo, DeptUserApi deptUserApi) {
        return new UserDeptService(repo, deptUserApi);
    }
}
