package com.ikingtech.platform.service.system.dept.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.SortEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("department")
public class DepartmentDO extends SortEntity implements Serializable {

	@Serial
    private static final long serialVersionUID = -7628716592427134884L;

	@TableField("full_path")
	private String fullPath;

	@TableField("parent_id")
	private String parentId;

	@TableField("tenant_code")
	private String tenantCode;

	@TableField("name")
	private String name;

	@TableField("type")
	private String type;

	@TableField("category")
	private String category;

	@TableField("status")
	private String status;

	@TableField("equity_ratio")
	private String equityRatio;

	@TableField("grade")
	private Integer grade;

	@TableField("code")
	private String code;

	@TableField("short_name")
	private String shortName;

	@TableField("establish_date")
	private LocalDate establishDate;

	@TableField("manager_id")
	private String managerId;

	@TableField("juridical_person_name")
	private String juridicalPersonName;

	@TableField("juridical_person_phone")
	private String juridicalPersonPhone;

	@TableField("juridical_person_identity_no")
	private String juridicalPersonIdentityNo;

	@TableField("phone")
	private String phone;

	@TableField("province_code")
	private String provinceCode;

	@TableField("province_name")
	private String provinceName;

	@TableField("city_code")
	private String cityCode;

	@TableField("city_name")
	private String cityName;

	@TableField("district_code")
	private String districtCode;

	@TableField("district_name")
	private String districtName;

	@TableField("town_code")
	private String townCode;

	@TableField("town_name")
	private String townName;

	@TableField("street_code")
	private String streetCode;

	@TableField("street_name")
	private String streetName;

	@TableField("address")
	private String address;

	@TableField("social_credit_identity_code")
	private String socialCreditIdentityCode;

	@TableField("bank_name")
	private String bankName;

	@TableField("bank_account")
	private String bankAccount;

	@TableField("home_page")
	private String homePage;

	@TableField(" postal_code")
	private String postalCode;

	@TableField("fax")
	private String fax;

	@TableField("introduction_type")
	private String introductionType;

	@TableField("introduction")
	private String introduction;

	@TableLogic
	@TableField("del_flag")
	private Boolean delFlag;
}
