package com.ikingtech.platform.service.system.dept.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * 部门相关异常信息
 *
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DeptExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 部门不存在
     */
    DEPT_NOT_FOUND("deptNotFound"),

    /**
     * 已存在相同名称的部门
     */
    DUPLICATE_DEPT_NAME("duplicateDeptName"),

    /**
     * 父部门不存在
     */
    PARENT_DEPT_NOT_FOUND("parentDeptNotFound");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "system-department";
    }
}
