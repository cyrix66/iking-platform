package com.ikingtech.platform.service.system.dept.service;

import com.ikingtech.framework.sdk.component.model.ComponentDepartment;
import com.ikingtech.platform.service.system.dept.service.repository.DeptRepository;
import com.ikingtech.platform.service.system.dept.service.repository.ModelConverter;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public class CompDepartmentService extends AbstractCompDepartmentService {

    public CompDepartmentService(DeptRepository service, ModelConverter converter) {
        super(service, converter);
    }

    @Override
    public List<ComponentDepartment> listByName(String name, Boolean dataScopeOnly) {
        return super.listByName(name, dataScopeOnly);
    }

    @Override
    public List<ComponentDepartment> listByParentId(String parentId, Boolean dataScopeOnly) {
        return super.listByParentId(parentId, dataScopeOnly);
    }

    @Override
    public ComponentDepartment load(String parentId) {
        return super.load(parentId);
    }

    @Override
    public Map<String, String> extractDepartmentFullName(Map<String, String> deptFullPathMap) {
        return super.extractDepartmentFullName(deptFullPathMap);
    }
}
