package com.ikingtech.platform.service.system.dept.service;

import com.ikingtech.framework.sdk.base.model.DictItem;
import com.ikingtech.framework.sdk.department.api.DeptDictApi;

import java.util.*;

/**
 * @author tie yan
 */
public class DefaultDeptDictService implements DeptDictApi {

    @Override
    public DictItem load(String value, String tenantCode) {
        return null;
    }

    @Override
    public Map<String, DictItem> load(List<String> values, String tenantCode) {
        return Collections.emptyMap();
    }
}
