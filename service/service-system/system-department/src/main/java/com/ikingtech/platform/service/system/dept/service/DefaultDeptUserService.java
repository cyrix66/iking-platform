package com.ikingtech.platform.service.system.dept.service;

import com.ikingtech.framework.sdk.department.api.DeptUserApi;
import com.ikingtech.framework.sdk.department.model.DeptManagerDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public class DefaultDeptUserService implements DeptUserApi {

    @Override
    public void moveUser(String parentId, List<String> deptIds, String tenantCode) {

    }

    @Override
    public DeptManagerDTO loadManager(String userId) {
        return null;
    }

    @Override
    public List<DeptManagerDTO> loadManagers(List<String> userIds) {
        return new ArrayList<>();
    }

    @Override
    public Map<String, Integer> getUserCount(List<String> deptIds) {
        return new HashMap<>();
    }
}
