package com.ikingtech.platform.service.system.dict.configuration;

import com.ikingtech.framework.sdk.department.api.DeptDictApi;
import com.ikingtech.platform.service.system.dict.service.DeptDictService;
import com.ikingtech.platform.service.system.dict.service.repository.DictItemRepository;
import com.ikingtech.platform.service.system.dict.service.repository.DictRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class DictConfiguration {

    @Bean
    public DictRepository dictRepository() {
        return new DictRepository();
    }

    @Bean
    public DictItemRepository dictItemRepository() {
        return new DictItemRepository();
    }

    @Bean
    @ConditionalOnMissingBean({DeptDictApi.class})
    public DeptDictApi deptDictApi(DictItemRepository repo) {
        return new DeptDictService(repo);
    }
}
