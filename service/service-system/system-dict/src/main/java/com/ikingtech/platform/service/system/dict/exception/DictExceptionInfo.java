package com.ikingtech.platform.service.system.dict.exception;


import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DictExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 指定字典不存在
     */
    DICT_NOT_FOUND("dictNotFound"),

    /**
     * 不允许删除系统字典
     */
    DELETE_SYSTEM_DICT_IS_NOT_ALLOWED("deleteSystemDictIsNotAllowed"),

    /**
     * 已存在相同名称的字典
     */
    DUPLICATE_DICT_NAME("duplicateDictName"),

    /**
     * 已存在相同标识的字典
     */
    DUPLICATE_DICT_CODE("duplicateDictCode"),

    /**
     * 指定字典项不存在
     */
    DICT_ITEM_NOT_FOUND("dictItemNotFound"),

    /**
     * 已存在相同名称的字典
     */
    DUPLICATE_DICT_ITEM_NAME("duplicateDictItemName"),

    /**
     * 父级字典项不存在
     */
    PARENT_DICT_ITEM_NOT_FOUND("parentDictItemNotFound"),

    /**
     * 系统字典不允许添加字典项
     */
    ADD_SYSTEM_DICT_ITEM_IS_NOT_ALLOWED("addSystemDictItemIsNotAllowed"),

    /**
     * 不允许删除系统字典项
     */
    DELETE_SYSTEM_DICT_ITEM_IS_NOT_ALLOWED("deleteSystemDictItemIsNotAllowed");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "system-dict";
    }
}
