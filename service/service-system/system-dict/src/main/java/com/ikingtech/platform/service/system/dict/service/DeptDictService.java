package com.ikingtech.platform.service.system.dict.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.base.model.DictItem;
import com.ikingtech.framework.sdk.department.api.DeptDictApi;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.dict.entity.DictItemDO;
import com.ikingtech.platform.service.system.dict.service.repository.DictItemRepository;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class DeptDictService implements DeptDictApi {

    private final DictItemRepository repo;

    @Override
    public DictItem load(String value, String tenantCode) {
        String label = this.repo.getObj(Wrappers.<DictItemDO>lambdaQuery()
                        .select(DictItemDO::getLabel)
                        .eq(DictItemDO::getValue, value)
                        .eq(DictItemDO::getTenantCode, tenantCode),
                String.class::cast);
        return Tools.Str.isNotBlank(label) ? new DictItem(value, label) : null;
    }

    @Override
    public Map<String, DictItem> load(List<String> values, String tenantCode) {
        if (Tools.Coll.isBlank(values)) {
            return Collections.emptyMap();
        }
        List<DictItemDO> entities = this.repo.list(Wrappers.<DictItemDO>lambdaQuery()
                .in(DictItemDO::getValue, values)
                .eq(DictItemDO::getTenantCode, tenantCode));
        return Tools.Coll.convertMap(entities, DictItemDO::getValue, entity -> new DictItem(entity.getValue(), entity.getLabel()));
    }
}
