package com.ikingtech.platform.service.system.division.configuration;

import com.ikingtech.framework.sdk.component.api.CompDivisionApi;
import com.ikingtech.platform.service.system.division.service.CompDivisionService;
import com.ikingtech.platform.service.system.division.service.DivisionService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * @author tie yan
 */
@Configuration
public class DivisionConfiguration {

    @Bean
    public DivisionService divisionService(StringRedisTemplate redisTemplate) {
        return new DivisionService(redisTemplate);
    }

    @Bean
    @ConditionalOnMissingBean({CompDivisionApi.class})
    public CompDivisionApi compDivisionApi(StringRedisTemplate redisTemplate) {
        return new CompDivisionService(redisTemplate);
    }
}
