package com.ikingtech.platform.service.system.division.service;

import com.ikingtech.framework.sdk.component.model.DivisionElement;
import com.ikingtech.framework.sdk.enums.system.division.DivisionLevelEnum;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

/**
 * @author tie yan
 */
public class CompDivisionService extends AbstractCompDivisionService {

    public CompDivisionService(StringRedisTemplate redisTemplate) {
        super(redisTemplate);
    }

    @Override
    public List<DivisionElement> load(DivisionLevelEnum level, Long parentCode) {
        return super.load(level, parentCode);
    }
}
