package com.ikingtech.platform.service.system.menu.configuration;

import com.ikingtech.framework.sdk.application.api.ApplicationMenuApi;
import com.ikingtech.framework.sdk.menu.api.MenuRoleApi;
import com.ikingtech.framework.sdk.menu.api.MenuUserApi;
import com.ikingtech.framework.sdk.user.api.UserMenuApi;
import com.ikingtech.platform.service.system.menu.service.ApplicationMenuService;
import com.ikingtech.platform.service.system.menu.service.DefaultMenuRoleService;
import com.ikingtech.platform.service.system.menu.service.DefaultMenuUserService;
import com.ikingtech.platform.service.system.menu.service.UserMenuService;
import com.ikingtech.platform.service.system.menu.service.repository.MenuRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.context.annotation.Bean;

/**
 * @author tie yan
 */
public class MenuConfiguration {

    @Bean
    public MenuRepository menuRepository() {
        return new MenuRepository();
    }

    @Bean
    @ConditionalOnMissingBean({ApplicationMenuApi.class})
    public ApplicationMenuApi applicationMenuApi(MenuRepository repo) {
        return new ApplicationMenuService(repo);
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.role.service.MenuRoleService"})
    @ConditionalOnMissingBean({MenuRoleApi.class})
    public MenuRoleApi menuRoleApi() {
        return new DefaultMenuRoleService();
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.user.service.MenuUserService"})
    @ConditionalOnMissingBean({MenuUserApi.class})
    public MenuUserApi menuUserApi() {
        return new DefaultMenuUserService();
    }

    @Bean
    @ConditionalOnMissingBean({UserMenuApi.class})
    public UserMenuApi userMenuApi(MenuRepository repo) {
        return new UserMenuService(repo);
    }
}
