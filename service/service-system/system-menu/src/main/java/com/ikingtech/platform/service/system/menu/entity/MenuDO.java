package com.ikingtech.platform.service.system.menu.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.SortEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@RequiredArgsConstructor
@TableName("menu")
public class MenuDO extends SortEntity implements Serializable {

	@Serial
    private static final long serialVersionUID = -1194157225308288847L;

	@TableField("full_path")
	private String fullPath;

	@TableField("parent_id")
	private String parentId;

	@TableField("name")
	private String name;

	@TableField("domain_code")
	private String domainCode;

	@TableField("tenant_code")
	private String tenantCode;

	@TableField("app_code")
	private String appCode;

	@TableField("component")
	private String component;

	@TableField("page_path")
	private String pagePath;

	@TableField("framework")
	private Boolean framework;

	@TableField("eu_name")
	private String euName;

	@TableField("permission_code")
	private String permissionCode;

	@TableField("icon")
	private String icon;

	@TableField("active_icon")
	private String activeIcon;

	@TableField("logo")
	private String logo;

	@TableField("link")
	private String link;

	@TableField("iframe")
	private String iframe;

	@TableField("default_opened")
	private Boolean defaultOpened;

	@TableField("permanent")
	private Boolean permanent;

	@TableField("sidebar")
	private Boolean sidebar;

	@TableField("breadcrumb")
	private Boolean breadcrumb;

	@TableField("active_menu")
	private String activeMenu;

	@TableField("cache")
	private String cache;

	@TableField("no_cache")
	private String noCache;

	@TableField("badge")
	private String badge;

	@TableField("copyright")
	private Boolean copyright;

	@TableField("padding_bottom")
	private String paddingBottom;

	@TableField("whitelist")
	private String whitelist;

	@TableField("menu_view_type")
	private String menuViewType;

	@TableField("menu_type")
	private String menuType;

	@TableField("jump_type")
	private String jumpType;

	@TableField("keep_alive")
	private Boolean keepAlive;

	@TableField("visible")
	private Boolean visible;

	@TableField("auto_render")
	private Boolean autoRender;

	@TableField("remark")
	private String remark;

	@TableField("preset")
	private Boolean preset;
}
