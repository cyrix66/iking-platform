package com.ikingtech.platform.service.system.menu.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum MenuExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 菜单不存在
     */
    MENU_NOT_FOUND("menuNotFound"),

    /**
     * 已存在相同名称的菜单
     */
    DUPLICATE_MENU_NAME("duplicateMenuName"),

    /**
     * 页签或按钮不能拖动到其他菜单下
     */
    DRAG_TAB_OR_BUTTON_TO_OTHER_MENU_NOT_ALLOWED("dragTabOrButtonToOtherMenuNotAllowed"),

    /**
     * 菜单不能拖动到页签或按钮下
     */
    DRAG_MENU_TO_TAB_OR_BUTTON_NOT_ALLOWED("dragMenuToTabOrButtonNotAllowed"),

    /**
     * 父菜单不存在
     */
    PARENT_MENU_NOT_FOUND("parentMenuNotFound");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "system-menu";
    }
}
