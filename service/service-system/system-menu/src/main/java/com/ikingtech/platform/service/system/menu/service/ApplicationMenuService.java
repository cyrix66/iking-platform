package com.ikingtech.platform.service.system.menu.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.application.api.ApplicationMenuApi;
import com.ikingtech.framework.sdk.enums.domain.DomainEnum;
import com.ikingtech.framework.sdk.enums.system.menu.MenuJumpTypeEnum;
import com.ikingtech.framework.sdk.enums.system.menu.MenuTypeEnum;
import com.ikingtech.framework.sdk.enums.system.menu.MenuViewTypeEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.menu.entity.MenuDO;
import com.ikingtech.platform.service.system.menu.service.repository.MenuRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class ApplicationMenuService implements ApplicationMenuApi {

    private final MenuRepository repo;

    @Override
    public void saveOrUpdate(String id,
                             String name,
                             String parentId,
                             String icon,
                             String link,
                             String component,
                             Boolean sidebar,
                             String activeMenu,
                             Integer sortOrder,
                             String tenantCode,
                             String appCode) {
        MenuDO entity = this.repo.getOne(Wrappers.<MenuDO>lambdaQuery().eq(MenuDO::getPermissionCode, id).eq(MenuDO::getTenantCode, tenantCode));
        String parentMenuId = Tools.Str.EMPTY;
        if (Tools.Str.isNotBlank(parentId)) {
            parentMenuId = this.repo.getObj(Wrappers.<MenuDO>lambdaQuery().eq(MenuDO::getPermissionCode, parentId).eq(MenuDO::getTenantCode, tenantCode), String.class::cast);
        }
        if (null == entity) {
            entity = new MenuDO();
            entity.setId(Tools.Id.uuid());
            entity.setDomainCode(DomainEnum.APPLICATION.name());
            entity.setTenantCode(tenantCode);
            entity.setAppCode(appCode);
            entity.setName(name);
            entity.setParentId(parentMenuId);
            entity.setPermissionCode(id);
            entity.setIcon(icon);
            entity.setLink(link);
            entity.setMenuType(MenuTypeEnum.MENU.name());
            entity.setMenuViewType(MenuViewTypeEnum.MANAGE.name());
            entity.setJumpType(MenuJumpTypeEnum.FRONT_ROUTE.name());
            entity.setComponent(component);
            entity.setVisible(true);
            entity.setFramework(true);
            entity.setBreadcrumb(true);
            entity.setSidebar(sidebar);
            entity.setActiveMenu(activeMenu);
            entity.setSortOrder(sortOrder);
        } else {
            entity.setParentId(parentMenuId);
            entity.setIcon(icon);
            entity.setName(name);
            entity.setPermissionCode(id);
            entity.setLink(link);
            entity.setComponent(component);
            entity.setSidebar(sidebar);
            entity.setActiveMenu(activeMenu);
        }
        entity.setFullPath(this.repo.parseFullPath(parentMenuId, entity.getId()));
        this.repo.saveOrUpdate(entity);
    }

    @Override
    public void removeByPermissionCode(String pageId, String tenantCode, String appCode) {
        this.repo.remove(Wrappers.<MenuDO>lambdaQuery().eq(MenuDO::getPermissionCode, pageId).eq(MenuDO::getTenantCode, tenantCode).eq(MenuDO::getAppCode, appCode));
    }

    @Override
    public void removeByPermissionCodes(List<String> pageIds, String tenantCode, String appCode) {
        if (Tools.Coll.isBlank(pageIds)) {
            return;
        }
        this.repo.remove(Wrappers.<MenuDO>lambdaQuery()
                .in(MenuDO::getPermissionCode, pageIds)
                .eq(MenuDO::getTenantCode, tenantCode)
                .eq(MenuDO::getAppCode, appCode));
    }
}
