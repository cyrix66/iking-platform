package com.ikingtech.platform.service.system.post.configuration;

import com.ikingtech.framework.sdk.approve.api.ApproveFormPostApi;
import com.ikingtech.framework.sdk.component.api.CompPostApi;
import com.ikingtech.framework.sdk.post.api.PostUserApi;
import com.ikingtech.framework.sdk.user.api.UserPostApi;
import com.ikingtech.platform.service.system.post.service.ApproveFormPostService;
import com.ikingtech.platform.service.system.post.service.CompPostService;
import com.ikingtech.platform.service.system.post.service.DefaultPostUserService;
import com.ikingtech.platform.service.system.post.service.UserPostService;
import com.ikingtech.platform.service.system.post.service.repository.PostRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class PostConfiguration {

    @Bean
    public PostRepository postRepository() {
        return new PostRepository();
    }

    @Bean
    @ConditionalOnMissingBean({CompPostApi.class})
    public CompPostApi compPostApi(PostRepository service) {
        return new CompPostService(service);
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.user.service.PostUserService"})
    @ConditionalOnMissingBean({PostUserApi.class})
    public PostUserApi postUserApi() {
        return new DefaultPostUserService();
    }

    @Bean
    @ConditionalOnMissingBean({UserPostApi.class})
    public UserPostApi userPostApi(PostRepository repo) {
        return new UserPostService(repo);
    }

    @Bean
    @ConditionalOnMissingBean({ApproveFormPostApi.class})
    public ApproveFormPostApi approveFormPostApi(PostRepository service) {
        return new ApproveFormPostService(service);
    }
}
