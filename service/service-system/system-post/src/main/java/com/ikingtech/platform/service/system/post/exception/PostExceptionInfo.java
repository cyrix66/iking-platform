package com.ikingtech.platform.service.system.post.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum PostExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 已存在相同名称的岗位
     */
    DUPLICATE_POST_NAME("duplicatePostName"),

    /**
     * 指定岗位不存在
     */
    POST_NOT_FOUND("postNotFound");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "system-post";
    }
}
