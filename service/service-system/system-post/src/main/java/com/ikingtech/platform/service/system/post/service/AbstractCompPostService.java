package com.ikingtech.platform.service.system.post.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.component.api.CompPostApi;
import com.ikingtech.framework.sdk.component.model.ComponentPost;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.enums.common.ElementTypeEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.post.entity.PostDO;
import com.ikingtech.platform.service.system.post.service.repository.PostRepository;
import lombok.RequiredArgsConstructor;

import java.util.*;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public abstract class AbstractCompPostService implements CompPostApi {

    private final PostRepository service;

    @Override
    public List<ComponentPost> listByName(String name) {
        return this.convert(this.service.list(Wrappers.<PostDO>lambdaQuery()
                .like(PostDO::getName, name)
                .eq(PostDO::getTenantCode, Me.tenantCode())));
    }

    private List<ComponentPost> convert(List<PostDO> posts) {
        return Tools.Coll.convertList(posts, post -> {
            ComponentPost compPost = new ComponentPost();
            compPost.setElementId(post.getId());
            compPost.setElementName(post.getName());
            compPost.setElementType(ElementTypeEnum.POST);
            compPost.setTenantCode(post.getTenantCode());
            return compPost;
        });
    }
}
