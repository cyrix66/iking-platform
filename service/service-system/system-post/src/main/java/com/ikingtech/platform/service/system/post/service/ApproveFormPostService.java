package com.ikingtech.platform.service.system.post.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.approve.api.ApproveFormPostApi;
import com.ikingtech.framework.sdk.approve.model.ApproveFormPost;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.post.entity.PostDO;
import com.ikingtech.platform.service.system.post.service.repository.PostRepository;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class ApproveFormPostService implements ApproveFormPostApi {

    private final PostRepository repo;

    @Override
    public Map<String, ApproveFormPost> mapByIds(List<String> postIds) {
        // 如果ID列表为空，则返回空的HashMap
        if (Tools.Coll.isBlank(postIds)) {
            return Collections.emptyMap();
        }
        // 查询PostDO列表，根据ID进行查询，且租户代码相同
        return Tools.Coll.convertMap(this.repo.listByIds(postIds),
                PostDO::getId,
                entity -> {
                    ApproveFormPost post = new ApproveFormPost();
                    post.setPostId(entity.getId());
                    post.setPostName(entity.getName());
                    return post;
                });
    }

    @Override
    public List<String> loadIdByName(String postName, String tenantCode) {
        return this.repo.listObjs(Wrappers.<PostDO>lambdaQuery()
                .select(PostDO::getId)
                .eq(PostDO::getTenantCode, tenantCode)
                .like(PostDO::getName, postName));
    }
}
