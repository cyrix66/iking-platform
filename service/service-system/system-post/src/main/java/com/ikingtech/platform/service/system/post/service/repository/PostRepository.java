package com.ikingtech.platform.service.system.post.service.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.post.model.PostQueryParamDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.post.entity.PostDO;
import com.ikingtech.platform.service.system.post.mapper.PostMapper;

/**
 * @author tie yan
 */
public class PostRepository extends ServiceImpl<PostMapper, PostDO> {

    public static LambdaQueryWrapper<PostDO> createWrapper(PostQueryParamDTO queryParam, String tenantCode) {
        return Wrappers.<PostDO>lambdaQuery()
                .like(Tools.Str.isNotBlank(queryParam.getName()), PostDO::getName, queryParam.getName())
                .like(Tools.Str.isNotBlank(queryParam.getRemark()), PostDO::getRemark, queryParam.getRemark())
                .like(Tools.Str.isNotBlank(queryParam.getCreateName()), PostDO::getCreateName, queryParam.getCreateName())
                .eq(PostDO::getTenantCode, tenantCode)
                .orderByDesc(PostDO::getCreateTime);
    }
}
