package com.ikingtech.platform.service.system.role.configuration;

import com.iking.framework.sdk.authorization.api.AuthorizationRoleApi;
import com.ikingtech.framework.sdk.approve.api.ApproveFormRoleApi;
import com.ikingtech.framework.sdk.component.api.CompRoleApi;
import com.ikingtech.framework.sdk.department.api.DeptRoleDataScopeApi;
import com.ikingtech.framework.sdk.menu.api.MenuRoleApi;
import com.ikingtech.framework.sdk.role.api.RoleUserApi;
import com.ikingtech.framework.sdk.user.api.UserRoleApi;
import com.ikingtech.platform.service.system.role.service.*;
import com.ikingtech.platform.service.system.role.service.repository.RoleDataScopeRepository;
import com.ikingtech.platform.service.system.role.service.repository.RoleMenuRepository;
import com.ikingtech.platform.service.system.role.service.repository.RoleRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class RoleConfiguration {

    @Bean
    public RoleRepository roleRepository() {
        return new RoleRepository();
    }

    @Bean
    public RoleDataScopeRepository roleDataScopeRepository() {
        return new RoleDataScopeRepository();
    }

    @Bean
    public RoleMenuRepository roleMenuRepository() {
        return new RoleMenuRepository();
    }

    @Bean
    @ConditionalOnMissingBean({CompRoleApi.class})
    public CompRoleApi compRoleApi(RoleRepository service) {
        return new CompRoleService(service);
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.user.service.RoleUserService"})
    @ConditionalOnMissingBean({RoleUserApi.class})
    public RoleUserApi roleUserApi() {
        return new DefaultRoleUserService();
    }

    @Bean
    @ConditionalOnMissingBean({AuthorizationRoleApi.class})
    public AuthorizationRoleApi authorizationRoleApi(RoleMenuRepository roleMenuRepo) {
        return new AuthorizationRoleService(roleMenuRepo);
    }

    @Bean
    @ConditionalOnMissingBean({DeptRoleDataScopeApi.class})
    public DeptRoleDataScopeApi deptRoleDataScopeApi(RoleDataScopeRepository roleDataScopeService) {
        return new DeptRoleDataScopeService(roleDataScopeService);
    }

    @Bean
    @ConditionalOnMissingBean({MenuRoleApi.class})
    public MenuRoleApi menuRoleApi(RoleMenuRepository roleMenuRepo) {
        return new MenuRoleService(roleMenuRepo);
    }

    @Bean
    @ConditionalOnMissingBean({ApproveFormRoleApi.class})
    public ApproveFormRoleApi approveFormRoleApi(RoleRepository repo) {
        return new ApproveFormRoleService(repo);
    }

    @Bean
    @ConditionalOnMissingBean({UserRoleApi.class})
    public UserRoleApi userRoleApi(RoleRepository repo,
                                   RoleDataScopeRepository roleDataScopeRepo,
                                   RoleMenuRepository roleMenuRepo) {
        return new UserRoleService(repo, roleDataScopeRepo, roleMenuRepo);
    }
}
