/*
 *
 *      Copyright (c) 2018-2025, iKing Tech All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: tie yan
 *
 */

package com.ikingtech.platform.service.system.role.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.SortEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("role")
public class RoleDO extends SortEntity implements Serializable {

	@Serial
    private static final long serialVersionUID = 3485783530578477387L;

	@TableField(value = "name")
	private String name;

	@TableField("domain_code")
	private String domainCode;

	@TableField("tenant_code")
	private String tenantCode;

	@TableField("app_code")
	private String appCode;

	@TableField(value = "data_scope_type")
	private String dataScopeType;

	@TableField(value = "remark")
	private String remark;

	@TableLogic
	@TableField(value = "del_flag")
	private Boolean delFlag;
}
