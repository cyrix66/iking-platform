package com.ikingtech.platform.service.system.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.system.role.entity.RoleDataScopeDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Administrator
 */
@Mapper
public interface RoleDataScopeMapper extends BaseMapper<RoleDataScopeDO> {
}
