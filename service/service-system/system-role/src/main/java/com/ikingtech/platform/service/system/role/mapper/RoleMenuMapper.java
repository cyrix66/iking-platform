package com.ikingtech.platform.service.system.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.system.role.entity.RoleMenuDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface RoleMenuMapper extends BaseMapper<RoleMenuDO> {
}
