package com.ikingtech.platform.service.system.role.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.approve.api.ApproveFormRoleApi;
import com.ikingtech.framework.sdk.approve.model.ApproveFormRole;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.role.entity.RoleDO;
import com.ikingtech.platform.service.system.role.service.repository.RoleRepository;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class ApproveFormRoleService implements ApproveFormRoleApi {

    private final RoleRepository repo;

    @Override
    public Map<String, ApproveFormRole> mapByIds(List<String> roleIds) {
        // 如果角色ID列表为空，则返回空的映射结果
        if (Tools.Coll.isBlank(roleIds)) {
            return Collections.emptyMap();
        }
        // 使用角色ID列表查询角色数据，并将查询结果转换为映射结果
        return Tools.Coll.convertMap(this.repo.listByIds(roleIds),
                RoleDO::getId,
                entity -> {
                    ApproveFormRole role = new ApproveFormRole();
                    role.setRoleId(entity.getId());
                    role.setRoleName(entity.getName());
                    return role;
                });
    }

    @Override
    public List<String> loadIdByName(String roleName, String tenantCode) {
        return this.repo.listObjs(Wrappers.<RoleDO>lambdaQuery()
                .select(RoleDO::getId)
                .eq(RoleDO::getTenantCode, tenantCode)
                .like(RoleDO::getName, roleName));
    }
}
