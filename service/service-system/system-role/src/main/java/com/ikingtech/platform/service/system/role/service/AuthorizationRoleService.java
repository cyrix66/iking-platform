package com.ikingtech.platform.service.system.role.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.iking.framework.sdk.authorization.api.AuthorizationRoleApi;
import com.iking.framework.sdk.authorization.model.AuthorizationRoleMenu;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.role.entity.RoleMenuDO;
import com.ikingtech.platform.service.system.role.service.repository.RoleMenuRepository;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class AuthorizationRoleService implements AuthorizationRoleApi {

    private final RoleMenuRepository roleMenuRepo;

    @Override
    public List<AuthorizationRoleMenu> loadMenu(List<String> roleIds) {
        if (Tools.Coll.isBlank(roleIds)) {
            return new ArrayList<>();
        }
        return Tools.Coll.convertList(this.roleMenuRepo.list(Wrappers.<RoleMenuDO>lambdaQuery().in(RoleMenuDO::getRoleId, roleIds)), roleMenuEntity -> {
            AuthorizationRoleMenu roleMenu = new AuthorizationRoleMenu();
            roleMenu.setMenuId(roleMenuEntity.getMenuId());
            roleMenu.setRoleId(roleMenuEntity.getRoleId());
            return roleMenu;
        });
    }
}
