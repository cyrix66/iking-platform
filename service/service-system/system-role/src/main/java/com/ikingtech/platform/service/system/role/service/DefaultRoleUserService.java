package com.ikingtech.platform.service.system.role.service;

import com.ikingtech.framework.sdk.role.api.RoleUserApi;

import java.util.List;

/**
 * @author tie yan
 */
public class DefaultRoleUserService implements RoleUserApi {

    @Override
    public void removeUserRole(String roleId) {

    }

    @Override
    public void bindUserRole(String roleId, List<String> userIds) {

    }

    @Override
    public void unbindUserRole(String roleId, List<String> userIds) {

    }
}
