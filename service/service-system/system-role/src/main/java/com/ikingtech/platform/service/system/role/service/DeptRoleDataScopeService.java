package com.ikingtech.platform.service.system.role.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.department.api.DeptRoleDataScopeApi;
import com.ikingtech.platform.service.system.role.entity.RoleDataScopeDO;
import com.ikingtech.platform.service.system.role.service.repository.RoleDataScopeRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class DeptRoleDataScopeService implements DeptRoleDataScopeApi {

    private final RoleDataScopeRepository roleDataScopeRepo;

    /**
     * 根据部门ID移除角色数据范围
     * @param deptIds 部门ID集合
     */
    @Override
    public void removeRoleDataScopeByDeptIds(List<String> deptIds, String tenantCode) {
        // 使用lambdaQuery方法创建查询对象
        this.roleDataScopeRepo.remove(Wrappers.<RoleDataScopeDO>lambdaQuery()
                // 设置查询条件：数据范围代码等于传入的部门ID
                .in(RoleDataScopeDO::getDataScopeCode, deptIds)
                // 设置查询条件：租户代码等于当前租户的租户代码
                .eq(RoleDataScopeDO::getTenantCode, tenantCode));
    }
}
