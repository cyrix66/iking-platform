package com.ikingtech.platform.service.system.role.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.menu.api.MenuRoleApi;
import com.ikingtech.platform.service.system.role.entity.RoleMenuDO;
import com.ikingtech.platform.service.system.role.service.repository.RoleMenuRepository;
import lombok.RequiredArgsConstructor;

import java.util.Collection;
import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class MenuRoleService implements MenuRoleApi {

    private final RoleMenuRepository roleMenuRepo;

    /**
     * 移除角色菜单集合中的菜单
     *
     * @param menuIds 要移除的菜单ID集合
     */
    @Override
    public void removeRoleMenu(Collection<String> menuIds) {
        // 使用lambdaQuery方法创建查询对象
        this.roleMenuRepo.remove(Wrappers.<RoleMenuDO>lambdaQuery()
                // 根据菜单ID进行IN查询，将menuIds中的菜单ID作为参数传入
                .in(RoleMenuDO::getMenuId, menuIds));
    }

    /**
     * 根据角色ID加载菜单ID列表
     *
     * @param roleId 角色ID
     * @return 菜单ID列表
     */
    @Override
    public List<String> loadMenuId(String roleId) {
        return this.roleMenuRepo.listObjs(Wrappers.<RoleMenuDO>lambdaQuery()
                // 选择菜单ID
                .select(RoleMenuDO::getMenuId)
                // 等于角色ID
                .eq(RoleMenuDO::getRoleId, roleId));
    }

    /**
     * 根据角色ID列表加载菜单ID列表
     * @param roleIds 角色ID列表
     * @return 菜单ID列表
     */
    @Override
    public List<String> loadMenuId(List<String> roleIds) {
        // 使用lambdaQuery方法构建查询条件
        return this.roleMenuRepo.listObjs(Wrappers.<RoleMenuDO>lambdaQuery()
                .select(RoleMenuDO::getMenuId)
                .in(RoleMenuDO::getRoleId, roleIds));
    }
}
