package com.ikingtech.platform.service.system.user.configuration;

import com.iking.framework.sdk.authorization.api.AuthorizationUserApi;
import com.ikingtech.framework.sdk.approve.api.ApproveFormUserApi;
import com.ikingtech.framework.sdk.component.api.CompDepartmentApi;
import com.ikingtech.framework.sdk.component.api.CompUserApi;
import com.ikingtech.framework.sdk.department.api.DeptUserApi;
import com.ikingtech.framework.sdk.menu.api.MenuUserApi;
import com.ikingtech.framework.sdk.post.api.PostUserApi;
import com.ikingtech.framework.sdk.role.api.RoleUserApi;
import com.ikingtech.framework.sdk.tenant.api.TenantUserApi;
import com.ikingtech.framework.sdk.user.api.*;
import com.ikingtech.framework.sdk.workbench.api.ScheduleUserApi;
import com.ikingtech.platform.service.system.user.service.*;
import com.ikingtech.platform.service.system.user.service.repository.*;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class UserConfiguration {

    @Bean
    public UserRepository userRepository() {
        return new UserRepository();
    }

    @Bean
    public UserRoleRepository userRoleRepository() {
        return new UserRoleRepository();
    }

    @Bean
    public UserDeptRepository userDeptRepository() {
        return new UserDeptRepository();
    }

    @Bean
    public UserPostRepository userPostRepository() {
        return new UserPostRepository();
    }

    @Bean
    public UserConfigRepository userConfigRepository() {
        return new UserConfigRepository();
    }

    @Bean
    public UserCategoryRepository userCategoryRepository() {
        return new UserCategoryRepository();
    }

    @Bean
    public UserTenantRepository userTenantRepository() {
        return new UserTenantRepository();
    }

    @Bean
    public UserSocialRepository userSocialRepository() {
        return new UserSocialRepository();
    }

    @Bean
    public UserTrackRepository userTrackRepository() {
        return new UserTrackRepository();
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.dept.service.CompDepartmentService"})
    @ConditionalOnMissingBean(CompDepartmentApi.class)
    public CompDepartmentApi defaultCompDepartmentService() {
        return new DefaultCompDepartmentService();
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.dept.service.UserDeptService"})
    @ConditionalOnMissingBean(UserDeptApi.class)
    public UserDeptApi userDeptApi() {
        return new DefaultUserDeptService();
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.role.service.UserRoleService"})
    @ConditionalOnMissingBean(UserRoleApi.class)
    public UserRoleApi userRoleApi() {
        return new DefaultUserRoleService();
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.menu.service.UserMenuService"})
    @ConditionalOnMissingBean(UserMenuApi.class)
    public UserMenuApi userMenuApi() {
        return new DefaultUserMenuService();
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.system.post.service.UserPostService"})
    @ConditionalOnMissingBean(UserPostApi.class)
    public UserPostApi userPostApi() {
        return new DefaultUserPostService();
    }

    @Bean
    @ConditionalOnMissingClass({"com.ikingtech.platform.service.tenant.service.UserTenantService"})
    @ConditionalOnMissingBean(UserTenantApi.class)
    public UserTenantApi userTenantApi() {
        return new DefaultUserTenantService();
    }

    @Bean
    @ConditionalOnMissingBean(AuthorizationUserApi.class)
    public AuthorizationUserApi authorizationUserApi(UserRepository repo,
                                                     UserRoleRepository userRoleRepo,
                                                     UserDeptRepository userDeptRepo,
                                                     UserRoleApi userRoleApi,
                                                     UserDeptApi userDeptApi) {
        return new AuthorizationUserService(repo, userRoleRepo, userDeptRepo, userRoleApi, userDeptApi);
    }

    @Bean
    @ConditionalOnMissingBean(DeptUserApi.class)
    public DeptUserApi deptUserApi(UserRepository repo, UserDeptRepository userDeptRepo) {
        return new DeptUserService(repo, userDeptRepo);
    }

    @Bean
    @ConditionalOnMissingBean(MenuUserApi.class)
    public MenuUserApi menuUserApi(UserRoleRepository userRoleRepo) {
        return new MenuUserService(userRoleRepo);
    }

    @Bean
    @ConditionalOnMissingBean(PostUserApi.class)
    public PostUserApi postUserApi(UserPostRepository userPostRepo) {
        return new PostUserService(userPostRepo);
    }

    @Bean
    @ConditionalOnMissingBean(RoleUserApi.class)
    public RoleUserApi roleUserApi(UserRoleRepository userRoleRepo) {
        return new RoleUserService(userRoleRepo);
    }

    @Bean
    @ConditionalOnMissingBean(TenantUserApi.class)
    public TenantUserApi tenantUserApi(UserTenantRepository userTenantRepo) {
        return new TenantUserService(userTenantRepo);
    }

    @Bean
    @ConditionalOnMissingBean(ApproveFormUserApi.class)
    public ApproveFormUserApi approveFormUserApi(UserRepository repo,
                                                 ModelConverter converter,
                                                 UserRoleRepository userRoleRepo,
                                                 UserDeptRepository userDeptRepo,
                                                 UserPostRepository userPostRepo,
                                                 UserMenuApi userMenuApi,
                                                 UserRoleApi userRoleApi) {
        return new ApproveFormUserService(repo, converter, userRoleRepo, userDeptRepo, userPostRepo, userMenuApi, userRoleApi);
    }


    @Bean
    @ConditionalOnMissingBean(ScheduleUserApi.class)
    public ScheduleUserApi scheduleUserService(UserRepository repo, ModelConverter converter) {
        return new ScheduleUserService(repo, converter);
    }

    @Bean
    @ConditionalOnMissingBean(CompUserApi.class)
    public CompUserApi compUserApi(UserRepository service,
                                   UserDeptRepository userDeptService,
                                   UserTenantRepository userTenantService,
                                   CompDepartmentApi departmentApi,
                                   ModelConverter converter) {
        return new CompUserService(service, userDeptService, userTenantService, departmentApi, converter);
    }
}
