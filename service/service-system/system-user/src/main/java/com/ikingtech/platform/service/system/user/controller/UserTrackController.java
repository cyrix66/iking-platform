package com.ikingtech.platform.service.system.user.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import com.ikingtech.platform.service.system.user.entity.UserTrackDO;
import com.ikingtech.platform.service.system.user.model.UserTrackDTO;
import com.ikingtech.platform.service.system.user.model.UserTrackQueryParamDTO;
import com.ikingtech.platform.service.system.user.service.repository.UserTrackRepository;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@RequiredArgsConstructor
@ApiController(value = "/system/user/track", name = "系统管理-用户行为埋点", description = "系统管理-用户行为埋点")
public class UserTrackController {

    private final UserTrackRepository repo;

    @PostRequest(order = 1, value = "/add", summary = "新增", description = "新增")
    public R<Object> add(@Parameter(name = "user", description = "用户埋点信息")
                         @RequestBody UserTrackDTO track) {
        UserTrackDO entity = Tools.Bean.copy(track, UserTrackDO.class);
        entity.setId(Tools.Id.uuid());
        this.repo.save(entity);
        return R.ok();
    }

    @PostRequest(order = 2, value = "/list/page", summary = "分页查询", description = "分页查询")
    public R<List<UserTrackDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                                      @RequestBody UserTrackQueryParamDTO queryParam) {
        return R.ok(PageResult.build(this.repo.page(new Page<>(queryParam.getPage(), queryParam.getRows()),
                        this.repo.createWrapper(queryParam,
                                null == queryParam.getOperationStartDate() ? null : LocalDateTime.of(queryParam.getOperationStartDate(), LocalTime.MIDNIGHT),
                                null == queryParam.getOperationEndDate() ? null : LocalDateTime.of(queryParam.getOperationEndDate(), LocalTime.MAX)))
                .convert(entity -> Tools.Bean.copy(entity, UserTrackDTO.class))));
    }

    @PostRequest(order = 3, value = "/list/daily", summary = "按天查询", description = "按天查询")
    public R<List<UserTrackDTO>> listByDaily(@Parameter(name = "queryParam", description = "查询条件")
                                             @RequestBody UserTrackQueryParamDTO queryParam) {
        return R.ok(PageResult.build(this.repo.page(new Page<>(queryParam.getPage(), queryParam.getRows()),
                        this.repo.createWrapper(queryParam,
                                null == queryParam.getOperationStartDate() ? null : LocalDateTime.of(queryParam.getOperationStartDate(), LocalTime.MIDNIGHT),
                                null == queryParam.getOperationStartDate() ? null : LocalDateTime.of(queryParam.getOperationStartDate(), LocalTime.MAX)))
                .convert(entity -> Tools.Bean.copy(entity, UserTrackDTO.class))));
    }

    @PostRequest(order = 4, value = "/list/monthly", summary = "按月查询", description = "按月查询")
    public R<List<UserTrackDTO>> listByMonthly(@Parameter(name = "queryParam", description = "查询条件")
                                               @RequestBody UserTrackQueryParamDTO queryParam) {
        return R.ok(PageResult.build(this.repo.page(new Page<>(queryParam.getPage(), queryParam.getRows()),
                        this.repo.createWrapper(queryParam,
                                null == queryParam.getOperationStartDate() ? null : LocalDateTime.of(queryParam.getOperationStartDate().minusDays(30), LocalTime.MIDNIGHT),
                                null == queryParam.getOperationStartDate() ? null : LocalDateTime.of(queryParam.getOperationStartDate(), LocalTime.MAX)))
                .convert(entity -> Tools.Bean.copy(entity, UserTrackDTO.class))));
    }
}
