package com.ikingtech.platform.service.system.user.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("user_category")
public class UserCategoryDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -7997563493240528926L;

    @TableField("user_id")
    private String userId;

    @TableField("category_code")
    private String categoryCode;

    @TableField("tenant_code")
    private String tenantCode;
}
