package com.ikingtech.platform.service.system.user.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("user_role")
public class UserRoleDO extends BaseEntity implements Serializable {

	@Serial
    private static final long serialVersionUID = 2778740033460400850L;

	@TableField(value = "user_id")
	private String userId;

	@TableField(value = "domain_code")
	private String domainCode;

	@TableField(value = "tenant_code")
	private String tenantCode;

	@TableField(value = "app_code")
	private String appCode;

	@TableField(value = "role_id")
	private String roleId;
}
