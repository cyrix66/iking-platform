package com.ikingtech.platform.service.system.user.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.TenantEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("user_track")
public class UserTrackDO extends TenantEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 2482775889616450451L;

    @TableField("business_data_id")
    private String businessDataId;

    @TableField("user_id")
    private String userId;

    @TableField("user_name")
    private String userName;

    @TableField("operation")
    private String operation;

    @TableField("operation_props")
    private String operationProps;
}
