package com.ikingtech.platform.service.system.user.exception;


import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum UserExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 用户不存在
     */
    USER_NOT_FOUND("userNotFound"),

    /**
     * 平台用户不允许删除
     */
    DELETE_PLATFORM_USER_NOT_ALLOWED("deletePlatformUserNotAllowed"),

    /**
     * 原密码错误
     */
    INVALID_OLD_PASSWORD("invalidOldPassword"),

    /**
     * 用户名密码错误
     */
    INVALID_USERNAME_OR_PASSWORD("invalidUsernameOrPassword"),

    /**
     * 用户名密码错误
     */
    INVALID_USERNAME("invalidUsername"),

    /**
     * 无效的社交号(微信小程序OpenId等)
     */
    INVALID_SOCIAL_NO("invalidSocialNo"),

    /**
     * 新密码与原密码相同
     */
    PASSWORD_SAME_WITH_OLD_ONE("passwordSameWithOldOne"),

    /**
     * 已存在相同用户名的用户
     */
    DUPLICATE_USERNAME("duplicateUserUsername"),

    /**
     * 已存在相同用户名的用户
     */
    DUPLICATE_PHONE("duplicateUserPhone"),

    /**
     * 用户姓名不能包含空格
     */
    USER_NAME_SHOULD_NOT_CONTAIN_BLANK("userNameShouldNotContainBlank"),

    /**
     * 用户昵称不能包含空格
     */
    USER_NICKNAME_SHOULD_NOT_CONTAIN_BLANK("userNicknameShouldNotContainBlank"),

    /**
     * 用户昵称不能包含空格
     */
    WEAK_PASSWORD("weakPassword");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "iking-framework-system-user";
    }
}
