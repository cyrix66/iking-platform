package com.ikingtech.platform.service.system.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.system.user.entity.UserSocialDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserSocialMapper extends BaseMapper<UserSocialDO> {
}
