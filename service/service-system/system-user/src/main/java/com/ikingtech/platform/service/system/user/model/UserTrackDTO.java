package com.ikingtech.platform.service.system.user.model;

import com.ikingtech.framework.sdk.base.model.BaseModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "UserTrackDTO", description = "用户埋点信息")
public class UserTrackDTO extends BaseModel implements Serializable {

    @Serial
    private static final long serialVersionUID = -3247635465379173078L;

    @Schema(name = "userId", description = "用户编号")
    private String userId;

    @Schema(name = "userName", description = "用户姓名")
    private String userName;

    @Schema(name = "operation", description = "操作内容")
    private String operation;

    @Schema(name = "operationProps", description = "操作内容属性")
    private String operationProps;
}
