package com.ikingtech.platform.service.system.user.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(name = "UserTrackQueryParamDTO", description = "用户埋点信息查询参数")
public class UserTrackQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -3247635465379173078L;

    @Schema(name = "userId", description = "用户编号")
    private String userId;

    @Schema(name = "operation", description = "操作内容")
    private String operation;

    @Schema(name = "operationStartDate", description = "操作开始日期")
    private LocalDate operationStartDate;

    @Schema(name = "operationEndDate", description = "操作结束日期")
    private LocalDate operationEndDate;
}
