package com.ikingtech.platform.service.system.user.service;

import com.ikingtech.framework.sdk.component.api.CompDepartmentApi;
import com.ikingtech.framework.sdk.component.model.ComponentDepartment;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public class DefaultCompDepartmentService implements CompDepartmentApi {

    @Override
    public List<ComponentDepartment> listByName(String name, Boolean dataScopeOnly) {
        return Collections.emptyList();
    }

    @Override
    public List<ComponentDepartment> listByParentId(String parentId, Boolean dataScopeOnly) {
        return Collections.emptyList();
    }

    @Override
    public ComponentDepartment load(String parentId) {
        return null;
    }

    @Override
    public ComponentDepartment getRootDepartment() {
        return null;
    }

    @Override
    public Map<String, String> extractDepartmentFullName(Map<String, String> deptFullPathMap) {
        return Collections.emptyMap();
    }
}
