package com.ikingtech.platform.service.system.user.service;

import com.ikingtech.framework.sdk.user.api.UserDeptApi;
import com.ikingtech.framework.sdk.user.model.UserDeptDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public class DefaultUserDeptService implements UserDeptApi {

    @Override
    public List<UserDeptDTO> loadByIds(List<String> deptIds) {
        return new ArrayList<>();
    }

    @Override
    public List<String> loadSubAllId(String deptId) {
        return List.of();
    }
}
