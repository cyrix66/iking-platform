package com.ikingtech.platform.service.system.user.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.menu.api.MenuUserApi;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.user.entity.UserRoleDO;
import com.ikingtech.platform.service.system.user.service.repository.UserRoleRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class MenuUserService implements MenuUserApi {

    private final UserRoleRepository userRoleRepo;

    @Override
    public List<String> loadRoleId(String userId, String domain, String tenantCode, String appId) {
        return this.userRoleRepo.listObjs(Wrappers.<UserRoleDO>lambdaQuery()
                .select(UserRoleDO::getRoleId)
                .eq(Tools.Str.isNotBlank(domain), UserRoleDO::getDomainCode, domain)
                .eq(Tools.Str.isNotBlank(tenantCode), UserRoleDO::getTenantCode, tenantCode)
                .eq(Tools.Str.isNotBlank(appId), UserRoleDO::getAppCode, appId)
                .eq(UserRoleDO::getUserId, userId), String.class::cast);
    }
}
