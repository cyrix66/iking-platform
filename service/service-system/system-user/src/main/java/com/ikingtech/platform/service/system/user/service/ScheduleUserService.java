package com.ikingtech.platform.service.system.user.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.approve.api.ApproveFormUserApi;
import com.ikingtech.framework.sdk.approve.model.ApproveFormDepartment;
import com.ikingtech.framework.sdk.approve.model.ApproveFormUser;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.user.api.UserMenuApi;
import com.ikingtech.framework.sdk.user.api.UserRoleApi;
import com.ikingtech.framework.sdk.user.model.UserDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.workbench.api.ScheduleUserApi;
import com.ikingtech.framework.sdk.workbench.model.ScheduleUser;
import com.ikingtech.platform.service.system.user.entity.UserDO;
import com.ikingtech.platform.service.system.user.entity.UserDeptDO;
import com.ikingtech.platform.service.system.user.entity.UserPostDO;
import com.ikingtech.platform.service.system.user.entity.UserRoleDO;
import com.ikingtech.platform.service.system.user.service.repository.*;
import lombok.RequiredArgsConstructor;

import java.util.*;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class ScheduleUserService implements ScheduleUserApi {

    private final UserRepository repo;

    private final ModelConverter converter;

    @Override
    public ScheduleUser loadById(String userId) {
        UserDO entity = this.repo.getById(userId);
        if (null == entity) {
            return null;
        }
        return this.convert(this.converter.modelConvert(entity));
    }

    private ScheduleUser convert(UserDTO user) {
        ScheduleUser scheduleUser = new ScheduleUser();
        scheduleUser.setUserId(user.getId());
        scheduleUser.setUserName(user.getName());
        scheduleUser.setUserAvatar(user.getAvatar());
        scheduleUser.setUserDeptIds(user.getDeptIds());
        scheduleUser.setUserPostIds(user.getPostIds());
        scheduleUser.setUserRoleIds(user.getRoleIds());
        return scheduleUser;
    }
}
