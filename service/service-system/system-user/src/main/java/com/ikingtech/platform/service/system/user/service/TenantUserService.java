package com.ikingtech.platform.service.system.user.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.tenant.api.TenantUserApi;
import com.ikingtech.platform.service.system.user.entity.UserTenantDO;
import com.ikingtech.platform.service.system.user.service.repository.UserTenantRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * @author Administrator
 */
@RequiredArgsConstructor
public class TenantUserService implements TenantUserApi {

    private final UserTenantRepository userTenantRepo;

    @Override
    public List<String> loadCodeByUserId(String userId) {
        return this.userTenantRepo.listObjs(Wrappers.<UserTenantDO>lambdaQuery()
                .select(UserTenantDO::getTenantCode)
                .eq(UserTenantDO::getUserId, userId), String.class::cast);
    }
}
