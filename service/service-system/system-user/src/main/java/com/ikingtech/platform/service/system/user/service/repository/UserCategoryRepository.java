package com.ikingtech.platform.service.system.user.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.system.user.entity.UserCategoryDO;
import com.ikingtech.platform.service.system.user.mapper.UserCategoryMapper;

/**
 * @author tie yan
 */
public class UserCategoryRepository extends ServiceImpl<UserCategoryMapper, UserCategoryDO> {
}
