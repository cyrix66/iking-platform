package com.ikingtech.platform.service.system.user.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.system.user.entity.UserConfigDO;
import com.ikingtech.platform.service.system.user.mapper.UserConfigMapper;

/**
 * @author tie yan
 */
public class UserConfigRepository extends ServiceImpl<UserConfigMapper, UserConfigDO> {
}
