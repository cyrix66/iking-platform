package com.ikingtech.platform.service.system.user.service.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.user.model.UserQueryParamDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.user.entity.UserDO;
import com.ikingtech.platform.service.system.user.mapper.UserMapper;

import java.time.LocalDate;

/**
 * @author tie yan
 */
public class UserRepository extends ServiceImpl<UserMapper, UserDO> {

    public static LambdaQueryWrapper<UserDO> createWrapper(UserQueryParamDTO queryParam) {
        LambdaQueryWrapper<UserDO> queryWrapper = Wrappers.<UserDO>lambdaQuery()
                .like(Tools.Str.isNotBlank(queryParam.getName()), UserDO::getName, queryParam.getName())
                .like(Tools.Str.isNotBlank(queryParam.getUsername()), UserDO::getUsername, queryParam.getUsername())
                .like(Tools.Str.isNotBlank(queryParam.getNickname()), UserDO::getNickname, queryParam.getNickname())
                .like(Tools.Str.isNotBlank(queryParam.getPhone()), UserDO::getPhone, queryParam.getPhone())
                .in(Tools.Coll.isNotBlank(queryParam.getPhones()), UserDO::getPhone, queryParam.getPhones())
                .like(Tools.Str.isNotBlank(queryParam.getEmail()), UserDO::getEmail, queryParam.getEmail())
                .eq(null != queryParam.getAdmin(), UserDO::getAdminUser, queryParam.getAdmin())
                .eq(null != queryParam.getLocked(), UserDO::getLocked, queryParam.getLocked())
                .eq(Tools.Str.isNotBlank(queryParam.getSex()), UserDO::getSex, queryParam.getSex())
                .in(Tools.Coll.isNotBlank(queryParam.getUserIds()), UserDO::getId, queryParam.getUserIds())
                .eq(Tools.Str.isNotBlank(queryParam.getLockType()), UserDO::getLockType, queryParam.getLockType())
                .orderByDesc(UserDO::getCreateTime);
        LocalDate createTime = queryParam.getCreateTime();
        if (createTime != null) {
            queryWrapper.between(UserDO::getCreateTime, createTime.atStartOfDay(), createTime.atStartOfDay().plusDays(1).minusSeconds(1));
        }
        LocalDate updateTime = queryParam.getUpdateTime();
        if (updateTime != null) {
            queryWrapper.between(UserDO::getUpdateTime, updateTime.atStartOfDay(), updateTime.atStartOfDay().plusDays(1).minusSeconds(1));
        }
        return queryWrapper;
    }
}
