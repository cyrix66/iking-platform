package com.ikingtech.platform.service.system.user.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.system.user.entity.UserSocialDO;
import com.ikingtech.platform.service.system.user.mapper.UserSocialMapper;

/**
 * @author tie yan
 */
public class UserSocialRepository extends ServiceImpl<UserSocialMapper, UserSocialDO> {
}
