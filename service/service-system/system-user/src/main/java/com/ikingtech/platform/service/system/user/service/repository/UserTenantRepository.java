package com.ikingtech.platform.service.system.user.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.system.user.entity.UserTenantDO;
import com.ikingtech.platform.service.system.user.mapper.UserTenantMapper;

/**
 * @author zhangqiang
 */
public class UserTenantRepository extends ServiceImpl<UserTenantMapper, UserTenantDO> {
}
