package com.ikingtech.platform.service.system.user.service.repository;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.user.entity.UserTrackDO;
import com.ikingtech.platform.service.system.user.mapper.UserTrackMapper;
import com.ikingtech.platform.service.system.user.model.UserTrackQueryParamDTO;

import java.time.LocalDateTime;

/**
 * @author tie yan
 */
public class UserTrackRepository extends ServiceImpl<UserTrackMapper, UserTrackDO> {

    public Wrapper<UserTrackDO> createWrapper(UserTrackQueryParamDTO queryParam, LocalDateTime startTime, LocalDateTime endTime) {
        return Wrappers.<UserTrackDO>lambdaQuery()
                .like(Tools.Str.isNotBlank(queryParam.getOperation()), UserTrackDO::getOperation, queryParam.getOperation())
                .eq(Tools.Str.isNotBlank(queryParam.getUserId()), UserTrackDO::getUserId, queryParam.getUserId())
                .ge(null != startTime, UserTrackDO::getCreateTime, startTime)
                .le(null != startTime, UserTrackDO::getCreateTime, endTime);
    }
}
