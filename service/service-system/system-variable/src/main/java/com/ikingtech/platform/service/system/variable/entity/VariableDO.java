package com.ikingtech.platform.service.system.variable.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("variable")
public class VariableDO extends BaseEntity implements Serializable {

	@Serial
    private static final long serialVersionUID = 4401989245214453678L;

	@TableField(value = "name")
	private String name;

	@TableField(value = "variable_key")
	private String variableKey;

	@TableField(value = "variable_value")
	private String variableValue;

	@TableField(value = "type")
	private String type;
}
