package com.ikingtech.platform.service.system.variable.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.variable.model.VariableQueryParamDTO;
import com.ikingtech.platform.service.system.variable.entity.VariableDO;
import com.ikingtech.platform.service.system.variable.mapper.VariableMapper;

/**
 * @author tie yan
 */
public class VariableRepository extends ServiceImpl<VariableMapper, VariableDO> {

    public static LambdaQueryWrapper<VariableDO> createWrapper(VariableQueryParamDTO queryParam, String tenantCode) {
        return Wrappers.<VariableDO>lambdaQuery()
                .like(Tools.Str.isNotBlank(queryParam.getName()), VariableDO::getName, queryParam.getName())
                .like(Tools.Str.isNotBlank(queryParam.getVariableKey()), VariableDO::getVariableKey, queryParam.getVariableKey())
                .like(Tools.Str.isNotBlank(queryParam.getVariableValue()), VariableDO::getVariableValue, queryParam.getVariableValue())
                .eq(Tools.Str.isNotBlank(queryParam.getType()), VariableDO::getType, queryParam.getType())
                .orderByDesc(VariableDO::getCreateTime);
    }
}
