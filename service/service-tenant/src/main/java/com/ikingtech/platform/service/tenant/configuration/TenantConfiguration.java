package com.ikingtech.platform.service.tenant.configuration;

import com.ikingtech.framework.sdk.tenant.api.InitializerTenantApi;
import com.ikingtech.framework.sdk.tenant.api.TenantUserApi;
import com.ikingtech.framework.sdk.user.api.UserTenantApi;
import com.ikingtech.platform.service.tenant.service.DefaultTenantUserService;
import com.ikingtech.platform.service.tenant.service.InitializerTenantService;
import com.ikingtech.platform.service.tenant.service.repository.TenantRepository;
import com.ikingtech.platform.service.tenant.service.UserTenantService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class TenantConfiguration {

    @Bean
    public TenantRepository tenantRepository() {
        return new TenantRepository();
    }

    @Bean
    public UserTenantApi userTenantService(TenantRepository repo) {
        return new UserTenantService(repo);
    }

    @Bean
    public InitializerTenantApi initializerTenantApi(TenantRepository repo) {
        return new InitializerTenantService(repo);
    }

    @Bean
    @ConditionalOnMissingClass("com.ikingtech.platform.service.system.user.service.TenantUserService")
    public TenantUserApi defaultTenantUserService() {
        return new DefaultTenantUserService();
    }
}
