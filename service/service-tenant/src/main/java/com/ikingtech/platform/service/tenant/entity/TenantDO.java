package com.ikingtech.platform.service.tenant.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.SortEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("tenant")
public class TenantDO extends SortEntity implements Serializable {

	@Serial
    private static final long serialVersionUID = 2832824413455388200L;

	@TableField("name")
	private String name;

	@TableField("code")
	private String code;

	@TableField("type")
	private String type;

	@TableField("tenant_domain")
	private String tenantDomain;

	@TableField("logo")
	private String logo;

	@TableField("icon")
	private String icon;

	@TableField("element")
	private String element;

	@TableField("arrange")
	private String arrange;

	@TableField("alignment")
	private String alignment;

	@TableField("icon_open")
	private String iconOpen;

	@TableField("icon_close")
	private String iconClose;

	@TableField("menu_name")
	private String menuName;

	@TableField("favicon")
	private String favicon;

	@TableField("favicon_name")
	private String faviconName;

	@TableField("home_page")
	private String homePage;

	@TableField(value = "start_date")
	private LocalDate startDate;

	@TableField(value = "end_date")
	private LocalDate endDate;

	@TableField("status")
	private String status;

	@TableField("remark")
	private String remark;

	@TableLogic
	@TableField("del_flag")
	private Boolean delFlag;
}
