package com.ikingtech.platform.service.tenant.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum TenantExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 租户不存在
     */
    TENANT_NOT_FOUND("tenantNotFound"),

    /**
     * 在正常状态下，开始日期无效
     */
    INVALID_START_DATE_WITH_NORMAL_STATUS("invalidStartDateWithNormalStatus"),

    /**
     * 在正常状态下，结束日期无效
     */
    INVALID_END_DATE_WITH_NORMAL_STATUS("invalidEndDateWithNormalStatus"),

    /**
     * 租户编码重复
     */
    DUPLICATE_TENANT_CODE("duplicateTenantCode"),

    /**
     * 租户名称重复
     */
    DUPLICATE_TENANT_NAME("duplicateTenantName"),

    /**
     * 在冻结状态下，结束日期无效
     */
    INVALID_END_DATE_WITH_FREEZE_STATUS("invalidEndDateWithFreezeStatus");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "iking-framework-system-post";
    }
}
