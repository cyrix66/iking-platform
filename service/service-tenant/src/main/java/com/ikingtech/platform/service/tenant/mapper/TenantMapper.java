package com.ikingtech.platform.service.tenant.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.tenant.entity.TenantDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface TenantMapper extends BaseMapper<TenantDO> {
}
