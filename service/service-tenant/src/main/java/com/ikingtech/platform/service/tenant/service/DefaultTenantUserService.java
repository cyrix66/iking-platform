package com.ikingtech.platform.service.tenant.service;

import com.ikingtech.framework.sdk.tenant.api.TenantUserApi;

import java.util.Collections;
import java.util.List;

/**
 * @author tie yan
 */
public class DefaultTenantUserService implements TenantUserApi {

    @Override
    public List<String> loadCodeByUserId(String userId) {
        return Collections.emptyList();
    }
}
