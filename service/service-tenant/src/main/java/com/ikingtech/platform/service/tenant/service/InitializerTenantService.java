package com.ikingtech.platform.service.tenant.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.enums.system.tenant.TenantStatusEnum;
import com.ikingtech.framework.sdk.tenant.api.InitializerTenantApi;
import com.ikingtech.platform.service.tenant.entity.TenantDO;
import com.ikingtech.platform.service.tenant.service.repository.TenantRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class InitializerTenantService implements InitializerTenantApi {

    private final TenantRepository repo;

    @Override
    public List<String> loadAllCodes() {
        return this.repo.listObjs(Wrappers.<TenantDO>lambdaQuery().select(TenantDO::getCode).eq(TenantDO::getStatus, TenantStatusEnum.NORMAL.name()));
    }
}
