package com.ikingtech.platform.service.tenant.service.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.tenant.model.TenantQueryParamDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.tenant.entity.TenantDO;
import com.ikingtech.platform.service.tenant.mapper.TenantMapper;

import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * @author tie yan
 */
public class TenantRepository extends ServiceImpl<TenantMapper, TenantDO> {

    public static LambdaQueryWrapper<TenantDO> createWrapper(TenantQueryParamDTO queryParam) {
        return Wrappers.<TenantDO>lambdaQuery()
                .like(Tools.Str.isNotBlank(queryParam.getName()), TenantDO::getName, queryParam.getName())
                .like(Tools.Str.isNotBlank(queryParam.getCode()), TenantDO::getCode, queryParam.getCode())
                .eq(Tools.Str.isNotBlank(queryParam.getType()), TenantDO::getType, queryParam.getType())
                .like(Tools.Str.isNotBlank(queryParam.getTenantDomain()), TenantDO::getTenantDomain, queryParam.getTenantDomain())
                .eq(Tools.Str.isNotBlank(queryParam.getStatus()), TenantDO::getStatus, queryParam.getStatus())
                .ge(queryParam.getCreateTimeStartDate() != null && queryParam.getCreateTimeEndDate() == null, TenantDO::getCreateTime, null == queryParam.getCreateTimeStartDate() ? null : LocalDateTime.of(queryParam.getCreateTimeStartDate(), LocalTime.of(0, 0, 0)))
                .le(queryParam.getCreateTimeStartDate() == null && queryParam.getCreateTimeEndDate() != null, TenantDO::getCreateTime, null == queryParam.getCreateTimeEndDate() ? null : LocalDateTime.of(queryParam.getCreateTimeEndDate(), LocalTime.of(23, 59, 59)))
                .between(queryParam.getCreateTimeStartDate() != null && queryParam.getCreateTimeEndDate() != null, TenantDO::getCreateTime, null == queryParam.getCreateTimeStartDate() ? null : LocalDateTime.of(queryParam.getCreateTimeStartDate(), LocalTime.of(0, 0, 0)), null == queryParam.getCreateTimeEndDate() ? null : LocalDateTime.of(queryParam.getCreateTimeEndDate(), LocalTime.of(23, 59, 59)))
                .ge(queryParam.getUpdateTimeStartDate() != null && queryParam.getUpdateTimeEndDate() == null, TenantDO::getUpdateTime, null == queryParam.getUpdateTimeStartDate() ? null : LocalDateTime.of(queryParam.getUpdateTimeStartDate(), LocalTime.of(0, 0, 0)))
                .le(queryParam.getUpdateTimeStartDate() == null && queryParam.getUpdateTimeEndDate() != null, TenantDO::getUpdateTime, null == queryParam.getUpdateTimeEndDate() ? null : LocalDateTime.of(queryParam.getUpdateTimeEndDate(), LocalTime.of(23, 59, 59)))
                .between(queryParam.getUpdateTimeStartDate() != null && queryParam.getUpdateTimeEndDate() != null, TenantDO::getUpdateTime, null == queryParam.getUpdateTimeStartDate() ? null : LocalDateTime.of(queryParam.getUpdateTimeStartDate(), LocalTime.of(0, 0, 0)), null == queryParam.getUpdateTimeEndDate() ? null : LocalDateTime.of(queryParam.getUpdateTimeEndDate(), LocalTime.of(23, 59, 59)))
                .ge(queryParam.getStartTimeStartDate() != null && queryParam.getStartTimeEndDate() == null, TenantDO::getStartDate, queryParam.getStartTimeStartDate())
                .le(queryParam.getStartTimeStartDate() == null && queryParam.getStartTimeEndDate() != null, TenantDO::getStartDate, queryParam.getStartTimeEndDate())
                .between(queryParam.getStartTimeStartDate() != null && queryParam.getStartTimeEndDate() != null, TenantDO::getStartDate, queryParam.getStartTimeStartDate(), queryParam.getStartTimeEndDate())
                .ge(queryParam.getEndTimeStartDate() != null && queryParam.getEndTimeEndDate() == null, TenantDO::getEndDate, queryParam.getEndTimeStartDate())
                .le(queryParam.getEndTimeStartDate() == null && queryParam.getEndTimeEndDate() != null, TenantDO::getEndDate, queryParam.getEndTimeEndDate())
                .between(queryParam.getEndTimeStartDate() != null && queryParam.getEndTimeEndDate() != null, TenantDO::getEndDate, queryParam.getEndTimeStartDate(), queryParam.getEndTimeEndDate())
                .orderByDesc(TenantDO::getCreateTime);
    }
}
