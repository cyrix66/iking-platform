package com.ikingtech.platform.service.wechat.mini.configuration;

import com.ikingtech.framework.sdk.wechat.embedded.properties.WechatProperties;
import com.ikingtech.framework.sdk.wechat.mini.api.WechatMiniConfigLoader;
import com.ikingtech.platform.service.wechat.mini.service.WechatMiniConfigLoaderImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class WechatMiniConfiguration {

    @Bean
    @ConditionalOnMissingClass("com.ikingtech.platform.service.platform.config.service.loader.WechatMiniConfigLoaderImpl")
    @ConditionalOnMissingBean({WechatMiniConfigLoader.class})
    public WechatMiniConfigLoader wechatMiniConfigLoader(WechatProperties properties) {
        return new WechatMiniConfigLoaderImpl(properties);
    }
}
