package com.ikingtech.platform.service.wechat.mini.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum WechatMiniExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 指定微信小程序不存在
     */
    WECHAT_MINI_NOT_FOUND("wechatMiniNotFound");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "system-post";
    }
}
