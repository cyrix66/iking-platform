package com.ikingtech.platform.service.wechat.mini.service;

import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.wechat.embedded.properties.WechatProperties;
import com.ikingtech.framework.sdk.wechat.mini.api.WechatMiniConfigLoader;
import com.ikingtech.framework.sdk.wechat.mini.model.WechatMiniDTO;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class WechatMiniConfigLoaderImpl implements WechatMiniConfigLoader {

    private final WechatProperties properties;

    @Override
    public WechatMiniDTO load(String wechatMiniId) {
        List<WechatProperties.WechatMini> wechatMiniConfigs = this.properties.getMini();
        if (Tools.Coll.isBlank(wechatMiniConfigs)) {
            throw new FrameworkException("wechatMiniNotFound");
        }
        for (WechatProperties.WechatMini wechatMiniConfig : wechatMiniConfigs) {
            if (Tools.Str.equals(wechatMiniId, wechatMiniConfig.getId())) {
                return Tools.Bean.copy(wechatMiniConfig, WechatMiniDTO.class);
            }
        }
        throw new FrameworkException("wechatMiniNotFound");
    }
}
